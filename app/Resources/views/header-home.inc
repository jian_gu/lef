<div class="header header-home">
    <div class="row">
        <div class="col-sm-6 block-top-left">
            <h1 class="site-name">Louer-en-France.com <br> <small class="slogan">Et dans le monde entier</small> </h1>
            <ul class="nav nav-pills nav-stacked nav-top-left">
                <li><a href="{{ path('home') }}">Accueil</a></li>
                <li><a href="{{ path('locations') }}">Toutes les locations</a></li>
                <li><a href="{{ path('location_filtre_promo', {type_location : 'promotions'}) }}">Les promotions</a></li>                {# Modified le 150814 par Jian #}
                <li class="cm-item">
                    <span><span>Espace vacances</span>
                        <a href="{{ path('location_filtre_vacs', {type_location : 'mer'}) }}">Mer</a>
                        <a href="{{ path('location_filtre_vacs', {type_location : 'soleil-lointain'}) }}">Soleil lointain</a>
                        <a href="{{ path('location_filtre_vacs', {type_location : 'ski'}) }}">Ski</a>
                    </span>
                </li>
                <li><a href="{{ path('location_filtre_promo', {type_location : 'derniere-minute'}) }}">Last minute</a></li>
            </ul>
            <ul class="reseaux">
                <li><a href="https://twitter.com/LouerEnFrance"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.facebook.com/LouerEnFrance"><i class="fa fa-facebook"></i></a></li>
                <!--<li><a href="#google"><i class="fa fa-google-plus"></i></a></li>-->
            </ul>
        </div>
        <div class="col-sm-6 block-top-right">
            <div class="col-sm-1"></div>
            <div class="col-sm-9">
                <form role="form" action="{{path('locations')}}" id="search-form" method="post">
                    <div class="input-container">
                        <h2>Rechercher une location</h2>
                        <!-- ============ -->
                        <!-- Localisation -->
                        <!-- ============ -->
                        <div class="form-group">
                            <input type="text" class="form-control input-lg autocomplete" 
                                   searchby="localisation" id="search-input-geo" 
                                   placeholder="Ville, région, pays">
                        </div>
                        <!-- ============== -->
                        <!-- Radius Geoloca -->
                        <!-- ============== -->
                        <div class="form-group">
                            <!--<input id="slider-geo-radius" />-->
                            <strong>Radius: </strong>
                            <input class="lef-srch-item" type="radio" name="radius" value="100" checked="checked">100m&nbsp;
                            <input class="lef-srch-item" type="radio" name="radius" value="500">500m&nbsp;
                            <input class="lef-srch-item" type="radio" name="radius" value="1000">1km&nbsp;
                            <input class="lef-srch-item" type="radio" name="radius" value="5000">5km&nbsp;
                            <input class="lef-srch-item" type="radio" name="radius" value="10000">10km
                        </div>
                        <!-- ============== -->
                        <!-- Date d'arrivée -->
                        <!-- ============== -->
                        <div class="form-group">
                            <input type="text" class="lef-srch-item form-control input-lg datepicker" 
                                   id="inputDateArrivee" placeholder="Date d'arrivée" 
                                   name="datearrivee">
                        </div>
                        <div class="row">
                            <!-- ==================== -->
                            <!-- Durée de la location -->
                            <!-- ==================== -->
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <select class="lef-srch-item form-control" 
                                            id="inputDuree" 
                                            name="duree">
                                        <option disabled selected>Durée</option>
                                        <option value="7">1 semaine</option>
                                        <option value="14">2 semaines</option>
                                        <option value="21">3 semaines</option>
                                        <option value="28">4 semaines</option>
                                        <option value="1">1 nuit 2 jours</option>
                                        {% for d in 2..28 %}
                                        <option value="{{d}}">{{d}} nuits {{d+1}} jours</option>
                                        {% endfor %}
                                    </select>
                                </div><!-- /form-group -->
                            </div><!-- /.col-lg-6 -->
                            <!-- ================== -->
                            <!-- Nombre de personne -->
                            <!-- ================== -->
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <select class="lef-srch-item form-control" 
                                            id="inputNbPers" 
                                            name="nbrperso">
                                        <option disabled selected>Nb Pers.</option>
                                        {% for p in 2..20 %}
                                        <option value="{{p}}">{{p}}</option>
                                        {% endfor %}
                                    </select>
                                </div><!-- /form-group -->
                            </div><!-- /.col-lg-6 -->

                            <div class="col-lg-12">
                                <div class="col-lg-7"></div>
                                <div class="col-lg-5">
                                    <button type="submit" class="btn btn2 btn-warning btn-lg" style="margin-right: 0px;">Recherche</button>
                                </div>
                            </div> 
                        </div><!-- /.row -->                       
                    </div>
                </form>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
</div>

