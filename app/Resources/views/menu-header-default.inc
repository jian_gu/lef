                <!-- Static navbar -->
                <div class="navbar navbar-default navbar-lef" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!-- a class="navbar-brand" href="{{ path('home') }}">LeF</a -->
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="{{ path('home') }}">Home</a></li>
                                <li><a href="{{ path('locations', {'page':1}) }}">Toutes les locations</a></li>
                                <li class="cm-item">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Les promotions <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ path('location_filtre_promo', {type_location : 'promotions'}) }}">Toutes les promotions</a></li>                {# Modified le 140814 par Jian #}
                                        <li><a href="{{ path('location_filtre_promo', {type_location : 'premiere-minute'}) }}">Promoation de première minute</a></li>
                                        <li><a href="{{ path('location_filtre_promo', {type_location : 'derniere-minute'}) }}">Promoation de dérnière minute</a></li>
                                    </ul>
                                </li>
                                <li class="cm-item">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Espace vacances <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        {#<li><a href="{{ path('location_filtre_vacs', {type_location : 'ete'}) }}">Ete</a></li>#}
                                        <li><a href="{{ path('location_filtre_vacs', {type_location : 'ski'}) }}">Ski</a></li>
                                        <li><a href="{{ path('location_filtre_vacs', {type_location : 'soleil-lointain'}) }}">Soleil lointain</a></li>
                                        <li><a href="{{ path('location_filtre_vacs', {type_location : 'mer'}) }}">Mer</a></li>
                                    </ul>
                                </li>
                                <li class="cm-item">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Service du site <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ path('contact') }}">Contact administration</a></li>
                                        <li><a href="{{ path('static', {'page':'calendrier-scolaire'}) }}">Calendrier scolaire</a></li>
                                        <li><a href="{{ path('static', {'page':'guide-de-location'}) }}">Guide de la location</a></li>
                                        <li><a href="{{ path('static', {'page':'faq'}) }}">FAQ</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div><!--/.container-fluid -->
                </div>
