<div class="row">
	<div class="col-sm-6">
		<div class="panel panel-offre panel-offre-proprio">
			<img src="{{ asset('img/bg-panel-offre-proprio.jpg') }}" >
			<a href="#" class="btn btn-primary">
				<strong>Notre offre proprietaire</strong><br>Déposer votre annonce simplement <br>rapidement et surtout gratuitement
			</a>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="panel panel-offre panel-offre-locataire">
			<img src="{{ asset('img/bg-panel-offre-locataire.jpg') }}" >
			<a href="{{ path('locations', {'page':1}) }}" class="btn btn-primary">
				<strong>Notre offre locataire</strong> <br>Pour les vacances ou pour votre vie de <br>tous les jours, trouvez la location révée
			</a>
		</div>
	</div>
</div>