		<div class="alert alert-info alert-why-us">
			<h2>Pourquoi choisir Louer-En-France ?</h2>
			<div class="row">
				<div class="col-sm-3">
					<!--<img src="{{ asset('img/expret.png') }}" alt="Un expert à votre écoute">-->
                                        <i style="text-align: center; vertical-align: middle; margin-bottom: 30px; background: url('/img/image_sprite.png')  0 199px; height: 59px; width: 59px; display: inline-block;"></i>
					<div class="why-us-content">
						<strong>Un expert à votre écoute</strong>
						<p>Avec {{nbAnnonce}} locations de vacances en catalogue Louer-en-France.com vous offre un des plus grands choix d’annonces de locations saisonnières du marché.</p>
					</div>
				</div>
				<div class="col-sm-3">
					<!--<img src="{{ asset('img/solution.png') }}" alt="Solution 100% sécurisé">-->
                                        <i style="text-align: center; vertical-align: middle; margin-bottom: 30px; background: url('/img/image_sprite.png')  -60px 199px; height: 59px; width: 59px; display: inline-block;"></i>
					<div class="why-us-content">
						<strong>Transaction sécurisée</strong>
						<p>Quand vous sélectionnez une annonce de location et que vous cliquez sur réserver, notre site vous redirige sur l’espace réservation du partenaire qui gère la location. Vous êtes alors sûr de la disponibilité et du tarif. Louer-en-France.com ne gère aucune transaction, ni location, nous sommes uniquement un site gratuit de comparaison d’annonce</p>
					</div>
				</div>
				<div class="col-sm-3">
					<!--<img src="{{ asset('img/gestion.png') }}" alt="Gestion des litiges">-->
                                        <i style="text-align: center; vertical-align: middle; margin-bottom: 30px; background: url('/img/image_sprite.png')  -120px 199px; height: 59px; width: 59px; display: inline-block;"></i>
					<div class="why-us-content">
						<strong>Pesez le pour et le contre</strong>
						<p>Notre comparateur vous permet de sélectionner plusieurs annonces pour les comparer et retenir celle qui vous convient le mieux. Si une location est commercialisée dans plusieurs réseaux trouvez en deux clics le partenaire qui vous convient le mieux</p>
					</div>
				</div>
				<div class="col-sm-3">
					<!--<img src="{{ asset('img/simplifier.png') }}" alt="Simplifier-vous la vie">-->
                                        <i style="text-align: center; vertical-align: middle; margin-bottom: 30px; background: url('/img/image_sprite.png')  -180px 199px; height: 59px; width: 59px; display: inline-block;"></i>
					<div class="why-us-content">
						<strong>Simplifier-vous la vie</strong>
						<p>Vous faites un voyage itinérant sur plusieurs destinations : en regroupant les catalogues de grandes agences européennes de locations de vacances et des propriétaires particuliers louer-en-France.com vous permet à chaque étape d’être sûr de trouver, la meilleure location qui vous convienne.</p>
					</div>
				</div>
			</div>
		</div>