<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailingAdmin
 *
 * @author HugoG
 */
namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class EmailingAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('mailexpe', 'text', array('label' => 'Expediteur'))
            ->add('maildest', 'text', array('label' => 'Destinateur'))
            ->add('value', 'text', array('label' => 'Valeur'))
            ->add('message', 'text', array('label' => 'Message'))
            ->add('date', 'datetime', array('label' => 'Date'))
            ->add('image', 'text', array('label' => 'Image'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('mailexpe')
            ->add('maildest')
            ->add('date')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('mailexpe')
            ->add('maildest')
            ->add('date')
        ;
    }
}