<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailingAdmin
 *
 * @author HugoG
 */

namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;

class factureAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->add('annonceur', 'entity', array(
                    'property' => 'NomPrenomUserName',
                    'class' => 'LefBoBundle:User',
                    'query_builder' => function(EntityRepository $er){
                return $er->createQueryBuilder('u')
                        ->where('u.roles LIKE \'%ROLE_ANNONCEUR%\'')
                        ->orderBy('u.username', 'ASC');
            },)
                )
                ->add('libelle', 'text', array('label' => 'Libellé'))
                ->add('date', 'datetime', array('label' => 'Date de facturation',
                    'date_format' => 'ddMyyyy',
                    'date_widget' => 'choice',
                    'time_widget' => 'choice',
                    'data' => new \DateTime()))
                ->add('montant', 'number', array('label' => 'Montant'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('annonceur')
                ->add('date')
                ->add('montant')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('libelle')
                ->add('annonceur')
                ->add('date', 'datetime', array('label' => 'Date de facturation',
                    'format' => 'd/m/Y H:i'))
                ->add('montant')
        ;
    }

}
