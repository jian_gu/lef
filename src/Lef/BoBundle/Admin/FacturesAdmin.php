<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FacturesAdmin
 *
 * @author HugoG
 */

namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class FacturesAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('userid', 'integer', array('label' => ''))
                ->add('opcode', 'text', array('label' => ''))
                ->add('datefact', 'text', array('label' => ''))
                ->add('libelle', 'text', array('label' => ''))
                ->add('nombre', 'integer', array('label' => ''))
                ->add('pu', 'number', array('label' => ''))
                ->add('pht', 'number', array('label' => ''))
                ->add('pttc', 'number', array('label' => ''))
                ->add('fnombre1', 'integer', array('label' => ''))
                ->add('libel1', 'text', array('label' => ''))
                ->add('totalht1', 'number', array('label' => ''))
                ->add('fnombre2', 'integer', array('label' => ''))
                ->add('libel2', 'text', array('label' => ''))
                ->add('totalht2', 'number', array('label' => ''))
                ->add('fnombre3', 'integer', array('label' => ''))
                ->add('libel3', 'text', array('label' => ''))
                ->add('totalht3', 'number', array('label' => ''))
                ->add('totalht', 'number', array('label' => ''))
                ->add('tva', 'number', array('label' => ''))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('userid')
                ->add('datefact')
                ->add('libelle')
                ->add('totalht')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('userid')
                ->add('datefact')
                ->add('libelle')
                ->add('totalht')
        ;
    }

}
