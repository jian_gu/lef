<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InterMaisonAdmin
 *
 * @author HugoG
 */

namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class InterMaisonAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('nomcomp', 'text', array('label' => ''))
                ->add('descript', 'text', array('label' => ''))
                ->add('type', 'text', array('label' => ''))
                ->add('payname', 'text', array('label' => ''))
                ->add('regionname', 'text', array('label' => ''))
                ->add('placename', 'text', array('label' => ''))
                ->add('capacite', 'text', array('label' => ''))
                ->add('truc', 'text', array('label' => ''))
                ->add('liendetail', 'text', array('label' => ''))
                ->add('lienphot1', 'text', array('label' => ''))
                ->add('lienphot2', 'text', array('label' => ''))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('nomcomp')
                ->add('type')
                ->add('payname')
                ->add('capacite')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('nomcomp')
                ->add('type')
                ->add('payname')
                ->add('capacite')
        ;
    }

}
