<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailingAdmin
 *
 * @author HugoG
 */

namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Lef\DataBundle\Form\TranchePrixType;
use Lef\DataBundle\Form\MediaType;

class LocationAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('reference')
                ->add('titre')
                ->add('adresse')
                ->add('pays', 'country', array('preferred_choices' => array('FR'),))
                ->add('region')
                ->add('departement')
                ->add('ville')
                ->add('latitude')
                ->add('longitude')
                ->add('ZipPostalCode')
                ->add('nombreDePersonne')
                ->add('nombreEtoiles', 'choice', array(
                    'choices' => array(
                        0 => '0',
                        1 => '1',
                        2 => '2',
                        3 => '3',
                        4 => '4',
                        5 => '5',
                    ),
                    'multiple' => FALSE,
                ))
                ->add('surfaceHabitable')
                ->add('nombrePiece')
                ->add('nombreSalleDeBain')
                ->add('nombreChambre')
                ->add('animauxDomestiques')
                ->add('piscine')
                ->add('descriptionBreve')
                ->add('descriptionDetaillee')
                ->add('caracteristiques')
                ->add('amenagement')
                ->add('sejourAvecCouchage')
                ->add('coinRepas')
                ->add('cuisine')
                ->add('sanitaire')
                ->add('distanceOther')
                ->add('distanceMer')
                ->add('distanceLac')
                ->add('distanceRemontees')
                ->add('distanceCentreVille')
                ->add('distanceShopping')
                // ==== with association tables ====
                // ==== ManyToMany BiDirec ====
//                ->add('alerts')
                ->add('medias', 'collection', array(
                    'type' => new MediaType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'required' => FALSE,))
//                ->add('offres')
//                ->add('plannings')
                // ==== OneToOne UniDirec ====
                ->add('tranchePrix', new TranchePrixType(), array('required' => FALSE,))
                // ==== OneToMany UniDirec ====
                ->add('bienEtres')
                ->add('equipements')
                ->add('localisations')
                ->add('optionEnfants')
                ->add('optionHandicapees')
                ->add('saisons')
                ->add('themes')
                ->add('types')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('reference')
                ->add('creationDate', 'doctrine_orm_date')
                ->add('modificationDate', 'doctrine_orm_date')
                ->add('adm')
                ->add('apm')
                ->add('promo')
                ->add('locked')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('reference')
                ->add('titre')
                ->add('annonceur')
                ->add('apm', 'boolean', array('editable' => true))
                ->add('adm', 'boolean', array('editable' => true))
                ->add('promo', 'boolean', array('editable' => true))
                ->add('locked', 'boolean', array('label' => 'Bloquée', 'editable' => true))
                ->add('creationDate', 'date', array('format' => 'd/m/Y H:i', 'label' => 'Date création'))
                ->add('modificationDate', 'date', array('format' => 'd/m/Y H:i', 'label' => 'Date Modification'))
                ->add('nbrVus30j', NULL, array('label' => 'Nbr vus 30j'))
                ->add('nbrVusAll', NULL, array('label' => 'Nbr vus tout'))
                ->add('nbrResa30j', NULL, array('label' => 'Nbr résa 30j'))
                ->add('nbrResaAll', NULL, array('label' => 'Nbr résa tout'))
        ;
    }

}
