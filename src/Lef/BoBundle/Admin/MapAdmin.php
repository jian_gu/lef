<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MapAdmin
 *
 * @author HugoG
 */

namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MapAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('ftpId', 'integer', array('label' => ''))
                ->add('targetTable', 'text', array('label' => ''))
                ->add('targetField', 'text', array('label' => ''))
                ->add('xmlFile', 'text', array('label' => ''))
                ->add('xmlPath', 'text', array('label' => ''))
                ->add('xmlAttr', 'text', array('label' => ''))
                ->add('xmlOption', 'text', array('label' => ''))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('ftpId')
                ->add('targetTable')
                ->add('xmlFile')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('ftpId')
                ->add('targetTable')
                ->add('xmlFile')
        ;
    }

}
