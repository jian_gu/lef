<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PartnersDataAdmin
 *
 * @author HugoG
 */
namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PartnersDataAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('code', 'text', array('label' => 'code'))
            ->add('name', 'text', array('label' => 'Nom'))
            ->add('country', 'text', array('label' => 'Pays'))
            ->add('region', 'text', array('label' => 'Région'))
            ->add('place', 'text', array('label' => 'Place'))
            ->add('zip', 'integer', array('label' => 'CP'))
            ->add('type', 'text', array('label' => 'type'))
            ->add('details', 'text', array('label' => 'Détails'))
            ->add('quality', 'integer', array('label' => 'Qualité'))
            ->add('brand', 'text', array('label' => 'Marque'))
            ->add('pax', 'integer', array('label' => 'PAX'))
            ->add('sqm', 'integer', array('label' => 'SQM'))
            ->add('rooms', 'integer', array('label' => 'Pièce'))
            ->add('bedrooms', 'integer', array('label' => 'Chambre'))
            ->add('toilets', 'integer', array('label' => 'Toilette'))
            ->add('bathrooms', 'integer', array('label' => 'SDB'))
            ->add('geodata', 'integer', array('label' => 'Coordonnée Géo'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('code')
            ->add('name')
            ->add('country')
            ->add('zip')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('code')
            ->add('name')
            ->add('country')
            ->add('zip')
        ;
    }
}