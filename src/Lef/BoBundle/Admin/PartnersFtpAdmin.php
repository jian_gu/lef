<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PartnersFtpAdmin
 *
 * @author HugoG
 */
namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PartnersFtpAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('partnersId', 'entity', array('label' => 'Partenaire', 'class' => 'Lef\DataBundle\Entity\PartnersData'))
            ->add('host', 'text', array('label' => 'Host'))
            ->add('user', 'text', array('label' => 'Utilisateur'))
            ->add('srcDirectory', 'text', array('label' => 'Répertoire src'))
            ->add('password', 'text', array('label' => 'MDP'))
            ->add('port', 'text', array('label' => 'Port'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('partnersId')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('partnersId')
            ->add('host')
            ->add('user')
        ;
    }
}