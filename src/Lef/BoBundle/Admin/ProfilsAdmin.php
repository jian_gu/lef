<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProfilsAdmin
 *
 * @author HugoG
 */
namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProfilsAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('societe', 'text', array('label' => ''))
            ->add('nom', 'text', array('label' => ''))
            ->add('prenom', 'text', array('label' => ''))
            ->add('adresse', 'text', array('label' => ''))
            ->add('cp', 'text', array('label' => ''))
            ->add('ville', 'text', array('label' => ''))
            ->add('pays', 'text', array('label' => ''))
            ->add('tel1', 'text', array('label' => ''))
            ->add('tel2', 'text', array('label' => ''))
            ->add('fax', 'text', array('label' => ''))
            ->add('email', 'text', array('label' => ''))
            ->add('site', 'text', array('label' => ''))
            ->add('login', 'text', array('label' => ''))
            ->add('password', 'text', array('label' => ''))
            ->add('cpassword', 'text', array('label' => ''))
            ->add('sid', 'text', array('label' => ''))
            ->add('datecrea', 'datetime', array('label' => ''))
            ->add('langang', 'checkbox', array('label' => ''))
            ->add('langall', 'checkbox', array('label' => ''))
            ->add('langesp', 'checkbox', array('label' => ''))
            ->add('langita', 'checkbox', array('label' => ''))
            ->add('langaut', 'checkbox', array('label' => ''))
            ->add('langauttxt', 'text', array('label' => ''))
            ->add('credit', 'checkbox', array('label' => ''))
            ->add('bonus', 'checkbox', array('label' => ''))
            ->add('unitadm', 'checkbox', array('label' => ''))
            ->add('cradm', 'checkbox', array('label' => ''))
            ->add('active', 'checkbox', array('label' => ''))
            ->add('admactive', 'checkbox', array('label' => ''))
            ->add('datemaj', 'datetime', array('label' => ''))
            ->add('isadmin', 'checkbox', array('label' => ''))
            ->add('notifysup', 'checkbox', array('label' => ''))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom')
            ->add('cp')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nom')
            ->add('cp')
        ;
    }
}