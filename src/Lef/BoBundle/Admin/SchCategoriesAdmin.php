<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SchCategoriesAdmin
 *
 * @author HugoG
 */

namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SchCategoriesAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('idCatMere', 'integer', array('label' => 'Catégorie supérieure'))
                ->add('catName', 'text', array('label' => 'Nom catégorie'))
                ->add('catRepertoire', 'text', array('label' => 'Répertoire catégorie'))
                ->add('texte', 'textarea', array('label' => 'Texte'))
                ->add('activation', 'checkbox', array('label' => 'Activation'))
                ->add('position', 'checkbox', array('label' => 'Position'))
                ->add('canAdd', 'checkbox', array('label' => 'Ajoutable'))
                ->add('motCle', 'text', array('label' => 'Mot clé'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('idCatMere')
                ->add('catName')
                ->add('activation')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('catName')
                ->add('idCatMere')
                ->add('activation', null, array('editable' => true))
        ;
    }

}
