<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SchCommentairesAdmin
 *
 * @author HugoG
 */

namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SchCommentairesAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('idSite', 'integer', array('label' => 'Site'))
                ->add('commentaire', 'textarea', array('label' => 'Commentaire'))
                ->add('nomPosteur', 'text', array('label' => 'Nom posteur'))
                ->add('emailPosteur', 'text', array('label' => 'Email posteur'))
                ->add('detePostage', 'date', array('label' => 'Date postage'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('idSite')
                ->add('nomPosteur')
                ->add('emailPosteur')
                ->add('datePostage')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('nomPosteur')
                ->add('emailPosteur')
                ->add('idSite')
                ->add('datePostage')
        ;
    }

}
