<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SchInfosAdmin
 *
 * @author HugoG
 */

namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SchInfosAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('baseurl', 'text', array('label' => ''))
                ->add('titreSite', 'text', array('label' => ''))
                ->add('urlSite', 'text', array('label' => ''))
                ->add('nomMaster', 'text', array('label' => ''))
                ->add('emailMaster', 'text', array('label' => ''))
                ->add('urlRep', 'text', array('label' => 'urlRep'))
                ->add('useNewsletter', 'checkbox', array('label' => ''))
                ->add('nbSitesInTopclics', 'checkbox', array('label' => ''))
                ->add('nbSitesInTopvotes', 'checkbox', array('label' => ''))
                ->add('nbDays2benew', 'checkbox', array('label' => ''))
                ->add('nbScatsOnIndex', 'checkbox', array('label' => ''))
                ->add('maxPresence', 'checkbox', array('label' => ''))
                ->add('nbSitesOnPage', 'checkbox', array('label' => ''))
                ->add('autoValidate', 'checkbox', array('label' => ''))
                ->add('funcMail', 'checkbox', array('label' => ''))
                ->add('funcGetMetaTags', 'checkbox', array('label' => ''))
                ->add('allowImages', 'checkbox', array('label' => ''))
                ->add('detectImages', 'text', array('label' => ''))
                ->add('codeVerif', 'checkbox', array('label' => ''))
                ->add('searchMotsComplets', 'checkbox', array('label' => ''))
                ->add('detectUrl', 'checkbox', array('label' => ''))
                ->add('kwMax', 'checkbox', array('label' => ''))
                ->add('kwCliquables', 'checkbox', array('label' => ''))
                ->add('kwDelai', 'integer', array('label' => ''))
                ->add('kwGoogle', 'checkbox', array('label' => ''))
                ->add('kwexclude', 'text', array('label' => ''))
                ->add('affPr', 'checkbox', array('label' => ''))
                ->add('affPd', 'checkbox', array('label' => ''))
                ->add('urlRewritting', 'checkbox', array('label' => ''))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('baseurl')
                ->add('titreSite')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('baseurl')
                ->add('titreSite')
                ->add('urlSite')
                ->add('nomMaster')
        ;
    }

}
