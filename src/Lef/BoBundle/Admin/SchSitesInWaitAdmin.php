<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SchSitesInWaitAdmin
 *
 * @author HugoG
 */

namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SchSitesInWaitAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('idCat', 'integer', array('label' => ''))
                ->add('racine', 'integer', array('label' => ''))
                ->add('balTitle', 'text', array('label' => ''))
                ->add('titre', 'text', array('label' => ''))
                ->add('description', 'text', array('label' => ''))
                ->add('url', 'text', array('label' => ''))
                ->add('urlLink', 'text', array('label' => ''))
                ->add('urlImage', 'text', array('label' => ''))
                ->add('nomProprio', 'text', array('label' => ''))
                ->add('emailProprio', 'text', array('label' => ''))
                ->add('lng', 'number', array('label' => ''))
                ->add('lat', 'number', array('label' => ''))
                ->add('motCle', 'text', array('label' => ''))
                ->add('dateSoumission', 'date', array('label' => ''))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('idCat')
                ->add('titre')
                ->add('motCle')
                ->add('dateSoumission')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('titre')
                ->add('idCat')
                ->add('racine')
                ->add('balTitle')
        ;
    }

}
