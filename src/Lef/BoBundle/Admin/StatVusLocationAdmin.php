<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailingAdmin
 *
 * @author HugoG
 */

namespace Lef\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class StatVusLocationAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('location')
                ->add('visiteur')
                ->add('date', 'datetime', array('label' => 'Date visite', 'date_format' => 'ddMMyyyy'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('location')
                ->add('visiteur')
                ->add('date')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id')
                ->add('location')
                ->add('visiteur')
                ->add('date')
        ;
    }

    /*
     * Override Parent class function, customize export fields
     */

    public function getExportFields() {
        $translator = $this->getTranslator();
        $labels = array(
            $translator->trans('Id') => 'id',
            $translator->trans('Location') => 'location',
            $translator->trans('Visiteur') => 'visiteur',
            $translator->trans('Date visite') => 'date',
        );
        return $labels;
    }

}
