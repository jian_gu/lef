<?php

namespace Lef\BoBundle\Block;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\BaseBlockService;

class AdminEmailBlockService extends BaseBlockService {

    public function getName() {
        return 'Envoyer Email';
    }

    public function setDefaultSettings(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'destination' => '',
            'objet' => '',
            'content' => '',
            'template' => 'LefBoBundle:Block:admin_block_email.html.twig',
        ));
    }

    public function validateBlock(ErrorElement $errorElement, BlockInterface $block) {
        $errorElement
                ->with('settings.destination')
                ->assertNotNull(array())
                ->assertNotBlank()
                ->end();
    }

    public function buildEditForm(FormMapper $formMapper, BlockInterface $block) {
        $formMapper->add('settings', 'sonata_type_immutable_array', array());
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null) {
        // merge settings
        $settings = $blockContext->getSettings();


        return $this->renderResponse($blockContext->getTemplate(), array(
                    'block' => $blockContext->getBlock(),
                    'settings' => $settings
                        ), $response);
    }

}
