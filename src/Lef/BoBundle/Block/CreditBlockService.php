<?php

namespace Lef\BoBundle\Block;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\BaseBlockService;
use Lef\BoBundle\Controller\CreditController;

class CreditBlockService extends BaseBlockService {

    protected $securityContext;
    protected $entityManager;
    protected $name;
    protected $templating;

    function __construct($secuCtxt, $em, $name, $templating) {
        $this->securityContext = $secuCtxt;
        $this->entityManager = $em;
        $this->name = $name;
        $this->templating = $templating;
    }

    public function getName() {
        return 'Credit';
    }

    public function setDefaultSettings(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'template' => 'LefBoBundle:Block:sonata_block_credit.html.twig',
            'roles' => '',
        ));
    }

    public function validateBlock(ErrorElement $errorElement, BlockInterface $block) {
        $errorElement
//                ->with('settings.destination')
//                ->assertNotNull(array())
//                ->assertNotBlank()
//                ->end()
        ;
    }

    public function buildEditForm(FormMapper $formMapper, BlockInterface $block) {
        $formMapper->add('settings', 'sonata_type_immutable_array', array());
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null) {
        // merge settings
        $settings = $blockContext->getSettings();
        // allowed role
        $allowedRole = $settings['roles'];
        // get connected user roles
        $currentUserRoles = $this->_getConnectedUser()->getRoles();
        
        $correspond = array_intersect($allowedRole, $currentUserRoles);

        if (count($correspond) > 0) {
            return $this->renderResponse($blockContext->getTemplate(), array(
                        'block' => $blockContext->getBlock(),
                        'settings' => $settings,
                        'credits' => $this->_getConnectedUser()->getCreditPromo()
                            ), $response);
        } else {
            return new Response();
        }
    }

    /**
     * function get current connected user
     * @return object
     */
    private function _getConnectedUser() {
        if (!$this->securityContext->getToken()->getUser()) {
            throw $this->securityContext->createNotFoundException('Utilisateur non connecté.');
        }
        return $this->securityContext->getToken()->getUser();
    }

}
