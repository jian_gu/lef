<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Lef\BoBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AccountCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('account:deletea');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
//        $this->container = $this->getApplication()->getKernel()->getContainer();
//        $myService = $this->getContainer()->get('lef_bo.user.delete.3j');
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $myService = $this->getContainer()->get('lef_bo.auto.user.delete.3j');
//        $myService->tt();
        $myService->deleteAccountNonValidatedIn3Days($em);
        
//        $output->writeln('Command called ');
    }

}
