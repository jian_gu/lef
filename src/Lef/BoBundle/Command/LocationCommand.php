<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Lef\BoBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LocationCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('location:autocheckupdate');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $myService = $this->getContainer()->get('lef_bo.auto.location.check_days');
        // not update in 30 days
        $myService->actionLocationsNotUpdateInDays(30);
        // not update in 33 days
        $myService->actionLocationsNotUpdateInDays(33);
        // not update in 120 days
        $myService->actionLocationsNotUpdateInDays(120);
        // not update in 123 days
        $myService->actionLocationsNotUpdateInDays(123);
        
        // remove promo if locations are not update in ex. 28 days
        $myService->actionRemovePromoNonMaj();
        
//        $output->writeln('Command called ');
    }

}
