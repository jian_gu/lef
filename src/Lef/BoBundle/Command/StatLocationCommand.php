<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Lef\BoBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StatLocationCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('location:stat');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $myService = $this->getContainer()->get('lef_bo.auto.location.stat');
        // ===
        $myService->updateVus();
        
//        $output->writeln('Command called ');
    }

}
