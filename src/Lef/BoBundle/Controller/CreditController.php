<?php

namespace Lef\BoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lef\BoBundle\Form\Type\DemandeCreditType as CreditType;

class CreditController extends Controller {

    protected static $currentUser;

    function __construct() {
        
    }

    /**
     * Lists all Facture entities.
     *
     */
    public function indexAction(Request $request) {
        if ($this->_getConnectedUser() == 'anon.') {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $byAnnonceur = array('id' => $this->_getConnectedUser()->getId());

        $em = $this->getDoctrine()->getManager();

        $entitie = $em->getRepository('LefBoBundle:User')->findBy($byAnnonceur);

        // ===== Form ======
        $form = $this->createDemandeForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            // prepare to send email to admin
            // get demande detail
            $valDemande = $form["choiceNbrCredit"]->getData();
            $nbrCredit = (integer) substr($valDemande, 0, 2);
            $prix = (integer) substr($valDemande, 3, 5) / 2;
            // prepaire token unique
            $token = 'ADM-' . $this->_getConnectedUser()->getId() . '-' . time();

            $this->_sendEmailMessage($nbrCredit, $prix, $token);
            // Create flash message
            $this->_createFlashMessage('success', 'Votre demande a été envoyée, veuillez vérifier dans votre boîte courriel.');
            // stay on the current page
            return $this->redirect($this->generateUrl('credit'));
        }
        // ====== end form ======


        return $this->render('LefBoBundle:Credit:index.html.twig', array(
                    'entity' => $entitie[0],
                    'form' => $form->createView()
        ));
    }

    /**
     * Creates a form to send credit demande
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDemandeForm() {
        $form = $this->createForm(new CreditType(), null, array(
            'action' => $this->generateUrl('credit'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Envoyer la demande'));

        return $form;
    }

    /**
     * function get current connected user
     * @return object
     */
    private function _getConnectedUser() {
        if (!$this->getUser()) {
            throw $this->createNotFoundException('Utilisateur non connecté.');
        }
        return $this->getUser();
    }

    /**
     * Create flash message
     * @param string $type
     * @param string $contents
     */
    private function _createFlashMessage($type, $contents) {
        $this->get('session')->getFlashBag()->add(
                $type, $contents
        );
    }

    /**
     * Send email with mailer service
     * @param type $nbrCredit
     * @param type $prix
     * @param type $token
     */
    private function _sendEmailMessage($nbrCredit, $prix, $token) {
        $annonceur = $this->_getConnectedUser();
        $mailer = $this->get('lef_bo.mailer.twig_swift');

        $mailer->sendDemandeCreditPromoEmailMessage($annonceur, $nbrCredit, $prix, $token);
    }

}
