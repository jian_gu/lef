<?php

namespace Lef\BoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
    public function testAction() {
        $myService = $this->container->get('lef_bo.user.delete.3j');
        $myService->tt();
        die();
    }
}
