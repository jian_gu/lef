<?php

namespace Lef\BoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class EmailController extends Controller {

    private static $to = '';
    private static $from = '';
    private static $template = '';

    /**
     * @return \Sonata\AdminBundle\Admin\Pool
     */
    protected function getAdminPool() {
        return $this->container->get('sonata.admin.pool');
    }

    /**
     * @return string
     */
    protected function getBaseTemplate() {
        if ($this->container->get('request')->isXmlHttpRequest()) {
            return $this->getAdminPool()->getTemplate('ajax');
        }

        return $this->getAdminPool()->getTemplate('layout');
    }

    /**
     * Administrator send mail to anyone
     * @param string $to
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return type
     */
    public function adminSendEmailAction($to, Request $request) {
        $form = $this->buildForm();
        self::$from = $this->getConnectedUser()->getEmail();
        self::$to = $to;

        // if destination is set in URL
        if (strlen(trim($to)) > 0) {
            $form->remove('to');
        }

        $blocks = array(
            'top' => array(),
            'left' => array(),
            'center' => array(),
            'right' => array(),
            'bottom' => array()
        );

        foreach ($this->container->getParameter('sonata.admin.configuration.dashboard_blocks') as $block) {
            $blocks[$block['position']][] = $block;
        }

        // Call funciton to send email
        $this->sendEmail($to, $form->handleRequest($request));
        return $this->render('LefBoBundle:Email:adminSendEmail.html.twig', array(
                    'base_template' => $this->getBaseTemplate(),
                    'admin_pool' => $this->container->get('sonata.admin.pool'),
                    'from' => self::$from,
                    'to' => self::$to,
                    'form' => $form->createView(),
                    'blocks' => $blocks,
        ));
    }

    /**
     * Create form
     * @return formType
     */
    public function buildForm() {
        $defaultData = array('content' => '');
        $form = $this->createFormBuilder($defaultData)
                ->add('to', 'email')
                ->add('object', 'text')
                ->add('content', 'textarea')
                ->add('send', 'submit')
                ->getForm();
        return $form;
    }

    /**
     * Function send email
     * @param object $formRqst
     */
    private function sendEmail($to, $formRqst) {
        if ($formRqst->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $formRqst->getData();

            $destination = $to ? $to : self::$to;

            // Swift
            $message = \Swift_Message::newInstance()
                    ->setContentType("text/html")
                    ->setSubject($data['object'])
                    ->setFrom(self::$from)
                    ->setTo($destination)
                    ->setBody(
                    $this->renderView(
                            'LefBoBundle:Email:adminSendEmailTemplate.html.twig', array('message' => $data['content'])
                    )
            );

            // send Mail
            $this->get('mailer')->send($message);

            // generate flash message
            $this->flashMsg('success', 'Un email est envoyé à ' . self::$to . ' avec succès.');
        }
    }

    /**
     * Get current connected user
     * @return object
     */
    private function getConnectedUser() {
        $user = $this->container->get('security.context')->getToken()->getUser();
        return $user;
    }

    /**
     * Set flash message
     * @param string $type
     * @param string $msg
     */
    private function flashMsg($type, $msg) {
        $this->get('session')->getFlashBag()->add(
                $type, $msg
        );
    }

}
