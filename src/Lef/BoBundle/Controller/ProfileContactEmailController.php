<?php

namespace Lef\BoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lef\BoBundle\Form\Type\ContactEmailType as EmailType;

class ProfileContactEmailController extends Controller {

    protected $from;
    protected $to;

    /**
     * Lists all Facture entities.
     *
     */
    public function indexAction(Request $request) {
        if ($this->_getConnectedUser() == 'anon.') {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        // get annonceur id array
        $byAnnonceur = array('id' => $this->_getConnectedUser()->getId());

        $em = $this->getDoctrine()->getManager();

        $entitie = $em->getRepository('LefBoBundle:User')->findBy($byAnnonceur);
        $this->from = $entitie[0]->getEmail();
        // Set destination
        $this->to = $this->container->getParameter('email_address_admin');

        // ===== Form ======
        $form = $this->createDemandeForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            // prepare to send email to admin
            // get demande detail
            $formData = $form->getData();
            $object = $formData['object'];
            $contents = $formData['contents'];

            // Send mail with copy to client
            $this->_sendMessageContact($this->_getConnectedUser(), 'client', $this->_getConnectedUser()->getEmail(), $object, $contents);
            $this->_sendMessageContact($this->_getConnectedUser(), 'admin', $this->to, $object, $contents);
            // Create flash message
            $this->_createFlashMessage('success', 'Votre message a été envoyée.');
            // stay on the current page
            return $this->redirect($this->generateUrl('LefBoBundle_profile_contact'));
        }
        // ====== end form ======


        return $this->render('LefBoBundle:Email:profileContactSendEmail.html.twig', array(
                    'entity' => $entitie[0],
                    'form' => $form->createView(),
                    'from' => $this->from,
        ));
    }

//////////////////    

    /**
     * Creates a form to send credit demande
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDemandeForm() {
        $form = $this->createForm(new EmailType(), null, array(
            'action' => $this->generateUrl('LefBoBundle_profile_contact'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Envoyer'));

        return $form;
    }

    /**
     * function get current connected user
     * @return object
     */
    private function _getConnectedUser() {
        if (!$this->getUser()) {
            throw $this->createNotFoundException('Utilisateur non connecté.');
        }
        return $this->getUser();
    }

    /**
     * Create flash message
     * @param string $type
     * @param string $contents
     */
    private function _createFlashMessage($type, $contents) {
        $this->get('session')->getFlashBag()->add(
                $type, $contents
        );
    }

    /**
     * Prepare sending email
     * @param type $user
     * @param type $type
     * @param type $to
     * @param type $object
     * @param type $contents
     */
    private function _sendMessageContact($user, $type, $to, $object, $contents) {
        $template = '';

        switch ($type) {
            case 'admin':
                $objectDisplay = 'Contact client -- sujet: ' . $object;
                $template = 'LefBoBundle:ProfileContactMail:emailAdmin.html.twig';
                break;
            case 'client':
                $objectDisplay = 'Vous venez de contacter Louer-en-France';
                $template = 'LefBoBundle:ProfileContactMail:emailClient.html.twig';
                break;

            default:
                break;
        }
        $this->_sendEmail($user, $to, $objectDisplay, $object, $contents, $template);
    }

    /**
     * Function send email
     * @param type $user
     * @param type $to
     * @param type $objectDisplay   object which will be displayed in email
     * @param type $object          the real object
     * @param type $contents
     * @param type $template
     */
    private function _sendEmail($user, $to, $objectDisplay, $object, $contents, $template) {

        $destination = $to ? $to : $this->to;

        // Swift
        $message = \Swift_Message::newInstance()
                ->setContentType("text/html")
                ->setSubject($objectDisplay)
                ->setFrom($this->from)
                ->setTo($destination)
                ->setBody(
                $this->renderView(
                        $template, array('sujet' => $object,
                    'message' => $contents,
                    'user' => $user)
                )
        );
//                ->setBody($contents);
        // send Mail
        $this->get('mailer')->send($message);
    }

}
