<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Lef\BoBundle\Form\Handler;

use FOS\UserBundle\Model\UserManagerInterface;
use Lef\BoBundle\Model\UserInterface;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Lef\BoBundle\Controller\RegistrationFOSUser1Controller as RegCtrller;

class RegistrationFormHandler {

    protected $request;
    protected $userManager;
    protected $form;
    protected $mailer;
    protected $tokenGenerator;

    public function __construct(FormInterface $form, Request $request, UserManagerInterface $userManager, MailerInterface $mailer, TokenGeneratorInterface $tokenGenerator) {
        $this->form = $form;
        $this->request = $request;
        $this->userManager = $userManager;
        $this->mailer = $mailer;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * 
     * @param array $groups object group, string group names
     * @param boolean $confirmation
     * @return boolean
     */
    public function process($groups, $confirmation = false) {
        $user = $this->createUser();
        $this->form->setData($user);

        if ('POST' === $this->request->getMethod()) {
            $this->form->bind($this->request);

            // !!!! return false for test
            if ($this->form->isValid()) {
                $this->onSuccess($user, $confirmation, $groups);

                return true;
            }
        }

        return false;
    }

    /**
     * 
     * @param \Lef\BoBundle\Model\UserInterface $user
     * @param boolean $confirmation
     * @param array $groups object group, string group names
     */
    protected function onSuccess(UserInterface $user, $confirmation, $groups) {
        // if cofirmation mail is needed
        if ($confirmation) {
            $user->setEnabled(false);
            if (null === $user->getConfirmationToken()) {
                $user->setConfirmationToken($this->tokenGenerator->generateToken());
            }

            // send email
            $this->mailer->sendConfirmationEmailMessage($user);
        } else {
            $user->setEnabled(true);
        }

        // set numéro droit publication annonce pour annonceur
        $this->_setNbrDroitAnnonceur($user);
        // set user group
        $userGroup = $this->_pickGroupUser($user, $groups);
        $user->setGroups($userGroup);

        // update/insert user
        $this->userManager->updateUser($user);
    }

    /**
     * @return UserInterface
     */
    protected function createUser() {
        return $this->userManager->createUser();
    }

    /**
     * function return a goup according to current user role
     * @param object $user
     * @param objectArray $groups
     * @return object group
     */
    private function _pickGroupUser($user, $groups) {
        $group = "";
        $userRoles = $user->getRoles();
        $userRole = $userRoles[0];
        switch ($userRole) {
            case 'ROLE_ANNONCEUR':
                $userRole = $groups['names']['Annonceurs'];
                break;
            case 'ROLE_VISITEUR':
                $userRole = $groups['names']['Visiteurs'];
                break;
            default:
                $userRole = 'User';
                break;
        }
        foreach ($groups['objects'] as $gp) {
            if ($gp->getName() == $userRole) {
                $group = $gp;
            }
        }
        return array($group);
    }
    
    /**
     * set numeros concerne de la publication d'annonce
     * @param type $user
     * @return type
     */
    private function _setNbrDroitAnnonceur($user) {
        $userRoles = $user->getRoles();
        $userRole = $userRoles[0];
        if($userRole == 'ROLE_ANNONCEUR') {
            // Set initial free annonce nbr to 5
        $user->setNbrAnnonce(5);
        // Set initial free annonce nbr to 3
        $user->setCreditPromo(3);
        }
        return $user;
    }

}
