<?php

namespace Lef\BoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DemandeCreditType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('choiceNbrCredit', 'choice', array('choices' => array(
                        '01010' => '   1     ADM soit  5      € TTC',
                        '02020' => '   2  ADM soit  10    €TTC',
                        '03025' => '   3  ADM soit  12,5 €TTC soit la troisième annonce 1/2 tarif ',
                        '04035' => '   4  ADM soit  17,5 €TTC',
                        '05045' => '   5  ADM soit  22,5 € TTC  ',
                        '06050' => '   6  ADM soit  25    € TTC soit la sixième annonce 1/2 tarif ',
                        '07060' => '   7  ADM soit  30    € TTC',
                        '08070' => '   8  ADM soit  35    € TTC',
                        '09075' => '   9  ADM soit  37,5 € TTC soit la neuvième annonce 1/2 tarif ',
                        '10085' => '10  ADM soit  42,5  € TTC',
                        '11095' => '11  ADM soit  47,5  € TTC',
                        '12100' => '12  ADM soit  50     € TTC soit la douzième annonce 1/2 tarif ',
                        '13110' => '13  ADM soit  55     € TTC',
                        '14120' => '14  ADM soit  60     € TTC ',
                        '15125' => '15  ADM soit  62,5  € TTC soit la quinzième annonce gratuite ',
                        '16135' => '16  ADM soit  67,5  € TTC',
                        '17145' => '17  ADM soit  72,5  € TTC',
                        '18150' => '18  ADM soit  75 € TTC soit la dix-huitième annonce 1/2 tarif',
                        '19160' => '19  ADM soit  80 € TTC',
                        '20170' => '20  ADM soit  85 € TTC',
                        '21175' => '21  ADM soit  87,5 € TTC soit la vingt et unième annonce 1/2 tarif',
                    ),
                    'empty_value' => '-',
                    'empty_data' => null,
                    'required' => TRUE,
                    'multiple' => FALSE)
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'lef_bobundle_demande_credit';
    }

}
