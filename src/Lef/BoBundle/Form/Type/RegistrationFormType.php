<?php

namespace Lef\BoBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RegistrationFormType
 *
 * @author HugoG
 */
class RegistrationFormType extends BaseType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);

        // add your custom field
        parent::buildForm($builder, $options);

        $builder
                ->add('roles', 'collection', array(
                    'type' => 'choice',
                    'label' => 'Rôle',
                    'options' => array(

                        'choices' => array('ROLE_ANNONCEUR' => 'Annonceur', 'ROLE_VISITEUR' => 'Visiteur'),
                        'label' => false,
                        'attr' => array('style' => 'width:300px', 'customattr' => 'customdata'),
                    )
                        )
                )
                ->add('raisonsociale')
                ->add('typesociete')

                ->add('gender', 'choice', array(
                    'empty_value' => 'Civilité',
                    'choices' => array(
                        'm' => 'Monsieur', 'f' => 'Madame'),
                    'required' => false,
                ))

                ->add('Firstname')
                ->add('Lastname')
                ->add('adresse')
                ->add('phone', 'repeated', array(
                    'type' => 'text',
                    'first_options' => array('label' => 'Numéro téléphone'),
                    'second_options' => array('label' => 'Vérification'),
                    'invalid_message' => 'Les deux numéro téléphone ne sont pas identiques',
                ))

                ->add('email', 'repeated', array(
                    'type' => 'email',
                    'first_options' => array('label' => 'Adresse mail'),
                    'second_options' => array('label' => 'Vérification'),
                    'invalid_message' => 'Les deux adresses mail ne sont pas identiques',
                ))

                ->add('newsletter')
        ;
    }

    public function getName() {
        return 'lef_user_registration';
    }

}
