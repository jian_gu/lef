<?php

namespace Lef\BoBundle\Handler;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface {

    protected $router;
    protected $security;
    protected $session;

    public function __construct(Router $router, SecurityContext $security) {
        $this->router = $router;
        $this->security = $security;
        $this->session = new Session();
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token) {

        if ($this->security->isGranted('ROLE_ADMIN')) {
            $response = new RedirectResponse($this->router->generate('sonata_admin_dashboard'));            // Modified le 250814 par Jian
        } elseif ($this->security->isGranted('ROLE_ANNONCEUR')) {
            $response = new RedirectResponse($this->router->generate('fos_user_profile_show'));             // Modified le 150814 par Jian
        } elseif ($this->security->isGranted('ROLE_VISITEUR')) {
            // get referer path stocked in session
            if ($this->session->get('refererPath')) {
                $referer_url = $this->session->get('refererPath');
                $this->session->remove('refererPath');
            } else {
                // redirect the user to where they were before the login process begun.
                $referer_url = $request->headers->get('referer');
            }


            $response = new RedirectResponse($referer_url);
        }

        return $response;
    }

}
