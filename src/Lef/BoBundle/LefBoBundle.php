<?php

namespace Lef\BoBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LefBoBundle extends Bundle
{
    public function getParent()
    {
        return 'ApplicationSonataUserBundle';
    }
}
