<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Lef\BoBundle\Mailer;

use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Lef\BoBundle\Controller\RegistrationFOSUser1Controller as RegCtrller;
use Lef\DataBundle\Entity\Location;

/**
 * @author Christophe Coevoet <stof@notk.org>
 */
class TwigSwiftMailer implements MailerInterface {

    protected $mailer;
    protected $router;
    protected $twig;
    protected $parameters;

    public function __construct(\Swift_Mailer $mailer, UrlGeneratorInterface $router, \Twig_Environment $twig, array $parameters) {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->twig = $twig;
        $this->parameters = $parameters;
    }

    // Registration
    public function sendConfirmationEmailMessage(UserInterface $user) {
        $template = $this->parameters['template']['confirmation'];
        $templateAdmin = $this->parameters['template']['confirmationAdmin'];
        $url = $this->router->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), true);
        $context = array(
            'user' => $user,
            'confirmationUrl' => $url
        );

        // Send email to user
        $this->sendMessage($template, $context, $this->parameters['from_email']['confirmation'], $user->getEmail());
        // Send email to admin
        $this->sendMessage($templateAdmin, $context, $this->parameters['from_email']['confirmationAdmin'], $this->parameters['to_email']['confirmationAdmin']);
    }

    // Resetting
    public function sendResettingEmailMessage(UserInterface $user) {
        $template = $this->parameters['template']['resetting'];
        $templateAdmin = $this->parameters['template']['resettingAdmin'];
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), true);
        $context = array(
            'user' => $user,
            'confirmationUrl' => $url
        );
        // send to user
        $this->sendMessage($template, $context, $this->parameters['from_email']['addrNoReply'], $user->getEmail());
        // send to admin
        $this->sendMessage($templateAdmin, $context, $this->parameters['from_email']['addrNoReply'], $user->getEmail());
    }

    // Change password
    // TODO
    // Change profile
    public function sendChangeProfileEmailMessage(UserInterface $user) {
        $template = $this->parameters['template']['changeProfile'];
        $templateAdmin = $this->parameters['template']['changeProfileAdmin'];
        $context = array(
            'user' => $user
        );

        // Send email to user
        $this->sendMessage($template, $context, $this->parameters['from_email']['changeProfile'], $user->getEmail());
        // Send email to admin
        $this->sendMessage($templateAdmin, $context, $this->parameters['from_email']['changeProfileAdmin'], $this->parameters['to_email']['confirmationAdmin']);
    }

    // =======================
    // ====== Location =======
    // =======================
    /**
     * Create an announce
     * @param \FOS\UserBundle\Model\UserInterface $user
     * @param \Lef\DataBundle\Entity\Location $location
     */
    public function sendCreateLocationEmailMessage(UserInterface $user, Location $location) {
        $template = $this->parameters['template']['locationCreateAnnonceur'];
        $templateAdmin = $this->parameters['template']['locationCreateAdmin'];
        $context = array(
            'user' => $user,
            'location' => $location
        );

        // Send email to user
        $this->sendMessage($template, $context, $this->parameters['from_email']['location'], $user->getEmail());
        // Send email to admin
        $this->sendMessage($templateAdmin, $context, $this->parameters['from_email']['locationAdmin'], $this->parameters['to_email']['confirmationAdmin']);
    }

    /**
     * Modify, Update an announce
     * @param \FOS\UserBundle\Model\UserInterface $user
     * @param \Lef\DataBundle\Entity\Location $location
     */
    public function sendUpdateLocationEmailMessage(UserInterface $user, Location $location) {
        $template = $this->parameters['template']['locationUpdateAnnonceur'];
        $templateAdmin = $this->parameters['template']['locationUpdateAdmin'];
        $context = array(
            'user' => $user,
            'location' => $location
        );

        // Send email to user
        $this->sendMessage($template, $context, $this->parameters['from_email']['location'], $user->getEmail());
        // Send email to admin
        $this->sendMessage($templateAdmin, $context, $this->parameters['from_email']['locationAdmin'], $this->parameters['to_email']['confirmationAdmin']);
    }

    /**
     * Location / Planning not updated in $days days
     * @param \FOS\UserBundle\Model\UserInterface $user
     * @param \Lef\DataBundle\Entity\Location $location
     * @param type $days
     * @return type
     */
    public function sendAvertiLocationEmailMessage(UserInterface $user, Location $location, $days) {
        $template;
        // If the day numbers is not defined:
        if (!$days) {
            return;
        }
        // According to the number of days, define our template
        switch ($days) {
            case 30:
                $template = $this->parameters['template']['notUpdatedIn30Days'];
                break;
            case 33:
                $template = $this->parameters['template']['notUpdatedIn33Days'];
                break;
            case 120:
                $template = $this->parameters['template']['notUpdatedIn120Days'];
                break;
            case 123:
                $template = $this->parameters['template']['notUpdatedIn123Days'];
                break;

            default:
                break;
        }

        $context = array(
            'user' => $user,
            'location' => $location
        );

        // Send email to user
        $this->sendMessage($template, $context, $this->parameters['from_email']['location'], $user->getEmail());
    }

    /**
     * Demande crédit promotion
     * @param \FOS\UserBundle\Model\UserInterface $user
     * @param type $nbr
     * @param type $prix
     */
    public function sendDemandeCreditPromoEmailMessage(UserInterface $user, $nbrCredit, $prix, $token) {
        $template = $this->parameters['template']['creditDemandeAnnonceur'];
        $templateAdmin = $this->parameters['template']['creditDemandeAdmin'];
        $context = array(
            'user' => $user,
            'nbrCredit' => $nbrCredit,
            'prix' => $prix,
            'token' => $token
        );

        // Send email to user
        $this->sendMessage($template, $context, $this->parameters['from_email']['credit'], $user->getEmail());
        // Send email to admin
        $this->sendMessage($templateAdmin, $context, $this->parameters['from_email']['credit'], $this->parameters['to_email']['creditAdmin']);
    }

    // =======================
    // ====== Location =======
    // =======================
    /**
     * Passer en promotion d'une annonce
     * @param \FOS\UserBundle\Model\UserInterface $annonceur
     * @param entity $location
     */
    public function sendPasserPromoEmailMessage(UserInterface $user, $location) {
        $template = $this->parameters['template']['passPromoAnnonceur'];
        $templateAdmin = $this->parameters['template']['passPromoAdmin'];
        $context = array(
            'user' => $user,
            'location' => $location,
        );

        // Send email to annonceur
        $this->sendMessage($template, $context, $this->parameters['from_email']['addrNoReply'], $user->getEmail());
        // Send email to admin
        $this->sendMessage($templateAdmin, $context, $this->parameters['from_email']['credit'], $this->parameters['to_email']['addrAdmin']);
    }

    /**
     * Send mails to admin and client when a contac is demanded
     * Added le 120814 par Jian
     * @param type $context
     */
    public function sendContactNousEmailMessage($context) {
        $tplAdmin = $this->parameters['template']['contactAdmin'];
        $tplClient = $this->parameters['template']['contactClient'];

        // Send email to admin
        $this->sendMessage($tplAdmin, $context, $this->parameters['from_email']['addrNoReply'], $this->parameters['to_email']['addrAdmin']);
        // Send email to client
        $this->sendMessage($tplClient, $context, $this->parameters['from_email']['credit'], $context['email']);
    }

    // =======================
    // ====== Sending =======
    // =======================

    /**
     * @param string $templateName
     * @param array  $context
     * @param string $fromEmail
     * @param string $toEmail
     */
    protected function sendMessage($templateName, $context, $fromEmail, $toEmail) {
        $template = $this->twig->loadTemplate($templateName);
        $subject = $template->renderBlock('subject', $context);
        $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);

        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($fromEmail)
                ->setTo($toEmail);

        if (!empty($htmlBody)) {
            $message->setBody($htmlBody, 'text/html')
                    ->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }

        $this->mailer->send($message);
    }

}

