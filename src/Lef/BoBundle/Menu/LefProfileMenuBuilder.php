<?php

// src/Acme/DemoBundle/Menu/Builder.php

namespace Lef\BoBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class LefProfileMenuBuilder extends ContainerAware {

    public function mainMenu(FactoryInterface $factory, array $options) {
        $menu = $factory->createItem('root');

        $menu->setChildrenAttribute('class', 'cms-block cms-block-element nav');
//        $menu->setCurrentUri($this->container->get('request')->getRequestUri());

        $menu->addChild('Mon espace', array('route' => 'sonata_user_profile_show'));
        $menu->addChild('Mon profil', array('route' => 'sonata_user_profile_edit'));
        $menu->addChild('Information de connexion', array('route' => 'sonata_user_profile_edit_authentication'));

        // ======= Menu pour annonceurs ========
        $currentUser = $this->_getConnectedUser();
        $currentUserRoles = $currentUser->getRoles();
        
        if (in_array('ROLE_ANNONCEUR', $currentUserRoles)) {
            $menu->addChild('Mes annonces', array(
                'route' => 'location_annonceur',
                'routeParameters' => array()
            ));
            $menu->addChild('Crédit', array(
                'route' => 'credit',
                'routeParameters' => array()
            ));
            $menu->addChild('Facture', array(
                'route' => 'facture',
                'routeParameters' => array()
            ));
        }
        // === end ===
        $menu->addChild('Contact', array('route' => 'LefBoBundle_profile_contact'));

        // ... add more children

        return $menu;
    }

    /**
     * function get current connected user
     * @return object
     */
    private function _getConnectedUser() {
        if (!$this->container->get('security.context')->getToken()->getUser()) {
            throw $this->createNotFoundException('Utilisateur non connecté.');
        }
        return $this->container->get('security.context')->getToken()->getUser();
    }

}
