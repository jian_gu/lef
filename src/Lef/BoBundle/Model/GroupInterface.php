<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Lef\BoBundle\Model;

use FOS\UserBundle\Model\GroupInterface as FOSGroupInterface;

/**
 * @author Jian GU <g.jian@umanteam.com>
 */
interface GroupInterface extends FOSGroupInterface
{


}
