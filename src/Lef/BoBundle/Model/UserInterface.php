<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Lef\BoBundle\Model;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use FOS\UserBundle\Model\UserInterface as FOSUserInterface;

/**
 * @author Jian GU <g.jian@umanteam.com>
 */
interface UserInterface extends FOSUserInterface
{
    

    /**
     * Sets the default free announce number of the user.
     *
     * This overwrites any previous roles.
     *
     * @param integer $nbrAnnonce
     *
     * @return self
     */
    public function setNbrAnnonce( $nbrAnnonce);
    
    /**
     * 
     */
    public function setCreditPromo($creditPromo);

}
