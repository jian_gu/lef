/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function() {
    // Afficher - cacher des champs selon le rôle 
    $('#form-div-role').find('select').on('change', function() {
        var valRole = $(this).val();
        if($.trim(valRole) === 'ROLE_VISITEUR') {
            $('.onlyAnnonceur').addClass('hidden');
        }
        if($.trim(valRole) === 'ROLE_ANNONCEUR') {
            $('.onlyAnnonceur').removeClass('hidden');
        }
    });
});
