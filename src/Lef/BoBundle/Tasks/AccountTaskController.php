<?php

namespace Lef\BoBundle\Tasks;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AccountTaskController extends Controller {

    private $entityManager;

    function __construct($entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * Supprimer des comptes qui ne sont pas validés pendant 3 jours du moment 
     * d'inscription
     */
    public function deleteAccountNonValidatedIn3Days($em) {
        $accounts = $this->accountsNotValidatedIn3Days($em);
        // if there is/are accounts mean to be deleted
        if (!empty($accounts)) {
            foreach ($accounts as $account) {
                $idUser = $account->getId();
                $this->deleteAccount($idUser, $em);
            }
        }
    }

    /**
     * Récupérer tout les comptes qui ne sont pas validé dans 3 jours
     * @return type
     */
    private function accountsNotValidatedIn3Days($em) {
        // Compare current time and time-account-created
//        $em = $this->getDoctrine()->getManager();
        $accounts = $em->getRepository('LefBoBundle:User')
                ->getAllAccountsNotValidatedIn3Days();

        return $accounts;
    }

    /**
     * Supprimer le compte correspondant
     * @param type $idUser
     * @throws type
     */
    private function deleteAccount($idUser, $em) {
//        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('LefBoBundle:User')->find($idUser);

        if (!$entity) {
            throw $this->createNotFoundException('Utilisateur non trouvable.');
        }

        // =====test yo====
//        $entity->setPhone(date('i'));
        // ================
        $em->remove($entity);
        $em->flush();
    }

}
