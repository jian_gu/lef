<?php

namespace Lef\BoBundle\Tasks;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use Lef\BoBundle\Mailer\TwigSwiftMailer;

class LocationTaskController extends Controller {

    /**
     * EntityManager
     * @var Object 
     */
    protected $em;

    /**
     * TwigSwiftMailer
     * @var service 
     */
    protected $mailer;
    
    /**
     * days no update used for remove promo
     * @var integer 
     */
    protected $daysNoUpdate;

    function __construct(EntityManager $em, TwigSwiftMailer $mailer, $daysNoUpdate) {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->daysNoUpdate = $daysNoUpdate;
    }

    /**
     * If an announce is not updated until $days days:
     * @param integer $days
     */
    public function actionLocationsNotUpdateInDays($days) {
        $em = $this->em;

        $locations = $this->_locationsNotUpdatedInDays($days);
        // if there is/are locations coorespond to condition
        if (!empty($locations)) {
            foreach ($locations as $location) {

                $annonceur = $location->getAnnonceur();
                $this->sendAvertisEmail($annonceur, $location, $days);

                // Different action according to days number
                switch ($days) {
                    case 33:
                        // suspention de l'annonce
                        $location->setLocked(true);
                        // persist modification to BDD
                        $em->persist($location);
                        $em->flush();
                        break;

                    case 123:
                        // Suppression de l'annonce
                        $idLocation = $location->getId();
                        $refLocation = $location->getReference();
                        $this->deleteLocation($idLocation, $refLocation);
                        break;

                    default:
                        break;
                }
            }
        }
    }

    /**
     * Function remove pomotion if it is not updated in ex. 28 days
     */
    public function actionRemovePromoNonMaj() {
        $em = $this->em;

        $days = $this->daysNoUpdate;
        $locations = $this->_locationPromoNotUpdateInDays($days);

        if (!empty($locations)) {
            foreach ($locations as $location) {
//                $location->setPromo(FALSE);                                   // Commmented le 140814 par Jian
                $location->setADM(FALSE);                                       // Added le 140814 par Jian
                $em->persist($location);
                $em->flush();
            }
        }
    }

    // =======================
    // == support functions ==
    // =======================

    /**
     * Send email to annonceur with different contents according to the days number
     * @param type $annonceur
     * @param type $location
     * @param type $days
     */
    private function sendAvertisEmail($annonceur, $location, $days) {
//        die('so far so good');
        $mailer = $this->mailer;
        switch ($days) {
            case 30:
                $mailer->sendAvertiLocationEmailMessage($annonceur, $location, 30);
                break;
            case 33:
                $mailer->sendAvertiLocationEmailMessage($annonceur, $location, 33);
                break;
            case 120:
                $mailer->sendAvertiLocationEmailMessage($annonceur, $location, 120);
                break;
            case 123:
                $mailer->sendAvertiLocationEmailMessage($annonceur, $location, 123);
                break;

            default:
                break;
        }
    }

    /**
     * Delete a location 
     * @param int $idLocation
     * @param string $refLocation
     * @throws exception
     */
    private function deleteLocation($idLocation, $refLocation) {
        $conditions = array('id' => $idLocation, 'reference' => $refLocation);
        $em = $this->em;
        $entity = $em->getRepository('LefBoBundle:User')->findBy($conditions);

        if (!$entity) {
            throw $this->createNotFoundException('Location non trouvable.');
        }

        $em->remove($entity);
        $em->flush();
    }

    /**
     * Function get all Locations not updated in $days Days
     * @param integer $days
     * @return Array
     */
    private function _locationsNotUpdatedInDays($days) {
        $em = $this->em;
        $locations = $em->getRepository('LefDataBundle:Location')
                ->getAllLocationsNotUpdatedInDays($days);
        return $locations;
    }

    /**
     * Function get all location en promo not updated in $days Days
     * @param integer $days 28 ex.
     * @return entityArray
     */
    private function _locationPromoNotUpdateInDays($days) {
        $em = $this->em;
        $locations = $em->getRepository('LefDataBundle:Location')
                ->getAllLocationsPromoNotUpdatedInDays($days);
        return $locations;
    }

}

