<?php

namespace Lef\BoBundle\Tasks;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;

class StatLocationTaskController extends Controller {

    /**
     * EntityManager
     * @var Object 
     */
    protected $em;

    /**
     * days interval
     * @var int 
     */
    protected $interval;

    function __construct(EntityManager $em, $interval) {
        $this->em = $em;
        $this->interval = $interval;
    }

    /**
     * 
     */
    public function updateVus(){
        $this->_loopInsertVusToLocation();
    } 

    // =======================
    // == support functions ==
    // =======================


    /**
     * function loop stat and insert them into locations correspendant
     */
    private function _loopInsertVusToLocation() {
        $vusAll = $this->_vusLocationsAll();
        $vus30j = $this->_vusLocations30j();
        $em = $this->em;

        // Insert vus 30j
        foreach ($vus30j as $vu) {
            $idLocation = $vu['location'];
            $nbrVus = $vu['vus'];
            $location = $em->getRepository('LefDataBundle:Location')->find($idLocation);
            $location->setNbrVus30j($nbrVus);
            $em->persist($location);
            $em->flush();
        }
        $em->clear();
        // Insert vus all
        foreach ($vusAll as $vu) {
            $idLocation = $vu['location'];
            $nbrVus = $vu['vus'];
            $location = $em->getRepository('LefDataBundle:Location')->find($idLocation);
            $location->setNbrVusAll($nbrVus);
            $em->persist($location);
            $em->flush();
        }
        $em->close();
    }

    /**
     * Function get all vus of all locations
     * @return array statVusLocation ['location', 'vus']
     */
    private function _vusLocationsAll() {
        $em = $this->em;
        $vusAll = $em->getRepository('LefDataBundle:StatVusLocation')
                ->findAllPerLocation();
        return $vusAll;
    }

    /**
     * Function get all vus from 30 days before till now of all locations
     * @return array  ['location', 'vus']
     */
    private function _vusLocations30j() {
        $interval = $this->interval;
        $em = $this->em;
        $vus30j = $em->getRepository('LefDataBundle:StatVusLocation')
                ->find30DaysPerLocation($interval);
        return $vus30j;
    }

}
