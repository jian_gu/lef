<?php

namespace Lef\DataBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Lef\DataBundle\Entity\BienEtre;
use Lef\DataBundle\Form\BienEtreType;

/**
 * BienEtre controller.
 *
 */
class BienEtreController extends Controller
{

    /**
     * Lists all BienEtre entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LefDataBundle:BienEtre')->findAll();

        return $this->render('LefDataBundle:BienEtre:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new BienEtre entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new BienEtre();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('bienetre_show', array('id' => $entity->getId())));
        }

        return $this->render('LefDataBundle:BienEtre:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a BienEtre entity.
    *
    * @param BienEtre $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(BienEtre $entity)
    {
        $form = $this->createForm(new BienEtreType(), $entity, array(
            'action' => $this->generateUrl('bienetre_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new BienEtre entity.
     *
     */
    public function newAction()
    {
        $entity = new BienEtre();
        $form   = $this->createCreateForm($entity);

        return $this->render('LefDataBundle:BienEtre:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a BienEtre entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:BienEtre')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BienEtre entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LefDataBundle:BienEtre:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing BienEtre entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:BienEtre')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BienEtre entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LefDataBundle:BienEtre:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a BienEtre entity.
    *
    * @param BienEtre $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(BienEtre $entity)
    {
        $form = $this->createForm(new BienEtreType(), $entity, array(
            'action' => $this->generateUrl('bienetre_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing BienEtre entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:BienEtre')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BienEtre entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('bienetre_edit', array('id' => $id)));
        }

        return $this->render('LefDataBundle:BienEtre:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a BienEtre entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LefDataBundle:BienEtre')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find BienEtre entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('bienetre'));
    }

    /**
     * Creates a form to delete a BienEtre entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bienetre_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
