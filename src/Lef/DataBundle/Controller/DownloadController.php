<?php

namespace Lef\DataBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \ZipArchive;

class DownloadController extends Controller {

    var $session = null;

    public function __construct() {
        
    }

    public function increaseMemoryAllocated() {
        ini_set("user_agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
    }

    public function ftp_sync($ftpSrcDir, $dir, $conn_id, $ftpId, $filesToGet, Request $request) {
        $target = "temp/$ftpId/zipfiles/";
        $is_synch = false;

        ftp_pasv($conn_id, true);

        if ($dir != ".") {
            if (ftp_chdir($conn_id, $dir) == false) {
                echo ("Change Dir Failed: $dir<BR>\r\n");
                return;
            }
            if (!(is_dir($dir)))
                mkdir($dir, 0777);
            chdir($dir);
        }

        putenv("TMPDIR=/temp/");  //before the ftp_nlist statement
        //
        // Get contents in the specific directory
        // Directory is not defined
        if (trim($ftpSrcDir) == '' && count($filesToGet) > 0) {
            foreach ($filesToGet as $file) {
                $pathFileArray = explode('/', $file);
                $lenPathFileArray = (integer) count($pathFileArray);
                $fileName = $pathFileArray[$lenPathFileArray - 1];
                ftp_get($conn_id, $target . $fileName, $file, $this->get_ftp_mode($file));
                $is_synch = true;
            }

            ftp_chdir($conn_id, "..");
            chdir("..");

            return $is_synch;
        } else {
            // Directory defined
            $contents = ftp_nlist($conn_id, $ftpSrcDir);

            // Remove files that we don't want from the ftp_nlist
            $unwantedValue = array('./documentation', './special', './TEST', './xsd');
            foreach ($unwantedValue as $todel) {
                if (($key = array_search($todel, $contents)) !== false) {
                    unset($contents[$key]);
                }
            }


            // Loop files stored in configuration list, and download them
            foreach ($contents as $file) {

                if ($file == '.' || $file == '..') {
                    continue;
                }

                // Si un fichier dans ftp_nlist ne contient pas le nom de dossier complèt,
                // on va l'ajouter avant le nom du fichier:
                if ($ftpSrcDir != "") {
                    if (strpos($file, $ftpSrcDir) == FALSE) {
                        $file = $ftpSrcDir . '/' . $file;
                    }
                }

                // Test  si le fichier courant fait partie des fichier à
                // télécharger sinon on n'en tient pas compte
                if (!in_array($file, $filesToGet, TRUE)) {
                    continue;
                }

                // On test si le fichier est de type zip ,xml ou json
                if (in_array($this->getExtentionFile($file), array("zip", "xml", "json"), TRUE)) {
                    if (@ftp_chdir($conn_id, $file)) {
                        ftp_chdir($conn_id, "..");
                        $this->ftp_sync($ftpSrcDir, $file, $conn_id, $ftpId, $filesToGet, $request);
                    } else {
                        $this->increaseMemoryAllocated();
                        if ($ftpSrcDir != "") {
                            $newFile = str_replace("$ftpSrcDir" . "/", "", "$file");
                            ftp_get($conn_id, $target . $newFile, $file, $this->get_ftp_mode($file));
                            $is_synch = true;
                        } else {
                            // if the directory path is empty, means file directly under root
                            ftp_get($conn_id, $target . $file, $file, $this->get_ftp_mode($file));
                            $is_synch = true;
                        }
                    }
                }
            }
            ftp_chdir($conn_id, "..");
            chdir("..");

            return $is_synch;
        }
    }

    public function syncFtpAction($ftpserver, $login, $password, $directory, $ftpId, $filesToGet, $ftpSrcDir, Request $request) {
//        $target = "temp/$ftpId/zipfiles/";
        $ftpSrcDir_explode = explode(",", $ftpSrcDir);
        foreach ($filesToGet as $value) {
            foreach ($ftpSrcDir_explode as $ftpSrcDir_implode) {
                if ($ftpSrcDir_implode == "") {
                    $filesToGet2[] = trim($ftpSrcDir_implode . $value);
                } else {
                    $filesToGet2[] = trim($ftpSrcDir_implode . '/' . $value);
                }
            }
        }

        // Prepare connection
        $conn_id = ftp_connect($ftpserver) or die("Couldn't connect to $ftp_server");
        $login_result = ftp_login($conn_id, $login, $password);
        if ((!$conn_id) || (!$login_result))
            die("FTP Connection Failed");

        // Download treatements
        foreach ($ftpSrcDir_explode as $ftpSrcDir) {
            $is_synch = $this->ftp_sync($ftpSrcDir, $directory, $conn_id, $ftpId, $filesToGet2, $request);
        }

        ftp_close($conn_id);

        return $is_synch;
    }

    public function deleteData($folder) {
        $files = glob($folder); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file))
                unlink($file); // delete file
        }
    }

    public function unzip($file, $out) {
        set_time_limit(3600);
        $zip = new ZipArchive;
        $res = $zip->open($file);
        $is_unzip = false;
        if ($res === TRUE) {
            $zip->extractTo($out);
            $zip->close();
            $is_unzip = true;
        } else {
            echo 'ftp error';
            $is_unzip = false;
        }
        return $is_unzip;
    }

    // Enregistrement de flux dans un fichier
    public function saveFulx($entity, $path) {
        // Récupèration du code des maison
        $json_HouseCodes = $this->ListOfHousesV1($entity);
        // Transforme l'objrct en JSON
        $json_HouseCodes = implode(",", $json_HouseCodes);

        set_time_limit(3600);
        $zip = new ZipArchive;
        $filename = $path . "houses_code.zip";

        if ($zip->open($filename, ZipArchive::CREATE) !== TRUE) {
            exit("Impossible d'ouvrir le fichier <$filename>\n");
        }

        // Enregistrement du json_HouseCodes dans un fichier data.json qui sera créé dans l'archive
        $zip->addFromString("houses_code.json", $json_HouseCodes);
    }

    public function getExtentionFile($file) {
        $info = new \SplFileInfo($file);
        return $info->getExtension();
    }

    public function delTree($dir) {
        $it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($it, \RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $file) {
            if ($file->getFilename() === '.' || $file->getFilename() === '..') {
                continue;
            }
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($dir);
    }

    function get_ftp_mode($file) {
        $path_parts = pathinfo($file);

        if (!isset($path_parts['extension']))
            return FTP_BINARY;
        switch (strtolower($path_parts['extension'])) {
            case 'am':case 'asp':case 'bat':case 'c':case 'cfm':case 'cgi':case 'conf':
            case 'cpp':case 'css':case 'dhtml':case 'diz':case 'h':case 'hpp':case 'htm':
            case 'html':case 'in':case 'inc':case 'js':case 'm4':case 'mak':case 'nfs':
            case 'nsi':case 'pas':case 'patch':case 'php':case 'php3':case 'php4':case 'php5':
            case 'phtml':case 'pl':case 'po':case 'py':case 'qmail':case 'sh':case 'shtml':
            case 'sql':case 'tcl':case 'tpl':case 'txt':case 'vbs':case 'xml':case 'xrc':
                return FTP_ASCII;
        }
        return FTP_BINARY;
    }

    // Leisure get list house v
    public function ListOfHousesV1($entity) {
        $row = array();

        $url = $entity->getHost();
        /* The JSON method name is part of the URL. use only lowercase characters in the URL */
        $post_data = array(
            'jsonrpc' => '2.0',
            'method' => 'ListOfHousesV1', /* Methodname is part of the URL */
            'params' => array(
                'WebpartnerCode' => $entity->getUser(),
                'WebpartnerPassword' => $entity->getPassword(),
            ),
            'id' => 211897585 /* a unique integer to identify the call for synchronisation */
        );
        $is_decoded = false;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type: application/json'));
//        curl_setopt($ch, CURLOPT_SSLVERSION, 3); /* Due to an OpenSSL issue */
        curl_setopt($ch, CURLOPT_SSLVERSION, 'CURL_SSLVERSION_TLSv1'); /* Due to error:14094410:SSL routines:SSL3_READ_BYTES:sslv3 alert handshake failure */
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);  /* Due to a wildcard certificate */
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_ENCODING, 1); /* If result is gzip then unzip */
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        ini_set('memory_limit', -1);
//        var_dump($post_data);die();
        if ($result = curl_exec($ch)) {
            if ($res = json_decode($result)) {
                $is_decoded = true;
            } else {
                echo json_last_error();
            }
        } else {
            echo curl_error($ch);
        }

        curl_close($ch);

        $json_houses_code = array();
        if ($is_decoded) {
            foreach ($res->result as $key => $value) {
                $json_houses_code[] = $value->HouseCode;
            }
        }

        return $json_houses_code;
    }

}
