<?php

namespace Lef\DataBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\Facture;

/**
 * Facture controller.
 *
 */
class FactureController extends Controller {

    /**
     * Lists all Facture entities.
     *
     */
    public function indexAction() {
        if ($this->getConnectedUser() == 'anon.') {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $annonceur = array('annonceur' => $this->getConnectedUser());

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LefDataBundle:Facture')->findBy($annonceur);

        // KNP pagenating
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $entities, $this->get('request')->query->get('page', 1)/* page number */, 10/* limit per page */
        );

        return $this->render('LefDataBundle:Facture:index.html.twig', array(
                    'entities' => $pagination,
        ));
    }

    /**
     * Finds and displays a Facture entity.
     *
     */
    public function showAction($id) {
        if ($this->getConnectedUser() == 'anon.') {
            return$this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Facture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Facture entity.');
        }

        return $this->render('LefDataBundle:Facture:show.html.twig', array(
                    'entity' => $entity,
        ));
    }

    /**
     * function get current connected user
     * @return object
     */
    protected function getConnectedUser() {
        if (!$this->get('security.context')->getToken()->getUser()) {
            throw $this->createNotFoundException('Utilisateur non connecté.');
        }
        return $this->get('security.context')->getToken()->getUser();
    }

}
