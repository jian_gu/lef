<?php

namespace Lef\DataBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lef\DataBundle\Entity\Alert;
use Lef\DataBundle\Entity\BienEtre;
use Lef\DataBundle\Entity\Equipement;
use Lef\DataBundle\Entity\Localisation;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Entity\Media;
use Lef\DataBundle\Entity\Offre;
use Lef\DataBundle\Entity\OptionEnfant;
use Lef\DataBundle\Entity\OptionHandicapees;
use Lef\DataBundle\Entity\Planning;
use Lef\DataBundle\Entity\Saison;
use Lef\DataBundle\Entity\Theme;
use Lef\DataBundle\Entity\TypeLocation;
use Lef\DataBundle\Entity\FileMap;
use Lef\DataBundle\Form\FileMapType;

/**
 * FileMap controller.
 *
 * @Route("/filemap")
 */
class FileMapController extends Controller
{

    /**
     * Lists all FileMap entities.
     *
     * @Route("/", name="filemap")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LefDataBundle:FileMap')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new FileMap entity.
     *
     * @Route("/", name="filemap_create")
     * @Method("POST")
     * @Template("LefDataBundle:FileMap:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new FileMap();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('filemap_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a FileMap entity.
     *
     * @param FileMap $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(FileMap $entity)
    {
        $form = $this->createForm(new FileMapType(), $entity, array(
            'action' => $this->generateUrl('filemap_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new FileMap entity.
     *
     * @Route("/new", name="filemap_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new FileMap();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a FileMap entity.
     *
     * @Route("/{id}", name="filemap_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:FileMap')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FileMap entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing FileMap entity.
     *
     * @Route("/{id}/edit", name="filemap_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:FileMap')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FileMap entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a FileMap entity.
     *
     * @param FileMap $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(FileMap $entity)
    {
        $form = $this->createForm(new FileMapType(), $entity, array(
            'action' => $this->generateUrl('filemap_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing FileMap entity.
     *
     * @Route("/{id}", name="filemap_update")
     * @Method("PUT")
     * @Template("LefDataBundle:FileMap:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:FileMap')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FileMap entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('filemap_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a FileMap entity.
     *
     * @Route("/{id}", name="filemap_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LefDataBundle:FileMap')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find FileMap entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('filemap'));
    }

    /**
     * Creates a form to delete a FileMap entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('filemap_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function mappingAction($file, $id, Request $request)
    {
        $file_name = $file;
        $em = $this->getDoctrine()->getManager();

        // Si aucune entité lié à notre fichier n'existe pas dans la bdd on le creer
        $partner = $em->getRepository('LefDataBundle:Partners')->find($id);
        $fileMap = $em->getRepository('LefDataBundle:FileMap')->findOneBy(array('fileName'=>$file_name, 'partnerId'=>$id));
        if (!$fileMap) {
            $fileMap = new FileMap();
            $fileMap->setPartnerId($id);
            $fileMap->setFileName($file_name);
        }
        
        // On récupère le mappage dèjà éfféctuer pour trier les champs a affciher ou pas pour la configuration
        $json = "{".substr($fileMap->getMapping(), 0, -1)."}";
        $mapping = json_decode($json, true);

        // Crétion du formulaire de l'entité FileMap
        $fileMappForm = $this->createForm(new FileMapType(), $fileMap, array(
            'action' => $this->generateUrl('partners_config_mapping', array('id'=>$id, 'file'=>$file_name )),
            'method' => 'POST',
        ));
        $fileMappForm->add('Enregistrer', 'submit', array('label' => 'Enregistrer', 'attr' => array('class'=> 'btn btn-primary btn-sm')));

        // Si le formualire estvalidé,  on sauvegarde
        if ($request->getMethod() == 'POST') {

            $fileMappForm->bind($request);       

            if ($fileMappForm->isValid()) {
                $em->flush();
                return $this->redirect($this->generateUrl('partners_config_mapping', array('id' => $id, 'file'=>$file_name)));
            }
        }

        $entities_attrs_array = array();
        // Instantiation d'une entité Location pour avoir ces champs
        $alrt = new Alert();
        $be = new BienEtre();
        $eqpmt = new Equipement();
        $lklz = new Localisation();
        $lks = new Location();
        $mdia = new Media();
        $ofr = new Offre();
        $plng = new Planning();
        $czn = new Saison();
        $tem = new Theme();
        $tplks = new TypeLocation();
        $optchld = new OptionEnfant();
        $opthdcp = new OptionHandicapees();
        array_push(
            $entities_attrs_array,
            $alrt->getAttrs(),
            $be->getAttrs(),
            $eqpmt->getAttrs(),
            $lklz->getAttrs(),
            $lks->getAttrs(),
            $mdia->getAttrs(),
            $ofr->getAttrs(),
            $plng->getAttrs(),
            $czn->getAttrs(),
            $tem->getAttrs(),
            $tplks->getAttrs(),
            $optchld->getAttrs(),
            $opthdcp->getAttrs()
            );
 
        // Augementation du temps limit d'execution et de la memoir limit pour lire les longs fichier xml
        ini_set("user_agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");

        // Tableau qui contindra les attributs xml et le nom des noeud
        $filePropertis = array();
        if (in_array($partner->getDataType(), array('XML', 'xml'))) 
        {
            // Chargement du fichier xml
            $xml = simplexml_load_file('temp/'.$id.'/data/'.$file); 

            $i = 0;
            
            if ($fileMap->getFileAttrubuts() == null) {

                $filePropertis = $this->getXmlChild(null, $xml);
                
                // On suprime les duplicata
                $filePropertis = array_unique($filePropertis);

                // On arrange les valeur par ordre croissant
                sort($filePropertis);

                $filePropertis_string = "";
                foreach ($filePropertis as $key => $value) {
                    $filePropertis_string .= $value.",";
                }
                $fileMap->setFileAttrubuts(substr($filePropertis_string, 0,-1));
                $fileMap->setIsMapped(true);
                $em->flush();
            }else{
                $filePropertis = explode(',', $fileMap->getFileAttrubuts());
            }

            // On va supprimer du tableau $filePropertis et $entities_attrs_array, 
            // les attributs et les champs déjà mappé
            foreach ($mapping as $del_val1 => $del_val2) {

                // Si $del_val1 existe dans le tableau des attributs, 
                // On récupère sa clé pour pouvoir le supprimer
                $key1 = array_search($del_val1, $filePropertis);
                if (false !== $key1) {
                    unset($filePropertis[$key1]);
                }
                // Si $del_val2 existe dans le tableau des champs, 
                // On récupère sa clé pour pouvoir le supprimer
                $key2 = array_search($del_val2, $entities_attrs_array);
                if (false !== $key2) {
                    unset($entities_attrs_array[$key2]);
                }
            }
        }
        else
        {
            $string = file_get_contents('temp/'.$id.'/data/'.$file);

            $houseCode = explode(",", $string);

            $houseCode = array_slice($houseCode, 0,5);

            $dataH = $this->dataOfHouses($partner, $houseCode);

            $dataH = json_encode($dataH);
            $dataH = json_decode($dataH, true);


            foreach ($dataH['result'] as $key => $value) {
                $this->jsonSaveAnnonce($value);
            }
        }

        return $this->render('LefDataBundle:Partners:mapping.html.twig', array(
            'mapping'               => $mapping,
            'file'                  => $file,
            'entity_id'             => $id,
            'entities_attrs_array'   => $entities_attrs_array,
            'xml_file_propertis'    => $filePropertis,
            'file_map_form'          => $fileMappForm->createView(),
        ));
    }


    public function jsonSaveAnnonce($arr)
    {
        $em = $this->getDoctrine()->getManager();

        $CostsOnSite = "";
        foreach ($arr['LanguagePackFRV4']['CostsOnSite'] as $key => $value) {
            $CostsOnSite .= $value['Description'].' : '.$value['Value']."; \n";
        }

        $_distanceOther = "Distance :\n";
        foreach ($arr['DistancesV2'] as $key => $value) {
            $_distanceOther .= $value['To'].' à '.$value['DistanceInKm']." km\n";
        }

        $newLocation = new Location();

        $newLocation->setReference($arr['HouseCode']);
        $newLocation->setTitre($arr['BasicInformationV3']['Name']);
        $newLocation->setAdresse(null);
        $newLocation->setPays($arr['BasicInformationV3']['Country']);
        $newLocation->setRegion($arr['BasicInformationV3']['Region']);
        $newLocation->setDepartement(null);
        $newLocation->setVille(($arr['LanguagePackFRV4']['City']=="")? $arr['LanguagePackFRV4']['City'] : $arr['LanguagePackFRV4']['SubCity']);
        $newLocation->setZipPostalCode($arr['BasicInformationV3']['ZipPostalCode']);
        $newLocation->setLongitude($arr['BasicInformationV3']['WGS84Longitude']);
        $newLocation->setLatitude($arr['BasicInformationV3']['WGS84Latitude']);
        $newLocation->setNombreDePersonne($arr['BasicInformationV3']['MaxNumberOfPersons']);
        $newLocation->setNombreEtoiles($arr['BasicInformationV3']['NumberOfStars']);
        $newLocation->setSurfaceHabitable($arr['BasicInformationV3']['DimensionM2']);
        $newLocation->setNombrePiece(null);
        $newLocation->setNombreSalleDeBain($arr['BasicInformationV3']['NumberOfBathrooms']); 
        $newLocation->setNombreChambre($arr['BasicInformationV3']['NumberOfBedrooms']);
        $newLocation->setAnimauxDomestiques($arr['BasicInformationV3']['NumberOfPets']);
        $newLocation->setPiscine(null);
        $newLocation->setDescriptionBreve($arr['LanguagePackFRV4']['ShortDescription']);
        $newLocation->setDescriptionDetaillee($arr['LanguagePackFRV4']['Description']);
        $newLocation->setCaracteristiques($arr['LanguagePackFRV4']['Remarks']."\n\n".$CostsOnSite);
        $newLocation->setAmenagement($arr['LanguagePackFRV4']['LayoutSimple']);
        $newLocation->setSejourAvecCouchage(null);
        $newLocation->setCoinRepas(null);
        $newLocation->setCuisine(null);
        $newLocation->setSanitaire(null);
        $newLocation->setDistanceMer(null);
        $newLocation->setDistanceLac(null);
        $newLocation->setDistanceRemontees(null);
        $newLocation->setDistanceCentreVille(null);
        $newLocation->setDistanceShopping(null);
        $newLocation->setDistanceOther($_distanceOther);
        $newLocation->setCreationDate($arr['BasicInformationV3']['CreationDate']);

        $curLocation = $em->getRepository('LefDataBundle:Location')->findOneByReference($newLocation->getReference());

        if ($curLocation != null) {
            $curLocation = $newLocation;
            $em->flush();
        }else{
            $curLocation = $newLocation;
            $em->persist($curLocation);
            $em->flush();
        }

        $curLocation = $em->getRepository('LefDataBundle:Location')->findOneByReference($newLocation->getReference());
        
        // Enregistrement des url images dans un tableau
        $medi_arr = $this->jsonSaveAnnonceMedia($arr['MediaV2'][0]['TypeContents']);

        // Ajout des planning dans la base de données
        $this->jsonSaveAnnoncePlanning($arr["AvailabilityPeriodV1"]);

        // Ajout des prix dans la base de données
        foreach ($arr["MinMaxPriceV1"] as $key => $value) {
            var_dump($value);
        }

        // die();

        // Ajout des images dans la base de données
        foreach ($medi_arr as $km_r => $vm_r) {
            $curMedia = $em->getRepository('LefDataBundle:Media')->findOneBy(array('titre'=>$vm_r[0], 'src'=>$vm_r[1]));
            if ($curMedia === null ) {
                $curMedia = new Media();
                $curMedia->setTitre($vm_r[0]);
                $curMedia->setSrc($vm_r[1]);
                $curMedia->setLocation($curLocation);
                $em->persist($curMedia);
                $em->flush();
            }
        }
    }

    public function jsonSaveAnnoncePlanning($arr)
    {
        foreach ($arr as $key => $value) 
        {

            $ArrivalDate = $value['ArrivalDate'];
            $ArrivalTimeFrom = $value['ArrivalTimeFrom'];
            $ArrivalTimeUntil = $value['ArrivalTimeUntil'];
            $DepartureDate = $value['DepartureDate'];
            $DepartureTimeFrom = $value['DepartureTimeFrom'];
            $DepartureTimeUntil = $value['DepartureTimeUntil'];

            $new_planning = new Planning();
            $new_planning->setDateArrivee($ArrivalDate);
            $new_planning->setHeureArrivee($ArrivalTimeFrom);
            $new_planning->setHeureLimitArrivee($ArrivalTimeUntil);
            $new_planning->setDateDepart($DepartureDate);
            $new_planning->setHeureDepart($DepartureTimeFrom);
            $new_planning->setHeureLimitDepart($DepartureTimeUntil);
            $new_planning->setSurDemande((strtolower($value['OnRequest'])=="yes")?true:false);
            $new_planning->setPrix($value['Price']);
            $new_planning->setPrixHtRemise($value['PriceExclDiscount']);
            $new_planning->setLocation($curLocation);
            
            $current_planning = $em->getRepository('LefDataBundle:Planning')->findOneBy(
                array(
                    "dateArrivee" => $ArrivalDate,
                    "heureArrivee" => $ArrivalTimeFrom,
                    "heureLimitArrivee" => $ArrivalTimeUntil,
                    "dateDepart" => $DepartureDate,
                    "heureDepart" => $DepartureTimeFrom,
                    "heureLimitDepart" => $DepartureTimeUntil,
                    "surDemande" => (strtolower($value['OnRequest'])=="yes")?true:false,
                    "prix" => $value['Price'],
                    "location" => $curLocation,
                    )
                );

            if ($current_planning == null) {   
                $current_planning = $new_planning;     
                $em->persist($current_planning);
                $em->flush();    
            }
        }
    }

    public function jsonSaveAnnonceMedia($arr)
    {
        $medi_arr = array();
        foreach ($arr as $key1 => $value1) {
            foreach ($value1['Versions'] as $key2 => $value2) {

                $img_titre = $value1['Tag'];
                $img_src = "http://".$value2['URL'];
                if (count($medi_arr)>=1) {
                    foreach ($medi_arr as $key3 => $value3) {
                        $can_push = false;
                        $current_url_arr = explode('/', $value3[1]);
                        $current_url_str = $value3[1];
                        $new_url = explode('/', $img_src);

                        if ($current_url_arr[4]=="web") {

                            preg_match("/_lsr_\K([^.]*)\./", $img_src, $output_array);
                            preg_match("/_lsr_\K([^.]*)\./", $value3[1], $output_array0);
                            if ($output_array[1] != $output_array0[1]) {
                                $can_push = true;
                            }
                        }else{
                            preg_match("/_lsr-b-\K([^.]*)\./", $img_src, $output_array1);
                            preg_match("/_lsr-b-\K([^.]*)\./", $value3[1], $output_array2);
                            if ((substr($output_array1[1], 0, 2)) != (substr($output_array2[1], 0, 2)) ) {
                                $can_push = true;
                            }
                        }
                    }

                    if ($can_push) {
                        array_push($medi_arr, array($img_titre, $img_src));
                    }
                }else{
                    array_push($medi_arr, array($img_titre, $img_src));
                }
            }
        }
        return $medi_arr;
    }

    // Leisure get data house v
    public function dataOfHouses($partner, $houseCode)
    {
        $rows = array();

        $url = "https://dataofHousesv1.jsonrpc-partner.net/cgi/lars/jsonrpc-partner/jsonrpc.htm";
               /* The JSON method name is part of the URL. use only lowercase characters in the URL */
        $post_data = array(
                'jsonrpc'   => '2.0', 
                'method'    => 'DataOfHousesV1', /* Methodname is part of the URL */
                'params'    => array(
                        'WebpartnerCode'        =>  $partner->getUser(), 
                        'WebpartnerPassword'    =>  $partner->getPassword(), 
                        'HouseCodes'            =>  $houseCode,
                        'Items'                 =>  array(
                            'BasicInformationV3',
                            'MediaV2',
                            'LanguagePackFRV4',
                            'MinMaxPriceV1',
                            'AvailabilityPeriodV1',
                            'PropertiesV1',
                            'CostsOnSiteV1',
                            'LayoutExtendedV2',
                            'DistancesV2',
                            'DistancesV1',
                            ),
                ),
                'id'        =>211897585 /* a unique integer to identify the call for synchronisation */
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type: application/json'));
        curl_setopt($ch, CURLOPT_SSLVERSION,3); /* Due to an OpenSSL issue */ 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);  /* Due to a wildcard certificate */
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_ENCODING, 1); /* If result is gzip then unzip */
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($result = curl_exec($ch)) {
           if ($res = json_decode($result)) {
              $rows[] = $res; 
           } else echo json_last_error();
        } else echo curl_error($ch);
        curl_close($ch);

        return $res;
    }

    public function getArrKeys($arr, $parent = "")
    {
        foreach ($arr as $key => $value) {
            if (is_array($value)) {
                // var_dump($key);
                $this->getArrKeys($value, $parent."//".$key);
                var_dump($parent."//".$key);
            }else{
                var_dump($parent."//".$key);
            }
        }
    }

    public function getProprtiesFile($array = array(), $parent = "")
    {
        $r = array();
        foreach ($array as $key => $value) {
            $kp = ($parent != "") ? $parent."//".$key : $parent.$key; 
            if (is_array($value)) {
                $r = array_merge($r, $this->getProprtiesFile($value, $kp));
            }else{
                $r[] = $kp;
            }
        }

        return $r;
    }

    private static function isEmptyDir($dir)
    {
        return (($files = @scandir($dir)) && count($files) <= 3);
    }

    public function getXmlAttr($parent=null, $xml)
    {
        // Création du tableau des resultats 
        $result = array();
        
        // Récuparation des attributs du neoud courant sous forme d'oabjet
        $atts_object = $xml->attributes();

        // Conveersion de l'objet en tableau
        $atts_array = (array) $atts_object;
        
        // On test si le tableau d'attributes n'est pas vide
        if (!empty($atts_array['@attributes'])) {
            // Parcours du tableu des attributs
            foreach ($atts_array['@attributes'] as $attrs => $val) {
                // On ajoute chaque attribut du tableau s'il n'y est pas déjà sous forme  $result[] = nom_du_noeud_//_nom_attribut  
                if(!in_array($attrs, $result)){
                    if ($parent!=null) {
                        array_push($result, $parent." // ".$xml->getName()." // ".$attrs);
                    }else{
                        array_push($result, $xml->getName()." // ".$attrs);
                    }
                    // On ajoute chaque attribut du tableau s'il n'y est pas déjà sous forme  $result[] = nom_du_noeud_//_nom_attribut   
                    if (in_array($attrs, array('name', 'id'))) {
                        if ($parent!=null) {
                            array_push($result, $parent." // ".$xml->getName()." // ".$attrs." // ".$val);
                        }else{
                            array_push($result, $xml->getName()." // ".$attrs." // ".$val);
                        }
                    }
                }
            }
        }
        return $result;
    }

    public function getXmlChild($parent=null, $xml)
    {

        // Création du tableau des resultats 
        $result = array();
        
        $i=0;
        foreach($xml->children() as $key => $child) {


            // Si le noeud courant a des enfants
            if (count($child->children())>0) 
            {   
                // var_dump($child->getName());
                // On ajoute le nom de la clé courant dans le tableau des resultats
                // Si il n'y est pas déjà
                if(!in_array($child->getName(), $result)){
                    if ($parent!=null) {
                        array_push($result, $parent." // ".$xml->getName()." // ".$child->getName());
                    }else{
                        array_push($result, $xml->getName()." // ".$child->getName());
                    }
                }

                // On relance cette fonction courant et on fusion le resultat dans le tableau des resulta
                if ($parent!=null) {
                    $result = array_merge( $result, $this->getXmlChild($parent." // ".$xml->getName(), $child));
                }else{
                    $result = array_merge( $result, $this->getXmlChild($xml->getName(), $child));
                }
            }else{
                if(!in_array($child->getName(), $result)){
                    if ($parent!=null) {
                        array_push($result, $parent." // ".$xml->getName()." // ".$child->getName());
                    }else{
                        array_push($result, $xml->getName()." // ".$child->getName());
                    }
                }
                
            }

            // On stop la boucle si on dépasse les 100 tour
            if ($i>100) {break;}
            $i++;

        }
        
        // On récupère les attributs du noeud actuel, qu'on ajoute dans le tableau des resultat
        $attributes = $this->getXmlAttr($parent, $xml);
        $result = array_merge($attributes, $result);
        sort($result);
        return $result;
    }

    public function filename_extension($filename) 
    {
        $pos = strrpos($filename, '.');
        if($pos===false) {
            return false;
        } else {
            return substr($filename, $pos+1);
        }
    }
}
