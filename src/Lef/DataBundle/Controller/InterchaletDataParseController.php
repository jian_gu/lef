<?php

namespace Lef\DataBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\RawIhAccommodation;
use Lef\DataBundle\Entity\RawIcBas;
use Lef\DataBundle\Entity\RawIcIlp;
use Lef\DataBundle\Entity\RawIcTxt;
use Lef\DataBundle\Entity\RawIcTranchePrix;
use Lef\DataBundle\Entity\RawIcLand;
use Lef\DataBundle\Entity\RawIcGeb;
use Lef\DataBundle\Entity\RawIcLandGebOrt;
use Lef\DataBundle\Entity\RawIcDispo;
use SimpleXMLElement;

class InterchaletDataParseController extends Controller {

    protected $em;

    function __construct(EntityManager $entityManager) {
        $this->em = $entityManager;
        // Disable log
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    /**
     * Parse and save raw data (xml) into database
     * A le 061014 2239 par Jian
     * @param file $xmlfile
     * @param string $entityName
     */
    public function parseSaveRawData($xmlfile, $entityName) {
        // Check which file is reading
        switch ($entityName) {
            // Accommodation
            case 'rawIcBas':
                $this->parseNSaveBas($xmlfile);
                $this->basKeepUpdatedLocation();
//                $this->removeDuplicatedLocations();
                break;
            case 'rawIcIlp':
                $this->parseNSaveIlp($xmlfile);
                break;
            case 'rawIcTxt':
                $this->parseNSaveTxt($xmlfile);
                break;
            case 'rawIcPrp':
                $this->parseNSavePrp($xmlfile);
                break;
            case 'rawIcLand':
                $this->parseNSaveLand($xmlfile);
                break;
            case 'rawIcGeb':
                $this->parseNSaveGeb($xmlfile);
                break;
            case 'rawIcOrt':
                $this->parseNSaveOrt($xmlfile);
                break;
            case 'rawIcDispo':
                // Empty the table
                $em = $this->em;
                $this->_truncateTable('raw_ic_dispo', $em);
                $this->parseNSaveDispoComplete($xmlfile[0]);
                $this->parseNSaveDispoIncremental($xmlfile[1]);
                break;

            default:
                break;
        }
    }

    /**
     * Function parse and save master data from xml to BDD
     * @param type $xmlfile
     */
    private function parseNSaveBas($xmlfile) {
        $em = $this->em;
        // Empty the table
        $this->_truncateTable('raw_ic_bas', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'accomodation') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'accomodation') {
            $node = new SimpleXMLElement($reader->readOuterXML());

            // Prepare data
            $validFrom = (string) $node->attributes()->validFrom;
            $code = (string) $node->code;
            $name = (string) $node->name;
            $latitude = (float) $node->geodata->lat;
            $longitude = (float) $node->geodata->lng;
            // Unset object
            $node = null;
            gc_collect_cycles();

            if ($latitude == (float) 0 && $longitude == (float) 0 || $name == 'Testobjekt') {
                // Jump to next node
                $reader->next('accomodation');
            } else {
                if (strlen(trim($name)) == 0) {
                    $name = '---';
                }
                // ////////////////////
                // Instantier l'entité
                $rawBas = new RawIcBas();
//            // Bind data
                $rawBas->setCode($code);
                $rawBas->setName($name);
                $rawBas->setValidFrom($validFrom);
                $rawBas->setLatitude($latitude);
                $rawBas->setLongitude($longitude);
//
//            // Persist entity
                $em->persist($rawBas);
                $em->flush();
                $em->clear();
                $rawBas = null;
                gc_collect_cycles();
////                        
//            // Jump to next node
                $reader->next('accomodation');
//                $counter ++;
//                if ($counter >= 5) {
//                    die('== 5 Bas ==');
//                }
            }
        }
        $reader->close();
    }

    /**
     * Remove duplicated locations, keep only the most updated
     */
    private function basKeepUpdatedLocation() {
        $em = $this->em;
        $count = $em->getRepository('LefDataBundle:RawIcBas')->countAll();
        $this->_increaseMemoryAllocated();
        for ($i = 1; $i <= $count; $i++) {
            $location = $em->getRepository('LefDataBundle:RawIcBas')->findOneById($i);
            if ($location != NULL) {
                $code = $location->getCode();
                $em->getRepository('LefDataBundle:RawIcBas')->keepUpdatedLocationByCode($code);
            }
            $em->clear();
        }
    }

    /**
     * Remove duplicated locations, which share same titre, lat, lng
     */
    public function removeDuplicatedLocations() {
        $em = $this->em;
        $count = $em->getRepository('LefDataBundle:RawIcBas')->countAll();
        $this->_increaseMemoryAllocated();
        for ($i = 1; $i <= $count; $i++) {
            $location = $em->getRepository('LefDataBundle:RawIcBas')->findOneById($i);
            if ($location != NULL) {
                $name = $location->getName();
                $titre = str_replace("'", "''", $name);
                $lat = $location->getLatitude();
                $lng = $location->getLongitude();
                $em->getRepository('LefDataBundle:RawIcBas')->removeDuplicatedLocations($titre, $lat, $lng);
            }
            $em->clear();
        }
    }

    /**
     * Function parse and save IMAGE data from xml to BDD
     * @param type $xmlfile
     */
    private function parseNSaveIlp($xmlfile) {
        $em = $this->em;
        // Empty the table
        $this->_truncateTable('raw_ic_ilp', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'row') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'row') {
            $node = new SimpleXMLElement($reader->readOuterXML());
            $columnsArray = (array) $node->column;

            // Prepare data
            $dataGot = $this->_parseNSaveImgGetDataFile($columnsArray);
            // if get null, means file not standard form
            if ($dataGot == NULL || (string) $columnsArray[0] != '1') {
                // Jump to next node
                $reader->next('row');
            } else {
                $locationRef = $dataGot['locationRef'];
                // Url
                $src = $dataGot['src'];
                // titre
                $titre = $dataGot['titre'];
                // Update datetime
//                $datetime = $dataGot['datetime'];
                // ////////////////////
                // Instantier l'entité
                $rawImg = new RawIcIlp();
//            // Bind data
                $rawImg->setLocationRef($locationRef);
                $rawImg->setSrc($src);
                $rawImg->setTitre($titre);
//                $rawImg->setDatetime($datetime);
//
//            // Persist entity
                $em->persist($rawImg);
                $em->flush();
                $em->clear();
                $rawImg = null;
                gc_collect_cycles();
////                        
//            // Jump to next node
                $reader->next('row');
//                $counter ++;
//                if ($counter >= 10) {
//                    die('== 10 Img ==');
//                }
            }
        }
        $reader->close();
    }

    /**
     * Function parse and save location data from xml to BDD
     * @param type $xmlfile
     */
    private function parseNSaveTxt($xmlfile) {
        $em = $this->em;
        // Empty the table
        $this->_truncateTable('raw_ic_txt', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'CompoundAttributes') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'CompoundAttributes') {
            $node = new SimpleXMLElement($reader->readOuterXML());

            $attributes = $node->attributes();
            // Get langue code
            $langCode = (string) $attributes->LangCode;
            // If not french location, move to next node
            if ($langCode && $langCode != 'fr') {
                $reader->next('CompoundAttributes');
            } else {

                // Get attributes
                $code = (string) $attributes->code;
                $codeVille = substr($code, 0, 3);
                $modificationDate = $attributes->lastChanged ? date_create_from_format('Y-m-d H:i:s.u', (string) $attributes->lastChanged) : NULL;
                $sectionsArray = (array) $node->Sections;
                $sectionArray = (array) $sectionsArray['Section'];

                // Declaire 6 sections
                $sectionBriefDescription = array();
                $sectionDetailDescription = array();
                $sectionInterior = array();
                $sectionExterieurPrivatif = array();
                $sectionExterieurs = array();
                $sectionDistances = array();


                foreach ($sectionArray as $section) {
                    $sectionId = (string) $section->attributes()->id;
                    switch ($sectionId) {
                        case 'BriefDescription':
                            $sectionBriefDescription = $section;
                            break;
                        case 'DetailDescription':
                            $sectionDetailDescription = $section;
                            break;
                        case 'Interior':
                            $sectionInterior = $section;
                            break;
                        case 'Außenbereich':
                            $sectionExterieurPrivatif = $section;
                            break;
                        case 'Plot':
                            $sectionExterieurs = $section;
                            break;
                        case 'Distances':
                            $sectionDistances = $section;
                            break;

                        default:
                            break;
                    }
                }
//            echo('<pre>');
//            var_dump($sectionBriefDescription);
//            echo('===</pre>');
//            die();
//          
//          ////////////////////////////////////
//          // vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//          ////////////////////////////////////
                // Initiate Entity
                // Declaire variables
                $titre = NULL;
                $nombreDePersonne = NULL;
                $surfaceHabitable = NULL;
                $nombrePiece = NULL;
                $nombreChambre = NULL;
                $descriptionDetaillee = NULL;
                $caracteristiques = NULL;
                $piscine = 0;
                $animauxDomestiques = 0;
                $amenagement = NULL;
                $equipementsPartner = NULL;
                $sejourAvecCouchage = NULL;
                $coinRepas = NULL;
                $cuisine = NULL;
                $sanitaire = NULL;
                $typePartner = NULL;
                $distanceOther = NULL;
                $distanceMer = NULL;
                $distanceRemontee = NULL;
                $distanceShopping = NULL;
                $distanceCentreVille = NULL;
                $distancePublicTransport = NULL;

                // Get fields data
                // Section 0
                foreach ($sectionBriefDescription->Label as $col) {
                    switch ($col->attributes()->name) {
                        case 'TypeOfProperty':
                            $titre = isset($col->p) ? (string) $this->_getOneValueFromArray((array) $col->p) : NULL;
                            break;
                        case 'AnzPax':
                            $nombreDePersonneRaw = isset($col->p) ? (string) $this->_getOneValueFromArray((array) $col->p) : NULL;
                            if ($nombreDePersonneRaw != NULL) {
                                // Get only the last number
                                preg_match("/\d$/", $nombreDePersonneRaw, $nombreDePersonneArray);
                                $nombreDePersonne = $nombreDePersonneArray[0];
                            }
                            break;
                        case 'LivingSpace':
                            $surfaceHabitableStr = isset($col->p) ? (string) $this->_getOneValueFromArray((array) $col->p) : NULL;
                            $surfaceHabitable = $this->_getNumberFromString($surfaceHabitableStr) != NULL ? $this->_getNumberFromString($surfaceHabitableStr) : $surfaceHabitableStr;
                            break;
                        case 'Rooms':
                            $nombrePieceRaw = isset($col->p) ? (string) $this->_getOneValueFromArray((array) $col->p) : NULL;
                            if ($nombrePieceRaw != NULL) {
                                // Get only the last number
                                preg_match("/\d$/", $nombrePieceRaw, $nombrePieceArray);
                                $nombrePiece = $nombrePieceArray[0];
                            }
                            break;
                        case 'BedRooms':
                            $nombreChambreRaw = isset($col->p) ? (string) $this->_getOneValueFromArray((array) $col->p) : NULL;
                            if ($nombreChambreRaw != NULL) {
                                // Get only the last number
                                preg_match("/\d$/", $nombreChambreRaw, $nombreChambreArray);
                                $nombreChambre = $nombreChambreArray[0];
                            }
                            break;
                        case 'Pool':
                            $piscine = isset($col->p) && isset($col->attributes()->isAvailable) ? (integer) $this->_trueFalseStrToNumber($col->attributes()->isAvailable) : NULL;
                            break;
                        case 'Boat':
                            $bateau = isset($col->p) && isset($col->attributes()->isAvailable) ? (integer) $this->_trueFalseStrToNumber($col->attributes()->isAvailable) : NULL;
                            $caracteristiques .= $bateau == 1 ? 'bateau disponible, ' : NULL;
                            break;
                        case 'Sauna':
                            $sauna = isset($col->p) && isset($col->attributes()->isAvailable) ? (integer) $this->_trueFalseStrToNumber($col->attributes()->isAvailable) : NULL;
                            $caracteristiques .= $sauna == 1 ? 'sauna disponible, ' : NULL;
                            break;
                        case 'Pet':
                            $animauxDomestiques = isset($col->p) && isset($col->attributes()->isAvailable) ? (integer) $this->_trueFalseStrToNumber($col->attributes()->isAvailable) : NULL;
                            break;
                        case 'Places':
                            $distanceOther = $col->attributes()->name == 'Places' && isset($col->p) ? $this->_getManyValueFromArray((array) $col->p) : NULL;
                            foreach ((array) $col->p as $distance) {
                                if (strpos($distance, 'centre') >= 0 || strpos($distance, 'Centre') >= 0) {
                                    $distanceCentreVille = $this->_getNumberFromString($distance);
                                    break;
                                }
                            }
                            break;
                        case 'Beach':
                            $distanceMerStr = $col->attributes()->name == 'Beach' && isset($col->p) ? (string) $this->_getOneValueFromArray((array) $col->p) : NULL;
                            $distanceMer = $this->_getNumberFromString($distanceMerStr);
                            $distanceOther .= ', ' . $distanceMerStr;
                            break;
                        case 'SkirunLift':
                            $distanceRemonteeStr = $col->attributes()->name == 'SkirunLift' && isset($col->p) ? (string) $this->_getOneValueFromArray((array) $col->p) : NULL;
                            $distanceRemontee = $this->_getNumberFromString($distanceRemonteeStr);
                            break;

                        default:
                            break;
                    }
                }
                // Section 1
                foreach ($sectionDetailDescription as $col) {
                    switch ($col->attributes()->name) {
                        case 'ObjectCode':
                            $descriptionDetaillee .= $col->attributes()->name == 'ObjectCode' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) . ' ' : NULL;
                            break;
                        case 'Characteristic':
                            $caracteristiques = $col->attributes()->name == 'Characteristic' && isset($col->p) ? (string) $this->_getOneValueFromArray((array) $col->p) : NULL;
                            break;
                        case 'Furnishing':
                            $amenagement = $col->attributes()->name == 'Furnishing' && isset($col->p) ? (string) $this->_getOneValueFromArray((array) $col->p) : NULL;
                            break;
                        case 'Equipment':
                            $equipementsPartner = $col->attributes()->name == 'Equipment' && isset($col->p) ? (string) $this->_getOneValueFromArray((array) $col->p) : NULL;
                            break;

                        default:
                            break;
                    }
                }
                // Section 2
                foreach ($sectionInterior as $col) {
                    switch ($col->attributes()->name) {
                        case 'LivingSpace':
                            $sejourAvecCouchage .= $col->attributes()->name == 'LivingSpace' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) . ' ' : NULL;
                            break;
                        case 'Dining':
                            $coinRepas .= $col->attributes()->name == 'Dining' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) . ' ' : NULL;
                            break;
                        case 'Kitchenette':
                            $cuisine .= $col->attributes()->name == 'Kitchenette' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) . ' ' : NULL;
                            break;
                        case 'Bedroom':
                            $sejourAvecCouchage .= $col->attributes()->name == 'Bedroom' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) . ' ' : NULL;
                            break;
                        case 'Sanitary':
                            $sanitaire .= $col->attributes()->name == 'Sanitary' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) . ' ' : NULL;
                            break;
                        case 'AdditionalInfo':
                            $caracteristiques .= $col->attributes()->name == 'AdditionalInfo' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) . ' ' : NULL;
                            break;

                        default:
                            break;
                    }
                }
                // Section 3
                foreach ($sectionExterieurPrivatif as $col) {
                    switch ($col->attributes()->name) {
                        case 'PropertySingleUse':
                            $caracteristiques .= $col->attributes()->name == 'PropertySingleUse' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) . ' ' : NULL;
                            break;

                        default:
                            break;
                    }
                }
                // Section 4
                foreach ($sectionExterieurs as $col) {
                    switch ($col->attributes()->name) {
                        case 'Plot':
                            $typePartner = $col->attributes()->name == 'Plot' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) : NULL;
                            break;
                        case 'Access':
                            $descriptionDetaillee .= $col->attributes()->name == 'Access' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) . ' ' : NULL;
                            break;
                        case 'Property':
                            $descriptionDetaillee .= $col->attributes()->name == 'Property' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) . ' ' : NULL;
                            break;

                        default:
                            break;
                    }
                }
                // Section 5
                foreach ($sectionDistances as $col) {
                    switch ($col->attributes()->name) {
                        case 'Shopping':
                            $distanceShoppingStrs = $col->attributes()->name == 'Shopping' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) : NULL;
                            $distanceShoppingStr = $col->attributes()->name == 'Shopping' && isset($col->p) ? (string) $this->_getOneValueFromArray((array) $col->p) : NULL;
                            $distanceShopping = $this->_getNumberFromString($distanceShoppingStr);
                            $distanceOther .= ', ' . $distanceShoppingStr;
                            break;
                        case 'Journey':
                            $distancePublicTransportStr = $col->attributes()->name == 'Journey' && isset($col->p) ? (string) $this->_getOneValueFromArray((array) $col->p) . ' ' : NULL;
                            $distancePublicTransport = $this->_getNumberFromString($distancePublicTransportStr);
                            $distancePublicTransportStrs = $col->attributes()->name == 'Journey' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) . ' ' : NULL;
                            $distanceOther .= ', ' . $distancePublicTransportStrs;
                            break;
                        case 'Property':
                            $descriptionDetaillee .= $col->attributes()->name == 'Property' && isset($col->p) ? (string) $this->_getManyValueFromArray((array) $col->p) . ' ' : NULL;
                            break;

                        default:
                            break;
                    }
                }
//            echo('<pre>');
//            var_dump($distancePublicTransport);
//            echo('===</pre>');
                // ////////////////////
                // Instantier l'entité
                $rawText = new RawIcTxt();
                // Bind data
                $rawText->setCode($code);
                $rawText->setCodeVille($codeVille);
                $rawText->setLangCode($langCode);
                $rawText->setTitre($titre);
                $rawText->setModificationDate($modificationDate);
                $rawText->setNombreDePersonne($nombreDePersonne);
                $rawText->setNombreChambre($nombreChambre);
                $rawText->setDescriptionDetaillee($descriptionDetaillee);
                $rawText->setCaracteristiques($caracteristiques);
                $rawText->setPiscine($piscine);
                $rawText->setAnimauxDomestiques($animauxDomestiques);
                $rawText->setEquipementsPartner($equipementsPartner);
                $rawText->setSejourAvecCouchage($sejourAvecCouchage);
                $rawText->setCoinRepas($coinRepas);
                $rawText->setCuisine($cuisine);
                $rawText->setSanitaire($sanitaire);
                $rawText->setTypePartner($typePartner);
                $rawText->setDistanceOther($distanceOther);
                $rawText->setDistanceMer($distanceMer);
                $rawText->setDistanceRemontee($distanceRemontee);
                $rawText->setDistanceCentreVille($distanceCentreVille);
                $rawText->setDistancePublicTransport($distancePublicTransport);
                $rawText->setSurfaceHabitable($surfaceHabitable);
                $rawText->setNombrePiece($nombrePiece);
                $rawText->setAmenagement($amenagement);
                $rawText->setDistanceShopping($distanceShopping);

//                echo('<pre>');
//                var_dump($rawText);
//                echo('</pre>');
//                die();
//
//            // Persist entity
                $em->persist($rawText);
                $em->flush();
                $em->clear();
                $rawText = null;
                gc_collect_cycles();
////                        
//            // Jump to next node
                $reader->next('CompoundAttributes');
//                $counter ++;
//                if ($counter >= 5) {
//                    die('== 5 Txt ==');
//                }
            }
        }
        $reader->close();
    }

    /**
     * Function parse and save price data into DB
     * @param type $xmlfile
     */
    private function parseNSavePrp($xmlfile) {
        $em = $this->em;
        // Empty the table
        $this->_truncateTable('raw_ic_tranche_prix', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'row') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'row') {
            $node = new SimpleXMLElement($reader->readOuterXML());
            $columnsArray = (array) $node->column;

            // If in not validate data, jump to next node
            if ((string) $columnsArray[0] != '1') {
                $reader->next('row');
            }
            // Prepare data
            $nowStr = date('Y-m-d');
            $dataGot = $this->_parseNSavePrpGetDataFile($columnsArray);
            $code = $dataGot['code'];
            $startDate = $dataGot['startDate'];
            $endDate = $dataGot['endDate'];
            $endDateStr = $dataGot['endDateStr'];
            $sellingPrice = $dataGot['sellingPrice'];
            $pricePerTW = $dataGot['pricePerTW'];
            $pricePerOP = $dataGot['pricePerOP'];
            $minStay = $dataGot['minStay'];
            $dayArrival = $dataGot['dayArrival'];

            if ($endDateStr < $nowStr) {
                $reader->next('row');
            } else {

                // ////////////////////
                // Instantier l'entité
                $rawTP = new RawIcTranchePrix();
//            // Bind data
                $rawTP->setLocationRef($code);
                $rawTP->setStartDate($startDate);
                $rawTP->setEndDate($endDate);
                $rawTP->setRentalPrice($sellingPrice);
                $rawTP->setPricePerTW($pricePerTW);
                $rawTP->setPricePerOP($pricePerOP);
                $rawTP->setMinStay($minStay);
                $rawTP->setDayArrival($dayArrival);
//
//            // Persist entity
                $em->persist($rawTP);
                $em->flush();
                $em->clear();
                $rawTP = NULL;
                gc_collect_cycles();
////                        
//            // Jump to next node
                $reader->next('row');
//                $counter ++;
//                if ($counter >= 10) {
//                    die('== 10 Preis ==');
//                }
            }
        }
        $reader->close();
    }

    /**
     * Function parse and save country data into DB
     * @param type $xmlfile
     */
    private function parseNSaveLand($xmlfile) {
        $em = $this->em;
        // Empty the table
        $this->_truncateTable('raw_ic_land', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'row') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'row') {
            $node = new SimpleXMLElement($reader->readOuterXML());
            $columnsArray = (array) $node->column;

            // Prepare data
//            $dataGot = $this->_parseNSaveLandGetDataFile($columnsArray);
            $codePays = (string) $columnsArray[5];
            $namePays = (string) $columnsArray[6];
            $isoPays = (string) $columnsArray[7];

            // ////////////////////
            // Instantier l'entité
            $rawLand = new RawIcLand();
//            // Bind data
            $rawLand->setCodePays($codePays);
            $rawLand->setNamePays($namePays);
            $rawLand->setIsoPays($isoPays);
//
//            // Persist entity
            $em->persist($rawLand);
            $em->flush();
            $em->clear();
            $rawLand = NULL;
            gc_collect_cycles();
////                      
//            // Jump to next node
            $reader->next('row');
//            $counter ++;
//                if ($counter >= 10) {
//                    die('== 10 ==');
//                }
        }
        $reader->close();
    }

    /**
     * Function parse and save region data into DB
     * @param type $xmlfile
     */
    private function parseNSaveGeb($xmlfile) {
        $em = $this->em;
        // Empty the table
        $this->_truncateTable('raw_ic_geb', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'row') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'row') {
            $node = new SimpleXMLElement($reader->readOuterXML());
            $columnsArray = (array) $node->column;

            // Prepare data
            $codePays = (string) $columnsArray[5];
            $codeRegion = (string) $columnsArray[6];
            $nameRegion = (string) $columnsArray[7];

            // ////////////////////
            // Instantier l'entité
            $rawRegion = new RawIcGeb();
//            // Bind data
            $rawRegion->setCodePays($codePays);
            $rawRegion->setCodeRegion($codeRegion);
            $rawRegion->setNameRegion($nameRegion);
//
//            // Persist entity
            $em->persist($rawRegion);
            $em->flush();
            $em->clear();
            $rawRegion = NULL;
            gc_collect_cycles();
////                     
//            // Jump to next node
            $reader->next('row');
//            $counter ++;
//                if ($counter >= 10) {
//                    die('== 10 ==');
//                }
        }
        $reader->close();
    }

    /**
     * Function parse and save city data into DB
     * @param type $xmlfile
     */
    private function parseNSaveOrt($xmlfile) {
        $em = $this->em;
        // Empty the table
        $this->_truncateTable('raw_ic_land_geb_ort', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'row') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'row') {
            $node = new SimpleXMLElement($reader->readOuterXML());
            $columnsArray = (array) $node->column;

            // Prepare data
            $codePays = (string) $columnsArray[5];
            $codeRegion = (string) $columnsArray[6];
            $codeVille = (string) $columnsArray[7];
            $nameVille = (string) $columnsArray[8];

            $regionEntity = $em->getRepository('LefDataBundle:RawIcGeb')->findOneBy(array('codePays' => $codePays, 'codeRegion' => $codeRegion));
            $nameRegion = $regionEntity != NULL ? $regionEntity->getNameRegion() : NULL;
            $paysEntity = $em->getRepository('LefDataBundle:RawIcLand')->findOneBy(array('codePays' => $codePays));
            $namePays = $paysEntity != NULL ? $paysEntity->getNamePays() : NULL;

            // ////////////////////
            // Instantier l'entité
            $rawVilleFull = new RawIcLandGebOrt();
//            // Bind data
            $rawVilleFull->setCodePays($codePays);
            $rawVilleFull->setNamePays($namePays);
            $rawVilleFull->setCodeRegion($codeRegion);
            $rawVilleFull->setNameRegion($nameRegion);
            $rawVilleFull->setCodeVille($codeVille);
            $rawVilleFull->setNameVille($nameVille);
//
//            // Persist entity
            $em->persist($rawVilleFull);
            $em->flush();
            $em->clear();
            $rawVilleFull = NULL;
            gc_collect_cycles();
////                       
//            // Jump to next node
            $reader->next('row');
//            $counter ++;
//            if ($counter >= 10) {
//                die('== Land Gebit 10 Ort ==');
//            }
        }
        $reader->close();
    }

    /**
     * Function parse and save date arrival / departure data into DB
     * @param type $xmlfile
     */
    private function parseNSaveDispoComplete($xmlfile) {
        $em = $this->em;
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'row') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'row') {
            $node = new SimpleXMLElement($reader->readOuterXML());
            $columnsArray = (array) $node->column;

            // Prepare data
            $isValid = (integer) $columnsArray[0];
            // If node is not valide, jump to next
            if ($isValid !== 1) {
                // Jump to next node
                $reader->next('row');
            }
            $codeVille = (string) $columnsArray[5];
            $codeHotel = (string) $columnsArray[6];
            $firstDate = (string) $columnsArray[7];
            $availability = (string) $columnsArray[8];

            $locationRef = $codeVille . $codeHotel;
            $firstDateObj = date_create_from_format('Y-m-d', $firstDate);

            // ////////////////////
            // Instantier l'entité
            $rawDispo = new RawIcDispo();
//            // Bind data
            $rawDispo->setLocationRef($locationRef);
            $rawDispo->setFirstDate($firstDateObj);
            $rawDispo->setAvailability($availability);
//
//            // Persist entity
            $em->persist($rawDispo);
            $em->flush();
            $em->clear();
            $rawDispo = NULL;
            gc_collect_cycles();
////                      
//            // Jump to next node
            $reader->next('row');
//            $counter ++;
//            if ($counter >= 10) {
//                die('== 10 Dispo complete ==');
//            }
        }
        $reader->close();
    }

    /**
     * Function parse and save vacance availability data into DB
     * @param type $xmlfile
     */
    private function parseNSaveDispoIncremental($xmlfile) {
        $em = $this->em;
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'row') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'row') {
            $node = new SimpleXMLElement($reader->readOuterXML());
            $columnsArray = (array) $node->column;

            // Prepare data
            $isValid = (integer) $columnsArray[0];
            // If node is not valide, jump to next
            if ($isValid !== 1) {
                // Jump to next node
                $reader->next('row');
            }
            $codeVille = (string) $columnsArray[5];
            $codeHotel = (string) $columnsArray[6];
            $firstDate = (string) $columnsArray[7];
            $availability = (string) $columnsArray[8];

            $locationRef = $codeVille . $codeHotel;
            $firstDateObj = date_create_from_format('Y-m-d', $firstDate);

            // ////////////////////
            // Find if a record already exists in the table
            $dispoExist = $em->getRepository('LefDataBundle:RawIcDispo')->findOneBy(array('locationRef' => $locationRef));
            // Instantier l'entité if no rerocd found
            $rawDispo = count($dispoExist) > 0 ? $dispoExist : new RawIcDispo();
//            // Bind data
            $rawDispo->setLocationRef($locationRef);
            $rawDispo->setFirstDate($firstDateObj);
            $rawDispo->setAvailability($availability);
//
//            // Persist entity if no recored found in table
            count($dispoExist) > 0 ? : $em->persist($rawDispo);
            $em->flush();
            $em->clear();
            $rawDispo = NULL;
            gc_collect_cycles();
////                      
//            // Jump to next node
            $reader->next('row');
//            $counter ++;
//            if ($counter >= 10) {
//                die('== 10 Dispo incrément==');
//            }
        }
        $reader->close();
    }

//    private function _parseNSaveTxtTreatBriefDescription($sectionBriefDescription) {
//        
//    }
//
//    private function _parseNSaveTxtTreatDetailDescription($sectionDetailDescription) {
//        
//    }
//
//    private function _parseNSaveTxtTreatInterior($sectionInterior) {
//        
//    }
//
//    private function _parseNSaveTxtTreatExterieurPrivatif($sectionExterieurPrivatif) {
//        
//    }
//
//    private function _parseNSaveTxtTreatExterieurs($sectionExterieurs) {
//        
//    }
//
//    private function _parseNSaveTxtTreatDistances($sectionDistances) {
//        
//    }

    /**
     * Get one value from array or string
     * @param type $input
     * @return type
     */
    private function _getOneValueFromArray($input) {
        $value = NULL;
        if (is_array($input)) {
            $value = (string) current($input);
        } else if ($input != NULL) {
            $value = (string) $input;
        }
        return $value;
    }

    /**
     * Get many values from array or string
     * @param type $input
     * @return type
     */
    private function _getManyValueFromArray($input) {
        $value = NULL;
        // Is array
        if (is_array($input)) {
            foreach ($input as $node) {
                $value .= (string) $node . ', ';
            }
            $value = substr($value, 0, -2);
        } else if ($input != NULL) {
            // is string
            $value = (string) $input;
        }
        return $value;
    }

    /**
     * Return number instead of bool str
     * @param type $bool
     * @return type
     */
    private function _trueFalseStrToNumber($bool) {
        return $bool == 'true' ? 1 : 0;
    }

    /**
     * Return the number inside a string
     * @param type $string
     * @return type
     */
    private function _getNumberFromString($string) {
        $result = NULL;
        // + m decimal
        if (preg_match('/\s(\d+[,]\d+)+\s[m]/', $string, $resultArray)) {
            $result = strpos($resultArray[1], ',') >= 0 ? (integer) str_replace(',', '.', $resultArray[1]) : (integer) $resultArray[1];
        }
        // + m integer
        if (preg_match('/\s(\d+)+\s[m]/', $string, $resultArray)) {
            $result = (integer) $resultArray[1];
        }
        // + km decimal
        if (preg_match('/\s(\d+[,]\d+)\s[k][m]/', $string, $resultArray)) {
            $ingerFy = strpos($resultArray[1], ',') >= 0 ? (integer) str_replace(',', '.', $resultArray[1]) : (integer) $resultArray[1];
            $result = $ingerFy * 1000;
        }
        // + km integer
        if (preg_match('/\s(\d+)\s[k][m]/', $string, $resultArray)) {
            $result = (integer) $resultArray[1] * 1000;
        }
        // + m² decimal
        if (preg_match('/\s(\d+[,]\d+)\s[m²]/', $string, $resultArray)) {
            $result = strpos($resultArray[1], ',') >= 0 ? (integer) str_replace(',', '.', $resultArray[1]) : (integer) $resultArray[1];
        }
        // + m² integer
        if (preg_match('/\s(\d+)\s[m²]/', $string, $resultArray)) {
            $result = (integer) $resultArray[1];
        }
        return $result;
    }

    /**
     * 
     * Get data from image file, 
     * part of function: parseNSaveImg
     * @param type $columnsArray
     * @return null or array
     */
    private function _parseNSaveImgGetDataFile($columnsArray) {
        /* Construction example
          <column name="FIRMA">1</column>
          <column name="OBJEKT_GEBIET">ELB</column>
          <column name="OBJEKT_ORT">CLV</column>
          <column name="OBJEKT_NUMMER">371</column>
          <column name="BILD">c467174a63.jpg</column>
          <column name="BILDART"/>
          <column name="BILD_BEISPIEL">Strand</column>
          <column name="BILD_BEMERKUNG">Pareti</column>
          <column name="BILD_POSITION">1011030090</column>
          <column name="BILD_VORSCHAU">null</column>
          <column name="DATUM">2014-06-03 12:46:33.747237</column>
         */
        $locationRef = $columnsArray[2] . $columnsArray[3];
        $imageType = $columnsArray[5];
        $imageName = $columnsArray[4];
        $validImageName = strchr($imageName, '.') < 0 ? false : true;
        // file name not standard, we return null
        if ($validImageName == false || $imageType == '' || !in_array($imageType, ['O', 'O1', 'G', 'G1', 'S', 'S1'])) {
            return NULL;
        }

        // Object image
        if (in_array($imageType, ['O', 'O1'])) {
            preg_match("/\S[^a-z]+(\w+)[.a-z]+/", $imageName, $explodedImageName);
            $folderName = $explodedImageName[1];
            // Save only medium and large images
            if ($folderName == 'a64' || $folderName == 'a280') {
                return NULL;
            }
            $src = "http://images.interchalet.de/objimg/$folderName/$imageName";
        } elseif (in_array($imageType, ['G', 'G1'])) {
            // Ground view image
            $src = "http://images.interchalet.de/floor_plan/$imageName";
        } elseif (in_array($imageType, ['S', 'S1'])) {
            // Ski maps
            $src = "http://images.interchalet.de/ski_maps/$imageName";
        }
        // Tire
        $titre = is_array($columnsArray[6]) || $columnsArray[6] == 'null' ? '--' : $columnsArray[6];
        // Update datetime
//            $datetimeStr = (string) $columnsArray[10];
//            $datetime = date_create_from_format('Y-m-d H:i:s.u', $datetimeStr);

        $returnArray = array('locationRef' => $locationRef, 'src' => $src,
            'titre' => $titre);
        return $returnArray;
    }

    /**
     * Get data from price file, 
     * part of function: parseNSavePrp
     * @param type $columnsArray
     * @return type
     */
    private function _parseNSavePrpGetDataFile($columnsArray) {
        $code = (string) $columnsArray[5] . $columnsArray[6];
        $startDate = date_create_from_format('Y-m-d', (string) $columnsArray[8]);
        $endDate = date_create_from_format('Y-m-d', (string) $columnsArray[9]);
        $endDateStr = (string) $columnsArray[9];
        $pricePerTW = (string) $columnsArray[10];
        $pricePerOP = (string) $columnsArray[12];
        $sellingPrice = (float) $columnsArray[15];
        $minStay = (integer) $columnsArray[17];
        $dayArrival = (string) $columnsArray[18];

        $returnArray = array('code' => $code, 'startDate' => $startDate,
            'endDate' => $endDate, 'endDateStr' => $endDateStr,
            'pricePerTW' => $pricePerTW, 'pricePerOP' => $pricePerOP,
            'minStay' => $minStay, 'dayArrival' => $dayArrival,
            'sellingPrice' => $sellingPrice);
        return $returnArray;
    }

    /**
     * Truncate a table
     * @param type $tableName
     * @param type $em
     */
    private function _truncateTable($tableName, $em) {

        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();

        $connection->executeUpdate($platform->getTruncateTableSQL($tableName, true /* whether to cascade */));
    }

    /**
     * Increase memory, incase of crash
     */
    private function _increaseMemoryAllocated() {
        ini_set("user_agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
        // for soap call, socket response
//        ini_set('default_socket_timeout', 600);
    }

}
