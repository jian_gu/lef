<?php

namespace Lef\DataBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\RawIhAccommodation;
use Lef\DataBundle\Entity\RawIcImg;
use SimpleXMLElement;

class InterchaletDataParseController extends Controller {

    /**
     * Parse and save raw data (xml) into database
     * A le 061014 2239 par Jian
     * @param file $xmlfile
     * @param object $em
     * @param string $entityName
     */
    public function parseSaveRawData($xmlfile, $em, $entityName) {
        // Check which file is reading
        switch ($entityName) {
            // Accommodation
            case 'rawIcImg':
//                $this->parseNSaveImg($xmlfile, $em);
                break;
            case 'rawIcTxt':
                $this->parseNSaveTxt($xmlfile, $em);
                break;
            case 'rawIcPrp':
                break;
            case 'rawIcLand':
                break;
            case 'rawIcGeb':
                break;
            case 'rawIcOrt':
                break;

            default:
                break;
        }
    }

    /**
     * Function parse and save image data from xml to BDD
     * @param type $xmlfile
     * @param type $em
     */
    private function parseNSaveImg($xmlfile, $em) {
        // Empty the table
        $this->_truncateTable('raw_ic_img', $em);
        // Set counter to 0
        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);

        // Start reading
        while ($reader->read() && $reader->name != 'row') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'row') {
            $node = new SimpleXMLElement($reader->readOuterXML());
            $columnsArray = (array) $node->column;

            // Prepare data
            $dataGot = $this->_parseNSaveImgGetDataFile($columnsArray);
            $locationRef = $dataGot['locationRef'];
            $src = $dataGot['src'];
            $titre = $dataGot['titre'];
            $datetime = $dataGot['datetime'];

            // ////////////////////
            // Instantier l'entité
            $rawImg = new RawIcImg();
//            // Bind data
            $rawImg->setLocationRef($locationRef);
            $rawImg->setSrc($src);
            $rawImg->setTitre($titre);
            $rawImg->setDatetime($datetime);
//
//            // Persist entity
            $em->persist($rawImg);
            $em->flush();
            $em->clear();
////                        
            $counter ++;
//            // Jump to next node
            $reader->next('row');
//            if ($counter >= 1) {
//                die('== 1 ==');
//            }
        }
        $reader->close();
    }

    private function parseNSaveTxt($xmlfile, $em) {
        // Empty the table
//        $this->_truncateTable('raw_ic_txt', $em);
        // Set counter to 0
        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);

        // Start reading
        while ($reader->read() && $reader->name != 'CompoundAttributes') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'CompoundAttributes') {
            $node = new SimpleXMLElement($reader->readOuterXML());


            $attributes = $node->attributes();
            $sections = (array) $node->Sections;
            $section = $sections['Section'];
            // node BriefDescription
            $section0 = (array) $section[0];
            $section1 = (array) $section[1];
            $section2 = (array) $section[2];
            $section3 = (array) $section[3];
            $section4 = (array) $section[4];
            $section5 = (array) $section[5];
            // Get langue code
            $langCode = (string) $attributes->LangCode;
            // If not french location, move to next node
            if ($langCode && $langCode != 'fr') {
                $reader->next(CompoundAttributes);
            }
            // Get attributes
            $code = (string) $attributes->code;
            $modificationData = $attributes->lastChanged ? date_create_from_format('Y-m-d H:i:s.u', (string) $attributes->lastChanged) : NULL;
            // Get fields data
            // Section 0
            $titre = (string) $section0['Label'][0]->attributes()->name == 'TypeOfProperty' && isset($section0['Label'][0]->p) ? $this->_getOneValueFromArray((array) $section0['Label'][0]->p) : NULL;
            $nombreDePersonne = (string) $section0['Label'][1]->attributes()->name == 'AnzPax' && isset($section0['Label'][1]->p) ? $this->_getOneValueFromArray((array) $section0['Label'][1]->p) : NULL;
            $surfaceHabitable = (string) $section0['Label'][2]->attributes()->name == 'LivingSpace' && isset($section0['Label'][2]->p) ? $this->_getOneValueFromArray((array) $section0['Label'][2]->p) : NULL;
            $nombrePiece = (string) $section0['Label'][3]->attributes()->name == 'Rooms' && isset($section0['Label'][3]->p) ? $this->_getOneValueFromArray((array) $section0['Label'][3]->p) : NULL;
            $nombreChambre = (string) $section0['Label'][4]->attributes()->name == 'BedRooms' && isset($section0['Label'][4]->p) ? $this->_getOneValueFromArray((array) $section0['Label'][4]->p) : NULL;
            $piscine = (string) $section0['Label'][5]->attributes()->name == 'Pool' && isset($section0['Label'][5]->attributes()->isAvailable) ? $this->_trueFalseStrToNumber($section0['Label'][5]->attributes()->isAvailable) : NULL;
            $bateau = (string) $section0['Label'][6]->attributes()->name == 'Boat' && isset($section0['Label'][6]->attributes()->isAvailable) ? $this->_trueFalseStrToNumber($section0['Label'][6]->attributes()->isAvailable) : NULL;
            $sauna = (string) $section0['Label'][7]->attributes()->name == 'Sauna' && isset($section0['Label'][7]->attributes()->isAvailable) ? $this->_trueFalseStrToNumber($section0['Label'][7]->attributes()->isAvailable) : NULL;
            $animauxDomestiques = (string) $section0['Label'][8]->attributes()->name == 'Pet' && isset($section0['Label'][8]->attributes()->isAvailable) ? $this->_trueFalseStrToNumber($section0['Label'][8]->attributes()->isAvailable) : NULL;
            $distanceOther1 = (string) $section0['Label'][9]->attributes()->name == 'Places' && isset($section0['Label'][9]->p) ? $this->_getManyValueFromArray((array) $section0['Label'][9]->p) : NULL;
            $distanceOther2 = (string) $section0['Label'][10]->attributes()->name == 'Beach' && isset($section0['Label'][10]->p) ? $this->_getManyValueFromArray((array) $section0['Label'][10]->p) : NULL;
            $distanceOther3 = (string) $section0['Label'][11]->attributes()->name == 'SkirunLift' && isset($section0['Label'][11]->p) ? $this->_getManyValueFromArray((array) $section0['Label'][11]->p) : NULL;
            $distanceMerStr = (string) $section0['Label'][10]->attributes()->name == 'Beach' && isset($section0['Label'][10]->p) ? $this->_getOneValueFromArray((array) $section0['Label'][10]->p) : NULL;
            $distanceMer = $this->_getNumberFromString($distanceMerStr);
            $distanceRemonteeStr = (string) $section0['Label'][11]->attributes()->name == 'SkirunLift' && isset($section0['Label'][11]->p) ? $this->_getOneValueFromArray((array) $section0['Label'][11]->p) : NULL;
            $distanceRemontee = $this->_getNumberFromString($distanceRemonteeStr);
            // Section 1
            // Section 2
            // Section 3
            // Section 4
            // Section 5
            echo('<pre>');
            var_dump($node->Sections);
            echo('</pre>');

            // Prepare data
//            $dataGot = $this->_parseNSaveImgGetDataFile($columnsArray);
//            $locationRef = $dataGot['locationRef'];
//            $src = $dataGot['src'];
//            $titre = $dataGot['titre'];
//            $datetime = $dataGot['datetime'];
            // ////////////////////
            // Instantier l'entité
//            $rawImg = new RawIcImg();
//            // Bind data
//            $rawImg->setLocationRef($locationRef);
//            $rawImg->setSrc($src);
//            $rawImg->setTitre($titre);
//            $rawImg->setDatetime($datetime);
//
//            // Persist entity
//            $em->persist($rawImg);
//            $em->flush();
//            $em->clear();
////                        
            $counter ++;
//            // Jump to next node
            $reader->next('CompoundAttributes');
            if ($counter >= 1) {
                die('== 1 ==');
            }
        }
        $reader->close();
    }

    /**
     * Get one value from array or string
     * @param type $input
     * @return type
     */
    private function _getOneValueFromArray($input) {
        $value = NULL;
        if (is_array($input)) {
            $value = (string) current($input);
        } else if ($input != NULL) {
            $value = (string) $input;
        }
        return $value;
    }

    /**
     * Get many values from array or string
     * @param type $input
     * @return type
     */
    private function _getManyValueFromArray($input) {
        $value = NULL;
        if (is_array($input)) {
            foreach ($input as $node) {
                $value .= (string) $node . ', ';
            }
            $value = substr($value, 0, -2);
        } else if ($input != NULL) {
            $value = (string) $input;
        }
        return $value;
    }

    /**
     * Return number instead of bool str
     * @param type $bool
     * @return type
     */
    private function _trueFalseStrToNumber($bool) {
        return $bool == 'true' ? 1 : 0;
    }

    /**
     * Return the number inside a string
     * @param type $string
     * @return type
     */
    private function _getNumberFromString($string) {
        $result = NULL;
        if (preg_match('/(\d+)+\s[m]/', $string, $resultArray)) {
            $result = $resultArray[1];
        }
        if (preg_match('/(\d+)\s[k][m]/', $string, $resultArray)) {
            $result = $resultArray[1];
        }
        return $result;
    }

    /**
     * Get data from image file, 
     * part of function: parseNSaveImg
     * @param type $columnsArray
     * @return type
     */
    private function _parseNSaveImgGetDataFile($columnsArray) {
        $locationRef = $columnsArray[2] . $columnsArray[3];
        $imageName = $columnsArray[5];
        $folderName = substr($imageName, 7, 3);
        $src = "http://images.interchalet.de/objimg/$folderName/$imageName";
        $titre = is_array($columnsArray[8]) ? '--' : $columnsArray[8];
        $datetimeStr = (string) $columnsArray[11];
        $datetime = date_create_from_format('Y-m-d H:i:s.u', $datetimeStr);

        $returnArray = array('locationRef' => $locationRef, 'src' => $src,
            'titre' => $titre, 'datetime' => $datetime);
        return $returnArray;
    }

    /**
     * Truncate a table
     * @param type $tableName
     * @param type $em
     */
    private function _truncateTable($tableName, $em) {

        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();

        $connection->executeUpdate($platform->getTruncateTableSQL($tableName, true /* whether to cascade */));
    }

    /**
     * Increase memory, incase of crash
     */
    private function _increaseMemoryAllocated() {
        ini_set("user_agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
        // for soap call, socket response
//        ini_set('default_socket_timeout', 600);
    }

}
