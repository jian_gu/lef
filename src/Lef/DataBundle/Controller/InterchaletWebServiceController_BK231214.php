<?php

namespace Lef\DataBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Entity\MediaPartner;
use Doctrine\ORM\EntityManager;

class InterchaletWebServiceController extends Controller {

    protected $em;

    public function __construct(EntityManager $entityManager) {
        $this->em = $entityManager;
    }

    /**
     * Update.
     * Call all update functions
     */
    public function majMegaBase() {
        // Increase memory allocation
        $this->_increaseMemoryAllocated();
        // update location, together with updating media
        $this->saveLocation();
    }

    public function saveLocation() {
        $em = $this->em;
        // Get total location number
//        $countLocation = $em->getRepository('LefDataBundle:RawIcTxt')->getCountAll();
        $countLocation = $em->getRepository('LefDataBundle:RawIcBas')->countAll();
        // Loop location saved in iobjtxt
        for ($i = 1; $i <= $countLocation; $i++) {
            // Increase memory allocation
            $this->_increaseMemoryAllocated();
            $em = $this->em;
//            $location = $em->getRepository('LefDataBundle:RawIcTxt')->findOneById($i);
            $locationBas = $em->getRepository('LefDataBundle:RawIcBas')->findOneById($i);
            if ($locationBas == NULL) {
                continue;
            }


            // Bas
            $code = $locationBas->getCode();
            $latitude = $locationBas->getLatitude() != 0 ? $locationBas->getLatitude() : NULL;
            $longitude = $locationBas->getLongitude() != 0 ? $locationBas->getLongitude() : NULL;
            $modificationDate = $this->_stringToDatetime($locationBas->getValidFrom());

            // Get descriptions location txt
            $location = $em->getRepository('LefDataBundle:RawIcTxt')->findOneByCode($code);
            // Txt
            $codeVille = $location->getCodeVille();
            $prvArray = $this->_saveLocationGetPaysRegionVille($codeVille);
            $namePays = $prvArray['namePays'];
            $nameRegion = $prvArray['nameRegion'];
            $nameVille = $prvArray['nameVille'];
            $titre = $location->getTitre();
//            $modificationDate = $location->getModificationDate();
            $nombreDePersonne = $location->getNombreDePersonne();
            $nombreChambre = $location->getNombreChambre();
            $descriptionDetaillee = $location->getDescriptionDetaillee();
            $caracteristiques = $location->getCaracteristiques();
            $piscine = $location->getPiscine();
            $animauxDomestiques = $location->getAnimauxDomestiques();
            $equipementsPartner = $location->getEquipementsPartner();
            $sejourAvecCouchage = $location->getSejourAvecCouchage();
            $coinRepas = $location->getCoinRepas();
            $cuisine = $location->getCuisine();
            $sanitaire = $location->getSanitaire();
            $typePartner = $location->getTypePartner();
            $distanceOther = $location->getDistanceOther();
            $distanceMer = $location->getDistanceMer();
            $distanceCentreVille = $location->getDistanceCentreVille();
            $distanceRemontee = $location->getDistanceRemontee();
            $distancePublicTransport = $location->getDistancePublicTransport();
            $surfaceHabitable = $location->getSurfaceHabitable();
            $nombrePiece = $location->getNombrePiece();
            $amenagement = $location->getAmenagement();
            $rentalPrice = $this->_saveLocationGetRentalPrice($code);


            // =====================
            // =====================
            // Save images
            $this->_saveLocationSaveImage($code);
            // Location exists indicator
            $locationExists = $em->getRepository('LefDataBundle:Location')
                    ->findOneByReference($code);
            // If not exists, insert new
            if ($locationExists == NULL) {
                // Instantier location
                $location = new Location();
                // Bind data
                $location->setReference($code);
                $location->setTitre($titre);
                $location->setPays($namePays);
                $location->setRegion($nameRegion);
                $location->setVille($nameVille);
                $location->setModificationDate($modificationDate);
                $location->setNombreDePersonne($nombreDePersonne);
                $location->setNombreChambre($nombreChambre);
                $location->setDescriptionDetaillee($descriptionDetaillee);
                $location->setCaracteristiques($caracteristiques);
                $location->setPiscine($piscine);
                $location->setAnimauxDomestiques($animauxDomestiques);
                $location->setEquipementsPartner($equipementsPartner);
                $location->setSejourAvecCouchage($sejourAvecCouchage);
                $location->setCoinRepas($coinRepas);
                $location->setCuisine($cuisine);
                $location->setSanitaire($sanitaire);
                $location->setTypePartner($typePartner);
                $location->setDistanceOther($distanceOther);
                $location->setDistanceMer($distanceMer);
                $location->setDistanceCentreVille($distanceCentreVille);
                $location->setDistanceRemontees($distanceRemontee);
                $location->setDistancePublicTransport($distancePublicTransport);
                $location->setBrandPartner('interchalet');
                $location->setSurfaceHabitable($surfaceHabitable);
                $location->setNombrePiece($nombrePiece);
                $location->setAmenagement($amenagement);
                $location->setRentalprice($rentalPrice);
                $location->setLatitude($latitude);
                $location->setLongitude($longitude);

                // Persist data
                $em->persist($location);
                $em->flush();
                $em->clear();
            } else {
                // Bind data
                $locationExists->setReference($code);
                $locationExists->setTitre($titre);
                $locationExists->setPays($namePays);
                $locationExists->setRegion($nameRegion);
                $locationExists->setVille($nameVille);
                $locationExists->setModificationDate($modificationDate);
                $locationExists->setNombreDePersonne($nombreDePersonne);
                $locationExists->setNombreChambre($nombreChambre);
                $locationExists->setDescriptionDetaillee($descriptionDetaillee);
                $locationExists->setCaracteristiques($caracteristiques);
                $locationExists->setPiscine($piscine);
                $locationExists->setAnimauxDomestiques($animauxDomestiques);
                $locationExists->setEquipementsPartner($equipementsPartner);
                $locationExists->setSejourAvecCouchage($sejourAvecCouchage);
                $locationExists->setCoinRepas($coinRepas);
                $locationExists->setCuisine($cuisine);
                $locationExists->setSanitaire($sanitaire);
                $locationExists->setTypePartner($typePartner);
                $locationExists->setDistanceOther($distanceOther);
                $locationExists->setDistanceMer($distanceMer);
                $locationExists->setDistanceCentreVille($distanceCentreVille);
                $locationExists->setDistanceRemontees($distanceRemontee);
                $locationExists->setDistancePublicTransport($distancePublicTransport);
                $locationExists->setSurfaceHabitable($surfaceHabitable);
                $locationExists->setNombrePiece($nombrePiece);
                $locationExists->setAmenagement($amenagement);
                $locationExists->setRentalprice($rentalPrice);
                $locationExists->setLatitude($latitude);
                $locationExists->setLongitude($longitude);

                // Persist data
                $em->flush();
                $em->clear();
            }
//            if($i >= 10){
//                break;
//            }
        }
    }

    /**
     * Get images
     * @param type $code
     */
    private function _saveLocationSaveImage($code) {
        $em = $this->em;
        $images = $em->getRepository('LefDataBundle:RawIcIlp')->findByLocationRef($code);
        foreach ($images as $image) {
            $src = $image->getSrc();
            $titre = $image->getTitre();

            // Image exists indicator
            $imgPartnerExists = $em->getRepository('LefDataBundle:MediaPartner')
                    ->findOneBy(array('locationRef' => $code, 'src' => $src));
            // Not exists, insert new
            if ($imgPartnerExists == NULL) {
                // Instantier media
                $media = new MediaPartner();
                // Bind data
                $media->setLocationRef($code);
                $media->setSrc($src);
                $media->setTitre($titre);

                // Persist data
                $em->persist($media);
                $em->flush();
                $em->clear();
            } else {
                // None    
            }
        }
    }

    /**
     * get pays, region and ville
     * @param type $codeVille
     * @return type
     */
    private function _saveLocationGetPaysRegionVille($codeVille) {
        $em = $this->em;
        $lgo = $em->getRepository('LefDataBundle:RawIcLandGebOrt')->findOneBy(array('codeVille' => $codeVille));

        $namePays = $this->_treatWord((string) $lgo->getNamePays());
        $nameRegion = $this->_treatWord((string) $lgo->getNameRegion());
        $nameVille = $this->_treatWord((string) $lgo->getNameVille());

        $result = array('namePays' => $namePays, 'nameRegion' => $nameRegion,
            'nameVille' => $nameVille);

        return $result;
    }

    /**
     * Get rental price
     * @param type $code
     * @return null
     */
    private function _saveLocationGetRentalPrice($code) {
        $rentalPrice = NULL;
        $now = date('Y-m-d');
        $em = $this->em;
        $prices = $em->getRepository('LefDataBundle:RawIcPrp')->findBy(array('code' => $code));
        // If no price record exists
        if ($prices == NULL) {
            return $rentalPrice;
        }
        // If records exist
        $rentalPrice = array_key_exists(0, $prices) ? $prices[0]->getRentalPrice() : NULL;
        foreach ($prices as $price) {
            $startDate = $price->getStartDate()->format('Y-m-d');
            $endDate = $price->getEndDate()->format('Y-m-d');
            if ($startDate <= $now && $now <= $endDate) {
                $rentalPrice = $price->getRentalPrice();
            }
        }
        return $rentalPrice;
    }

    /**
     * Deal with word contains '-', which will dérange la génération du url
     * @param type $word
     * @return type
     */
    private function _treatWord($word) {
        if (trim($word) == '') {
            $word = NULL;
        }
        if (strpos($word, '-') >= 0) {
            $word = str_replace('-', '_', $word);
        }
        return $word;
    }

    /**
     * String to datatime
     * @param type $time
     * @param type $format
     * @return type
     */
    private function _stringToDatetime($time, $format = 'Y-m-d') {
        $date = date_create_from_format($format, $time);
        return $date;
    }

    private function _increaseMemoryAllocated() {
        ini_set("user_agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
    }

}
