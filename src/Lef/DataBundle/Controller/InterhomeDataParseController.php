<?php

namespace Lef\DataBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\RawIhAccommodation;
use Lef\DataBundle\Entity\RawIhImg;
use Lef\DataBundle\Entity\RawIhAlert;
use Lef\DataBundle\Entity\RawIhCountryregionplace;
use Lef\DataBundle\Entity\RawIhInsidedescription;
use Lef\DataBundle\Entity\RawIhOutsidedescription;
use Lef\DataBundle\Entity\RawIhTranchePrix;
use Lef\DataBundle\Entity\RawIhDispo;
use Doctrine\ORM\EntityManager;
use SimpleXMLElement;

class InterhomeDataParseController extends Controller {

    protected $em;
    protected $trans_img = array('m' => 'principal', 'i' => 'intérieur', 'o' => 'extérieur', 's' => 'été', 'w' => 'hiver');

    function __construct(EntityManager $entityManager) {
        $this->em = $entityManager;
        // Disable log
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    /**
     * Parse and save raw data (xml) into database
     * A le 061014 2239 par Jian
     * @param file $xmlfile
     * @param string $entityName
     */
    public function parseSaveRawData($xmlfile, $entityName) {
        // Check which file is reading
        switch ($entityName) {
            // Accommodation
            case 'rawIhAccommodation':
                $this->_parseNSaveAccommo($xmlfile);
                $this->acKeepUpdatedLocation();
//                $this->_removeDuplicatedLocations();
                break;
            case 'rawIhInsidedescription':
                $this->_parseNSaveInsidedescription($xmlfile);
                break;
            case 'rawIhOutsidedescription':
                $this->_parseNSaveOutsidedescription($xmlfile);
                break;
            case 'rawIhImg':
                $this->_parseNSaveImages($xmlfile);
                break;
            case 'rawIhCountryregionplace':
                $this->_parseNSaveCountryregionplace($xmlfile);
                break;
            case 'rawIhPrice':
                $fileArray = $xmlfile;
                // Save price per day, weeks
                for ($i = 0; $i < count($fileArray); $i++) {
                    $this->_parseNSavePrice($fileArray[$i][0], $fileArray[$i][1]);
                }

                break;
            case 'rawIhDispo':
                $this->_parseNSaveDispo($xmlfile);
                break;
            case 'rawIhAlert':
                $this->_parseNSaveAlert($xmlfile);
                break;

            default:
                break;
        }
    }

    /**
     * Mecanism parse and save accommodation into BDD
     * @param type $xmlfile
     */
    private function _parseNSaveAccommo($xmlfile) {
        $em = $this->em;
        // Truncate the table
        $this->_truncateTable('raw_ih_accommodation', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'accommodation') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'accommodation') {
            // Get outer node
            $node = new SimpleXMLElement($reader->readOuterXML());
//            echo('<pre>');
//            var_dump($node);
//            echo('</pre>');
//            die();
            // Prepare data
            $code = isset($node->code) ? (string) $node->code : NULL;
            $name = isset($node->name) && trim($node->name) != '' ? (string) $node->name : '--';
            $country = isset($node->country) ? (string) $node->country : NULL;
            $region = isset($node->region) ? (string) $node->region : NULL;
            $place = isset($node->place) ? (string) $node->place : NULL;
            $zip = isset($node->zip) ? (string) $node->zip : NULL;
            $type = isset($node->type) ? (string) $node->type : NULL;
            $details = isset($node->details) ? (string) $node->details : NULL;
            $quality = isset($node->quality) ? (int) $node->quality : NULL;
            $brand = isset($node->brand) ? (string) $node->brand : NULL;
            $pax = isset($node->pax) ? (int) $node->pax : NULL;
            $sqm = isset($node->sqm) ? (int) $node->sqm : NULL;
            $floor = isset($node->floor) ? (int) $node->floor : NULL;
            $rooms = isset($node->rooms) ? (int) $node->rooms : NULL;
            $bedrooms = isset($node->bedrooms) ? (int) $node->bedrooms : NULL;
            $toilets = isset($node->toilets) ? (int) $node->toilets : NULL;
            $bathrooms = isset($node->bathrooms) ? (int) $node->bathrooms : NULL;
            $geodata = isset($node->geodata) ? (array) $node->geodata : NULL;
            $attributes = isset($node->attributes->attribute) ? (array) $node->attributes->attribute : NULL;
            $distancesRaw = isset($node->distances) ? (array) $node->distances : NULL;
            $distances = array();
            if (is_array($distancesRaw) && array_key_exists('distance', $distancesRaw)) {
                foreach ($distancesRaw['distance'] as $key => $value) {
                    $val = (array) $value;
                    $distances[$key] = $val;
                }
            } else {
//                $dd++;
//                echo('distancesRaw has error: <pre>');
//                var_dump($distancesRaw);
//                echo('<pre>');
//                die('dd');
            }
            $themes = isset($node->themes) ? (array) $node->themes : NULL;
//            $picturesRaw = isset($node->pictures) ? (array) $node->pictures : NULL;
            $pictures = array();
//            if (is_array($picturesRaw) && array_key_exists('picture', $picturesRaw)) {
//                foreach ($picturesRaw['picture'] as $key => $value) {
//                    $val = (array) $value;
//                    $pictures[$key] = $val;
//                }
//            } else {
////                echo('picturesRaw has error: <pre>');
////                var_dump($picturesRaw);
////                echo('<pre>');
////                die('pp');
//            }
            // vvvvvvvv get modified date vvvvvvvv
            $updateDate = NULL;
//            $boolDateMod = is_array($picturesRaw) && array_key_exists('picture', $picturesRaw) ? TRUE : FALSE;
//
//            if ($boolDateMod) {
//                $changeDateTimeMil = array_key_exists(0, $picturesRaw['picture']) ? $picturesRaw['picture'][0]->datetime : $picturesRaw['picture']->datetime;
//                $changeDateTimeArry = explode('.', $changeDateTimeMil);
//                $changeDateTimeRaw = $changeDateTimeArry[0];
////                $changeDateTime = substr($changeDateTimeRaw, 0, 4) . '-' . substr($changeDateTimeRaw, 4, 2) . '-' . substr($changeDateTimeRaw, 6, 2) . ' ' .
////                        substr($changeDateTimeRaw, 8, 2) . ':' . substr($changeDateTimeRaw, 10, 2) . ':' . substr($changeDateTimeRaw, 12, 2);
////                $updateDate = date_create_from_format('Y-m-d H:i:s', $changeDateTime) ? date_create_from_format('Y-m-d H:i:s', $changeDateTime) : NULL;
//                $updateDate = date_create_from_format('YmdHis', $changeDateTimeRaw) ? date_create_from_format('YmdHis', $changeDateTimeRaw) : NULL;
//            }
            // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            // Instantier l'entité
            $rawAccommo = new RawIhAccommodation();
            // Bind data
            $rawAccommo->setCode($code);
            $rawAccommo->setName($name);
            $rawAccommo->setCountry($country);
            $rawAccommo->setRegion($region);
            $rawAccommo->setPlace($place);
            $rawAccommo->setZip($zip);
            $rawAccommo->setType($type);
            $rawAccommo->setDetails($details);
            $rawAccommo->setQuality($quality);
            $rawAccommo->setBrand($brand);
            $rawAccommo->setPax($pax);
            $rawAccommo->setSqm($sqm);
            $rawAccommo->setFloor($floor);
            $rawAccommo->setRooms($rooms);
            $rawAccommo->setBedroorooms($bedrooms);
            $rawAccommo->setToilets($toilets);
            $rawAccommo->setBathrooms($bathrooms);
            $rawAccommo->setGeodata($geodata);
            $rawAccommo->setAttributes($attributes);
            $rawAccommo->setDistances($distances);
            $rawAccommo->setThemes($themes);
            $rawAccommo->setPictures($pictures);
            $rawAccommo->setUpdateDate($updateDate);

            // Persist entity
            $em->persist($rawAccommo);
            $em->flush();
            $em->clear();
            $rawAccommo = NULL;
            gc_collect_cycles();
//            echo('<pre>');
//            var_dump($code);
//            echo('<pre>');
//            die();
//                      
            // Jump to next node
            $reader->next('accommodation');

//            $counter ++;
//            if ($counter >= 5) {
//                die('== 5 Accommo ==');
//            }
        }

        $reader->close();
//        die('fin: ' . $counter);
    }

    private function _parseNSaveImages($xmlfile) {
        $em = $this->em;
        // Truncate the table
        $this->_truncateTable('raw_ih_img', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'accommodation') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'accommodation') {
            $node = new SimpleXMLElement($reader->readOuterXML());
            // Prepare data
            $code = isset($node->code) ? (string) $node->code : NULL;
            $picturesRaw = isset($node->pictures) ? (array) $node->pictures : NULL;
            if (is_array($picturesRaw) && array_key_exists('picture', $picturesRaw)) {
                foreach ($picturesRaw['picture'] as $value) {
                    $typeStr = (string) $value->type;
                    $type = array_key_exists($typeStr, $this->trans_img) ? $this->trans_img[$typeStr] : $typeStr;
                    $seasonStr = (string) $value->season;
                    $season = array_key_exists($seasonStr, $this->trans_img) ? $this->trans_img[$seasonStr] : $seasonStr;
                    $url = (string) $value->url;
                    $datetimeStrArray = explode('.', (string) $value->datetime);
                    $datetimeStr = $datetimeStrArray[0];
                    $datetime = date_create_from_format('YmdHis', $datetimeStr);

                    // Instantier l'entité
                    $rawIhImg = new RawIhImg();
                    $rawIhImg->setLocationRef($code);
                    $rawIhImg->setSrc($url);
                    $rawIhImg->setDatetime($datetime);
                    $rawIhImg->setType($type);
                    $rawIhImg->setSeason($season);
                    $em->persist($rawIhImg);
                    $em->flush();
                    $rawIhImg = NULL;
                    gc_collect_cycles();
                }
            } else {
//                echo('picturesRaw has error: <pre>');
//                var_dump($picturesRaw);
//                echo('<pre>');
//                die('pp');
            }
            // Jump to next node
            $reader->next('accommodation');

//            $counter ++;
//            if ($counter >= 5) {
//                die('== 5 Img ==');
//            }
        }
        // Close reader
        $reader->close();
    }

    /**
     * Remove duplicated locations, keep only the most updated
     */
    private function acKeepUpdatedLocation() {
        $em = $this->em;
        $count = $em->getRepository('LefDataBundle:RawIhAccommodation')->getCountAll();
        $this->_increaseMemoryAllocated();
        for ($i = 1; $i <= $count; $i++) {
            $location = $em->getRepository('LefDataBundle:RawIhAccommodation')->findOneById($i);
            if ($location != NULL) {
                $code = $location->getCode();
                $em->getRepository('LefDataBundle:RawIhAccommodation')->keepUpdatedLocationByCode($code);
            }
            $em->clear();
        }
    }

    public function _removeDuplicatedLocations() {
        $em = $this->em;
        $count = $em->getRepository('LefDataBundle:RawIhAccommodation')->getCountAll();
        $this->_increaseMemoryAllocated();
        for ($i = 1; $i <= $count; $i++) {
            $location = $em->getRepository('LefDataBundle:RawIhAccommodation')->findOneById($i);
            if ($location != NULL) {
                $name = $location->getName();
                $titre = str_replace("'", "''", $name);
                $geodata = $location->getGeodata();
                $em->getRepository('LefDataBundle:RawIhAccommodation')->removeDuplicatedLocations($titre, $geodata);
            }
            $em->clear();
        }
    }

    /**
     * Mecanism parse and save alert into BDD
     * @param type $xmlfile
     */
    private function _parseNSaveAlert($xmlfile, $em) {
        $em = $this->em;
        // Empty the table
        $this->_truncateTable('raw_ih_alert', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'alert') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'alert') {
            // Get outer node
            $node = new SimpleXMLElement($reader->readOuterXML());

            // Prepare data
            $code = isset($node->code) ? (string) $node->code : NULL;
            $startdate = isset($node->startdate) ? (string) $node->startdate : NULL;
            $enddate = isset($node->enddate) ? (string) $node->enddate : NULL;
            $text = isset($node->text) ? (string) $node->text : NULL;

            // Instantier l'entité
            $rawAlert = new RawIhAlert();
            // Bind data
            $rawAlert->setCode($code);
            $rawAlert->setStartdate($startdate);
            $rawAlert->setEnddate($enddate);
            $rawAlert->setText($text);

            // Persist entity
            $em->persist($rawAlert);
            $em->flush();
            $em->clear();
            $rawAlert = NULL;
            gc_collect_cycles();
//                        
            // Jump to next node
            $reader->next('alert');
//            $counter ++;
//            if ($counter >= 50) {
//                die('== 50 ==');
//            }
        }
        $reader->close();
    }

    /**
     * Mecanism parse and save country-region-place into BDD
     * Once reach the node subplace, save country-region-subregion-place-subplace, 
     * May contain many conditions: subregion, subplace...
     * @param type $xmlfile
     */
    private function _parseNSaveCountryregionplace($xmlfile) {
        $em = $this->em;
        // Initialize arries
        $codeC = NULL;
        $nameC = NULL;
        $codeR = NULL;
        $nameR = NULL;
        $codeSR = NULL;
        $nameSR = NULL;
        $codeP = NULL;
        $nameP = NULL;
        $codeSP = NULL;
        $nameSP = NULL;
        // Empty the table
        $this->_truncateTable('raw_ih_countryregionplace', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        // increase memory allocation
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);

        // Start reading
        while ($reader->read() && $reader->name != 'country') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'country') {
            // Get outer node
            $nodeC = new SimpleXMLElement($reader->readOuterXML());
            // Get country code and name
            $codeC = isset($nodeC->code) ? (string) $nodeC->code : NULL;
            $nameC = isset($nodeC->name) ? (string) $nodeC->name : NULL;
            // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            // Until country - region, this will not change
            ///////////////////////////////////////////////
            ///////////////////////////////////////////////
            // >>>>>>>>>>>>> Dig till subplace <<<<<<<<<<<<<<
            // Get regions
            $regionsArray = isset($nodeC->regions) ? (array) $nodeC->regions : array();
            $regions = array_key_exists('region', $regionsArray) ? (array) $regionsArray['region'] : array();

            // Has many regions
            if (count($regions) >= 1 && !array_key_exists('code', $regions)) {
                foreach ($regions as $region) {

                    $codeR = isset($region->code) ? (string) $region->code : NULL;
                    $nameR = isset($region->name) ? (string) $region->name : NULL;

                    $subregionsArray = isset($region->subregions) ? (array) $region->subregions : array();
                    $subregions = array_key_exists('subregion', $subregionsArray) ? (array) $subregionsArray['subregion'] : array();

                    // Has many subregions
                    if (count($subregions) >= 1 && !array_key_exists('code', $subregions)) {
                        foreach ($subregions as $subregion) {
                            $codeSR = isset($subregion->code) ? (string) $subregion->code : NULL;
                            $nameSR = isset($subregion->name) ? (string) $subregion->name : NULL;

                            $placesArray = isset($subregion->places) ? (array) $subregion->places : array();
                            $places = array_key_exists('place', $placesArray) ? (array) $placesArray['place'] : array();

                            // __Call function to deal with places -> subplaces
                            $this->__fromPlacesToSubplaces($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $places);
                        }
                    } else if (count($subregions) >= 1 && array_key_exists('code', $subregions)) {
                        // Has only one subregion
                        $codeSR = array_key_exists('code', $subregions) ? (string) $subregions['code'] : NULL;
                        $nameSR = array_key_exists('name', $subregions) ? (string) $subregions['name'] : NULL;

                        $placesArray = array_key_exists('places', $subregions) ? (array) $subregions['places'] : array();
                        $places = array_key_exists('place', $placesArray) ? (array) $placesArray['place'] : array();

                        // __Call function to deal with places -> subplaces
                        $this->__fromPlacesToSubplaces($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $places);
                    } else {
                        // Has no subregion
                        $codeSR = NULL;
                        $nameSR = NULL;

                        $placesArray = isset($region->places) ? (array) $region->places : array();
                        $places = array_key_exists('place', $placesArray) ? (array) $placesArray['place'] : array();

                        // __Call function to deal with places -> subplaces
                        $this->__fromPlacesToSubplaces($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $places);
                    }
                }
            } else {
                // Has Only one region
                $codeR = array_key_exists('code', $regions) ? (string) $regions['code'] : NULL;
                $nameR = array_key_exists('name', $regions) ? (string) $regions['name'] : NULL;

                $subregionsArray = array_key_exists('subregions', $regions) ? (array) $regions['subregions'] : array();
                $subregions = array_key_exists('subregion', $subregionsArray) ? (array) $subregionsArray['subregion'] : array();
                // Has many subregions
                if (count($subregions) >= 1 && !array_key_exists('code', $subregions)) {
//                    var_dump($subregions);
//                    die('One region, Many subregions');
                    foreach ($subregions as $subregion) {

                        $codeSR = isset($subregion->code) ? (string) $subregion->code : NULL;
                        $nameSR = isset($subregion->name) ? (string) $subregion->name : NULL;

                        $placesArray = isset($subregion->places) ? (array) $subregion->places : array();
                        $places = array_key_exists('place', $placesArray) ? (array) $placesArray['place'] : array();

                        // __Call function to deal with places -> subplaces
                        $this->__fromPlacesToSubplaces($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $places);
                    }
                } else if (count($subregions) == 1 && array_key_exists('code', $subregions)) {
//                    var_dump($subregions);
//                    die('<br/>One region, One subregion ');
                    // Has only one subregion
                    $codeSR = isset($subregions->code) ? (string) $subregions->code : NULL;
                    $nameSR = isset($subregions->name) ? (string) $subregions->name : NULL;

                    $subplacesArray = isset($subregions->subplaces) ? (array) $subregions->subplaces : array();
                    $places = array_key_exists('places', $placesArray) ? (array) $placesArray['places'] : array();

                    // __Call function to deal with places -> subplaces
                    $this->__fromPlacesToSubplaces($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $places);
                } else {
                    // Has no subregion
                    $codeSR = NULL;
                    $nameSR = NULL;
                    $placesArray = array_key_exists('places', $regions) ? (array) $regions['places'] : array();
                    $places = array_key_exists('place', $placesArray) ? (array) $placesArray['place'] : array();

                    // __Call function to deal with places -> subplaces
                    $this->__fromPlacesToSubplaces($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $places);
                }
            }


            // Jump to next node
            $reader->next('country');
//            $counter ++;
//            if ($counter >= 2) {
//                die('== 2 Country ==');
//            }
        }
        $reader->close();
//        die('<br/>Pays: ' . $counter);
    }

    private function _parseNSaveInsidedescription($xmlfile) {
        $em = $this->em;
        // Empty the table
        $this->_truncateTable('raw_ih_insidedescription', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabasge collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'description') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'description') {
            // Get outer node
            $node = new SimpleXMLElement($reader->readOuterXML());
//            echo('<pre>');
//            var_dump($node);
//            echo('</pre>');
//            die();
            // Prepare data
            $code = isset($node->code) ? (string) $node->code : NULL;
            $text = isset($node->text) ? (string) $node->text : NULL;

            // Instantier l'entité
            $rawInDesc = new RawIhInsidedescription();
            // Bind data
            $rawInDesc->setCode($code);
            $rawInDesc->setText($text);

            // Persist entity
            $em->persist($rawInDesc);
            $em->flush();
            $em->clear();
            $rawInDesc = NULL;
            gc_collect_cycles();
//                        
            // Jump to next node
            $reader->next('description');
//            $counter ++;
//            if ($counter >= 5) {
//                die('== 5 inside Description ==');
//            }
        }
        $reader->close();
    }

    private function _parseNSaveOutsidedescription($xmlfile) {
        $em = $this->em;
        // Empty the table
        $this->_truncateTable('raw_ih_outsidedescription', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable Gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'description') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'description') {
            // Get outer node
            $node = new SimpleXMLElement($reader->readOuterXML());
//            echo('<pre>');
//            var_dump($node);
//            echo('</pre>');
//            die();
            // Prepare data
            $code = isset($node->code) ? (string) $node->code : NULL;
            $text = isset($node->text) ? (string) $node->text : NULL;

            // Instantier l'entité
            $rawOutDesc = new RawIhOutsidedescription();
            // Bind data
            $rawOutDesc->setCode($code);
            $rawOutDesc->setText($text);

            // Persist entity
            $em->persist($rawOutDesc);
            $em->flush();
            $em->clear();
            $rawOutDesc = NULL;
            gc_collect_cycles();
//                        
            // Jump to next node
            $reader->next('description');
//            $counter ++;
//            if ($counter >= 5) {
//                die('== 5 Outside Description ==');
//            }
        }
        $reader->close();
    }

    /**
     * Save Tranche - prix par jour ou semaines
     * @param type $xmlfile
     * @param type $type durée, chiffre 0, 1, 2, 3 ou 4
     */
    private function _parseNSavePrice($xmlfile, $type) {
        $em = $this->em;
        // Empty the table
        $this->_truncateTable('raw_ih_tranche_prix', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage coleection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'price') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'price') {
            // Get outer node
            $node = new SimpleXMLElement($reader->readOuterXML());
//            echo('<pre>');
//            var_dump($node);
//            echo('</pre>');
//            die();
            // Prepare data
            $code = isset($node->code) ? (string) $node->code : NULL;
            $startdateStr = isset($node->startdate) ? (string) $node->startdate : NULL;
            $startdate = date_create($startdateStr);
            $enddateStr = isset($node->enddate) ? (string) $node->enddate : NULL;
            $enddate = date_create($enddateStr);
            $day = NULL;
            $weekend = NULL;
            $minrentalprice = NULL;
            switch ($type) {
                case 0:
                    $day = isset($node->rentalprice) ? (float) $node->rentalprice : NULL;
                    $weekend = isset($node->weekendrentalprice) ? (float) $node->weekendrentalprice : NULL;
                    break;

                case 1:
                case 2:
                case 3:
                case 4:
                    $minrentalprice = isset($node->rentalprice) ? (float) $node->rentalprice : NULL;
                    break;

                default:
                    break;
            }
            $now = date('Y-m-d');

            if ($enddateStr < $now) {
                $reader->next('price');
            } else {
                // Get one record with certain criteres: code, startDate, endDate
                $rawTPExists = $em->getRepository('LefDataBundle:RawIhTranchePrix')
                        ->findOneBy(array('locationRef' => $code, 'startDate' => $startdate, 'endDate' => $enddate));

                // Insert or update record
                $this->_parseNSavePriceInsert($rawTPExists, $type, $code, $startdate, $enddate, $day, $weekend, $minrentalprice);
                // Gabage collection
                gc_collect_cycles();
                // Jump to next node
                $reader->next('price');
//                $counter ++;
//                if ($counter >= 10) {
//                    die('== 10 Preis ==');
//                }
            }
        }
        $reader->close();
    }

    /**
     * Test existance and deside weither Inster or Update records 
     * to table tranche-prix
     * @param object $rawTPExists
     * @param integer $type
     * @param stirng $code
     * @param date $startdate
     * @param date $enddate
     * @param double $day
     * @param double $weekend
     * @param double $minrentalprice
     */
    private function _parseNSavePriceInsert($rawTPExists, $type, $code, $startdate, $enddate, $day, $weekend, $minrentalprice) {
        $em = $this->em;
        // Record with certain criteres exists, we update it
        if (defined($rawTPExists) && count($rawTPExists) > 0) {
            $rawTPExists->setWeek($minrentalprice);
            switch ($type) {
                case 0:
                    $rawTPExists->setDay($day);
                    $rawTPExists->setWeekend($weekend);
                    break;
                case 1:
                    $rawTPExists->setWeek($minrentalprice);
                    break;
                case 2:
                    $rawTPExists->setWeek2($minrentalprice);
                    break;
                case 3:
                    $rawTPExists->setWeek3($minrentalprice);
                    break;
                case 4:
                    $rawTPExists->setMonth($minrentalprice);
                    break;

                default:
                    break;
            }
            $em->flush();
            $em->clear();
            $rawTPExists = NULL;
        } else {
            // Instantier l'entité
            $rawTP = new RawIhTranchePrix();
            // Bind data
            $rawTP->setLocationRef($code);
            $rawTP->setStartDate($startdate);
            $rawTP->setEndDate($enddate);
            switch ($type) {
                case 0:
                    $rawTP->setDay($day);
                    $rawTP->setWeekend($weekend);
                    break;
                case 1:
                    $rawTP->setWeek($minrentalprice);
                    break;
                case 2:
                    $rawTP->setWeek2($minrentalprice);
                    break;
                case 3:
                    $rawTP->setWeek3($minrentalprice);
                    break;
                case 4:
                    $rawTP->setMonth($minrentalprice);
                    break;

                default:
                    break;
            }

            // Persist entity
            $em->persist($rawTP);
            $em->flush();
            $em->clear();
            $rawTP = NULL;
        }
    }

    /**
     * Save Raw disponibilité
     * @param type $xmlfile
     */
    private function _parseNSaveDispo($xmlfile) {
        $em = $this->em;
        // Empty the table
        $this->_truncateTable('raw_ih_dispo', $em);
        // Set counter to 0
//        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);
        // Enable gabage collection
        gc_enable();

        // Start reading
        while ($reader->read() && $reader->name != 'vacancy') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'vacancy') {
            // Get outer node
            $node = new SimpleXMLElement($reader->readOuterXML());
//            echo('<pre>');
//            var_dump($node);
//            echo('</pre>');
//            die();
            // Prepare data
            $code = isset($node->code) ? (string) $node->code : NULL;
            $startdayStr = isset($node->startday) ? (string) $node->startday : NULL;
            $availability = isset($node->availability) ? (string) $node->availability : NULL;
            $minstay = isset($node->minstay) ? (string) $node->minstay : NULL;

            // convert string to data object
            $firstDate = date_create_from_format('Y-m-d', $startdayStr);

            // Instantier l'entité
            $rawDispo = new RawIhDispo();
            // Bind data
            $rawDispo->setLocationRef($code);
            $rawDispo->setFirstDate($firstDate);
            $rawDispo->setAvailability($availability);
            $rawDispo->setMinstay($minstay);

            // Persist entity
            $em->persist($rawDispo);
            $em->flush();
            $em->clear();
            $rawDispo = NULL;
            gc_collect_cycles();
//                        
            // Jump to next node
            $reader->next('vacancy');
//            $counter ++;
//            if ($counter >= 10) {
//                die('== 10 Dispo ==');
//            }
        }
        $reader->close();
    }

    //==========================
    //=== support functions ====
    //==========================

    /**
     * Function loop different conditions from places to subplaces
     * Part of function _parseNSaveCountryregionplace
     * @param stirng $codeC
     * @param stirng $nameC
     * @param stirng $codeR
     * @param stirng $nameR
     * @param stirng $codeSR
     * @param stirng $nameSR
     * @param array $places
     */
    private function __fromPlacesToSubplaces($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $places) {
        $em = $this->em;
        // Has many places
        if (count($places) >= 1 && !array_key_exists('code', $places)) {
            foreach ($places as $place) {
                $codeP = isset($place->code) ? (string) $place->code : NULL;
                $nameP = isset($place->name) ? (string) $place->name : NULL;

                $subplacesArray = isset($place->subplaces) ? (array) $place->subplaces : array();
                $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();
                // Has many subplaces
                if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                    foreach ($subplaces as $subplace) {
                        $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                        $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                        // >>>>>>>> reach subplace, may do the persist
                        // __Call function to save CRP
                        $this->__saveCRP($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $codeP, $nameP, $codeSP, $nameSP);
                    }
                } else if (array_key_exists('code', $subplaces)) {
                    // Has Only one subplace
                    $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                    $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                    // >>>>>>>> reach subplace, may do the persist
                    // __Call function to save CRP
                    $this->__saveCRP($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $codeP, $nameP, $codeSP, $nameSP);
                } else {
                    // Has No subplace
                    $codeSP = NULL;
                    $nameSP = NULL;
                    // >>>>>>>> reach subplace, may do the persist
                    // __Call function to save CRP
                    $this->__saveCRP($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $codeP, $nameP, $codeSP, $nameSP);
                }
            }
        } else {
            // Has only one place
            $codeP = array_key_exists('code', $places) ? (string) $places['code'] : NULL;
            $nameP = array_key_exists('name', $places) ? (string) $places['name'] : NULL;

            $subplacesArray = array_key_exists('subplaces', $places) ? (array) $places['subplaces'] : array();
            $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();

            // Has many subplaces
            if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                foreach ($subplaces as $subplace) {
                    $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                    $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                    // >>>>>>>> reach subplace, may do the persist
                    // __Call function to save CRP
                    $this->__saveCRP($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $codeP, $nameP, $codeSP, $nameSP);
                }
            } else if (array_key_exists('code', $subplaces)) {
                // Has Only one subplace
                $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                // >>>>>>>> reach subplace, may do the persist
                // __Call function to save CRP
                $this->__saveCRP($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $codeP, $nameP, $codeSP, $nameSP);
            } else {
                // Has No subplace
                $codeSP = NULL;
                $nameSP = NULL;
                // >>>>>>>> reach subplace, may do the persist
                // __Call function to save CRP
                $this->__saveCRP($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $codeP, $nameP, $codeSP, $nameSP);
            }
        }
    }

    /**
     * Function persist and save CRP, country-region-place
     * part of function __fromPlacesToSubplaces
     * @param string $codeC
     * @param string $nameC
     * @param string $codeR
     * @param string $nameR
     * @param string $codeSR
     * @param string $nameSR
     * @param string $codeP
     * @param string $nameP
     * @param string $codeSP
     * @param string $nameSP
     */
    private function __saveCRP($codeC, $nameC, $codeR, $nameR, $codeSR, $nameSR, $codeP, $nameP, $codeSP, $nameSP) {
        // Enable gabage collection
        gc_enable();
        $em = $this->em;
        $rawCRP = new RawIhCountryregionplace();
        // Bind data
        $rawCRP->setCountryCode($codeC);
        $rawCRP->setCountryName($nameC);
        $rawCRP->setRegionCode($codeR);
        $rawCRP->setRegionName($nameR);
        $rawCRP->setSubregionCode($codeSR);
        $rawCRP->setSubregionName($nameSR);
        $rawCRP->setPlaceCode($codeP);
        $rawCRP->setPlaceName($nameP);
        $rawCRP->setSubplaceCode($codeSP);
        $rawCRP->setSubplaceName($nameSP);

        // Persist entity
        $em->persist($rawCRP);
        $em->flush();
        $em->clear();
        $rawCRP = NULL;
        gc_collect_cycles();
    }

    /**
     * Truncate a table
     * @param type $tableName
     * @param type $em
     */
    private function _truncateTable($tableName, $em) {

        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();

        $connection->executeUpdate($platform->getTruncateTableSQL($tableName, true /* whether to cascade */));
    }

    /**
     * Increase memory, incase of crash
     */
    private function _increaseMemoryAllocated() {
        ini_set("user_agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
        // for soap call, socket response
//        ini_set('default_socket_timeout', 600);
    }

}
