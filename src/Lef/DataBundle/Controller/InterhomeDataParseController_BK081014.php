<?php

namespace Lef\DataBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\RawIhAccommodation;
use Lef\DataBundle\Entity\RawIhAlert;
use Lef\DataBundle\Entity\RawIhCountryregionplace;
use Lef\DataBundle\Entity\RawIhInsidedescription;
use Lef\DataBundle\Entity\RawIhOutsidedescription;
use Lef\DataBundle\Entity\RawIhPrice;
use SimpleXMLElement;

class InterhomeDataParseController extends Controller {

    /**
     * Parse and save raw data (xml) into database
     * A le 061014 2239 par Jian
     * @param file $xmlfile
     * @param object $em
     * @param string $entityName
     */
    public function parseSaveRawData($xmlfile, $em, $entityName) {
        // Check which file is reading
        switch ($entityName) {
            // Accommodation
            case 'rawIhAccommodation':
                $this->_parseNSaveAccommo($xmlfile, $em);
                break;
            case 'rawIhAlert':
                $this->_parseNSaveAlert($xmlfile, $em);
                break;
            case 'rawIhCountryregionplace':
                $this->_parseNSaveCountryregionplace($xmlfile, $em);
                break;

            default:
                break;
        }
    }

    /**
     * Mecanism parse and save accommodation into BDD
     * @param type $xmlfile
     * @param type $em
     */
    private function _parseNSaveAccommo($xmlfile, $em) {
        // Empty the table
//        $em->getRepository('LefDataBundle:RawIhAccommodation')->emptyTable(TRUE);
        $this->_truncateTable('raw_ih_accommodation', $em);
        // Set counter to 0
        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);

        // Start reading
        while ($reader->read() && $reader->name != 'accommodation') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'accommodation') {
            // Get outer node
            $node = new SimpleXMLElement($reader->readOuterXML());

            // Prepare data
            $code = isset($node->code) ? (string) $node->code : NULL;
            $name = isset($node->name) ? (string) $node->name : '--';
            $country = isset($node->country) ? (string) $node->country : NULL;
            $region = isset($node->region) ? (string) $node->region : NULL;
            $place = isset($node->place) ? (string) $node->place : NULL;
            $zip = isset($node->zip) ? (string) $node->zip : NULL;
            $type = isset($node->type) ? (string) $node->type : NULL;
            $details = isset($node->details) ? (string) $node->details : NULL;
            $quality = isset($node->quality) ? (int) $node->quality : NULL;
            $brand = isset($node->brand) ? (string) $node->brand : NULL;
            $pax = isset($node->pax) ? (int) $node->pax : NULL;
            $sqm = isset($node->sqm) ? (int) $node->sqm : NULL;
            $floor = isset($node->floor) ? (int) $node->floor : NULL;
            $rooms = isset($node->rooms) ? (int) $node->rooms : NULL;
            $bedrooms = isset($node->bedrooms) ? (int) $node->bedrooms : NULL;
            $toilets = isset($node->toilets) ? (int) $node->toilets : NULL;
            $bathrooms = isset($node->bathrooms) ? (int) $node->bathrooms : NULL;
            $geodata = isset($node->geodata) ? (array) $node->geodata : NULL;
            $attributes = isset($node->attributes->attribute) ? (array) $node->attributes->attribute : NULL;
            $distancesRaw = isset($node->distances) ? (array) $node->distances : NULL;
            $distances = array();
            if (is_array($distancesRaw) && array_key_exists('distance', $distancesRaw)) {
                foreach ($distancesRaw['distance'] as $key => $value) {
                    $val = (array) $value;
                    $distances[$key] = $val;
                }
            } else {
//                echo('distancesRaw has error: <pre>');
//                var_dump($distancesRaw);
//                echo('<pre>');
            }
            $themes = isset($node->themes) ? (array) $node->themes : NULL;
            $picturesRaw = isset($node->pictures) ? (array) $node->pictures : NULL;
            $pictures = array();
            if (is_array($picturesRaw) && array_key_exists('picture', $picturesRaw)) {
                foreach ($picturesRaw['picture'] as $key => $value) {
                    $val = (array) $value;
                    $pictures[$key] = $val;
                }
            } else {
//                echo('picturesRaw has error: <pre>');
//                var_dump($picturesRaw);
//                echo('<pre>');
            }

            // Instantier l'entité
            $rawAccommo = new RawIhAccommodation();
            // Bind data
            $rawAccommo->setCode($code);
            $rawAccommo->setName($name);
            $rawAccommo->setCountry($country);
            $rawAccommo->setRegion($region);
            $rawAccommo->setPlace($place);
            $rawAccommo->setZip($zip);
            $rawAccommo->setType($type);
            $rawAccommo->setDetails($details);
            $rawAccommo->setQuality($quality);
            $rawAccommo->setBrand($brand);
            $rawAccommo->setPax($pax);
            $rawAccommo->setSqm($sqm);
            $rawAccommo->setFloor($floor);
            $rawAccommo->setRooms($rooms);
            $rawAccommo->setBedroorooms($bedrooms);
            $rawAccommo->setToilets($toilets);
            $rawAccommo->setBedroorooms($bedrooms);
            $rawAccommo->setGeodata($geodata);
            $rawAccommo->setAttributes($attributes);
            $rawAccommo->setDistances($distances);
            $rawAccommo->setThemes($themes);
            $rawAccommo->setPictures($pictures);

            // Persist entity
            $em->persist($rawAccommo);
            $em->flush();

//            echo('<pre>');
//            var_dump($code);
//            echo('<pre>');
//            die();
//                        
            $counter ++;
            // Jump to next node
            $reader->next('accommodation');

//            if ($counter >= 50) {
//                die('== 50 ==');
//            }
        }

        $reader->close();
    }

    /**
     * Mecanism parse and save alert into BDD
     * @param type $xmlfile
     * @param type $em
     */
    private function _parseNSaveAlert($xmlfile, $em) {
        // Empty the table
        $this->_truncateTable('raw_ih_alert', $em);
        // Set counter to 0
        $counter = 0;
        $reader = new \XMLReader();
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);

        // Start reading
        while ($reader->read() && $reader->name != 'alert') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'alert') {
            // Get outer node
            $node = new SimpleXMLElement($reader->readOuterXML());

            // Prepare data
            $code = isset($node->code) ? (string) $node->code : NULL;
            $startdate = isset($node->startdate) ? (string) $node->startdate : NULL;
            $enddate = isset($node->enddate) ? (string) $node->enddate : NULL;
            $text = isset($node->text) ? (string) $node->text : NULL;

            // Instantier l'entité
            $rawAlert = new RawIhAlert();
            // Bind data
            $rawAlert->setCode($code);
            $rawAlert->setStartdate($startdate);
            $rawAlert->setEnddate($enddate);
            $rawAlert->setText($text);

            // Persist entity
            $em->persist($rawAlert);
            $em->flush();
//                        
            $counter ++;
            // Jump to next node
            $reader->next('alert');
//            if ($counter >= 50) {
//                die('== 50 ==');
//            }
        }
        $reader->close();
    }

    /**
     * Mecanism parse and save country-region-place into BDD
     * Once reach the node subplace, save country-region-subregion-place-subplace, 
     * May contain many conditions: subregion, subplace...
     * @param type $xmlfile
     * @param type $em
     */
    private function _parseNSaveCountryregionplace($xmlfile, $em) {
        // Initialize arries
        $codeC = NULL;
        $nameC = NULL;
        $codeR = NULL;
        $nameR = NULL;
        $codeSR = NULL;
        $nameSR = NULL;
        $codeP = NULL;
        $nameP = NULL;
        $codeSP = NULL;
        $nameSP = NULL;
        // Empty the table
        $this->_truncateTable('raw_ih_countryregionplace', $em);
        // Set counter to 0
        $counter = 0;
        $reader = new \XMLReader();
        // increase memory allocation
        $this->_increaseMemoryAllocated();
        $reader->open($xmlfile);

        // Start reading
        while ($reader->read() && $reader->name != 'country') {
            // root node, do nothing
        }
        // Reach a node usable, we work here
        while ($reader->name == 'country') {
            // Get outer node
            $nodeC = new SimpleXMLElement($reader->readOuterXML());
            // Get country code and name
            $codeC = isset($nodeC->code) ? (string) $nodeC->code : NULL;
            $nameC = isset($nodeC->name) ? (string) $nodeC->name : NULL;
            // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            // Until country - region, this will not change
            ///////////////////////////////////////////////
            ///////////////////////////////////////////////
            // >>>>>>>>>>>>> Dig till subplace <<<<<<<<<<<<<<
            // Get regions
            $regionsArray = isset($nodeC->regions) ? (array) $nodeC->regions : array();
            $regions = array_key_exists('region', $regionsArray) ? (array) $regionsArray['region'] : array();

            // Has many regions
            if (count($regions) >= 1 && !array_key_exists('code', $regions)) {
                foreach ($regions as $region) {

                    $codeR = isset($region->code) ? (string) $region->code : NULL;
                    $nameR = isset($region->name) ? (string) $region->name : NULL;

                    $subregionsArray = isset($region->subregions) ? (array) $region->subregions : array();
                    $subregions = array_key_exists('subregion', $subregionsArray) ? (array) $subregionsArray['subregion'] : array();

                    // Has many subregions
                    if (count($subregions) >= 1 && !array_key_exists('code', $subregions)) {
                        foreach ($subregions as $subregion) {
                            $codeSR = isset($subregion->code) ? (string) $subregion->code : NULL;
                            $nameSR = isset($subregion->name) ? (string) $subregion->name : NULL;

                            $placesArray = isset($subregion->places) ? (array) $subregion->places : array();
                            $places = array_key_exists('place', $placesArray) ? (array) $placesArray['place'] : array();
                            // Has many places
                            if (count($places) >= 1 && !array_key_exists('code', $places)) {
//                                var_dump($places);
//                                die('<br/>Many regions, Many subregions, Many places');
                                // >>>>> BON <<<<<
                                foreach ($places as $place) {
                                    $codeP = isset($place->code) ? (string) $place->code : NULL;
                                    $nameP = isset($place->name) ? (string) $place->name : NULL;

                                    $subplacesArray = isset($place->subplaces) ? (array) $place->subplaces : array();
                                    $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();
                                    // Has many subplaces
                                    if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                                        foreach ($subplaces as $subplace) {
                                            $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                                            $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                                            // >>>>>>>> reach subplace, may do the persist
                                            echo('<pre>');
                                            var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                            echo('</pre>');
//                                            var_dump($subplace);
//                                            die('<br/>Many regions, Many subregions, Many places, Many subplaces');
                                            // >>>>> BON <<<<<
                                        }
                                    } else if (array_key_exists('code', $subplaces)) {
                                        // Has Only one subplace
                                        $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                                        $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                                        // >>>>>>>> reach subplace, may do the persist
                                        echo('<pre>');
                                        var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                        echo('</pre>');
//                                        var_dump($subplace);
//                                        die('<br/>Many regions, Many subregions, Many places, One subplace');
                                        // >>>>> BON <<<<<
                                    } else {
                                        // Has No subplace
                                        $codeSP = NULL;
                                        $nameSP = NULL;
                                        // >>>>>>>> reach subplace, may do the persist
                                        echo('<pre>');
                                        var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                        echo('</pre>');
//                                        var_dump($subplaces);
//                                        die('<br/>Many regions, Many subregions, Many places, No subplace');
                                        // >>>>> BON <<<<<
                                    }
                                }
                            } else {
                                // Has only one place
                                $codeP = array_key_exists('code', $places) ? (string) $places['code'] : NULL;
                                $nameP = array_key_exists('name', $places) ? (string) $places['name'] : NULL;

                                $subplacesArray = array_key_exists('subplaces', $places) ? (array) $places['subplaces'] : array();
                                $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();

                                // Has many subplaces
                                if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                                    foreach ($subplaces as $subplace) {
                                        $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                                        $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                                        // >>>>>>>> reach subplace, may do the persist
                                        echo('<pre>');
                                        var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                        echo('</pre>');
//                                        var_dump($subplace);
//                                        die('<br/>Many regions, Many subregions, One place, Many subplaces');
                                        // >>>>> BON <<<<<
                                    }
                                } else if (array_key_exists('code', $subplaces)) {
                                    // Has Only one subplace
                                    $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                                    $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
//                                    var_dump($subplaces);
//                                    die('<br/>Many regions, Many subregions, One place, One subplace');
                                    // >>>>> BON <<<<<
                                } else {
                                    // Has No subplace
                                    $codeSP = NULL;
                                    $nameSP = NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
//                                    var_dump($subplaces);
//                                    die('<br/>Many region, Many subregions, One place, No subplace');
                                    // >>>>> BON <<<<<
                                }
                            }
                        }
                    } else if (count($subregions) >= 1 && array_key_exists('code', $subregions)) {
                        // Has only one subregion
                        $codeSR = array_key_exists('code', $subregions) ? (string) $subregions['code'] : NULL;
                        $nameSR = array_key_exists('name', $subregions) ? (string) $subregions['name'] : NULL;

                        $placesArray = array_key_exists('places', $subregions) ? (array) $subregions['places'] : array();
                        $places = array_key_exists('place', $placesArray) ? (array) $placesArray['place'] : array();

                        // Has many places
                        if (count($places) >= 1 && !array_key_exists('code', $places)) {
//                            var_dump($places);
//                            die('<br/>Many regions, One subregion, Many places');
                            // >>>>> BON <<<<<
                            foreach ($places as $place) {
                                $codeP = isset($place->code) ? (string) $place->code : NULL;
                                $nameP = isset($place->name) ? (string) $place->name : NULL;

                                $subplacesArray = isset($place->subplaces) ? (array) $place->subplaces : array();
                                $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();
                                // Has many subplaces
                                if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                                    foreach ($subplaces as $subplace) {
                                        $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                                        $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                                        // >>>>>>>> reach subplace, may do the persist
                                        echo('<pre>');
                                        var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                        echo('</pre>');
//                                        var_dump($subplace);
//                                        die('<br/>Many regions, One subregion, Many places, Many subplaces');
                                        // >>>>> BON <<<<<
                                    }
                                } else if (array_key_exists('code', $subplaces)) {
                                    // Has Only one subplace
                                    $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                                    $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
//                                    var_dump($subplace);
//                                    die('<br/>Many regions, One subregion, Many places, One subplace');
                                    // >>>>> BON <<<<<
                                } else {
                                    // Has No subplace
                                    $codeSP = NULL;
                                    $nameSP = NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
//                                    var_dump($subplaces);
//                                    die('<br/>Many regions, One subregion, Many places, No subplace');
                                    // >>>>> BON <<<<<
                                }
                            }
                        } else {
                            // Has only one place
                            $codeP = array_key_exists('code', $places) ? (string) $places['code'] : NULL;
                            $nameP = array_key_exists('name', $places) ? (string) $places['name'] : NULL;

                            $subplacesArray = array_key_exists('subplaces', $places) ? (array) $places['subplaces'] : array();
                            $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();

                            // Has many subplaces
                            if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                                foreach ($subplaces as $subplace) {
                                    $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                                    $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
//                                    var_dump($subplace);
//                                    die('<br/>Many regions, One subregion, One place, Many subplaces');
                                    // >>>>> BON <<<<<
                                }
                            } else if (array_key_exists('code', $subplaces)) {
                                // Has Only one subplace
                                $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                                $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                                // >>>>>>>> reach subplace, may do the persist
                                echo('<pre>');
                                var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                echo('</pre>');
                                var_dump($subplaces);
                                die('<br/>Many regions, One subregion, One place, One subplace');
                            } else {
                                // Has No subplace
                                $codeSP = NULL;
                                $nameSP = NULL;
                                // >>>>>>>> reach subplace, may do the persist
                                echo('<pre>');
                                var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                echo('</pre>');
//                                var_dump($subplaces);
//                                die('<br/>Many region, One subregion, One place, No subplace');
                                // >>>>> BON <<<<<
                            }
                        }
                    } else {
                        // Has no subregion
                        $codeSR = NULL;
                        $nameSR = NULL;

                        $placesArray = isset($region->places) ? (array) $region->places : array();
                        $places = array_key_exists('place', $placesArray) ? (array) $placesArray['place'] : array();
                        // Has many places
                        if (count($places) >= 1 && !array_key_exists('code', $places)) {
//                            var_dump($places);
//                            die('<br/>Many regions, No subregion, Many places');
                            // >>>>> BON <<<<<
                            foreach ($places as $place) {
                                $codeP = isset($place->code) ? (string) $place->code : NULL;
                                $nameP = isset($place->name) ? (string) $place->name : NULL;

                                $subplacesArray = isset($place->subplaces) ? (array) $place->subplaces : array();
                                $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();
                                // Has many subplaces
                                if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                                    foreach ($subplaces as $subplace) {
                                        $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                                        $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                                        // >>>>>>>> reach subplace, may do the persist
                                        echo('<pre>');
                                        var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                        echo('</pre>');
//                                        var_dump($subplace);
//                                        die('<br/>Many regions, No subregion, Many places, Many subplaces');
                                        // >>>>> BON <<<<<
                                    }
                                } else if (array_key_exists('code', $subplaces)) {
                                    // Has Only one subplace
                                    $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                                    $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
//                                    var_dump($subplace);
//                                    die('<br/>Many regions, No subregion, Many places, One subplace');
                                    // >>>>> BON <<<<<
                                } else {
                                    // Has No subplace
                                    $codeSP = NULL;
                                    $nameSP = NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
//                                    var_dump($subplaces);
//                                    die('<br/>Many regions, No subregion, Many places, No subplace');
                                    // >>>>> BON <<<<<
                                }
                            }
                        } else {
                            // Has only one place
                            $codeP = array_key_exists('code', $places) ? (string) $places['code'] : NULL;
                            $nameP = array_key_exists('name', $places) ? (string) $places['name'] : NULL;

                            $subplacesArray = array_key_exists('subplaces', $places) ? (array) $places['subplaces'] : array();
                            $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();

                            // Has many subplaces
                            if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                                foreach ($subplaces as $subplace) {
                                    $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                                    $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
//                                    var_dump($subplace);
//                                    die('<br/>Many regions, No subregion, One place, Many subplaces');
                                    // >>>>> BON <<<<<
                                }
                            } else if (array_key_exists('code', $subplaces)) {
                                // Has Only one subplace
                                $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                                $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                                // >>>>>>>> reach subplace, may do the persist
                                echo('<pre>');
                                var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                echo('</pre>');
//                                var_dump($subplaces);
//                                die('<br/>Many regions, No subregion, One place, One subplace');
                                // >>>>> BON <<<<<
                            } else {
                                // Has No subplace
                                $codeSP = NULL;
                                $nameSP = NULL;
                                // >>>>>>>> reach subplace, may do the persist
                                echo('<pre>');
                                var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                echo('</pre>');
//                                var_dump($subplaces);
//                                die('<br/>Many regions, No subregion, One place, No subplace');
//                                 >>>>> BON <<<<<
                            }
                        }
                    }
                }
            } else {
                // Has Only one region
                $codeR = array_key_exists('code', $regions) ? (string) $regions['code'] : NULL;
                $nameR = array_key_exists('name', $regions) ? (string) $regions['name'] : NULL;

                $subregionsArray = array_key_exists('subregions', $regions) ? (array) $regions['subregions'] : array();
                $subregions = array_key_exists('subregion', $subregionsArray) ? (array) $subregionsArray['subregion'] : array();
                // Has many subregions
                if (count($subregions) >= 1 && !array_key_exists('code', $subregions)) {
                    var_dump($subregions);
                    die('One region, Many subregions');
                    foreach ($subregions as $subregion) {

                        $codeSR = isset($subregion->code) ? (string) $subregion->code : NULL;
                        $nameSR = isset($subregion->name) ? (string) $subregion->name : NULL;

                        $placesArray = isset($subregion->places) ? (array) $subregion->places : array();
                        $places = array_key_exists('place', $placesArray) ? (array) $placesArray['place'] : array();
                        // Has many places
                        if (count($places) >= 1 && !array_key_exists('code', $places)) {
                            var_dump($subplace);
                            die('<br/>One region, Many subregions, Many places');
                            // >>>>> NOT FOUND <<<<<
                            foreach ($places as $place) {
                                $codeP = isset($place->code) ? (string) $place->code : NULL;
                                $nameP = isset($place->name) ? (string) $place->name : NULL;

                                $subplacesArray = isset($place->subplaces) ? (array) $place->subplaces : array();
                                $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();
                                // Has many subplaces
                                if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                                    foreach ($subplaces as $subplace) {
                                        $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                                        $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                                        // >>>>>>>> reach subplace, may do the persist
                                        echo('<pre>');
                                        var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                        echo('</pre>');
                                        var_dump($subplace);
                                        die('<br/>One region, Many subregions, Many places, Many subplaces');
                                        // >>>>> NOT FOUND <<<<<
                                    }
                                } else if (array_key_exists('code', $subplaces)) {
                                    var_dump($subplaces);
                                    die('<br/>One region, Many subregions, Many places, One subplace');
                                    // Has Only one subplace
                                    $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                                    $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
                                    var_dump($subplace);
                                    die('<br/>One region, Many subregions, Many places, One subplace');
                                    // >>>>> NOT FOUND <<<<<
                                } else {
                                    // Has No subplace
                                    $codeSP = NULL;
                                    $nameSP = NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
                                    var_dump($subplaces);
                                    die('<br/>One region, Many subregions, Many places, No subplace');
                                    // >>>>> NOT FOUND <<<<<
                                }
                            }
                        } else {
                            // Has only one place
                            $codeP = array_key_exists('code', $places) ? (string) $places['code'] : NULL;
                            $nameP = array_key_exists('name', $places) ? (string) $places['name'] : NULL;

                            $subplacesArray = array_key_exists('subplaces', $places) ? (array) $places['subplaces'] : array();
                            $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();

                            // Has many subplaces
                            if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                                foreach ($subplaces as $subplace) {
                                    $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                                    $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
                                    var_dump($subplace);
                                    die('<br/>One region, Many subregions, One place, Many subplaces');
                                    // >>>>> NOT FOUND <<<<<
                                }
                            } else if (count($subplaces) >= 1) {
                                // Has Only one subplace
                                $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                                $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                                // >>>>>>>> reach subplace, may do the persist
                                echo('<pre>');
                                var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                echo('</pre>');
                                var_dump($subplaces);
                                die('<br/>One region, Many subregions, One place, One subplace');
                                // >>>>> NOT FOUND <<<<<
                            } else {
                                // Has No subplace
                                $codeSP = NULL;
                                $nameSP = NULL;
                                // >>>>>>>> reach subplace, may do the persist
                                echo('<pre>');
                                var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                echo('</pre>');
                                var_dump($subplaces);
                                die('<br/>One region, Many subregions, One place, No subplace');
                                // >>>>> NOT FOUND <<<<<
                            }
                        }
                    }
                } else if (count($subregions) == 1 && array_key_exists('code', $subregions)) {
                    var_dump($subregions);
                    die('<br/>One region, One subregion ');
                    // Has only one subregion
                    $codeSR = isset($subregions->code) ? (string) $subregions->code : NULL;
                    $nameSR = isset($subregions->name) ? (string) $subregions->name : NULL;

                    $subplacesArray = isset($subregions->subplaces) ? (array) $subregions->subplaces : array();
                    $places = array_key_exists('places', $placesArray) ? (array) $placesArray['places'] : array();
                    // Has many places
                    if (count($places) >= 1 && !array_key_exists('code', $places)) {
                        var_dump($subplace);
                        die('<br/>One region, One subregion, Many places');
                        // >>>>> NOT FOUND <<<<<
                        foreach ($places as $place) {
                            $codeP = isset($place->code) ? (string) $place->code : NULL;
                            $nameP = isset($place->name) ? (string) $place->name : NULL;

                            $subplacesArray = isset($place->subplaces) ? (array) $place->subplaces : array();
                            $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();
                            // Has many subplaces
                            if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                                foreach ($subplaces as $subplace) {
                                    $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                                    $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
                                    var_dump($subplace);
                                    die('<br/>One region, One subregion, Many places, Many subplaces');
                                    // >>>>> NOT FOUND <<<<<
                                }
                            } else if (array_key_exists('code', $subplaces)) {
                                var_dump($subplaces);
                                die('<br/>One region, One subregion, Many places, One subplace');
                                // Has Only one subplace
                                $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                                $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                                // >>>>>>>> reach subplace, may do the persist
                                echo('<pre>');
                                var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                echo('</pre>');
                                var_dump($subplace);
                                die('<br/>One region, One subregion, Many places, One subplace');
                                // >>>>> NOT FOUND <<<<<
                            } else {
                                // Has No subplace
                                $codeSP = NULL;
                                $nameSP = NULL;
                                // >>>>>>>> reach subplace, may do the persist
                                echo('<pre>');
                                var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                echo('</pre>');
                                var_dump($subplaces);
                                die('<br/>One region, One subregion, Many places, No subplace');
                                // >>>>> NOT FOUND <<<<<
                            }
                        }
                    } else {
                        // Has only one place
                        $codeP = array_key_exists('code', $places) ? (string) $places['code'] : NULL;
                        $nameP = array_key_exists('name', $places) ? (string) $places['name'] : NULL;

                        $subplacesArray = array_key_exists('subplaces', $places) ? (array) $places['subplaces'] : array();
                        $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();

                        // Has many subplaces
                        if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                            foreach ($subplaces as $subplace) {
                                $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                                $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                                // >>>>>>>> reach subplace, may do the persist
                                echo('<pre>');
                                var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                echo('</pre>');
                                var_dump($subplace);
                                die('<br/>One region, No subregion, One place, Many subplaces');
                                // >>>>> NOT FOUND <<<<<
                            }
                        } else if (count($subplaces) >= 1) {
                            // Has Only one subplace
                            $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                            $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                            // >>>>>>>> reach subplace, may do the persist
                            echo('<pre>');
                            var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                            echo('</pre>');
                            var_dump($subplaces);
                            die('<br/>One region, No subregion, One place, One subplace');
                            // >>>>> NOT FOUND <<<<<
                        } else {
                            // Has No subplace
                            $codeSP = NULL;
                            $nameSP = NULL;
                            // >>>>>>>> reach subplace, may do the persist
                            echo('<pre>');
                            var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                            echo('</pre>');
//                        var_dump($subplaces);
//                        die('<br/>One region, No subregion, One place, No subplace');
                            // >>>>> BON <<<<<
                        }
                    }
                } else {
                    // Has no subregion
                    $codeSR = NULL;
                    $nameSR = NULL;
                    $placesArray = array_key_exists('places', $regions) ? (array) $regions['places'] : array();
                    $places = array_key_exists('place', $placesArray) ? (array) $placesArray['place'] : array();

                    // Has many places
                    if (count($places) >= 1 && !array_key_exists('code', $places)) {
//                        var_dump($places);
//                        die('<br/>One region, No subregion, Many places');
                        // >>>>> BON <<<<<
                        foreach ($places as $place) {
                            $codeP = isset($place->code) ? (string) $place->code : NULL;
                            $nameP = isset($place->name) ? (string) $place->name : NULL;

                            $subplacesArray = isset($place->subplaces) ? (array) $place->subplaces : array();
                            $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();

                            // Has many subplaces
                            if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                                foreach ($subplaces as $subplace) {
                                    $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                                    $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                                    // >>>>>>>> reach subplace, may do the persist
                                    echo('<pre>');
                                    var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                    echo('</pre>');
                                    var_dump($subplace);
                                    die('<br/>One region, No subregion, Many places, Many subplaces');
                                    // >>>>> NOT FOUND <<<<<
                                }
                            } else if (array_key_exists('code', $subplaces)) {
                                // Has Only one subplace
                                $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                                $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                                // >>>>>>>> reach subplace, may do the persist
                                echo('<pre>');
                                var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                echo('</pre>');
                                var_dump($subplace);
                                die('<br/>One region, No subregion, Many places, One subplace');
                                // >>>>> NOT FOUND <<<<<
                            } else {
                                // Has No subplace
                                $codeSP = NULL;
                                $nameSP = NULL;
                                // >>>>>>>> reach subplace, may do the persist
                                echo('<pre>');
                                var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                echo('</pre>');
//                                var_dump($subplaces);
//                                die('<br/>One region, No subregion, Many places, No subplace');
                                // >>>>> BON <<<<<
                            }
                        }
                    } else {
                        // Has only one place
                        $codeP = array_key_exists('code', $places) ? (string) $places['code'] : NULL;
                        $nameP = array_key_exists('name', $places) ? (string) $places['name'] : NULL;

                        $subplacesArray = array_key_exists('subplaces', $places) ? (array) $places['subplaces'] : array();
                        $subplaces = array_key_exists('subplace', $subplacesArray) ? (array) $subplacesArray['subplace'] : array();
                        // Has many subplaces
                        if (count($subplaces) >= 1 && !array_key_exists('code', $subplaces)) {
                            foreach ($subplaces as $subplace) {
                                $codeSP = isset($subplace->code) ? (string) $subplace->code : NULL;
                                $nameSP = isset($subplace->name) ? (string) $subplace->name : NULL;
                                // >>>>>>>> reach subplace, may do the persist
                                echo('<pre>');
                                var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                                echo('</pre>');
//                                var_dump($subplace);
//                                die('<br/>One region, No subregion, One place, Many subplaces');
                                // >>>>> BON <<<<<
                            }
                        } else if (array_key_exists('code', $subplaces)) {
                            // Has Only one subplace
                            $codeSP = array_key_exists('code', $subplaces) ? (string) $subplaces['code'] : NULL;
                            $nameSP = array_key_exists('name', $subplaces) ? (string) $subplaces['name'] : NULL;
                            // >>>>>>>> reach subplace, may do the persist
                            echo('<pre>');
                            var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                            echo('</pre>');
                            var_dump($subplaces);
                            die('<br/>One region, No subregion, One place, One subplace');
                            // >>>>> NOT FOUND <<<<<
                        } else {
                            // Has No subplace
                            $codeSP = NULL;
                            $nameSP = NULL;
                            // >>>>>>>> reach subplace, may do the persist
                            echo('<pre>');
                            var_dump($nameC . ' -> ' . $nameR . ' -> ' . $nameSR . ' -> ' . $nameP . ' -> ' . $nameSP);
                            echo('</pre>');
//                            var_dump($subplaces);
//                            die('<br/>One region, No subregion, One place, No subplace');
                            // >>>>> BON <<<<<
                        }
                    }
                }
            }


            $counter ++;
            // Jump to next node
            $reader->next('country');
//            if ($counter >= 2) {
//                die('== 2 ==');
//            }
        }
        $reader->close();
        die('<br/>Pays: ' . $counter);
    }

    //==========================
    //=== support functions ====
    //==========================
    /**
     * Truncate a table
     * @param type $tableName
     * @param type $em
     */
    private function _truncateTable($tableName, $em) {

        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();

        $connection->executeUpdate($platform->getTruncateTableSQL($tableName, true /* whether to cascade */));
    }

    /**
     * Increase memory, incase of crash
     */
    private function _increaseMemoryAllocated() {
        ini_set("user_agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
        // for soap call, socket response
//        ini_set('default_socket_timeout', 600);
    }

}
