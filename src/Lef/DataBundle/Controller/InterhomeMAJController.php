<?php

namespace Lef\DataBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Entity\MediaPartner;
use Lef\DataBundle\Entity\TranchePrix;
use Lef\DataBundle\Entity\Planning;
use Lef\DataBundle\Entity\Alert;
use Lef\DataBundle\Entity\RawIhAccommodation;
use Lef\DataBundle\Entity\RawIhAlert;
use Lef\DataBundle\Entity\RawIhCountryregionplace;
use Lef\DataBundle\Entity\RawIhInsidedescription;
use Lef\DataBundle\Entity\RawIhOutsidedescription;
use Lef\DataBundle\Entity\RawIhPrice;

/**
 * This calss is defined as service in container
 * lef_data.maj.interhome
 */
class InterhomeMAJController extends Controller {

    // EntityManager
    protected $em;
    // translation array for attributes, EN -> FR
    private static $trans_attributes = ['bath' => 'salle de bain', 'shower' => 'douche', 'bathandshower' => 'bain et douche',
        'extrabed' => 'lit supplémentaire', 'nonsmoking' => 'non-fumeur',
        'petsallowed' => 'animaux domestiques acceptés', 'hikingmountains' => 'randonée montagne',
        'hikingplains' => 'visite avion', 'nordicwalking' => 'balade nordique',
        'mountainbike' => 'vtt montagne', 'winterwalking' => 'balade hiver',
        'riding' => 'équitation', 'cook' => 'cuisine', 'breakfast' => 'petit déjeuner',
        'elevator' => 'ascenceur', 'fenced' => 'grillé', 'garden' => 'jardin',
        'seaview' => 'vu mer', 'childrenplayground' => 'espace enfant', 'airconditionning' => 'air-conditionné',
        'balcony' => 'balcon', 'babycot' => 'lit bébé', 'dishwasher' => 'lave-vaisselle',
        'parkingcovered' => 'parking couvert', 'pool' => 'piscine', 'poolprivate' => 'piscine privé',
        'poolchildren' => 'piscine enfant', 'washingmachine' => 'lave-linge',
        'microwave' => 'micro-ondes', 'oven' => 'four', 'bikingplains' => 'plan du vélo',
        'fireplace' => 'cheminée'];
    // translation array for themes
    private static $trans_themes = ['CITYTRIPS' => 'Séjour en ville', 'FARMHOUSE' => 'Séjour à la campagne',
        'LAKES AND MOUNTAINS' => 'Lac et montagne', 'SUN AND BEACH' => 'Soleil et plage', 'SKI / WINTER' => 'Ski / hiver',
        'IN A LAKESIDE TOWN' => 'Codé d\'un lac', 'VILLA WITH POOL' => 'Villa avec piscine',
        'FAMILY' => 'Idéal pour famille', 'NIGHTLIFE' => 'Vie nocturne', 'SOMEWHEREQUIET' => 'Tranquil',
        'SPECIAL PROPERTY' => 'Propriété spéciale', 'SELECTION' => 'Sélection partenaire', 'CHEEPCHEEP' => 'Pas cher'];
    // translation array for details -- types
    private static $trans_details = ['A' => 'Apart-Hotel', 'B' => 'Bungalow', 'C' => 'Chalet', 'D' => 'Divers',
        'F' => 'Campagne', 'H' => 'Vacance', 'R' => 'Residence', 'S' => 'Chateau', 'V' => 'Villa', 'Y' => 'Yacht'];
    // Set username and password
    private $ih_soap_user;
    private $ih_soap_pass;
    // Set soap namespace
    private $ih_soap_namespace;
    // Create new instance of SoapClient with the interhome web service URL
    private $ih_client;
    // Create new soap header and set username and password
    private $ih_header;

    public function __construct(EntityManager $entitymanager) {
        $this->em = $entitymanager;
        // Disable log
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);

        $client_ws = 'https://webservices.interhome.com/partnerV3/WebService.asmx?WSDL';

        $this->ih_soap_user = 'FR700622';
        $this->ih_soap_pass = 'LOUER92';
        $this->ih_soap_namespace = 'http://www.interhome.com/webservice';
        $this->ih_client = new \SoapClient($client_ws);
        $this->ih_header = new \SoapHeader($this->ih_soap_namespace, 'ServiceAuthHeader', array('Username' => $this->ih_soap_user, 'Password' => $this->ih_soap_pass), true
        );
    }

    ////////////////////////
    ////////////////////////
    ////////////////////////

    public function majLocations() {
        // Increase memory allocation
        $this->_increaseMemoryAllocated();
        // Enable gabage collection
        gc_enable();
        $em = $this->em;
//        $metaAccommo = $em->getRepository('LefDataBundle:RawIhAccommodation')->findAll();
        $itemCountMax = (integer) $em->getRepository('LefDataBundle:RawIhAccommodation')->getMaxId();
        $entityCRP = $em->getRepository('LefDataBundle:RawIhCountryregionplace');
        $entityInsidedescription = $em->getRepository('LefDataBundle:RawIhInsidedescription');
        $entityOutsidedescription = $em->getRepository('LefDataBundle:RawIhOutsidedescription');
        $entityTranchPrice = $em->getRepository('LefDataBundle:RawIhTranchePrix');
        for ($i = 1; $i <= $itemCountMax; $i++) {
            $metaA = $em->getRepository('LefDataBundle:RawIhAccommodation')->findOneBy(array('id' => $i));

            $reference = $metaA->getCode();
            $titre = $metaA->getName();
            $_countryCode = $metaA->getCountry();
            $_countryObject = $entityCRP->findOneBy(array('countryCode' => $_countryCode));
            $pays = $_countryObject ? $_countryObject->getCountryName() : NULL;
            $pays = $this->_treatWord($pays);
            $_regionCode = $metaA->getRegion();
            $_regionObject = $entityCRP->findOneBy(array('countryCode' => $_countryCode, 'regionCode' => $_regionCode));
            $region = $_regionObject ? $_regionObject->getRegionName() : NULL;
            $region = $this->_treatWord($region);
            $_placeCode = $metaA->getPlace();
            $_placeObject = $entityCRP->findOneBy(array('countryCode' => $_countryCode, 'regionCode' => $_regionCode, 'placeCode' => $_placeCode));
            $ville = $_placeObject ? $_placeObject->getPlaceName() : NULL;
            $ville = $this->_treatWord($ville);
            $zipPostalCode = $metaA->getZip();
            $adresse = $ville . ',' . $zipPostalCode . ',' . $region . ',' . $pays;
            $_typeCode = $metaA->getType();
            $type = $_typeCode == 'A' ? 'Appartement' : $_typeCode == 'D' ? 'Maison' : NULL;
            $_detailsCode = trim($metaA->getDetails()) > 0 ? $metaA->getDetails() : NULL;
            $detials = array_key_exists($_detailsCode, self::$trans_details) ? self::$trans_details[$_detailsCode] : NULL;
            $typePartner = $type . ',' . $detials;
            $nombreEtoiles = $metaA->getQuality();
            $brand = 'interhome';
            $nombreDePersonne = $metaA->getPax();
            $surfaceHabitable = $metaA->getSqm();
            $nombrePiece = $metaA->getRooms();
            $nombreChambre = $metaA->getBedroorooms();
            $sanitaire = 'toilets : ' . $metaA->getToilets();
            $nombreSalleDeBain = $metaA->getBathrooms();
            $_geoDataArray = $metaA->getGeodata();
            $latitude = $_geoDataArray['lat'];
            $longitude = $_geoDataArray['lng'];
            $_attributeArray = $metaA->getAttributes();
            $caracteristiques = $this->_getCaracteristiques($_attributeArray);
            $animauxDomestiques = strpos($caracteristiques, 'animaux domestiques acceptés') >= 0 ? 1 : 0;
            $piscine = strpos($caracteristiques, 'piscine') >= 0 ? 1 : 0;
            $_distanceArray = (array) $metaA->getDistances();
            $treatedDistances = $this->_getDistances($_distanceArray);
            $distanceMer = $treatedDistances['mer'];
            $distanceLac = $treatedDistances['lac'];
            $distanceSki = $treatedDistances['ski'];
            $distancePublictransport = $treatedDistances['publictransport'];
            $distanceGolf = $treatedDistances['golf'];
            $distanceCentreVille = $treatedDistances['centreville'];

            $_themeArray = $metaA->getThemes();
            $themePartner = $this->_translateThemes($_themeArray);

            $modificationDate = $metaA->getUpdateDate();
            $etage = $metaA->getFloor();
            gc_collect_cycles();
            $_descriptionBreveObject = $entityOutsidedescription->findOneBy(array('code' => $reference));
            $descriptionBreve = $this->_getDescriptionBerve($_descriptionBreveObject);
            $_descriptionDetailleeObject = $entityInsidedescription->findOneBy(array('code' => $reference));
            $descriptionDetaillee = $this->_getDescriptionDetaillee($_descriptionDetailleeObject);
            $rentalPrice = $entityTranchPrice->findMinRentalPriceByRef($reference);
            // Empty object
            $metaA = NULL;
            $_descriptionBreveObject = NULL;
            $_descriptionDetailleeObject = NULL;
            $_priceObjectArray = NULL;

            // Alert exists indicator
            $isLocationExists = $em->getRepository('LefDataBundle:Location')->findOneBy(array('reference' => $reference));
            // If not exists, insert
            if ($isLocationExists == NULL) {
                /////////////////////////////
                // Instancier entity location
                $location = new Location();
                // Fill setters
                $location->setReference($reference);
                $location->setTitre($titre);
                $location->setAdresse($adresse);
                $location->setPays($pays);
                $location->setRegion($region);
                $location->setDepartement(NULL);
                $location->setVille($ville);
                $location->setLongitude($longitude);
                $location->setLatitude($latitude);
                $location->setZipPostalCode($zipPostalCode);
                $location->setNombreDePersonne($nombreDePersonne);
                $location->setNombreEtoiles($nombreEtoiles);
                $location->setSurfaceHabitable($surfaceHabitable);
                $location->setNombrePiece($nombrePiece);
                $location->setNombreSalleDeBain($nombreSalleDeBain);
                $location->setNombreChambre($nombreChambre);
                $location->setAnimauxDomestiques($animauxDomestiques);
                $location->setPiscine($piscine);
                $location->setDescriptionBreve($descriptionBreve);
                $location->setDescriptionDetaillee($descriptionDetaillee);
                $location->setCaracteristiques($caracteristiques);
                $location->setSanitaire($sanitaire);
                $location->setDistanceMer($distanceMer);
                $location->setDistanceLac($distanceLac);
                $location->setDistanceCentreVille($distanceCentreVille);
                $location->setDistanceGolf($distanceGolf);
                $location->setDistanceSki($distanceSki);
                $location->setDistancePublicTransport($distancePublictransport);
                $location->setModificationDate($modificationDate);
                $location->setBrandPartner($brand);
                $location->setEtage($etage);
                $location->setThemePartner($themePartner);
                $location->setTypePartner($typePartner);
                $location->setRentalprice($rentalPrice);
//                echo('<pre>');
//                var_dump($location);
//                echo('<pre>');
//                die('Insert');
//                
                // Persist data
                $em->persist($location);
                $em->flush();
                $em->clear();
                $location = NULL;
//                die('new');
            } else {
                // Bind data
                $isLocationExists->setTitre($titre);
                $isLocationExists->setAdresse($adresse);
                $isLocationExists->setPays($pays);
                $isLocationExists->setRegion($region);
                $isLocationExists->setDepartement(NULL);
                $isLocationExists->setVille($ville);
                $isLocationExists->setLongitude($longitude);
                $isLocationExists->setLatitude($latitude);
                $isLocationExists->setZipPostalCode($zipPostalCode);
                $isLocationExists->setNombreDePersonne($nombreDePersonne);
                $isLocationExists->setNombreEtoiles($nombreEtoiles);
                $isLocationExists->setSurfaceHabitable($surfaceHabitable);
                $isLocationExists->setNombrePiece($nombrePiece);
                $isLocationExists->setNombreSalleDeBain($nombreSalleDeBain);
                $isLocationExists->setNombreChambre($nombreChambre);
                $isLocationExists->setAnimauxDomestiques($animauxDomestiques);
                $isLocationExists->setPiscine($piscine);
                $isLocationExists->setDescriptionBreve($descriptionBreve);
                $isLocationExists->setDescriptionDetaillee($descriptionDetaillee);
                $isLocationExists->setCaracteristiques($caracteristiques);
                $isLocationExists->setSanitaire($sanitaire);
                $isLocationExists->setDistanceMer($distanceMer);
                $isLocationExists->setDistanceLac($distanceLac);
                $isLocationExists->setDistanceCentreVille($distanceCentreVille);
                $isLocationExists->setDistanceGolf($distanceGolf);
                $isLocationExists->setDistanceSki($distanceSki);
                $isLocationExists->setDistancePublicTransport($distancePublictransport);
                $isLocationExists->setModificationDate($modificationDate);
                $isLocationExists->setBrandPartner($brand);
                $isLocationExists->setEtage($etage);
                $isLocationExists->setThemePartner($themePartner);
                $isLocationExists->setTypePartner($typePartner);
                $isLocationExists->setRentalprice($rentalPrice);
//                var_dump($location);
//                echo('<pre>');
//                var_dump($isLocationExists);
//                echo('<pre>');
//                die('Update');
                // Persist data
                $em->flush();
                $em->clear();
//                die();
            }
            $isLocationExists = NULL;
            gc_collect_cycles();
            if ($i >= 5) {
                break;
            }
        }
        $entityCRP = NULL;
        $entityInsidedescription = NULL;
        $entityOutsidedescription = NULL;
        $entityTranchPrice = NULL;
        gc_collect_cycles();
    }

    public function majImages() {
        // Increase memory allocation
        $this->_increaseMemoryAllocated();
        // Enable gabage collection
        gc_enable();
        $em = $this->em;
        $countAccommoMax = (integer) $em->getRepository('LefDataBundle:RawIhAccommodation')->getMaxId();
        for ($i = 1; $i <= $countAccommoMax; $i++) {
            $metaARef = $em->getRepository('LefDataBundle:RawIhAccommodation')->findOneRefById($i);
            $imageArray = $em->getRepository('LefDataBundle:RawIhImg')->findBy(array('locationRef' => $metaARef));
            if (!is_array($imageArray)) {
                echo('hh');
                continue;
            }
            // Loop and save picture medias
            foreach ($imageArray as $image) {
                $this->__updateMediaPartner($metaARef, $image);
            }
        }
        gc_collect_cycles();
        echo('i');
    }

    public function majTranchePrix() {
        // Increase memory allocation
        $this->_increaseMemoryAllocated();
        // Enable gabage collection
        gc_enable();
        $em = $this->em;
        $countAccommoMax = (integer) $em->getRepository('LefDataBundle:RawIhAccommodation')->getMaxId();
        for ($i = 1; $i <= $countAccommoMax; $i++) {
            $metaARef = $em->getRepository('LefDataBundle:RawIhAccommodation')->findOneRefById($i);
            $currentLocation = $em->getRepository('LefDataBundle:Location')->findOneBy(array('reference' => $metaARef));
            if ($currentLocation == null || count($currentLocation) < 0) {
                continue;
            }
            $tranchePrixRawArray = $em->getRepository('LefDataBundle:RawIhTranchePrix')->findBy(array('locationRef' => $metaARef));
            if ($tranchePrixRawArray == NULL || count($tranchePrixRawArray) < 0) {
                continue;
            }
            // Remove tranche prix associated with a location
            $this->_removeTranchePrix($em, $currentLocation);
            // Loop and add tranche prix
            foreach ($tranchePrixRawArray as $tranchePrixRaw) {
                $this->__updateTranchePrix($em, $currentLocation, $tranchePrixRaw);
            }
            $em->flush();
            $em->clear();
            $currentLocation = NULL;
            gc_collect_cycles();
        }
    }

    public function majDispo() {
        // Increase memory allocation
        $this->_increaseMemoryAllocated();
        // Enable gabage collection
        gc_enable();
        $em = $this->em;
        $countAccommoMax = (integer) $em->getRepository('LefDataBundle:RawIhAccommodation')->getMaxId();
        for ($i = 1; $i <= $countAccommoMax; $i++) {
            $currentRefBas = $em->getRepository('LefDataBundle:RawIhAccommodation')->findOneRefById($i);
            // If no reference found
            if ($currentRefBas == NULL) {
                continue;
            }
            // Recuperer location dans la mege base
            $currentLocation = $em->getRepository('LefDataBundle:Location')->findOneBy(array('reference' => $currentRefBas));
            // If no location found
            if ($currentLocation == NULL) {
                continue;
            }
            $currentDispo = $em->getRepository('LefDataBundle:RawIhDispo')->findOneBy(array('locationRef' => $currentRefBas));
            // If no trancheprix found
            if (count($currentDispo) <= 0) {
                continue;
            }
            // Insert dispo into common table: Planning
            // Variables
            $date = $currentDispo->getFirstDate();
            $dispoRaw = $currentDispo->getAvailability();
            $valDispo = str_replace(array('Y', 'Q', 'N'), array('1', '1', '0'), $dispoRaw);
            // get planning with location relation
            $planningExists = $em->getRepository('LefDataBundle:Planning')->findOneBy(array('idLocation' => $currentLocation));

            if ($planningExists == NULL) {
                // Instancier Planning
                $planning = new Planning();
                $planning->setDate($date);
                $planning->setDispo($valDispo);
                $planning->setIdLocation($currentLocation);
                $planning->setIspartner(1);
                $em->persist($planning);
                $em->flush();
                $em->clear();
                $planning = NULL;
                // Gabage collection
                gc_collect_cycles();
            } else {
                // Instancier Planning
                $planningExists->setDate($date);
                $planningExists->setDispo($valDispo);
                $planningExists->setIdLocation($currentLocation);
                $planningExists->setIspartner(1);
                $em->flush();
                $em->clear();
                // Gabage collection
                gc_collect_cycles();
            }
            $planningExists = NULL;
            $currentLocation = NULL;
            $currentDispo = NULL;
            $currentBas = NULL;
            // Gabage collection
            gc_collect_cycles();
            if ($i >= 5) {
                break;
            }
        }
    }

    /**
     * Update alert
     */
    private function _updateAlert() {
        $em = $this->em;
        $itemCount = (integer) $em->getRepository('LefDataBundle:RawIhAlert')->getCountAll();

        for ($i = 1; $i <= $itemCount; $i++) {
            // Increase memory allocation
            $this->_increaseMemoryAllocated();
            $em = $this->em;
            $metaA = $em->getRepository('LefDataBundle:RawIhAlert')->findOneBy(array('id' => $i));

            $code = $metaA->getCode();
            $statedate = $metaA->getStartdate();
            $enddate = $metaA->getEnddate();
            $description = $metaA->getText();

            // Alert exists indicator
            $isAlertExists = $entities1 = $em->getRepository('LefDataBundle:Alert')->findOneBy(array('annonceRef' => $code, 'startdate' => $statedate, 'enddate' => $enddate));
            // If not exists, insert
            if ($isAlertExists == NULL) {

                $alert = new Alert();
                // Bind Data
                $alert->setAnnonceRef($code);
                $alert->setStartdate($statedate);
                $alert->setEnddate($enddate);
                $alert->setDescription($description);
//                echo('<pre>');
//                var_dump($alert);
//                echo('<pre>');
//                die('Insert');
                // Persist data
                $em->persist($alert);
                $em->flush();
                $em->clear();
            } else {
                // If exists, update
                $isAlertExists->setAnnonceRef($code);
                $isAlertExists->setStartdate($statedate);
                $isAlertExists->setEnddate($enddate);
                $isAlertExists->setDescription($description);

//                echo('<pre>');
//                var_dump($isAlertExists);
//                echo('<pre>');
//                die('Insert');
                // Persist data
                $em->flush();
                $em->clear();
            }
        }
    }

    /**
     * Update media partner
     * @param type $reference
     * @param type $pictureNode
     * @return type
     */
    private function __updateMediaPartner($reference, $pictureNode) {
        if (!property_exists($pictureNode, 'src')) {
            return;
        }
        $_type = $pictureNode->getType();
        $_season = $pictureNode->getSeason();
        $titre = $this->_getPictureTitle($_type, $_season);
        $url = $pictureNode->getSrc();

        // Get entitymanager
        $em = $this->em;
        // Media exists indicator
        $isMediaExists = $em->getRepository('LefDataBundle:MediaPartner')
                ->findOneBy(array('locationRef' => $reference, 'src' => $url));
        // If not exists, insert
        if ($isMediaExists == null) {
            // Instantier media
            $media = new MediaPartner();
            // Bind data
            $media->setLocationRef($reference);
            $media->setSrc($url);
            $media->setTitre($titre);

            // Persist data
            $em->persist($media);
            $em->flush();
            $em->clear();
            $media = NULL;
        } else {
            /*
              // None
              foreach ($isMediaExists as $media) {
              // If exists, update
              $media->setLocationRef($reference);
              $media->setSrc($url);
              $media->setTitre($titre);

              // Persist data
              $em->flush();
              $em->clear();
              $media = NULL;
              }
             * 
             */
        }
        gc_collect_cycles();
    }

    private function _removeTranchePrix($em, $currentLocation) {
        // Remove associated tranche prix
        $tranchePrixCollection = $currentLocation->getTranchePrix();
        if (count($tranchePrixCollection) > 0) {
            foreach ($tranchePrixCollection as $tranchePrix) {
                $currentLocation->removeTranchePrix($tranchePrix);
                $em->remove($tranchePrix);
                $em->flush();
            }
        }
    }

    private function __updateTranchePrix($em, $currentLocation, $tranchePrixRaw) {
        // Prepare variables
        $dateDebut = $tranchePrixRaw->getStartDate();
        $dateFin = $tranchePrixRaw->getEndDate();
        $day = $tranchePrixRaw->getDay();
        $weekend = $tranchePrixRaw->getWeekend();
        $week = $tranchePrixRaw->getWeek();
        $weeks2 = $tranchePrixRaw->getWeek2();
        $weeks3 = $tranchePrixRaw->getWeek3();
        $month = $tranchePrixRaw->getMonth();
        // Instancier tranche-prix
        $tranchePrix = new TranchePrix();
        $tranchePrix->setDateDebut($dateDebut);
        $tranchePrix->setDateFin($dateFin);
        $tranchePrix->setDay($day);
        $tranchePrix->setWeekend($weekend);
        $tranchePrix->setWeek($week);
        $tranchePrix->setWeeks2($weeks2);
        $tranchePrix->setWeeks3($weeks3);
        $tranchePrix->setMonth($month);
        ////////////
        // Add tranche prix;
        $currentLocation->addTranchePrix($tranchePrix);
        $em->persist($tranchePrix);
        $tranchePrix = NULL;
        gc_collect_cycles();
    }

    /**
     * Deal with word contains '-', which will dérange la génération du url
     * @param type $word
     * @return type
     */
    private function _treatWord($word) {
        if (trim($word) == '') {
            $word = NULL;
        }
        if (strpos($word, '-') >= 0) {
            $word = str_replace('-', '_', $word);
        }
        return $word;
    }

    /**
     * Get, translate and return distance array
     * @param array $arrayDistance
     * @return array
     */
    private function _getDistances($arrayDistance) {
        $distanceMer = NULL;
        $distanceLac = NULL;
        $distanceSki = NULL;
        $distancePublictransport = NULL;
        $distanceGolf = NULL;
        $distanceCentreVille = NULL;
        if (!count($arrayDistance)) {
            return array('mer' => $distanceMer, 'lac' => $distanceLac, 'ski' => $distanceSki,
                'publictransport' => $distancePublictransport, 'golf' => $distanceGolf,
                'centreville' => $distanceCentreVille);
        }
        foreach ($arrayDistance as $distance) {
            if (!array_key_exists('type', $distance) || is_array($distance['type'])) {
                return array('mer' => $distanceMer, 'lac' => $distanceLac, 'ski' => $distanceSki,
                    'publictransport' => $distancePublictransport, 'golf' => $distanceGolf,
                    'centreville' => $distanceCentreVille);
            }
            $value = $distance['value'];
            switch ($distance['type']) {
                case 'sea':
                    $distanceMer = $value;
                    break;
                case 'lake':
                    $distanceLac = $value;
                    break;
                case 'ski':
                    $distanceSki = $value;
                    break;
                case 'publictransport':
                    $distancePublictransport = $value;
                    break;
                case 'golf':
                    $distanceGolf = $value;
                    break;
                case 'center':
                    $distanceCentreVille = $value;
                    break;

                default:
                    break;
            }
        }
        return array('mer' => $distanceMer, 'lac' => $distanceLac, 'ski' => $distanceSki,
            'publictransport' => $distancePublictransport, 'golf' => $distanceGolf,
            'centreville' => $distanceCentreVille);
    }

    /**
     * Return rental price the most nearest
     * @param type $_priceObjectArray
     * @return null
     */
    private function _getRentalprice($_priceObjectArray) {
        $rentalPrice = NULL;
        $now = date('Y-m-d');
        // If no record exists
        if (!$_priceObjectArray) {
            return $rentalPrice;
        }
        $rentalPrice = NULL;
        // Record found
        foreach ($_priceObjectArray as $priceObject) {
            $startDateStr = $priceObject->getStartDate();
            $endDateStr = $priceObject->getEndDate();
            if ($startDateStr <= $now && $now <= $endDateStr) {
                $rentalPrice = $priceObject->getDay() != NULL ? $priceObject->getDay() : $priceObject->getWeek();
            }
        }
        // The First one is the nearest,  we use it
        return $rentalPrice;
    }

    /**
     * Description brève / outside
     * @param type $_descriptionBreveObject
     * @return null
     */
    private function _getDescriptionBerve($_descriptionBreveObject) {
        if (!$_descriptionBreveObject) {
            return NULL;
        }
//        var_dump($_descriptionBreveObject);
//        die();
        $text = $_descriptionBreveObject->getText();
        return $text;
    }

    /**
     * Description détaillée / intside
     * @param type $_descriptionDetailleeObject
     * @return null
     */
    private function _getDescriptionDetaillee($_descriptionDetailleeObject) {
        if (!$_descriptionDetailleeObject) {
            return NULL;
        }
        $text = $_descriptionDetailleeObject->getText();
        return $text;
    }

    ////////////////////////
    ////// Traduction //////
    ////////////////////////
    /**
     * Translate attributes/caractéritiques en->fr
     * @param type $attributesStr
     * @param type $dictArray
     * @return type
     */
    private function _translateAttributes($attributesStr, $dictArray) {
        foreach ($dictArray as $en => $fr) {
            if (strpos($attributesStr, $en) >= 0) {
                $attributesStr = str_replace($en, $fr, $attributesStr);
            }
        }
        return $attributesStr;
    }

    private function _getCaracteristiques($_attributeArray) {
        $caracteristiques = NULL;
        if (is_array($_attributeArray)) {
            $caracteristiquesStr = implode(', ', $_attributeArray);
            $caracteristiques = $this->_translateAttributes($caracteristiquesStr, self::$trans_attributes);
        } else if ($_attributeArray != NULL || strlen($_attributeArray) > 0) {
            $caracteristiques = $this->_translateAttributes($_attributeArray, self::$trans_attributes);
        }
        return $caracteristiques;
    }

    /**
     * Translate if exist, theme en->fr
     * @param type $_themeArray
     * @return null
     */
    private function _translateThemes($_themeArray) {
        $themePartner = NULL;
        if (is_array($_themeArray) && count($_themeArray) > 0 && array_key_exists('theme', $_themeArray)) {
            $keys = $_themeArray['theme'];
            if (is_array($keys)) {
                foreach ($keys as $value) {
                    $themePartner .= array_key_exists($value, self::$trans_themes) ? self::$trans_themes[$value] : NULL;
                    $themePartner .= ', ';
                }
                $themePartner = substr($themePartner, 0, -2);
            } else {
                $themePartner .= array_key_exists($keys, self::$trans_themes) ? self::$trans_themes[$keys] : NULL;
            }
        }
        return $themePartner;
    }

    /**
     * Return picture title season-type
     * @param type $type
     * @param type $season
     * @return string
     */
    private function _getPictureTitle($type, $season) {
        $titre = $season . ', ' . $type;
        return $titre;
    }

    /**
     * Increase memory, incase of crash
     */
    private function _increaseMemoryAllocated() {
        ini_set("user_agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
        // for soap call, socket response
//        ini_set('default_socket_timeout', 600);
    }

}
