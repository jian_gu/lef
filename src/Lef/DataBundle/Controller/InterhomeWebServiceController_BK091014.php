<?php

namespace Lef\DataBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Entity\MediaPartner;
use Lef\DataBundle\Entity\Alert;

class InterhomeWebServiceController extends Controller {

    // translation array for attributes, EN -> FR
    private static $trans_attributes = [];
    // translation array for themes
    private static $trans_themes = ['citytrips' => 'Séjour en ville', 'farmhouse' => 'Séjour à la campagne',
        'lakes and mountains' => 'Lac et montagne', 'sun and beach' => 'Soleil et plage', 'ski / winter' => 'Ski / hiver',
        'in a lakeside town' => 'Codé d\'un lac', 'villa with pool' => 'Villa avec piscine',
        'family' => 'Idéal pour famille', 'nightlife' => 'Vie nocturne', 'somewherequiet' => 'Tranquil',
        'special property' => 'Propriété spéciale', 'selection' => 'Sélection partenaire', 'cheep cheep' => 'Pas cher'];
    // Set username and password
    private $ih_soap_user;
    private $ih_soap_pass;
    // Set soap namespace
    private $ih_soap_namespace;
    // Create new instance of SoapClient with the interhome web service URL
    private $ih_client;
    // Create new soap header and set username and password
    private $ih_header;

    public function __construct() {

        $client_ws = 'https://webservices.interhome.com/partnerV3/WebService.asmx?WSDL';

        $this->ih_soap_user = 'FR700622';
        $this->ih_soap_pass = 'LOUER92';
        $this->ih_soap_namespace = 'http://www.interhome.com/webservice';
        $this->ih_client = new \SoapClient($client_ws);
        $this->ih_header = new \SoapHeader($this->ih_soap_namespace, 'ServiceAuthHeader', array('Username' => $this->ih_soap_user, 'Password' => $this->ih_soap_pass), true
        );
    }

    /**
     * SOAP CALL
     * Disponibilité d'une location
     * @param type $AccommodationCode
     * @param type $CheckIn
     * @param type $CheckOut
     * @return type
     */
    public function availability($AccommodationCode, $CheckIn, $CheckOut) {
        // Prepare parameters
        $params = array('inputValue' => array(
                'AccommodationCode' => $AccommodationCode,
                'CheckIn' => $CheckIn,
                'CheckOut' => $CheckOut,
            )
        );

        // about the cache
        ini_set('soap.wsdl_cache_enabled', 0);
        ini_set('soap.wsdl_cache_ttl', 0);

        // Call web service
        $r = $this->ih_client->__soapCall("Availability", array('parameters' => $params), null, $this->ih_header);

        return $r->AvailabilityResult;
    }

    /**
     * SOAP CALL
     * additional services for a location
     * @param type $AccommodationCode
     * @param type $CheckIn
     * @param type $CheckOut
     * @param type $LanguageCode
     * @param type $CurrencyCode
     * @param type $SalesOfficeCode
     * @param type $Adults
     * @param type $Children
     * @param type $Babies
     * @return type
     */
    public function additionalServices($AccommodationCode, $CheckIn, $CheckOut, $LanguageCode, $CurrencyCode, $SalesOfficeCode, $Adults, $Children, $Babies) {
        // Prepare parameters
        $params = array(
            'inputValue' => array(
                'AccommodationCode' => $AccommodationCode,
                'CheckIn' => $CheckIn,
                'CheckOut' => $CheckOut,
                'LanguageCode' => $LanguageCode,
                'CurrencyCode' => $CurrencyCode,
                'SalesOfficeCode' => $SalesOfficeCode,
                'Adults' => $Adults,
                'Children' => $Children,
                'Babies' => $Babies,
            )
        );

        // Call web service
        $r = $this->ih_client->__soapCall("AdditionalServices", array('parameters' => $params), null, $this->ih_header);
//        echo('<pre>');
//        var_dump($r);
//        echo('</pre>');
//        die();
        return $r->AdditionalServicesResult->AdditionalServices;
    }

    /**
     * SOAP CALL
     * Details d'une location
     * @param string $AccommodationCode
     * @param string $LanguageCode  FR
     * @return object
     */
    public function accommodationDetail($AccommodationCode, $LanguageCode) {
        // Prepare parameters
        $params = array(
            'inputValue' => array(
                'AccommodationCode' => $AccommodationCode,
                'LanguageCodeuageCode' => $LanguageCode,
            )
        );

        // Call web service
        $r = $this->ih_client->__soapCall("AccommodationDetail", array('parameters' => $params), null, $this->ih_header);
//        echo('<pre>');
//        var_dump($r);
//        echo('</pre>');
//        die();
        return $r->AccommodationDetailResult;
    }

    /**
     * Get complete geo data: pays, region, place, cp, sousplace...
     * @param type $codeRegion
     * @param type $codePlace
     * @param type $codePays
     * @param type $cp_rows
     * @return type
     */
    public function cpCompletName($codeRegion, $codePlace, $codePays, $cp_rows) {
        $cp = $cp2 = array();
        $cp_rows = json_decode(json_encode($cp_rows), FALSE);
        foreach ($cp_rows->country as $k1 => $country) {

            $_codePays = $country->code;
            $_namePays = $country->name;
            // Si le code pays fournis correspond au code pays de notre tableau
            if ($_codePays == $codePays) {
                $cp['codePays'] = $_codePays;
                $cp['nomPays'] = $_namePays;
            }

            if (isset($country->regions)) {
                // Si on a une ou plusieurs regions dans le pays, sinon on fait rien
                if (isset($country->regions->region->code)) {
                    // Si il y a une seul regions dans le pays
                    $_codeRegion = $country->regions->region->code;
                    $_nameRegion = $country->regions->region->name;
                    // Si le code region fournis correspond au code region courant
                    if ($_codePays == $codePays && $_codeRegion == $codeRegion) {
                        $cp['codeRegion'] = $_codeRegion;
                        $cp['nomRegion'] = $_nameRegion;
                    }

                    if (isset($country->regions->region->places)) {
                        // Si on a une ou plusieurs places dans la regions, sinon on fait rien
                        if (isset($country->regions->region->places->place->code)) {
                            // S'il y a une seul place dans la region
                            $_codePlace = $country->regions->region->places->place->code;
                            $_namePlace = $country->regions->region->places->place->name;
                            // Si le code place fournis correspond au code place courant
                            if ($_codePays == $codePays && $_codeRegion == $codeRegion && $_codePlace == $codePlace) {
                                $cp['codePlace'] = $_codePlace;
                                $cp['nomPlace'] = $_namePlace;
                            }

                            if (isset($country->regions->region->places->place->subplaces)) {
                                // place avec plusieurs sous-place
                                foreach ($country->regions->region->places->place->subplaces->subplace as $k2 => $subplace) {
                                    $_codeSubPlace = $subplace->code;
                                    $_nameSubPlace = $subplace->name;
                                    // Si le code sous-place fournis correspond au code sous-place courant
                                    if ($_codePays == $codePays && $_codeRegion == $codeRegion && $_codeSubPlace == $codePlace) {
                                        $cp['codePlace'] = $_codeSubPlace;
                                        $cp['nomPlace'] = $_nameSubPlace;
                                    }
                                }
                            }
                        } else {
                            // S'il y a une plusieurs place dans la region
                            foreach ($country->regions->region->places->place as $k3 => $place) {
                                $_codePlace = $place->code;
                                $_namePlace = $place->name;
                                // Si le code sous-place fournis correspond au code sous-place courant
                                if ($_codePays == $codePays && $_codeRegion == $codeRegion && $_codePlace == $codePlace) {
                                    $cp['codePlace'] = $_codePlace;
                                    $cp['nomPlace'] = $_namePlace;
                                }
                            }
                        }
                    }
                } else {
                    // Ici on a plusieurs régions dans le pays
                    foreach ($country->regions->region as $k4 => $region) {
                        //#############################
                        //#############################
                        //#############################
                        $_codeRegion = $region->code;
                        $_nameRegion = $region->name;
                        // Si le code region fournis correspond au code region courant
                        if ($_codePays == $codePays && $_codeRegion == $codeRegion) {
                            $cp['codeRegion'] = $_codeRegion;
                            $cp['nomRegion'] = $_nameRegion;
                        }

                        if (isset($region->places)) {
                            // Si on a une ou plusieurs places dans la regions, sinon on fait rien
                            if (isset($region->places->place->code)) {
                                // S'il y a une seul place dans la region
                                $_codePlace = $region->places->place->code;
                                $_namePlace = $region->places->place->name;
                                // Si le code place fournis correspond au code place courant
                                if ($_codePays == $codePays && $_codeRegion == $codeRegion && $_codePlace == $codePlace) {
                                    $cp['codePlace'] = $_codePlace;
                                    $cp['nomPlace'] = $_namePlace;
                                }

                                if (isset($region->places->place->subplaces)) {
                                    // place avec plusieurs sous-place
                                    foreach ($region->places->place->subplaces->subplace as $k2 => $subplace) {
                                        if (property_exists($subplace, 'code')) {
                                            $_codeSubPlace = $subplace->code;
                                        }
                                        if (property_exists($subplace, 'name')) {
                                            $_nameSubPlace = $subplace->name;
                                        }

                                        // Si le code sous-place fournis correspond au code sous-place courant
                                        if ($_codePays == $codePays && $_codeRegion == $codeRegion && $_codeSubPlace == $codePlace) {
                                            $cp['codePlace'] = $_codeSubPlace;
                                            $cp['nomPlace'] = $_nameSubPlace;
                                        }
                                    }
                                }
                            } else {
                                // S'il y a une plusieurs place dans la region
                                foreach ($region->places->place as $k3 => $place) {
                                    $_codePlace = $place->code;
                                    $_namePlace = $place->name;
                                    // Si le code sous-place fournis correspond au code sous-place courant
                                    if ($_codePays == $codePays && $_codeRegion == $codeRegion && $_codePlace == $codePlace) {
                                        $cp['codePlace'] = $_codePlace;
                                        $cp['nomPlace'] = $_namePlace;
                                    }
                                }
                            }
                        }
                        //#############################
                        //#############################
                        //#############################
                    }
                }
            }
        }
        return $cp;
    }

    public function saveAccommodation($location, $xmlAccomodation, $xmlCountryregion, $em) {
        // Augmentation de la memore allouée
        $this->increaseMemoryAllocated();


        $cpCompletName = $this->cpCompletName($xmlAccomodation['region'], $xmlAccomodation['place'], $xmlAccomodation['country'], $xmlCountryregion);

        $amenagement = "";
        if (array_key_exists('attributes', $xmlAccomodation) && is_array($xmlAccomodation['attributes'])) {
            foreach (current($xmlAccomodation['attributes']) as $key => $attributes) {
                $amenagement .= $attributes . ", ";
            }
            $amenagement = substr($amenagement, 0, -2);
        }

        $distances = "";
        if (array_key_exists('distances', $xmlAccomodation) && is_array($xmlAccomodation['distances'])) {
            foreach (current($xmlAccomodation['distances']) as $key => $distance) {
                if (is_array($distance) && array_key_exists('type', $distance) && array_key_exists('value', $distance)) {
                    $distances .= $distance['type'] . " : " . $distance['value'] . ", ";
                }
            }
            $distances = substr($distances, 0, -2);
        }

        //=============================
        //=============================
        $nomPlace = array_key_exists('nomPlace', $cpCompletName) ? $cpCompletName['nomPlace'] : NULL;
        $codePlace = array_key_exists('codePlace', $cpCompletName) ? $cpCompletName['codePlace'] : NULL;
        $nomRegion = array_key_exists('nomRegion', $cpCompletName) ? $cpCompletName['nomRegion'] : NULL;
        $nomPays = array_key_exists('nomPays', $cpCompletName) ? $cpCompletName['nomPays'] : NULL;

        $sqm = array_key_exists('sqm', $xmlAccomodation) ? $xmlAccomodation['sqm'] : 0;
        $latitude = array_key_exists('geodata', $xmlAccomodation) && array_key_exists('lat', $xmlAccomodation['geodata']) ? $xmlAccomodation['geodata']['lat'] : '';
        $longitude = array_key_exists('geodata', $xmlAccomodation) && array_key_exists('lat', $xmlAccomodation['geodata']) ? $xmlAccomodation['geodata']['lat'] : '';
        $zip = array_key_exists('zip', $xmlAccomodation) ? $xmlAccomodation['zip'] : NULL;
        $pax = array_key_exists('pax', $xmlAccomodation) ? $xmlAccomodation['pax'] : NULL;
        $pets = array_key_exists('ptes', $xmlAccomodation) ? $xmlAccomodation['pets'] : NULL;
        $piscines = array_key_exists('pool', $xmlAccomodation) ? $xmlAccomodation['pool'] : NULL;
        $quality = array_key_exists('quality', $xmlAccomodation) ? $xmlAccomodation['quality'] : NULL;
        $rooms = array_key_exists('rooms', $xmlAccomodation) ? $xmlAccomodation['rooms'] : '';
        $bathrooms = array_key_exists('bathrooms', $xmlAccomodation) ? $xmlAccomodation['bathrooms'] : NULL;
        $bedrooms = array_key_exists('bedrooms', $xmlAccomodation) ? $xmlAccomodation['bedrooms'] : NULL;
        $titre = is_array($xmlAccomodation['name']) ? '-' : $xmlAccomodation['name'];
        //
        //
        $typeLoc = array_key_exists('type', $xmlAccomodation) && ($xmlAccomodation['type'] == "a") ? "Appartement" : array_key_exists('bedrooms', $xmlAccomodation) && ($xmlAccomodation['type'] == "h") ? 'Maison' : null;
        $equipement = "";
        $arryEquip = array_key_exists('attributes', $xmlAccomodation) && isset($xmlAccomodation['attributes']['attribute']) ? $xmlAccomodation['attributes']['attribute'] : array();
        foreach ($arryEquip as $equip) {
            $equipement .= ($equip == 'parking') ? "Parking, " : null;
            $equipement .= ($equip == 'tv') ? "TV, " : null;
            $equipement .= ($equip == 'dishwasher') ? "Lave-vaisselle, " : null;
            $equipement .= ($equip == 'washingmachine') ? "Machine à laver, " : null;
            $equipement .= ($equip == 'aircondition') ? "Climatisation, " : null;
            $equipement .= ($equip == 'tennis') ? "Tennis, " : null;
            $equipement .= ($equip == 'sauna') ? "Sauna, " : null;
            $equipement .= ($equip == 'wheelchair') ? "Fauteuil roulant, " : null;
        }
        $equipement = substr($equipement, 0, -2);


        if (isset($xmlAccomodation['pictures']['picture']) && is_array($xmlAccomodation['pictures']['picture'])) {
            foreach ($xmlAccomodation['pictures']['picture'] as $key => $picture) {
                $src = $picture['url'];
                $saison = $picture['season'] == 's' ? 'été' : $picture['season'] == 'w' ? 'hiver' : NULL;
                $type = $picture['type'] == 'm' ? 'principal' : $picture['type'] == 'i' ? 'intérieur' : $picture['type'] == 'i' ? 'extérieur' : NULL;
                $titrePicture = $saison . '-' . $type;

                $check_media = $em->getRepository('LefDataBundle:MediaPartner')->findOneBy(array("locationRef" => $xmlAccomodation['code'], 'src' => $src));
                // If image not exists
                if ($check_media == null) {
//									$mediaP = new MediaPartner($picture, "", $locationDescIn['code']);
                    $locationP = $em->getRepository('LefDataBundle:Location')->findOneByReference($xmlAccomodation['code']);
                    // If the location to whom the picture belongs exists
                    if (is_object($locationP)) {
                        $mediaP = new MediaPartner($src, $titrePicture, $xmlAccomodation['code']);
                        $em->persist($mediaP);
                        $em->flush();
                    }
                }
            }
        } else {
            var_dump($xmlAccomodation['pictures']);
            die('<br/>pictures failure');
        }
        //
        //
        $boolDateMod = array_key_exists('pictures', $xmlAccomodation) && array_key_exists('picture', $xmlAccomodation['pictures']) && array_key_exists(0, $xmlAccomodation['pictures']['picture']) && array_key_exists('datetime', $xmlAccomodation['pictures']['picture'][0]) || array_key_exists('pictures', $xmlAccomodation) && array_key_exists('picture', $xmlAccomodation['pictures']) && array_key_exists('datetime', $xmlAccomodation['pictures']['picture']) ? TRUE : FALSE;

        if ($boolDateMod) {
            $changeDateTimeMil = array_key_exists(0, $xmlAccomodation['pictures']['picture']) ? $xmlAccomodation['pictures']['picture'][0]['datetime'] : $xmlAccomodation['pictures']['picture']['datetime'];
            $changeDateTimeArry = explode('.', $changeDateTimeMil);
            $changeDateTimeRaw = $changeDateTimeArry[0];
            $changeDateTime = substr($changeDateTimeRaw, 0, 4) . '-' . substr($changeDateTimeRaw, 4, 2) . '-' . substr($changeDateTimeRaw, 6, 2) . ' ' .
                    substr($changeDateTimeRaw, 8, 2) . ':' . substr($changeDateTimeRaw, 10, 2) . ':' . substr($changeDateTimeRaw, 12, 2);
        }

        //=============================
        //=============================

        $location->setReference($xmlAccomodation['code']);
        $location->setTitre($titre);
        $location->setAdresse($nomPlace . ', ' . $zip . ', ' . $nomPays);
        $location->setPays($nomPays);
        $location->setRegion($nomRegion); // Voir le fichier des regions et place xml
        $location->setDepartement(null);
        $location->setVille($nomPlace); // Voir le fichier des regions et place xml
        $location->setLatitude($latitude);
        $location->setLongitude($longitude);
        $location->setZipPostalCode($zip);
        $location->setNombreDePersonne($pax);
        $location->setAnimauxDomestiques($pets);
        $location->setNombreEtoiles($quality);
        $location->setSurfaceHabitable($sqm);
        $location->setNombrePiece($rooms);
        $location->setNombreSalleDeBain($bathrooms);
        $location->setNombreChambre($bedrooms);
        $location->setAmenagement($amenagement);
        $location->setDistanceOther($distances);
        $location->setTypePartner($typeLoc);
        $location->setPiscine($piscines);
        $location->setEquipementsPartner($equipement);
        if ($boolDateMod) {
            $dateM = date_create_from_format('Y-m-d H:i:s', $changeDateTime) ? date_create_from_format('Y-m-d H:i:s', $changeDateTime) : NULL;
            $location->setModificationDate($dateM);
        }

//        echo('<pre>');
//        var_dump($cpCompletName);
//        echo('====<br/>');
//        var_dump($xmlAccomodation);
//        echo('====<br/>');
//        var_dump($location);
//        echo('</pre>');
//        die('===<br/>');

        return $location;
    }

    /**
     * Save alerts
     * @param type $location
     * @param type $xml
     * @param type $em
     * @return type
     */
    public function saveAlert($location, $xml, $em) {
        if (array_key_exists('alert', $xml) && is_array($xml['alert'])) {
            foreach ($xml['alert'] as $key => $alert) {
                if ($location->getReference() == $alert['code']) {
                    $code = $alert['code'];
                    $startdate = $alert['startdate'];
                    $enddate = $alert['enddate'];
                    $text = $alert['text'];
                    $can_persist = false;
                    $alert = $em->getRepository('LefDataBundle:Alert')->findOneBy(array("annonceRef" => $code, "description" => $text, "enddate" => $enddate));

                    if ($alert === null) {
                        $alert = new Alert();
                        $alert->setAnnonceRef($code);
                        $can_persist = true;
                        $alert->setDescription($text);
                        $alert->setEnddate($enddate);
                        $alert->setStartdate($startdate);
                        $em->persist($alert);
                        $em->flush();
                        $em->clear();
                    }
                }
            }
            return $location;
        }
    }

    /**
     * Save inside description ansi que les images
     * @param type $location
     * @param type $xml
     * @param type $em
     * @return type
     */
    public function saveIndesc($location, $xml, $em) {
        // Augmentation de la memore allouée
        $this->increaseMemoryAllocated();
//        echo('<pre>');
//        var_dump($location);
//        echo('</pre>');
//        die();
        if (array_key_exists('description', $xml) && is_array($xml['description'])) {
            foreach ($xml['description'] as $key => $locationDescIn) {
                if ($locationDescIn['code'] == $location->getReference()) {

                    $location->setDescriptionDetaillee($locationDescIn['text']);
                }
            }
        }

        return $location;
    }

    /**
     * Save outside description
     * @param type $location
     * @param file $xml
     * @return type
     */
    public function saveOutdesc($location, $xml) {
// Augmentation de la memore allouée
        $this->increaseMemoryAllocated();

        if (array_key_exists('description', $xml) && is_array($xml['description'])) {
//            var_dump($xml['description']);
//            die();
            foreach ($xml['description'] as $key => $locationDescOut) {
                if ($locationDescOut['code'] == $location->getReference()) {
                    $location->setDescriptionBreve($locationDescOut['text']);
                }
            }

            return $location;
        }
    }

    /**
     * Save rental price
     * @param type $location
     * @param type $xml
     * @return type
     */
    public function savePrice($location, $xml) {
        // Augmentation de la memore allouée
        $this->increaseMemoryAllocated();
        if (array_key_exists('price', $xml) && is_array($xml['price'])) {
            foreach ($xml['price'] as $key => $price) {
                if ($location->getReference() == $price['code']) {
                    $location->setRentalprice($price['rentalprice']);
                }
            }
            return $location;
        }
    }

    /**
     * Read data files, retrieve data and insert into BDD
     * Function called by PartnersController
     * @param xml $countryRegion
     * @param xml $accommodation
     * @param xml $alert
     * @param xml $indesc
     * @param xml $outdesc
     * @param xml $prices
     * @param EntityManager $em
     */
    public function getFiles($countryRegion, $accommodation, $alert, $indesc, $outdesc, $prices, $em) {
        // Augmentation de la memore allouée
        $this->increaseMemoryAllocated();

        $countryregion = json_decode(json_encode(simplexml_load_file($countryRegion)), true);
        $accommodation = json_decode(json_encode(simplexml_load_file($accommodation)), true);
        $alert = json_decode(json_encode(simplexml_load_file($alert)), true);
        $indesc = json_decode(json_encode(simplexml_load_file($indesc)), true);
        $outdesc = json_decode(json_encode(simplexml_load_file($outdesc)), true);
        $prices = json_decode(json_encode(simplexml_load_file($prices)), true);

        if (array_key_exists('accommodation', $accommodation) && is_array($accommodation['accommodation'])) {
            foreach ($accommodation['accommodation'] as $key => $locAccommodation) {
//                echo('<pre>');
//                var_dump($locAccommodation);
//                echo('</pre>');
                $location = new Location();
                $location = $this->saveAccommodation($location, $locAccommodation, $countryregion, $em);
                $location = $this->saveAlert($location, $alert, $em);
                $location = $this->saveIndesc($location, $indesc, $em);
                $location = $this->saveOutdesc($location, $outdesc);
                $location = $this->savePrice($location, $prices);
                $location->setBrandPartner('interhome');

//                echo('<pre>');
//                var_dump($location);
//                echo('</pre>');
//                die('===<br/>');

                if (isset($locAccommodation['code'])) {
                    $check_location = $em->getRepository('LefDataBundle:Location')->findOneBy(array('reference' => $locAccommodation['code']));
                    // if location not exists in Table
                    // insert new
                    if ($check_location == null) {
                        $em->persist($location);
                        $em->flush();
                        $em->clear();
                    } else {
                        // Update
                        $check_location->setTitre($location->getTitre());
                        $check_location->setAdresse($location->getAdresse());
                        $check_location->setPays($location->getPays());
                        $check_location->setRegion($location->getRegion());
                        $check_location->setVille($location->getVille());
                        $check_location->setLongitude($location->getLongitude());
                        $check_location->setLatitude($location->getLatitude());
                        $check_location->setZipPostalCode($location->getZipPostalCode());
                        $check_location->setNombreDePersonne($location->getNombreDePersonne());
                        $check_location->setNombreEtoiles($location->getNombreEtoiles());
                        $check_location->setSurfaceHabitable($location->getSurfaceHabitable());
                        $check_location->setnombrePiece($location->getNombrePiece());
                        $check_location->setnombreSalleDeBain($location->getNombreSalleDeBain());
                        $check_location->setNombreChambre($location->getNombreChambre());
                        $check_location->setAnimauxDomestiques($location->getAnimauxDomestiques());
                        $check_location->setPiscine($location->getPiscine());
                        $check_location->setDescriptionBreve($location->getDescriptionBreve());
                        $check_location->setDescriptionDetaillee($location->getDescriptionDetaillee());
                        $check_location->setAmenagement($location->getAmenagement());
                        $check_location->setDistanceOther($location->getDistanceOther());
                        $check_location->setRentalPrice($location->getRentalprice());
                        $check_location->setEquipementsPartner($location->getEquipementsPartner());
                        $check_location->setTypePartner($location->getTypePartner());
                        $check_location->setModificationDate($location->getModificationDate());
                        
                        $em->flush();
                        $em->clear();
                    }
                }
//                $em->clear();
            }
        }
    }

    /**
     * Increase memory, incase of crash
     */
    public function increaseMemoryAllocated() {
        ini_set("user_agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
        // for soap call, socket response
        ini_set('default_socket_timeout', 600);
    }

}
