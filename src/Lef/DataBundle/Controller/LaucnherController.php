<?php

namespace Lef\DataBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LaucnherController extends Controller
{
    /**
    * @Template
    */
    public function indexAction()
    {

    	return $this->redirect($this->generateUrl('partners'));
    }

    public function synch(){}
    public function unpack(){}
}