<?php

namespace Lef\DataBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\MediaPartner;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Entity\RawLeisureLoc;
use Lef\DataBundle\Entity\RawLeisureImg;
use Lef\DataBundle\Entity\RawLeisureTranchePrixDispo;
use Doctrine\ORM\EntityManager;

class LeisureDataParseController extends Controller {

    protected $em;
    protected $container;
    // Set username and password 
    private $user;
    private $pass;

    public function __construct(EntityManager $entityManager, $container = null) {
        $this->em = $entityManager;
        $this->container = $container;
        $this->user = 'ciel';
        $this->pass = 'ci9el3';
        // Disable log
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    /**
     * Action dispatcher
     * @param type $xmlfile
     * @param type $entityName
     */
    public function parseSaveRawData($jsonStr, $entityName) {
        // Check which file is reading
        switch ($entityName) {
            // Accommodation
            case 'rawLeisureLocation':
                $this->saveRawLocation($jsonStr);
                break;
            case 'rawLeisureImg':
                $this->saveRawImage($jsonStr);
                break;
            case 'rawLeisureTranchePrixDispo':
                $this->saveRawTranchePrixDispo($jsonStr);
                break;
            default:
                break;
        }
    }

    /**
     * Start treat and save locations
     * @param type $jsonString
     */
    private function saveRawLocation($jsonString) {
        $em = $this->em;
        $housesCodes = explode(",", $jsonString);
        // === LIMITATEUR!!! ===
        // Pout tester, limiter nombre à 300
        $housesCodes = array_slice($housesCodes, 0, 5);
        // truncate tranch-prix table
        $this->_truncateTable('raw_leisure_loc', $em);
        // Assign items
        $items = array('BasicInformationV3',
            'LanguagePackFRV4',
            'MinMaxPriceV1',
            'DistancesV1',);
        // Loop and save data
        foreach ($housesCodes as $code) {
            $dataOfHouses = $this->getHouseDataWS($code, $items);
            $this->saveRawLocationProcessCell($dataOfHouses);
            // $checkAvailability = $webService->checkAvailability($code, '2015-08-30', '2015-09-01', 168);
        }
    }

    /**
     * Start treat and save images
     * @param type $jsonString
     */
    private function saveRawImage($jsonString) {
        $em = $this->em;
        $housesCodes = explode(",", $jsonString);
        // === LIMITATEUR!!! ===
        // Pout tester, limiter nombre à 300
        $housesCodes = array_slice($housesCodes, 0, 10);
        // truncate tranch-prix table
//        $this->_truncateTable('raw_leisure_img', $em);
        // Assign items
        $items = array('MediaV2');
        // Loop and save data
        foreach ($housesCodes as $code) {
            $dataOfHouses = $this->getHouseDataWS($code, $items);
            $this->saveRawImageProcessCell($dataOfHouses);
            // $checkAvailability = $webService->checkAvailability($code, '2015-08-30', '2015-09-01', 168);
        }
    }

    private function saveRawTranchePrixDispo($jsonString) {
        $em = $this->em;
        $housesCodes = explode(",", $jsonString);
        // === LIMITATEUR!!! ===
        // Pout tester, limiter nombre à 300
        $housesCodes = array_slice($housesCodes, 0, 1);
        // truncate tranch-prix table
//        $this->_truncateTable('raw_leisure_img', $em);
        // Assign items
        $items = array('AvailabilityPeriodV1');
        // Loop and save data
        foreach ($housesCodes as $code) {
            $dataOfHouses = $this->getHouseDataWS($code, $items);
            $this->saveRawTranchePrixDispoCell($dataOfHouses);
            // $checkAvailability = $webService->checkAvailability($code, '2015-08-30', '2015-09-01', 168);
        }
    }

    /////////////////////////////////
    /////////////////////////////////

    private function saveRawLocationProcessCell($stdClass) {
        $this->_increaseMemoryAllocated();
        // Enable gabage collection
        gc_enable();
        if (isset($stdClass->LanguagePackFRV4)) {
            $CostsOnSite = "";
            foreach ((array) $stdClass->LanguagePackFRV4->CostsOnSite as $key => $value) {
                $CostsOnSite .= $value->Description . ' : ' . $value->Value . "; \n";
            }

            $distanceOther = "Distance :\n";
            foreach ((array) $stdClass->DistancesV1 as $key => $value) {
                $distanceOther .= $value->To . ' à ' . $value->DistanceInKm . " km\n";
            }

            include 'pays.php';

            // Prepare data
            $reference = $stdClass->HouseCode;
            $titre = $stdClass->BasicInformationV3->Name;
            $pays = $pays[$stdClass->BasicInformationV3->Country];
            $region = $stdClass->BasicInformationV3->Region;
            $ville = ((string) $stdClass->LanguagePackFRV4->City == "") ? (string) $stdClass->LanguagePackFRV4->City : (string) $stdClass->LanguagePackFRV4->SubCity;
            $zipCodePostal = $stdClass->BasicInformationV3->ZipPostalCode;
            $longitude = $stdClass->BasicInformationV3->WGS84Longitude;
            $latitude = $stdClass->BasicInformationV3->WGS84Latitude;
            preg_match("/\d$/", $stdClass->BasicInformationV3->MaxNumberOfPersons, $nombreDePersonneArray);
            $nombreDePersonne = $nombreDePersonneArray[0];
            $nombreEtoiles = $stdClass->BasicInformationV3->NumberOfStars;
            $surfaceHabitable = $stdClass->BasicInformationV3->DimensionM2;
            preg_match("/\d$/", $stdClass->BasicInformationV3->NumberOfBathrooms, $nombreSalleDeBainArray);
            $nombreSalleDeBain = $nombreSalleDeBainArray[0];
            preg_match("/\d$/", $stdClass->BasicInformationV3->NumberOfBedrooms, $nombreChambreArray);
            $nombreChambre = $nombreChambreArray[0];
            $animauxDomestiques = $stdClass->BasicInformationV3->NumberOfPets;
            $descriptionBreve = $stdClass->LanguagePackFRV4->ShortDescription;
            $descriptionDetaillee = $stdClass->LanguagePackFRV4->Description;
            $caracteristiques = $stdClass->LanguagePackFRV4->Remarks . "\n\n" . $CostsOnSite;
            $amenagement = $stdClass->LanguagePackFRV4->LayoutSimple;
            $distanceMer = $this->_grabSingleDistance('Sea', $stdClass->DistancesV1);
            $distanceLac = $this->_grabSingleDistance('Lake', $stdClass->DistancesV1);
            $distanceCenter = $this->_grabSingleDistance('Centre', $stdClass->DistancesV1);
            $distanceShopping = $this->_grabSingleDistance('Foods', $stdClass->DistancesV1);
            $distancePublicTransport = $this->_grabSingleDistance('PublicTransport', $stdClass->DistancesV1);
            $creationDate = new \DateTime($stdClass->BasicInformationV3->CreationDate);


            $minPrice = 0;
            foreach ((array) $stdClass->MinMaxPriceV1 as $key => $value) {
                if ($minPrice == 0) {
                    $minPrice = $value->MinPrice;
                } else if ($minPrice > $value->MinPrice) {
                    $minPrice = $value->MinPrice;
                }
            }
            // Insert location into table Raw
            $this->saveRawLocProcessCellInsert($reference, $titre, $pays, $region, $ville, $zipCodePostal, $longitude, $latitude, $nombreDePersonne, $nombreEtoiles, $surfaceHabitable, $nombreSalleDeBain, $nombreChambre, $animauxDomestiques, $descriptionBreve, $descriptionDetaillee, $caracteristiques, $amenagement, $distanceMer, $distanceLac, $distanceCenter, $distanceShopping, $distancePublicTransport, $distanceOther, $creationDate, $minPrice);
        } else {
            echo('Error: source JSON.');
            var_dump($stdClass);
        }
    }

    /**
     * Insert one location Into table Raw
     */
    private function saveRawLocProcessCellInsert($reference, $titre, $pays, $region, $ville, $zipCodePostal, $longitude, $latitude, $nombreDePersonne, $nombreEtoiles, $surfaceHabitable, $nombreSalleDeBain, $nombreChambre, $animauxDomestiques, $descriptionBreve, $descriptionDetaillee, $caracteristiques, $amenagement, $distanceMer, $distanceLac, $distanceCenter, $distanceShopping, $distancePublicTransport, $distanceOther, $creationDate, $minPrice) {
        $em = $this->em;
        $newLocation = new RawLeisureLoc();

        $newLocation->setReference($reference);
        $newLocation->setTitre($titre);
        $newLocation->setPays($pays);
        $newLocation->setRegion($region);
        $newLocation->setVille($ville);
        $newLocation->setZipPostalCode($zipCodePostal);
        $newLocation->setLongitude($longitude);
        $newLocation->setLatitude($latitude);
        $newLocation->setNombreDePersonne($nombreDePersonne);
        $newLocation->setNombreEtoiles($nombreEtoiles);
        $newLocation->setSurfaceHabitable($surfaceHabitable);
        $newLocation->setNombreSalleDeBain($nombreSalleDeBain);
        $newLocation->setNombreChambre($nombreChambre);
        $newLocation->setAnimauxDomestiques($animauxDomestiques);
        $newLocation->setDescriptionBreve($descriptionBreve);
        $newLocation->setDescriptionDetaillee($descriptionDetaillee);
        $newLocation->setCaracteristiques($caracteristiques);
        $newLocation->setAmenagement($amenagement);
        $newLocation->setDistanceMer($distanceMer);
        $newLocation->setDistanceLac($distanceLac);
        $newLocation->setDistanceCentreVille($distanceCenter);
        $newLocation->setDistanceShopping($distanceShopping);
        $newLocation->setDistancePublicTransport($distancePublicTransport);
        $newLocation->setDistanceOther($distanceOther);
        $newLocation->setCreationDate($creationDate);
        $newLocation->setRentalprice($minPrice);
        // Persist
        $em->persist($newLocation);
        $em->flush();
        $em->clear();
        $newLocation = null;
        gc_collect_cycles();
    }

    /**
     * Get and return single distance in meter
     * @param string $destination
     * @param array $distances
     * @return float
     */
    private function _grabSingleDistance($destination, $distances) {
        $result = NULL;
        foreach ($distances as $distance) {
            $to = $distance->To;
            if ($to == $destination) {
                $result = (float) $distance->DistanceInKm * 1000;
            }
        }
        return $result;
    }

    /////////////////////////////////
    /////////////////////////////////

    /**
     * Treat and save images
     * @param type $stdClass
     * @return type
     */
    private function saveRawImageProcessCell($stdClass) {
        $this->_increaseMemoryAllocated();
        // Enable gabage collection
        gc_enable();

        $hoseCode = (string) $stdClass->HouseCode;
        $mediaArray = (array) $stdClass->MediaV2;
        $photoArray = array();
        foreach ($mediaArray as $media) {
            $contentsArray = $media->Type == 'Photos' ? $media->TypeContents : NULL;
            foreach ($contentsArray as $content) {
                $photoArray[] = $content;
            }
        }
        // Return if no image is found
        if ($photoArray == NULL) {
            return;
        }

        // Loop images and save them
        foreach ($photoArray as $photo) {
            $titre = (string) $photo->Tag;
            // There are many version(size) of images, we take the first and biggest one
            $url = (string) $photo->Versions[0]->URL;
            // Insert image
            $this->saveRawImageProcessCellInsert($hoseCode, $titre, $url);
        }
    }

    /**
     * Insert one image into the table raw
     * @param type $code
     * @param type $titre
     * @param type $url
     */
    private function saveRawImageProcessCellInsert($code, $titre, $url) {
        $em = $this->em;
        $image = new RawLeisureImg;
        $image->setLocationRef($code);
        $image->setTitre($titre);
        $image->setSrc($url);
        $em->persist($image);
        $em->flush();
        $em->clear();
        $image = null;
        gc_collect_cycles();
    }

    /////////////////////////////////
    /////////////////////////////////

    private function saveRawTranchePrixDispoCell($stdClass) {
        $this->_increaseMemoryAllocated();
        // Enable gabage collection
        gc_enable();

//        echo('<pre>');
//        var_dump($stdClass);
//        echo('<pre>');
//        die('');

        $locationRef = (string) $stdClass->HouseCode;
        $availabilityArray = (array) $stdClass->AvailabilityPeriodV1;
        // Return if no image is found
        if (!is_array($availabilityArray) || count($availabilityArray) <= 0) {
            return;
        }

        // Loop images and save them
        foreach ($availabilityArray as $availability) {
            $startDate = date_create((string) $availability->ArrivalDate);
            $endDate = date_create((string) $availability->DepartureDate);
            $surDemande = $availability->OnRequest == 'Yes' ? TRUE : FALSE;
            $prix = (double) $availability->Price;
            $rentalPrice = (double) $availability->PriceExclDiscount;
            // Insert TP dispo
            $this->saveRawTranchePrixDispoCellInsert($locationRef, $startDate, $endDate, $surDemande, $prix, $rentalPrice);
        }
    }

    private function saveRawTranchePrixDispoCellInsert($locationRef, $startDate, $endDate, $surDemande, $prix, $rentalPrice) {
        $em = $this->em;
        $tpd = new RawLeisureTranchePrixDispo();
        $tpd->setLocationRef($locationRef);
        $tpd->setStartDate($startDate);
        $tpd->setEndDate($endDate);
        $tpd->setSurDemande($surDemande);
        $tpd->setPrix($prix);
        $tpd->setRentalPrice($rentalPrice);
        $em->persist($tpd);
        $em->flush();
        $em->clear();
        $tpd = null;
        gc_collect_cycles();
    }

    private function getHouseDataWS($houseCode, $items) {
        $ws_client = "https://dataofHousesv1.jsonrpc-partner.net/cgi/lars/jsonrpc-partner/jsonrpc.htm";

        /* The JSON method name is part of the URL. use only lowercase characters in the URL */
        $post_data = array(
            'jsonrpc' => '2.0',
            'method' => 'DataOfHousesV1', /* Methodname is part of the URL */
            'params' => array(
                'WebpartnerCode' => $this->user,
                'WebpartnerPassword' => $this->pass,
                'HouseCodes' => array($houseCode),
                'Items' => $items,
            ),
            'id' => 211897585 /* a unique integer to identify the call for synchronisation */
        );

        $reponse = $this->initCurl($ws_client, $post_data);
//        echo('<pre>');
//        var_dump($res);
//        echo('</pre>');
//        die();
        return current($reponse);
    }

    /**
     * InitCurl call web service
     * @param type $ws_client
     * @param type $post_data
     * @return type
     */
    private function initCurl($ws_client, $post_data) {
        $rows = array();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ws_client);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type: application/json'));
//        curl_setopt($ch, CURLOPT_SSLVERSION, 3); /* Due to an OpenSSL issue */
        curl_setopt($ch, CURLOPT_SSLVERSION, 'CURL_SSLVERSION_TLSv1'); /* Due to some php version issue */
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);  /* Due to a wildcard certificate */
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_ENCODING, 1); /* If result is gzip then unzip */
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if ($result = curl_exec($ch)) {
            $this->_increaseMemoryAllocated();
            if ($res = json_decode($result)) {
                $rows = $res;
            } else {
                echo json_last_error();
            }
        } else {
            var_dump(curl_error($ch));
        }
        curl_close($ch);

        return property_exists($rows, 'result') ? $rows->result : $rows->error;
    }

    /**
     * Truncate a table
     * @param type $tableName
     * @param type $em
     */
    private function _truncateTable($tableName, $em) {

        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();

        $connection->executeUpdate($platform->getTruncateTableSQL($tableName, true /* whether to cascade */));
    }

    /**
     * Increase memory, incase of crash
     */
    private function _increaseMemoryAllocated() {
        ini_set("user_agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
        // for soap call, socket response
//        ini_set('default_socket_timeout', 600);
    }

}
