<?php

namespace Lef\DataBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\MediaPartner;
use Lef\DataBundle\Entity\TranchePrix;
use Lef\DataBundle\Entity\Planning;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Entity\RawLeisureTranchePrix;
use Doctrine\ORM\EntityManager;

class LeisureMAJController extends Controller {

    protected $em;
    protected $container;
    // Set username and password 
    private $user;
    private $pass;

    public function __construct(EntityManager $entityManager, $container = null) {
        $this->em = $entityManager;
        $this->container = $container;
        $this->user = 'ciel';
        $this->pass = 'ci9el3';
        // Disable log
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    public function majLocations() {
        // Increase memory allocation
        $this->_increaseMemoryAllocated();
        // Enable gabage collection
        gc_enable();
        $em = $this->em;
        // Here we go
        $countLocation = $em->getRepository('LefDataBundle:RawLeisureLoc')->getMaxId();

        // Loop location saved in location
        for ($i = 1; $i <= $countLocation; $i++) {
            // Increase memory allocation
            $this->_increaseMemoryAllocated();
            $em = $this->em;
            $locationLeieure = $em->getRepository('LefDataBundle:RawLeisureLoc')->findOneById($i);
            if ($locationLeieure == NULL) {
                continue;
            }


            // Loc
            $reference = $locationLeieure->getReference();
            $latitude = $locationLeieure->getLatitude() != 0 ? $locationLeieure->getLatitude() : NULL;
            $longitude = $locationLeieure->getLongitude() != 0 ? $locationLeieure->getLongitude() : NULL;
            $titre = $locationLeieure->getTitre();
            $pays = $locationLeieure->getPays();
            $region = $locationLeieure->getRegion();
            $ville = $locationLeieure->getVille();
            $zipPostalCode = $locationLeieure->getZipPostalCode();
            $nombreDePersonne = $locationLeieure->getNombreDePersonne();
            $nombreEtoiles = $locationLeieure->getNombreEtoiles();
            $surfaceHabitable = $locationLeieure->getSurfaceHabitable();
            $nombreSalleDeBain = $locationLeieure->getNombreSalleDeBain();
            $nombreChambre = $locationLeieure->getNombreChambre();
            $animauxDomestiques = $locationLeieure->getAnimauxDomestiques();
            $descriptionBreve = $locationLeieure->getDescriptionBreve();
            $descriptionDetaillee = $locationLeieure->getDescriptionDetaillee();
            $caracteristiques = $locationLeieure->getCaracteristiques();
            $amenagement = $locationLeieure->getAmenagement();
            $distanceMer = $locationLeieure->getDistanceMer();
            $distanceLac = $locationLeieure->getDistanceLac();
            $distanceCentreVille = $locationLeieure->getDistanceCentreVille();
            $distanceShopping = $locationLeieure->getDistanceShopping();
            $distancePublicTransport = $locationLeieure->getDistancePublicTransport();
            $distanceOther = $locationLeieure->getDistanceOther();
            $creationDate = $locationLeieure->getCreationDate();
            $rentalprice = $locationLeieure->getRentalprice();



            // =====================
            // =====================
            // Location exists indicator
            $locationExists = $em->getRepository('LefDataBundle:Location')
                    ->findOneByReference($reference);
            // If not exists, insert NEW
            if ($locationExists == NULL) {
                // Instantier location
                $location = new Location();
                // Bind data
                $location->setReference($reference);
                $location->setTitre($titre);
                $location->setPays($pays);
                $location->setRegion($region);
                $location->setVille($ville);
                $location->setLatitude($latitude);
                $location->setLongitude($longitude);
                $location->setZipPostalCode($zipPostalCode);
                $location->setNombreEtoiles($nombreEtoiles);
                $location->setNombreDePersonne($nombreDePersonne);
                $location->setNombreSalleDeBain($nombreSalleDeBain);
                $location->setNombreChambre($nombreChambre);
                $location->setDescriptionBreve($descriptionBreve);
                $location->setDescriptionDetaillee($descriptionDetaillee);
                $location->setCaracteristiques($caracteristiques);
                $location->setAnimauxDomestiques($animauxDomestiques);
                $location->setDistanceOther($distanceOther);
                $location->setDistanceMer($distanceMer);
                $location->setDistanceLac($distanceLac);
                $location->setDistanceCentreVille($distanceCentreVille);
                $location->setDistanceShopping($distanceShopping);
                $location->setDistancePublicTransport($distancePublicTransport);
                $location->setBrandPartner('leisure');
                $location->setSurfaceHabitable($surfaceHabitable);
                $location->setAmenagement($amenagement);
                $location->setCreationDate($creationDate);
                $location->setRentalprice($rentalprice);

                // Persist data
                $em->persist($location);
                $em->flush();
                $em->clear();
                $location = NULL;
                // Gabage collection
                gc_collect_cycles();
            } else {
                // Bind data
                $locationExists->setReference($reference);
                $locationExists->setTitre($titre);
                $locationExists->setPays($pays);
                $locationExists->setRegion($region);
                $locationExists->setVille($ville);
                $locationExists->setLatitude($latitude);
                $locationExists->setLongitude($longitude);
                $locationExists->setZipPostalCode($zipPostalCode);
                $locationExists->setNombreEtoiles($nombreEtoiles);
                $locationExists->setNombreDePersonne($nombreDePersonne);
                $locationExists->setNombreSalleDeBain($nombreSalleDeBain);
                $locationExists->setNombreChambre($nombreChambre);
                $locationExists->setDescriptionBreve($descriptionBreve);
                $locationExists->setDescriptionDetaillee($descriptionDetaillee);
                $locationExists->setCaracteristiques($caracteristiques);
                $locationExists->setAnimauxDomestiques($animauxDomestiques);
                $locationExists->setDistanceOther($distanceOther);
                $locationExists->setDistanceMer($distanceMer);
                $locationExists->setDistanceLac($distanceLac);
                $locationExists->setDistanceCentreVille($distanceCentreVille);
                $locationExists->setDistanceShopping($distanceShopping);
                $locationExists->setDistancePublicTransport($distancePublicTransport);
                $locationExists->setSurfaceHabitable($surfaceHabitable);
                $locationExists->setAmenagement($amenagement);
                $locationExists->setCreationDate($creationDate);
                $locationExists->setRentalprice($rentalprice);

                // Persist data
                $em->flush();
                $em->clear();
                $locationExists = NULL;
                // Gabage collection
                gc_collect_cycles();
            }
//            if($i >= 5){
//                break;
//            }
        }
    }

    public function majImages() {
        // Get locations count
        $em = $this->em;
        $countLocation = $em->getRepository('LefDataBundle:RawLeisureLoc')->getMaxId();
        // Loop location saved in iobjtxt
        for ($i = 1; $i <= $countLocation; $i++) {
            $currentRefLoc = $em->getRepository('LefDataBundle:RawLeisureLoc')->findOneRefById($i);
            // If no reference found
            if ($currentRefLoc == NULL) {
                continue;
            }
            // Save images
            $this->_saveLocationSaveImage($currentRefLoc);
            $currentRefLoc = NULL;
        }
    }

    public function majTranchePrix() {
        // Increase memory allocation
        $this->_increaseMemoryAllocated();
        // Enable gabage collection
        gc_enable();
        $em = $this->em;
        $countAccommoMax = (integer) $em->getRepository('LefDataBundle:RawLeisureLoc')->getMaxId();
        for ($i = 1; $i <= $countAccommoMax; $i++) {
            $metaARef = $em->getRepository('LefDataBundle:RawLeisureLoc')->findOneRefById($i);
            $currentLocation = $em->getRepository('LefDataBundle:Location')->findOneBy(array('reference' => $metaARef));
            if ($currentLocation == null || count($currentLocation) < 0) {
                continue;
            }
            $tranchePrixRawArray = $em->getRepository('LefDataBundle:RawLeisureTranchePrixDispo')->findBy(array('locationRef' => $metaARef));
            if ($tranchePrixRawArray == NULL || count($tranchePrixRawArray) < 0) {
                continue;
            }
            // Remove tranche prix associated with a location
            $this->_removeTranchePrix($em, $currentLocation);
            // Loop and add tranche prix
            foreach ($tranchePrixRawArray as $tranchePrixRaw) {
                $this->__updateTranchePrix($em, $currentLocation, $tranchePrixRaw);
            }
            $em->flush();
            $em->clear();
            $currentLocation = NULL;
            gc_collect_cycles();
        }
    }

    /**
     * Get images
     * @param type $code
     */
    private function _saveLocationSaveImage($code) {
        $em = $this->em;
        // Increase memory allocation
        $this->_increaseMemoryAllocated();
        // Enable gabage collection
        gc_enable();
        $images = $em->getRepository('LefDataBundle:RawLeisureImg')->findByLocationRef($code);
        foreach ($images as $image) {
            $src = $image->getSrc();
            $titre = $image->getTitre();

            // Image exists indicator
            $imgPartnerExists = $em->getRepository('LefDataBundle:MediaPartner')
                    ->findOneBy(array('locationRef' => $code, 'src' => $src));
            // Not exists, insert new
            if ($imgPartnerExists == NULL) {
                // Instantier media
                $media = new MediaPartner();
                // Bind data
                $media->setLocationRef($code);
                $media->setSrc($src);
                $media->setTitre($titre);

                // Persist data
                $em->persist($media);
                $em->flush();
                $em->clear();
                $media = NULL;
                // Gabage collection
                gc_collect_cycles();
            } else {
                // Update
                // None    
            }
        }
    }

    private function _removeTranchePrix($em, $currentLocation) {
        // Remove associated tranche prix
        $tranchePrixCollection = $currentLocation->getTranchePrix();
        if (count($tranchePrixCollection) > 0) {
            foreach ($tranchePrixCollection as $tranchePrix) {
                $currentLocation->removeTranchePrix($tranchePrix);
                $em->remove($tranchePrix);
                $em->flush();
            }
        }
    }

    private function __updateTranchePrix($em, $currentLocation, $tranchePrixRaw) {
        // Prepare variables
        $dateDebut = $tranchePrixRaw->getStartDate();
        $dateFin = $tranchePrixRaw->getEndDate();
        $rentalPrice = $tranchePrixRaw->getRentalPrice();
        // Instancier tranche-prix
        $tranchePrix = new TranchePrix();
        $tranchePrix->setDateDebut($dateDebut);
        $tranchePrix->setDateFin($dateFin);
        $tranchePrix->setDay($rentalPrice);
        ////////////
        // Add tranche prix;
        $currentLocation->addTranchePrix($tranchePrix);
        $em->persist($tranchePrix);
        $tranchePrix = NULL;
        gc_collect_cycles();
    }

    /**
     * Truncate a table
     * @param type $tableName
     * @param type $em
     */
    private function _truncateTable($tableName, $em) {

        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();

        $connection->executeUpdate($platform->getTruncateTableSQL($tableName, true /* whether to cascade */));
    }

    /**
     * Increase memory, incase of crash
     */
    public function _increaseMemoryAllocated() {
        ini_set("user_agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
    }

}
