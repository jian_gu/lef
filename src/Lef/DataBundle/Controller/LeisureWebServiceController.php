<?php

namespace Lef\DataBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\MediaPartner;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Entity\RawLeisureTranchePrix;
use Doctrine\ORM\EntityManager;

class LeisureWebServiceController extends Controller {

    protected $em;
    protected $container;
    // Set username and password 
    private $user;
    private $pass;

    public function __construct(EntityManager $entityManager, $container = null) {
        $this->em = $entityManager;
        $this->container = $container;
        $this->user = 'ciel';
        $this->pass = 'ci9el3';
        // Disable log
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    public function getAndSaveHouseData($jsonString) {
        $em = $this->em;
        $housesCode = explode(",", $jsonString);
        // === LIMITATEUR!!! ===
        // Pout tester, limiter nombre à 300
        $housesCode = array_slice($housesCode, 0, 10);
        // truncate tranch-prix table
        $this->_truncateTable('raw_leisure_tranche_prix_dispo', $em);
        // Loop and save data
        foreach ($housesCode as $code) {
            $dataOfHouses = $this->dataOfHouses($code);
            $this->jsonSaveAnnonce($dataOfHouses);
            // $checkAvailability = $webService->checkAvailability($code, '2015-08-30', '2015-09-01', 168);
        }
    }

    /**
     * 
     * @param type $arr
     */
    public function jsonSaveAnnonce($arr) {
        $this->increaseMemoryAllocated();
        // Enable gabage collection
        gc_enable();
        $arr = json_decode(json_encode($arr), true);

        echo('<pre>');
        var_dump($arr);
        echo('</pre>');
        echo memory_get_usage() . "<br>";
        echo date('d/m/Y H:i:s') . '<br/>';
        die();
        if (isset($arr['LanguagePackFRV4'])) {
            $CostsOnSite = "";
            foreach ($arr['LanguagePackFRV4']['CostsOnSite'] as $key => $value) {
                $CostsOnSite .= $value['Description'] . ' : ' . $value['Value'] . "; \n";
            }

            $_distanceOther = "Distance :\n";
            foreach ($arr['DistancesV1'] as $key => $value) {
                $_distanceOther .= $value['To'] . ' à ' . $value['DistanceInKm'] . " km\n";
            }

            include 'pays.php';

            // Prepare data
            $reference = $arr['HouseCode'];
            $titre = $arr['BasicInformationV3']['Name'];
            $pays = $pays[$arr['BasicInformationV3']['Country']];
            $region = $arr['BasicInformationV3']['Region'];
            $ville = ($arr['LanguagePackFRV4']['City'] == "") ? $arr['LanguagePackFRV4']['City'] : $arr['LanguagePackFRV4']['SubCity'];
            $zipCodePostal = $arr['BasicInformationV3']['ZipPostalCode'];
            $longitude = $arr['BasicInformationV3']['WGS84Longitude'];
            $latitude = $arr['BasicInformationV3']['WGS84Latitude'];
            preg_match("/\d$/", $arr['BasicInformationV3']['MaxNumberOfPersons'], $nombreDePersonneArray);
            $nombreDePersonne = $nombreDePersonneArray[0];
            $nombreEtoiles = $arr['BasicInformationV3']['NumberOfStars'];
            $surfaceHabitable = $arr['BasicInformationV3']['DimensionM2'];
            preg_match("/\d$/", $arr['BasicInformationV3']['NumberOfBathrooms'], $nombreSalleDeBainArray);
            $nombreSalleDeBain = $nombreSalleDeBainArray[0];
            preg_match("/\d$/", $arr['BasicInformationV3']['NumberOfBedrooms'], $nombreChambreArray);
            $nombreChambre = $nombreChambreArray[0];
            $animauxDomestiques = $arr['BasicInformationV3']['NumberOfPets'];
            $descriptionBreve = $arr['LanguagePackFRV4']['ShortDescription'];
            $descriptionDetaillee = $arr['LanguagePackFRV4']['Description'];
            $caracteristiques = $arr['LanguagePackFRV4']['Remarks'] . "\n\n" . $CostsOnSite;
            $amenagement = $arr['LanguagePackFRV4']['LayoutSimple'];
            $distanceMer = $this->grabSingleDistance('Sea', $arr['DistancesV1']);
            $distanceLake = $this->grabSingleDistance('Lake', $arr['DistancesV1']);
            $distanceCenter = $this->grabSingleDistance('Centre', $arr['DistancesV1']);
            $distanceShopping = $this->grabSingleDistance('Foods', $arr['DistancesV1']);
            $distancePublicTransport = $this->grabSingleDistance('PublicTransport', $arr['DistancesV1']);
            $creationDate = new \DateTime($arr['BasicInformationV3']['CreationDate']);


            $MinPrice = 0;
            foreach ($arr["MinMaxPriceV1"] as $key => $value) {
                if ($MinPrice == 0) {
                    $MinPrice = $value['MinPrice'];
                } else {
                    if ($MinPrice > $value['MinPrice']) {
                        $MinPrice = $value['MinPrice'];
                    }
                }
            }


            $em = $this->em;
            // Try to find if the current location exists
            $curLocation = $em->getRepository('LefDataBundle:Location')->findOneByReference($arr['HouseCode']);

            // If location déjà existe, do the update
            if ($curLocation != null) {
                // Set
                $curLocation->setTitre($titre);
                $curLocation->setPays($pays);
                $curLocation->setRegion($region);
                $curLocation->setVille($ville);
                $curLocation->setZipPostalCode($zipCodePostal);
                $curLocation->setLongitude($longitude);
                $curLocation->setLatitude($latitude);
                $curLocation->setNombreDePersonne($nombreDePersonne);
                $curLocation->setNombreEtoiles($nombreEtoiles);
                $curLocation->setSurfaceHabitable($surfaceHabitable);
                $curLocation->setNombreSalleDeBain($nombreSalleDeBain);
                $curLocation->setNombreChambre($nombreChambre);
                $curLocation->setAnimauxDomestiques($animauxDomestiques);
                $curLocation->setDescriptionBreve($descriptionBreve);
                $curLocation->setDescriptionDetaillee($descriptionDetaillee);
                $curLocation->setCaracteristiques($caracteristiques);
                $curLocation->setAmenagement($amenagement);
                $curLocation->setDistanceMer($distanceMer);
                $curLocation->setDistanceLac($distanceLake);
                $curLocation->setDistanceCentreVille($distanceCenter);
                $curLocation->setDistanceShopping($distanceShopping);
                $curLocation->setDistancePublicTransport($distancePublicTransport);
                $curLocation->setDistanceOther($_distanceOther);
                $curLocation->setCreationDate($creationDate);
                $curLocation->setRentalprice($MinPrice);

                $em->flush();
                $em->clear();
                $curLocation = null;
                gc_collect_cycles();
            } else {
                // If it's a new location
                $newLocation = new Location();

                $newLocation->setReference($reference);
                $newLocation->setTitre($titre);
                $newLocation->setPays($pays);
                $newLocation->setRegion($region);
                $newLocation->setVille($ville);
                $newLocation->setZipPostalCode($zipCodePostal);
                $newLocation->setLongitude($longitude);
                $newLocation->setLatitude($latitude);
                $newLocation->setNombreDePersonne($nombreDePersonne);
                $newLocation->setNombreEtoiles($nombreEtoiles);
                $newLocation->setSurfaceHabitable($surfaceHabitable);
                $newLocation->setNombreSalleDeBain($nombreSalleDeBain);
                $newLocation->setNombreChambre($nombreChambre);
                $newLocation->setAnimauxDomestiques($animauxDomestiques);
                $newLocation->setDescriptionBreve($descriptionBreve);
                $newLocation->setDescriptionDetaillee($descriptionDetaillee);
                $newLocation->setCaracteristiques($caracteristiques);
                $newLocation->setAmenagement($amenagement);
                $newLocation->setDistanceMer($distanceMer);
                $newLocation->setDistanceLac($distanceLake);
                $newLocation->setDistanceCentreVille($distanceCenter);
                $newLocation->setDistanceShopping($distanceShopping);
                $newLocation->setDistancePublicTransport($distancePublicTransport);
                $newLocation->setDistanceOther($_distanceOther);
                $newLocation->setBrandPartner('leisure');
                $newLocation->setCreationDate($creationDate);
                $newLocation->setRentalprice($MinPrice);
                // Persist
                $em->persist($newLocation);
                $em->flush();
                $em->clear();
                $newLocation = null;
                gc_collect_cycles();
            }

            // Enregistrement des url images dans un tableau
            $medi_arr = $this->jsonSaveAnnonceMedia($arr['MediaV2'][0]['TypeContents']);

            // Ajout des images dans la base de données
            foreach ($medi_arr as $vm_r) {
                $curMedia = $em->getRepository('LefDataBundle:MediaPartner')->findOneBy(array('locationRef' => $arr['HouseCode'], 'titre' => $vm_r[0], 'src' => $vm_r[1]));
                if ($curMedia === null) {
                    $curMedia = new MediaPartner();
                    $curMedia->setTitre($vm_r[0]);
                    $curMedia->setSrc($vm_r[1]);
                    $curMedia->setLocationRef($arr['HouseCode']);
                    $em->persist($curMedia);
                    $em->flush();
                    $em->clear();
                    $curMedia = null;
                    gc_collect_cycles();
                }
            }

            // Save PLANNING, period 2 years?
            // ATTENTION!! Too much for saving, we don't do this, almost 6000 
            // Plannings for one single annonce'
            $this->jsonSaveAnnoncePlanning($reference, $arr['AvailabilityPeriodV1'], $em);
        } else {
            var_dump($arr);
        }
    }

    /**
     * Get and return single distance in meter
     * @param string $destination
     * @param array $distances
     * @return float
     */
    private function grabSingleDistance($destination, $distances) {
        $result = NULL;
        foreach ($distances as $distance) {
            $to = $distance['To'];
            if ($to == $destination) {
                $result = (float) $distance["DistanceInKm"] * 1000;
            }
        }
        return $result;
    }

    /**
     * 
     * @param type $arr
     */
    public function jsonSaveAnnoncePlanning($locationRef, $AvailabilityPeriodV1, $em) {
//        $em = $this->em;
        $this->increaseMemoryAllocated();
        // get nombre tranche prix par location
        $limitTPPL = $this->container->getParameter('leisure_tranche_prix_par_location');
        // Enable gabage collection
        gc_enable();
        // Set counter to 0
        $counter = 0;
        foreach ($AvailabilityPeriodV1 as $index => $value) {

            $ArrivalDate = date_create_from_format('Y-m-d', $value['ArrivalDate']);
            $DepartureDate = date_create_from_format('Y-m-d', $value['DepartureDate']);
            $onRequest = (strtolower($value['OnRequest']) == "yes") ? true : false;
            $price = $value['Price'];
            $priceExclDiscount = $value['PriceExclDiscount'];
//            $ArrivalTimeFrom = date_create_from_format('H:i', $value['ArrivalTimeFrom']);
//            $ArrivalTimeUntil = date_create_from_format('H:i', $value['ArrivalTimeUntil']);
//            $DepartureTimeFrom = date_create_from_format('H:i', $value['DepartureTimeFrom']);
//            $DepartureTimeUntil = date_create_from_format('H:i', $value['DepartureTimeUntil']);

            $new_TP = new RawLeisureTranchePrix();
            $new_TP->setStartDate($ArrivalDate);
            $new_TP->setEndDate($DepartureDate);
            $new_TP->setSurDemande($onRequest);
            $new_TP->setPrix($price);
            $new_TP->setRentalPrice($priceExclDiscount);
            $new_TP->setLocationRef($locationRef);
//            $new_TP->setHeureArrivee($ArrivalTimeFrom);
//            $new_TP->setHeureLimitArrivee($ArrivalTimeUntil);
//            $new_TP->setHeureDepart($DepartureTimeFrom);
//            $new_TP->setHeureLimitDepart($DepartureTimeUntil);

            $em->persist($new_TP);
            $em->flush();
            $em->clear();
            $new_TP = null;
            gc_collect_cycles();

//            $counter ++;
//            if ($counter >= 10) {
//                echo "<br/>" . memory_get_usage() . "<br>";
//                echo date('d/m/Y H:i:s') . '<br/>';
//                die('== 10 ==');
//            }
            if ($index >= $limitTPPL - 1) {
                break;
            }
        }
    }

    /**
     * 
     * @param type $arr
     * @return array
     */
    public function jsonSaveAnnonceMedia($arr) {
        $medi_arr = array();
        foreach ($arr as $key1 => $value1) {
            foreach ($value1['Versions'] as $key2 => $value2) {

                $img_titre = $value1['Tag'];
                $img_src = "http://" . $value2['URL'];
                if (count($medi_arr) >= 1) {
                    foreach ($medi_arr as $key3 => $value3) {
                        $can_push = false;
                        $current_url_arr = explode('/', $value3[1]);
                        $current_url_str = $value3[1];
                        $new_url = explode('/', $img_src);

                        if ($current_url_arr[4] == "web") {

                            preg_match("/_lsr_\K([^.]*)\./", $img_src, $output_array);
                            preg_match("/_lsr_\K([^.]*)\./", $value3[1], $output_array0);
                            if ($output_array[1] != $output_array0[1]) {
                                $can_push = true;
                            }
                        } else {
                            preg_match("/_lsr-b-\K([^.]*)\./", $img_src, $output_array1);
                            preg_match("/_lsr-b-\K([^.]*)\./", $value3[1], $output_array2);
                            if (isset($output_array1[1]) && isset($output_array2[1])) {
                                if ((substr($output_array1[1], 0, 2)) != (substr($output_array2[1], 0, 2))) {
                                    $can_push = true;
                                }
                            }
                        }
                    }

                    if ($can_push) {
                        array_push($medi_arr, array($img_titre, $img_src));
                    }
                } else {
                    array_push($medi_arr, array($img_titre, $img_src));
                }
            }
        }
        // var_dump($medi_arr); die();
        return $medi_arr;
    }

    /**
     * Leisure get data house v
     * @param type $partner
     * @param type $houseCode
     * @return type
     */
    public function dataOfHouses($houseCode) {
        $rows = array();
        $ws_client = "https://dataofHousesv1.jsonrpc-partner.net/cgi/lars/jsonrpc-partner/jsonrpc.htm";

        /* The JSON method name is part of the URL. use only lowercase characters in the URL */
        $post_data = array(
            'jsonrpc' => '2.0',
            'method' => 'DataOfHousesV1', /* Methodname is part of the URL */
            'params' => array(
                'WebpartnerCode' => $this->user,
                'WebpartnerPassword' => $this->pass,
                'HouseCodes' => array($houseCode),
                'Items' => array(
                    'BasicInformationV3',
                    'MediaV2',
                    'LanguagePackFRV4',
                    'MinMaxPriceV1',
                    'AvailabilityPeriodV1',
                    'DistancesV1',
                ),
            ),
            'id' => 211897585 /* a unique integer to identify the call for synchronisation */
        );

        $res = $this->initCurl($ws_client, $post_data);
//        echo('<pre>');
//        var_dump($res);
//        echo('</pre>');
//        die();
        return current($res);
    }

    public function checkAvailability($houseCode, $ArrivalDate, $DepartureDate, $Price) {
        $ws_client = "https://checkavailabilityv1.jsonrpc-partner.net/cgi/lars/jsonrpc-partner/jsonrpc.htm";

        /* The JSON method name is part of the URL. use only lowercase characters in the URL */

        $post_data = '{
			"jsonrpc": "2.0",
			"method": "CheckAvailabilityV1",
			"params": {
				"WebpartnerCode": "' . $this->user . '",
				"WebpartnerPassword": "' . $this->pass . '",
				"HouseCode": "' . $houseCode . '",
				"ArrivalDate": "' . $ArrivalDate . '",
				"DepartureDate": "' . $DepartureDate . '",
				"Price": ' . $Price . '
			},
			"id":   211897585
		}';

        // $post_data = ;

        $res = $this->initCurl($ws_client, json_decode($post_data, true));

        return $res;
    }

    public function initCurl($ws_client, $post_data) {
        $rows = array();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ws_client);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type: application/json'));
//        curl_setopt($ch, CURLOPT_SSLVERSION, 3); /* Due to an OpenSSL issue */
        curl_setopt($ch, CURLOPT_SSLVERSION, 'CURL_SSLVERSION_TLSv1'); /* Due to some php version issue */
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);  /* Due to a wildcard certificate */
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_ENCODING, 1); /* If result is gzip then unzip */
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if ($result = curl_exec($ch)) {
            $this->increaseMemoryAllocated();
            if ($res = json_decode($result)) {
                $rows = $res;
            } else {
                echo json_last_error();
            }
        } else {
            var_dump(curl_error($ch));
        }
        curl_close($ch);

        return property_exists($rows, 'result') ? $rows->result : $rows->error;
    }

    /**
     * Truncate a table
     * @param type $tableName
     * @param type $em
     */
    private function _truncateTable($tableName, $em) {

        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();

        $connection->executeUpdate($platform->getTruncateTableSQL($tableName, true /* whether to cascade */));
    }

    /**
     * Increase memory, incase of crash
     */
    public function increaseMemoryAllocated() {
        ini_set("user_agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
    }

}
