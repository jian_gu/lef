<?php

namespace Lef\DataBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Entity\Media;
use Lef\DataBundle\Entity\Type;
use Lef\DataBundle\Entity\TranchePrix;
use Lef\DataBundle\Form\LocationType;
use Lef\BoBundle\Mailer\MailerInterface;
use Symfony\Component\Form\FormError;
use Lef\DataBundle\Entity\StatVusLocation;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Location controller.
 *
 */
class LocationController extends Controller {

    protected $passerEnPromo = false;

    /**
     * Lists all Location entities.
     *
     */
    public function indexAction() {
        // if no user connected
        if ($this->_getConnectedUser() == 'anon.') {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $currentUserRoles = $this->_getConnectedUser()->getRoles();
        $currentUserRole = $currentUserRoles[0];
        $arry_roles_admin = array('ROLE_ADMIN', 'ROLE_SONATA_ADMIN', 'ROLE_SUPER_ADMIN');
        $em = $this->getDoctrine()->getManager();

        // Show list according to user role
        if (in_array($currentUserRole, $arry_roles_admin)) {
            $entities = $em->getRepository('LefDataBundle:Location')->findAll();
        } else if ($currentUserRole == 'ROLE_ANNONCEUR') {
            $arry_ref_annonceur = array('annonceur' => $this->_getConnectedUser());
            $entities = $em->getRepository('LefDataBundle:Location')->findBy($arry_ref_annonceur);
        } else {
            //            throwException('Vous n\'avez pas droit d\'acceder à cette page.');
            //            return;
            // set flash message                                                                        // Modifier le 250814 par Jian
            $this->_createFlashMessage('notice', 'Vous n\'avez pas droit d\'acceder à cette page.');    // Modifier le 250814 par Jian
            return $this->redirect($this->generateUrl('fos_user_profile_show'));                        // Modifier le 250814 par Jian
        }


        // KNP pagenating
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $entities, $this->get('request')->query->get('page', 1)/* page number */, 5/* limit per page */
        );

        return $this->render('LefDataBundle:Location:index.html.twig', array(
                    'entities' => $pagination,
        ));
    }

    /**
     * Creates a new Location entity.
     *
     */
    public function createAction(Request $request) {
        $requestParam = $request->request->get('lef_databundle_location');
        $countryCode = $requestParam['pays'];
        $postCode = $requestParam['ZipPostalCode'];

        $entity = new Location();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            // Set Id manually
            //            $entity->setId($this->getLastId() + 1);
            //            
            // generate reference for current annonce
            $currentRef = $this->generateCurrentReference($countryCode, $postCode);
            // Set ref as session variable
            $this->get('session')->set('currentAnonceRef', $currentRef);
            // Set reference
            $entity->setReference($currentRef);
            // Set creation date to current
            $entity->setCreationDate(date_create(date('Y-m-d H:i:s')));
            // Duplicate / Set Update date to current
            $entity->setModificationDate(date_create(date('Y-m-d H:i:s')));
            // Set annonceur
            $entity->setAnnonceur($this->_getConnectedUser());
            // Set last minute
            $entity->setAdm(0);
            // Set promo
            $entity->setPromo(0);
            // Set locked
            $entity->setLocked(0);
            // Set medias' srcs
            $this->_setSrcsImage($entity->getMedias());

            //default titre tranche de prix
            $entity->getTranchePrix()[0]->setTitre("prix de base");

//            set disponibilités
            $dateDebut = $entity->getDateDispoDebut();
            $planning = new \Lef\DataBundle\Entity\Planning();

            $planning->setDate($dateDebut);
            $planning->setIspartner(FALSE);

            $dateDiff = $dateDebut->diff($entity->getDateDispoFin(), true);

            $chaineDispo = "";
            for ($i = 0; $i <= $dateDiff->days; $i++) {
                $chaineDispo .="1";
            }

            $planning->setDispo($chaineDispo);

            $em->persist($entity);
            $planning->setIdLocation($entity);
            $em->persist($planning);
//            echo('<pre>');
//            var_dump($entity);
//            echo('</pre>');
//            die();
            $em->flush();

            // Add suffix to Reference
            $ref1 = $entity->getReference();
            $newId = $entity->getId();
            $refComplete = $ref1 . $newId;
            // Set ref as session variable
            $this->get('session')->set('currentAnonceRef', $refComplete);
            $entity->setReference($refComplete);
            // Set medias' srcs
            $this->_setSrcsImage($entity->getMedias());
            // Upload image for media
            $this->_uploadMediaImage($entity->getMedias());
//            echo('<pre>');
//            var_dump($entity);
//            echo('</pre>');
//            die();
            // calculer free announce, disponibilité - 1
            $entity->getAnnonceur()->setNbrAnnonce($entity->getAnnonceur()->getNbrAnnonce() - 1);

            $em->persist($entity);
            $em->flush();

            // send email
            $this->_sendMail($entity, 'c');

            // empty session variables
            $this->get('session')->remove('currentAnonceRef');

            // set flash message
            $this->_createFlashMessage('success', 'Votre annonce a été créée avec succès.');

            return $this->redirect($this->generateUrl('location_show', array('reference' => $entity->getReference(),
                                'pays' => $entity->getPays(),
                                'region' => $entity->getRegion(),
                                'ville' => $entity->getVille())));
        }

        return $this->render('LefDataBundle:Location:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Location entity.
     *
     * @param Location $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Location $entity) {

        $form = $this->createForm(new LocationType(), $entity, array(
            'action' => $this->generateUrl('location_create'),
            'method' => 'POST',
        ));


        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Location entity.
     *
     */
    public function newAction() {
        $annonceur = $this->_getConnectedUser();
        // Si l'utilisateur connecté est un visiteur                            // Ajouté le 250814 par Jian
        $currentUserRoles = $annonceur->getRoles();                             // Ajouté le 250814 par Jian
        $currentUserRole = $currentUserRoles[0];                                // Ajouté le 250814 par Jian
        if ($currentUserRole == 'ROLE_VISITEUR') {                              // Ajouté le 250814 par Jian
            // set flash message                                                                        // Modifier le 250814 par Jian
            $this->_createFlashMessage('notice', 'Vous n\'avez pas droit d\'acceder à cette page.');    // Modifier le 250814 par Jian
            return $this->redirect($this->generateUrl('fos_user_profile_show'));                        // Ajouté le 250814 par Jian
        }                                                                       // Ajouté le 250814 par Jian
        // Si un annonceur n'a plus droit de passer une annonce'
        if ($annonceur->getNbrAnnonce() <= 0) {
            $this->_createFlashMessage('warning', 'Vous n\'avez plus droit de passer une annonce gratuitement!');
            return $this->indexAction();
        }

        $em = $this->getDoctrine()->getManager();
//        $entity = new Location();
//        $form = $this->createCreateForm($entity);
        // Add embedded forms 3 for media
        $entity = new Location();

        // dummy code - this is here just so that the Task has some tags
        // otherwise, this isn't an interesting example
        // === Forms Media 3 Min ===
        $m1 = new Media();
        $entity->getMedias()->add($m1);
        $m2 = new Media();
        $entity->getMedias()->add($m2);
        $m3 = new Media();
        $entity->getMedias()->add($m3);
        // === Forms TranchePrix ===
//        $arryTitreTranchePrix = $em->getRepository('LefDataBundle:TranchePrixLabel')->getTranchesPrixLabels();
//        foreach ($arryTitreTranchePrix as $key => $value) {
        $tp = new TranchePrix();
        $entity->addTranchePrix($tp);
//        }
        // end dummy code
        $form = $this->createCreateForm($entity);


        return $this->render('LefDataBundle:Location:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Location entity.
     *
     */
    public function showAction($id, $mode) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Location')->find(array('id' => $id));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Location entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        switch ($mode) {
            case 'v':
                // Save Count visit click to consultation count table
                $this->_saveCountVusation($entity);
                // Insert values to Location table
                $this->_insertCountVusation($entity);

                return $this->render('LefDataBundle:Location:show.html.twig', array(
                            'entity' => $entity,
                ));
                break;
            case 'a':

                return $this->render('LefDataBundle:Location:show.html.twig', array(
                            'entity' => $entity,
                            'delete_form' => $deleteForm->createView(),));
                break;
            default:
                break;
        }
    }

    /**
     * Displays a form to edit an existing Location entity.
     *
     */
    public function editAction($id) {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Location')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Location entity.');
        }

        // dummy code - this is here just so that the Task has some tags        // Added le 150814 par Jian
        // otherwise, this isn't an interesting example
        // === Forms Media 3 Min ===
        $m1 = new Media();
        $entity->getMedias()->add($m1);
        $m2 = new Media();
        $entity->getMedias()->add($m2);
        $m3 = new Media();
        $entity->getMedias()->add($m3);

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LefDataBundle:Location:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'id' => $entity->getId(),
        ));
    }

    /*
     * @Autor Abdel
     * traitement ajax de la mise à jour du calendrier associées a des tranches de prix pour une annonce   
     */

    public function addTranchePrixAction() {

        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {
            $id = $request->get("idLocation");
            $tranchePrix = new \Lef\DataBundle\Entity\Trancheprix();
            $tranchePrix->setTitre($request->get("titrePeriod"));
            $tranchePrix->setDay($request->get("priceOneDay"));
            $tranchePrix->setWeekend($request->get("priceWeekEnd"));
            $tranchePrix->setWeek($request->get("priceOneWeek"));
            $tranchePrix->setWeeks2($request->get("priceTwoWeek"));
            $tranchePrix->setWeeks3($request->get("priceThreeWeek"));
            $tranchePrix->setMonth($request->get("priceMonth"));
//            $dateArrivee = \DateTime::createFromFormat("dd/mm/yyyy", $request->get("dateBeginPeriod"));
            $dateDebut = new \DateTime();
            $tdate = explode("/", $request->get("dateBeginPeriod"));
            $dateDebut->setDate($tdate[2], $tdate[1], $tdate[0]);
            $tranchePrix->setDateDebut($dateDebut);
//            $dateDepart = \DateTime::createFromFormat("dd/mm/yyyy", $request->get("dateFinishPeriod")); 
            $tdate = explode("/", $request->get("dateFinishPeriod"));
            $dateFin = new \DateTime();
            $dateFin->setDate($tdate[2], $tdate[1], $tdate[0]);
            $tranchePrix->setDateFin($dateFin);

            $em = $this->get("doctrine")->getManager();
            $entity = $em->getRepository('LefDataBundle:Location')->find($id);

            $entity->addTranchePrix($tranchePrix);

//            var_dump();
//            exit();
            $em->persist($entity);
            $em->flush();

            $tTranchePrix = array();
            $serializer = $this->container->get('serializer');
            foreach ($entity->getTranchePrix() as $trancheP) {

                $dd = $trancheP->getDateDebut()->format('d/m/y');
                $df = $trancheP->getDateFin()->format('d/m/y');
                $tTranchePrix[] = array($trancheP, $dd, $df);
            }

            $data = $serializer->serialize($tTranchePrix, 'json');
            return new Response($data);
        }
    }

    public function majplanningAction() {

        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {
            $id = $request->get("idLocation");
            $datesPlanning = $request->get("datePlanning");
            $statut = $request->get("statut");
            $em = $this->get("doctrine")->getManager();
            $entity = $em->getRepository('LefDataBundle:Location')->find($id);
            $planning = $em->getRepository('LefDataBundle:Planning')->findOneBy(array('idLocation' => $entity));

            $dispo = $planning->getDispo();

            foreach ($datesPlanning as $date) {
                $datemodif = new \DateTime();
                $tdatemodif = explode("/", $date);
                $datemodif->setDate($tdatemodif[2], $tdatemodif[1], $tdatemodif[0]);
                $dateDiff = $entity->getDateDispoDebut()->diff($datemodif, true);
                $dispo = substr_replace($dispo, $statut, $dateDiff->days, 1);
            }
            $planning->setDispo($dispo);
            $em->persist($planning);
            $em->flush();

            $tdispo = array();
            $tdispo[] = array(substr($dispo, 0, 1) => $planning->getDate()->format("d/m/Y"));
          
            for ($i = 1; $i <  strlen($dispo) ; $i++) {
                $date = new \DateTime();
                $interval = null;
                if ($i != 1) {
                    $interval = \DateInterval::createFromDateString("$i days");
                } else if ($i == 1) {
                    $interval = \DateInterval::createFromDateString("$i day");
                }
                
                $date = $planning->getDate()->add($interval);
                $dstr = $date->format("d/m/Y");
//                $d = $planning->getDate()->diff($date, true);
                $valDispo = substr($dispo, $i, 1);
                $tdispo[] = array(substr($dispo, $i, 1)=> $dstr );
            }

            return new Response(json_encode($tdispo));
        }
    }

    /**
     * Creates a form to edit a Location entity.
     *
     * @param Location $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Location $entity) {
        $form = $this->createForm(new LocationType(), $entity, array(
            'action' => $this->generateUrl('location_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        // Add field ADM to the form
        $form->add('adm', null, array('required' => FALSE));
        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Location entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Location')->find($id);
        $formerStatADM = $entity->getAdm();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Location entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $currentStatADM = $editForm['adm']->getData();
//        var_dump($entity->getAdm());die();
        // If the annonceur can pass an ADM
        $this->_ifCanPasserEnADM($entity->getAnnonceur()->getCreditPromo(), $editForm, 'adm', $formerStatADM, $currentStatADM);

        if ($editForm->isValid()) {
            $this->get('session')->set('currentAnonceRef', $entity->getReference());
            $entity->setModificationDate(date_create(date('Y-m-d H:i:s')));

            // Set medias' srcs
            $this->_setSrcsImage($entity->getMedias());
            // Upload image for media
            $this->_uploadMediaImage($entity->getMedias());
            // Unlock it if is locked by system due to not updated in 33 days
            $this->_reactivate($entity);
            // Passer en ADM
            $this->_passerEnADM($entity, $formerStatADM, $currentStatADM);

            $em->flush();

            // send email
            $this->_sendMail($entity, 'u');
            // Unset session variable
            $this->get('session')->remove('currentAnonceRef');

            // reset passerPromo indicator to false
            $this->passerEnPromo = FALSE;

            // set flash message
            $this->_createFlashMessage('success', 'Votre annonce a été modifiée.');

            return $this->redirect($this->generateUrl('location_edit', array('id' => $id)));
        }

        return $this->render('LefDataBundle:Location:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Location entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LefDataBundle:Location')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Location entity.');
            }

            // retrieve media foler name
            $mediaFolderName = $entity->getReference();

            $em->remove($entity);
            $em->flush();

            // delete media image folder, if it exists
            $mediaSrc = 'uploads/images/location/' . $mediaFolderName;
            $this->rrmdir($mediaSrc);
        }

        return $this->redirect($this->generateUrl('location_annonceur'));
    }

    /**
     * Creates a form to delete a Location entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('location_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    // =========================
    // ==== custom functions====
    // =========================
    /**
     * Function get last Id
     * @return integer
     */
    private function getLastId() {
        $em = $this->getDoctrine()->getManager();
        $lastId = $em->getRepository('LefDataBundle:Location')->getLastId();
        return $lastId['id'];
    }

    /**
     * get number of the same annonceur with same pays, CP, 
     * @param type $countryCode
     * @param type $postCode
     * @return type
     */
    private function getMySameAnnonces($countryCode, $postCode) {
        $em = $this->getDoctrine()->getManager();
        $conditions = array(
            'annonceur' => $this->_getConnectedUser()->getId(),
            'pays' => $countryCode,
            'cp' => $postCode
        );
        $aCount = $em->getRepository('LefDataBundle:Location')->getNumAnnonceSameConditions($conditions);
        return $aCount;
    }

    /**
     * Function generate reference for current annonce
     * @param string $countryCode
     * @param string $postCode
     * @return string
     */
    private function generateCurrentReference($countryCode, $postCode) {
        // Format: LEF-IdAnnonceur-FR-CP-(id+1)
        $countryCode = '' ? '99' : $countryCode;
        $postCode = '' ? '00000' : $postCode;
        // add prefix
        $ref = 'LEF-';
        // joint userId
        $ref.= (string) $this->_getConnectedUser()->getId() . '-';
        // Join country code
        $ref.=$countryCode . '-';
        // join CP
        $ref.=$postCode . '-';
        // join id suffix !!NOT USED!!
        // $ref.=(string) $this->getMySameAnnonces($countryCode, $postCode) + 1;

        return $ref;
    }

    /**
     * function get current connected user
     * @return object
     */
    private function _getConnectedUser() {
        if (!$this->get('security.context')->getToken()->getUser()) {
            throw $this->createNotFoundException('Utilisateur non connecté.');
        }
        return $this->get('security.context')->getToken()->getUser();
    }

    /**
     * Function to set media src
     * @param ArrayCollection $medias
     * @return ArrayCollection
     */
    private function _setSrcsImage($medias) {
        // generate folder name as reference
        $folderName = $this->get('session')->get('currentAnonceRef');

        foreach ($medias as $media) {
            // if image exist
            if ($media->getFile()) {
                $fileName = $media->getFile()->getClientOriginalName();
                // set local url for image
                $media->setSrc('/uploads/images/location/' . $folderName . '/' . $fileName);
            }
        }
        return $medias;
    }

    /**
     * Function upload media images
     * @param ArrayCollection $medias
     * @return ArrayCollection
     */
    private function _uploadMediaImage($medias) {
        // generate folder name as reference
        $folderName = $this->get('session')->get('currentAnonceRef');

        foreach ($medias as $media) {
            // upload file
            $media->upload('uploads/images/location/' . $folderName);
        }
        return $medias;
    }

    /**
     * Sending email
     * @param entity $location
     * @param string $action, crud
     */
    private function _sendMail($location, $action) {
        $annonceur = $this->_getConnectedUser();
        $mailer = $this->get('lef_bo.mailer.twig_swift');
        switch ($action) {
            case 'c':
                $mailer->sendCreateLocationEmailMessage($annonceur, $location);
                break;
            case 'u':
                $mailer->sendUpdateLocationEmailMessage($annonceur, $location);
                break;

            default:
                break;
        }
        if ($this->passerEnPromo) {
            $mailer->sendPasserPromoEmailMessage($annonceur, $location);
        }
    }

    /**
     * When the directory is not empty:
     * @param type $dir
     */
    private function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir")
                        rrmdir($dir . "/" . $object);
                    else
                        unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    /**
     * Create flash message
     * @param string $type (notice, success...)
     * @param text $contents
     */
    private function _createFlashMessage($type, $contents) {
        $this->get('session')->getFlashBag()->add(
                $type, $contents
        );
    }

    /**
     * Function reactivate announce if it's locked
     * @param entity $annonce
     * @return type
     */
    private function _reactivate($annonce) {
        $suspendu = $annonce->getLocked();
        if ($suspendu) {
            $annonce->setLocked(0);
        }
        return;
    }

    /**
     * Check if the announcer can pass an ADM
     * @param type $creditAvailable
     * @param type $form
     * @param type $fieldName
     */
    private function _ifCanPasserEnADM($creditAvailable, $form, $fieldName, $statBefore, $statNow) {
        // If no credit avaible
        if (!$statBefore && $statNow && $creditAvailable <= 0) {
            $form->get($fieldName)->addError(new FormError('Veuillez commander des crédits ADM.'));
        }
    }

    /**
     * make an announce into ADM
     * @param type $annonceur
     * @param type $statBefore
     * @param type $statNow
     */
    private function _passerEnADM($location, $statBefore, $statNow) {
        $annonceur = $location->getAnnonceur();
        $creditAvailable = $annonceur->getCreditPromo();

        // If before is 0, now is 1: en Adm now
        if ($statBefore == false && $statNow == true) {
            $this->passerEnPromo = TRUE;
            $annonceur->setCreditPromo($creditAvailable - 1);
        }
    }

    /**
     * Function save visit count
     * @param object $location
     */
    private function _saveCountVusation($location) {
        $em = $this->getDoctrine()->getManager();
        $entity = new StatVusLocation();
        $visiteur = $this->_getConnectedUser();

        if (is_object($visiteur)) {
            $entity->setVisiteur($visiteur);
        }
        $entity->setLocation($location);
        $entity->setDate(new \DateTime());

        $em->persist($entity);
        $em->flush();
    }

    /**
     * Function Insert numbre of consultation of the actul annonce into table Location
     * @param object $location
     * @return array ['all', '30days']
     */
    private function _insertCountVusation($location) {
        $em = $this->getDoctrine()->getManager();
        $jourFilant = $this->container->getParameter('jours_filant_stat_vus_location');

        $consultAll = $em->getRepository('LefDataBundle:StatVusLocation')->findBy(array('location' => $location));
        $consult30D = $em->getRepository('LefDataBundle:StatVusLocation')->find30DaysByLocation($location, $jourFilant);

        $nbrVusAll = count($consultAll);
        $nbrVus30D = count($consult30D);

        $location->setNbrVusAll($nbrVusAll);
        $location->setNbrVus30j($nbrVus30D);
        $em->persist($location);
        $em->flush();
    }

}
