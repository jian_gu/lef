<?php

namespace Lef\DataBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lef\DataBundle\Entity\Mapping;
use Lef\DataBundle\Form\MappingType;

/**
 * Mapping controller.
 *
 * @Route("/mapping")
 */
class MappingController extends Controller
{

    /**
     * Lists all Mapping entities.
     *
     * @Route("/", name="mapping")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LefDataBundle:Mapping')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Mapping entity.
     *
     * @Route("/", name="mapping_create")
     * @Method("POST")
     * @Template("LefDataBundle:Mapping:new.html.twig")
     */
    public function createAction(Request $request, $filename, $partner)
    {
        $entity = new Mapping();

        $entity->setPartner($partner);
        $entity->setfileName($filename);

        $form = $this->createCreateForm($entity,$filename, $partner);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('mapping_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Mapping entity.
    *
    * @param Mapping $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Mapping $entity, $filename, $partner)
    {
        $form = $this->createForm(new MappingType(), $entity, array(
            'action' => $this->generateUrl('mapping_create', array('filename'=>$filename,'partner'=>$partner)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Mapping entity.
     *
     * @Route("/new", name="mapping_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($filename, $partner)
    {
        $entity = new Mapping();

        $entity->setPartner($partner);
        $entity->setfileName($filename);
        $form   = $this->createCreateForm($entity, $filename, $partner);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Mapping entity.
     *
     * @Route("/{id}", name="mapping_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Mapping')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mapping entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Mapping entity.
     *
     * @Route("/{id}/edit", name="mapping_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Mapping')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mapping entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Mapping entity.
    *
    * @param Mapping $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Mapping $entity)
    {
        $form = $this->createForm(new MappingType(), $entity, array(
            'action' => $this->generateUrl('mapping_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Mapping entity.
     *
     * @Route("/{id}", name="mapping_update")
     * @Method("PUT")
     * @Template("LefDataBundle:Mapping:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Mapping')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mapping entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('mapping_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Mapping entity.
     *
     * @Route("/{id}", name="mapping_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LefDataBundle:Mapping')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Mapping entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('mapping'));
    }

    /**
     * Creates a form to delete a Mapping entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mapping_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
