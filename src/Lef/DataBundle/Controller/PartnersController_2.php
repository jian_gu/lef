<?php

// save 11/06 17h15

namespace Lef\DataBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Controller\DownloadController;

use Lef\DataBundle\Entity\Alert;
use Lef\DataBundle\Entity\BienEtre;
use Lef\DataBundle\Entity\Equipement;
use Lef\DataBundle\Entity\Localisation;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Entity\Media;
use Lef\DataBundle\Entity\Offre;
use Lef\DataBundle\Entity\OptionEnfant;
use Lef\DataBundle\Entity\OptionHandicapees;
use Lef\DataBundle\Entity\Planning;
use Lef\DataBundle\Entity\Saison;
use Lef\DataBundle\Entity\Theme;
use Lef\DataBundle\Entity\TypeLocation;

use Lef\DataBundle\Entity\Partners;
use Lef\DataBundle\Entity\Maping;
use Lef\DataBundle\Entity\Annonces;
use Lef\DataBundle\Entity\FileMap;
use Lef\DataBundle\Form\PartnersType;

/**
 * Partners controller.
 *
 */
class PartnersController extends Controller
{

    private $array_xml_attribute = array();
    private $values_seek = array();
    private $em;

    public function __construct(){
        
    }

    public function getArray_xml_attribute()
    {
        return $this->array_xml_attribute;
    }

    /**
     * Lists all Partners entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LefDataBundle:Partners')->findAll();

        return $this->render('LefDataBundle:Partners:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Partners entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Partners();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('partners_show', array('id' => $entity->getId())));
        }

        return $this->render('LefDataBundle:Partners:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Partners entity.
     *
     * @param Partners $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Partners $entity)
    {
        $form = $this->createForm(new PartnersType(), $entity, array(
            'action' => $this->generateUrl('partners_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Partners entity.
     *
     */
    public function newAction()
    {
        $entity = new Partners();
        $form   = $this->createCreateForm($entity);

        return $this->render('LefDataBundle:Partners:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Partners entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Partners')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Partners entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LefDataBundle:Partners:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Partners entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Partners')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Partners entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LefDataBundle:Partners:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Partners entity.
     *
     * @param Partners $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Partners $entity)
    {
        $form = $this->createForm(new PartnersType(), $entity, array(
            'action' => $this->generateUrl('partners_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Partners entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Partners')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Partners entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            // return $this->redirect($this->generateUrl('partners'));
            return $this->redirect($this->generateUrl('partners_config', array('id' => $id)));
        }

        return $this->render('LefDataBundle:Partners:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Partners entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LefDataBundle:Partners')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Partners entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('partners'));
    }

    /**
     * Creates a form to delete a Partners entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('partners_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer le partner', 'attr'=>array('class'=>'btn btn-primary btn-sm')))
            ->getForm()
        ;
    }

    public function syncAction($id, Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Partners')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Partners entity.');
        }
        $ftpId = $entity->getId();
        $ftpUser = $entity->getUser();
        $ftpHost = $entity->getHost();
        $ftpPassword = $entity->getPassword();
        $ftpSrcDir = $entity->getSrcDirectory();
        $ftpFiles = $entity->getFiles();

        $targetFtp = new DownloadController();
        if (is_dir('temp/'.$ftpId.'/')) 
        {
            $targetFtp->delTree('temp/'.$ftpId.'/');
        }
        else
        {
            while (!is_dir('temp/'.$ftpId.'/')) {
                mkdir('temp/'.$ftpId.'/', 0777);
                chmod('temp/'.$ftpId.'/', 0777);
            }
            if (is_dir('temp/'.$ftpId.'/')) {
                while (!is_dir('temp/'.$ftpId.'/zipfiles/')) {
                    mkdir('temp/'.$ftpId.'/zipfiles/', 0777);
                    chmod('temp/'.$ftpId.'/zipfiles/', 0777);
                    fopen('temp/'.$ftpId.'/zipfiles/.htaccess',"wb");
                }
                while (!is_dir('temp/'.$ftpId.'/data/')) {
                    mkdir('temp/'.$ftpId.'/data/', 0777);
                    chmod('temp/'.$ftpId.'/data/', 0777);
                    fopen('temp/'.$ftpId.'/data/.htaccess',"wb");
                }
            }
        }

        // On verifie si le dossier de stockage des fichier zip a bien étais créer 
        // puis e=on lance la téléchagement des fichier 
        if(is_dir('temp/'.$ftpId.'/zipfiles/')){       
            if (in_array($entity->getDataType(), array('XML', 'xml'))) {
                $fileToGet = explode(', ', $ftpFiles);
                $targetFtp->syncFtpAction("$ftpHost","$ftpUser","$ftpPassword",".",'temp/'.$ftpId.'/zipfiles/', $fileToGet, "$ftpSrcDir", $request);
            }else{
                $targetFtp->saveFulx($entity, $path = 'temp/'.$ftpId.'/zipfiles/');
            } 
        }
        else
        {
            $this->syncAction($id, $request);
        }

        return $this->redirect($this->generateUrl('partners_config', array('id'=> $ftpId)));
    }

    public function unpackAction($id)
    {
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Partners')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Partners entity.');
        }
        $ftpId = $entity->getId();

        $targetFtp = new DownloadController();
        $tempzips = glob('temp/'.$ftpId.'/zipfiles/*');
        $is_unzip = false;
        $fileUploaded = "<ul>";
        foreach($tempzips as $tempzip){ 
            $is_unzip = $targetFtp->unzip($tempzip,'temp/'.$ftpId.'/data/');
            if ($is_unzip) {
                $fileUploaded .= "<li>".$tempzip."</li>";
            }else{
                $fileUploaded .= "<li> 0 </li>";
            }
        }
        $fileUploaded .= "</ul>";
        return $this->redirect($this->generateUrl('partners_config', array('id'=> $id)));
    }

    public function configAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $partner_repo = $em->getRepository('LefDataBundle:Partners');
        $mapping_repo = $em->getRepository('LefDataBundle:Mapping');
        $location_repo = $em->getRepository('LefDataBundle:Location');
        
        $partner = $partner_repo->find($id);

        if (!$partner) {
            throw $this->createNotFoundException('Unable to find Partners partner.');
        }

        $editForm = $this->createEditForm($partner);

        /***********************************************/
        // On recupère les fichier déziper pour le partner courant
        $dataFolder_isEmpty = $zipFolder_isEmpty = true;
        $link = null;
        $files = array();
        if (is_dir('temp/'.$id.'/')) {
            $dataFolderContent = scandir('temp/'.$id.'/data/');
            $zipFolderContent = scandir('temp/'.$id.'/zipfiles/');
            
            $dataFolder_isEmpty = (count($dataFolderContent)>3) ? false : true;
            $zipFolder_isEmpty = (count($zipFolderContent)>3) ? false : true;

            if ($zipFolder_isEmpty) {
                $link['name'] = "Télécharger les données";
                $link['action'] = "partners_sync";
                $link['icon'] = "glyphicon-download-alt";
                $link['msg'] = "Aucune données n'a été télécharger";
            } elseif($dataFolder_isEmpty) {
                $link['name'] = "décompréssés les données";
                $link['action'] = "partners_unpack";
                $link['icon'] = "glyphicon-log-out";
                $link['msg'] = "Aucune données n'a été décompréssés";
            }

            foreach ($dataFolderContent as $key => $file) {
                if ($file == '.' || $file == '..' || $file == '.htaccess'){continue;}
                $files[] = $file;
                $fileMap = $em->getRepository('LefDataBundle:FileMap')->findOneBy(array('fileName'=>$file, 'partnerId'=>$id));
                if ($fileMap == null) {
                    $fileMap = new FileMap();
                    $fileMap->setPartnerId($id);
                    $fileMap->setFileName($file);
                    $em->persist($fileMap);
                    $em->flush();
                }
            }
        }

        // Augementation du temps limit d'execution et de la memoir limit pour lire les longs fichier xml
        ini_set("user_agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");

        // Pour chaque fichier en fonction du partner
        foreach ($files as $key => $file) :

            $mappingByFilename = $mapping_repo->findBy(array("filename"=>$file));

            // Si mon fichier est mapper
            if (!empty($mappingByFilename)) :

                $xml =  simplexml_load_file('temp/'.$id.'/data/'.$file);
                $xml_to_json = json_encode($xml);
                $json_to_array = json_decode($xml_to_json, true);

                // Pour tout les entrées de Mapping avec le nom du fichier courant
                foreach ($mappingByFilename as $key => $map) :

                    $local_Location = $this->arraySeek($json_to_array, $map->getLimiteLocation() );

                    // Instanciation d'une location
                    $new_loc = new Location();
 
                    // On cherche le tableau avec la cle $map->getLimiteLocation
                    // Parcour des annonces dans le fichier XML
                    foreach ($local_Location[$map->getLimiteLocation()] as $kll => $vll):

                        // On récupère la dénière paramètre dans le path
                        $c_xpath = str_replace($file, "", $map->getXpath());
                        $c_xpath = str_replace($map->getLimiteLocation(), "", $c_xpath);
                        $c_xpath = str_replace(".", "", $c_xpath);

                        // On récupère le nom l'attribut de location dans l'entité du mapping courrant
                        $refto_exploded = explode(" // ", $map->getRefto());
                        $refto = end($refto_exploded);


                        if ($refto == "reference") :

                            $new_loc1 = $location_repo->findOneByReference( $vll[$c_xpath] );

                            if ($new_loc1 != null) :
                                // $new_loc1->setReference($vll[$c_xpath]);
                            else:
                                $new_loc1 = $new_loc;
                                // $new_loc1->setReference($vll[$c_xpath]);                      
                            endif;  

                        endif;

                        if($refto != "reference" && isset($new_loc1)):

                            $_setter = ucfirst($refto);
                            $_setter = 'set'.$_setter;
                            $new_loc1->$_setter($vll[$c_xpath]);
    
                            var_dump($new_loc1);
    
                        endif;

                    endforeach;
                    
                endforeach;
            endif;
        endforeach;

        // mapping
        foreach ($files as $key => $file) 
        {
            // Si les fichier du partner sont en XML
            if (in_array($partner->getDataType(), array('XML', 'xml'))) 
            {
                // Chargement du fichier xml
                // $xml = simplexml_load_file('temp/'.$id.'/data/'.$file); 

                // $i = 0;
                
                // $this->getXmlChild($file, $xml);

                // // Tableau qui contindra les attributs xml 
                // $xPath_list = array();
                // $xPath_list = $this->setXpathList($this->array_xml_attribute);


                // On va supprimer du tableau $xPath_list et $entities_attrs_array, 
                // les attributs et les champs déjà mappé
                // foreach ($mapping as $del_val_file => $del_val_Bdd)
                // {

                //     $del_val_file = str_replace("@@", "", $del_val_file);
                //     // Si $del_val_file existe dans le tableau des attributs, 
                //     // On récupère sa clé pour pouvoir le supprimer
                //     $key1 = array_search($del_val_file, $xPath_list);
                //     if (false !== $key1) 
                //     {
                //         unset($xPath_list[$key1]);
                //     }

                //     $del_val_Bdd = str_replace(" ", "", $del_val_Bdd);
                //     $del_val_Bdd_arr = explode("//", $del_val_Bdd);
                //     $del_val_Bdd = end($del_val_Bdd_arr);
                //     // Si $del_val_Bdd existe dans le tableau des champs, 
                //     // On récupère sa clé pour pouvoir le supprimer
                //     foreach ($entities_attrs_array as $k1 => $entity_attrs_array) 
                //     {
                //         foreach ($entity_attrs_array as $k2 => $entity_attr_array) 
                //         {
                //             $key2 = array_search($del_val_Bdd, $entity_attr_array);

                //             if (false !== $key2) 
                //             {
                //                 unset($entities_attrs_array[$k1][$k2][$key2]);
                //             }
                //         }
                //     }
                // }
            }
            else
            {
                $string = file_get_contents('temp/'.$id.'/data/'.$file);

                $houseCode = explode(",", $string);

                $houseCode = array_slice($houseCode, 0,5);

                $dataH = $this->dataOfHouses($partner, $houseCode);

                $dataH = json_encode($dataH);
                $dataH = json_decode($dataH, true);

                foreach ($dataH['result'] as $key => $value) 
                {
                    $this->jsonSaveAnnonce($value);
                }
            }
        }

        return $this->render('LefDataBundle:Partners:config.html.twig', array(
            'partner'      => $partner,
            'config_form'   => $editForm->createView(),
            'files' => $files,
            'link' => $link,
        ));
    }

    public function arraySeek($array = array(), $key = null)
    {
        if (array_key_exists($key, $array)) {
            return $array;
        }else{
            foreach ($array as $k => $v) {
                $this->arraySeek($v, $key);
            }
        }
    }

    // Fonction pour lister les différents path dans le fichier xml
    public function setXpathList($array = array())
    {
        $xPath_list = array();

        foreach ($array as $k => $v) 
        {
            foreach ($v as $k1 => $v1) 
            {
                // Si $v1 est un tableau et qu'il n'est pas vide
                if (is_array($v1) && !empty($v1)) {
                    // Si le noeud a des attributs
                    if (array_key_exists('attrs', $v1)) 
                    {
                        foreach ($v1['attrs'] as $ka2 => $va2) 
                        {
                            // Si le noeud a des noeuds enfant
                            if (array_key_exists('noeuds', $v1)) 
                            {
                                foreach ($v1['noeuds'] as $kn2 => $vn2) 
                                {
                                    if (!in_array("$k1 : @$ka2='$va2' : #$vn2", $xPath_list)) 
                                    {
                                        array_push($xPath_list, "$k1 : @$ka2='$va2' : #$vn2");
                                    }
                                }
                            }
                            // Si le noeud n'a pas de noeuds enfant
                            else{}
                        }
                    // Si le noeud n'a pas d'attributs
                    }else{}
                // Si $v1 n'est pas un tableau
                }
                else
                {
                    if (!in_array("$k1", $xPath_list)) 
                    {
                        array_push($xPath_list, "$k1");
                    }
                }

            }
        }

        // On arrange les valeur par ordre croissant
        sort($xPath_list);
        return $xPath_list;
    }

    // Fonction pour lister les niveaux de neouds dans un fichier xml
    public function setXpathList2($array = array())
    {
        $xPath_list = array();

        foreach ($array as $k => $v) 
        {
            foreach ($v as $k1 => $v1) 
            {
                if (!in_array("$k1", $xPath_list)) 
                {
                    array_push($xPath_list, "$k1");
                }

            }
        }

        // On arrange les valeur par ordre croissant
        sort($xPath_list);
        return $xPath_list;
    }

    // Liste des attributs dans un neouds xml données
    public function xml_attribute($object, $attribute)
    {
        if(isset($object[$attribute]))
            return (string) $object[$attribute];
    }

    // Récuparation des neouds enfants dans un fichiers xml
    public function getXmlChild($parent=null, $xml)
    {
        $parent_row[$parent] = array();
        if (is_object($xml)) {
            $attrs_object = $xml->attributes();

            // Conveersion de l'objet en tableau
            $attrs_array = (array) $attrs_object;
            
            // On test si le tableau d'attributes n'est pas vide
            if (!empty($attrs_array['@attributes'])) {

                $attrs_kv = array();
            
                foreach ($attrs_array['@attributes'] as $key => $value) 
                {
                    $attrs_kv[$key] = $value;

                }

                if (!empty($attrs_kv)) 
                {
                    $parent_row[$parent]['attrs'] =  $attrs_kv;
                    $key_s = array();
                    foreach($xml as $key => $value) 
                    { 
                        if (!in_array($key, $key_s)) 
                        {
                            array_push($key_s, $key);
                        }
                    } 
                    $parent_row[$parent]['noeuds'] = $key_s;
                    if (!in_array($parent_row, $this->array_xml_attribute)) 
                    {
                        array_push($this->array_xml_attribute, $parent_row);
                    }

                }
            }
            else{
                if (!in_array($parent_row, $this->array_xml_attribute)) 
                {
                    array_push($this->array_xml_attribute, $parent_row);
                }
            }
        }

        $child_count = 0; 
        foreach($xml as $key => $value) 
        { 
            $child_count++;
            $this->getXmlChild($parent.".".$key, $value);
        } 

        return $child_count;
    }

    // Récupration des valeur d'un noeud xml avec un path données
    public function getXmlFileValue($parent, $mappingEnity, $xml)
    {   

        // Explode pour avoir l'entité et l'attribut réferencé
        $entity_ref_explode = explode(" // ", $mappingEnity->getRefto());
        $entity_refereced = trim($entity_ref_explode[0]);
        // $entity_ref = trim($entity_ref_explode[1]);
        $ucf_entity_ref = ucfirst(trim($entity_ref_explode[1]));

        // $partner_mappings = $mapping_repo->findOneBy(array("Reference"=>"Location // Reference", "filename"=>$file));
        $instance_entity_ref = new Location();

        $f_getter = "get$ucf_entity_ref";
        $f_setter = "set$ucf_entity_ref";


        $attrs_wanted = $mappingEnity->getOptions();
        $xpath = $mappingEnity->getXpath();
        $refto = $mappingEnity->getRefto();

        if ($refto != null) 
        {
            $child_count = 0; 
            $i = 0;
            foreach($xml as $key => $value) 
            { 
                $child_count++; 
                $i++;    
                $r_count = $this->getXmlFileValue($parent.".".$key, $mappingEnity, $value);
                $xpath_explode = explode(" : ", $xpath);

                // $parent = chemin xml
                // Si le chamin xml courant  = chemin xml de mappingEntyti

                if ($parent.".".$key == $xpath_explode[0])
                {   
                    // Récuparation des attribut du noeud xml actuel
                    $attrs_array = (array)$xml->attributes();

                    // Si le noeud actuel a des attributs
                    if (is_array(current($attrs_array))) 
                    {
                        foreach (current($attrs_array) as $k => $v) 
                        {
                            // Si les attribut du neud actuelle correspo,
                            if (isset($xpath_explode[1]) && $xpath_explode[1] == "@$k='$v'") 
                            {   
                                switch ($attrs_wanted) 
                                {
                                    case 'balise':
                                        // var_dump(trim((string)$value));
                                        break;
                                    default:
                                        // var_dump("$v");
                                        break;
                                }
                            }else
                            {
                                // Code
                            }
                        }
                    // Si le noeud actuel n'a psa d'attributs
                    }else
                    {
                        if( $r_count == 0)
                        {
                            // var_dump($parent,$xpath_explode[0]);
                            // var_dump(trim((string)$value));
                            // var_dump($parent);
                        }

                    }
                }
            } 
            return $child_count; 
        }else{
            var_dump("le fichier n'est ");
        }
    }

    public function jsonSaveAnnonce($arr)
    {
        $em = $this->getDoctrine()->getManager();

        $CostsOnSite = "";
        foreach ($arr['LanguagePackFRV4']['CostsOnSite'] as $key => $value) {
            $CostsOnSite .= $value['Description'].' : '.$value['Value']."; \n";
        }

        $_distanceOther = "Distance :\n";
        foreach ($arr['DistancesV2'] as $key => $value) {
            $_distanceOther .= $value['To'].' à '.$value['DistanceInKm']." km\n";
        }

        $newLocation = new Location();

        $newLocation->setReference($arr['HouseCode']);
        $newLocation->setTitre($arr['BasicInformationV3']['Name']);
        $newLocation->setAdresse(null);
        $newLocation->setPays($arr['BasicInformationV3']['Country']);
        $newLocation->setRegion($arr['BasicInformationV3']['Region']);
        $newLocation->setDepartement(null);
        $newLocation->setVille(($arr['LanguagePackFRV4']['City']=="")? $arr['LanguagePackFRV4']['City'] : $arr['LanguagePackFRV4']['SubCity']);
        $newLocation->setZipPostalCode($arr['BasicInformationV3']['ZipPostalCode']);
        $newLocation->setLongitude($arr['BasicInformationV3']['WGS84Longitude']);
        $newLocation->setLatitude($arr['BasicInformationV3']['WGS84Latitude']);
        $newLocation->setNombreDePersonne($arr['BasicInformationV3']['MaxNumberOfPersons']);
        $newLocation->setNombreEtoiles($arr['BasicInformationV3']['NumberOfStars']);
        $newLocation->setSurfaceHabitable($arr['BasicInformationV3']['DimensionM2']);
        $newLocation->setNombrePiece(null);
        $newLocation->setNombreSalleDeBain($arr['BasicInformationV3']['NumberOfBathrooms']); 
        $newLocation->setNombreChambre($arr['BasicInformationV3']['NumberOfBedrooms']);
        $newLocation->setAnimauxDomestiques($arr['BasicInformationV3']['NumberOfPets']);
        $newLocation->setPiscine(null);
        $newLocation->setDescriptionBreve($arr['LanguagePackFRV4']['ShortDescription']);
        $newLocation->setDescriptionDetaillee($arr['LanguagePackFRV4']['Description']);
        $newLocation->setCaracteristiques($arr['LanguagePackFRV4']['Remarks']."\n\n".$CostsOnSite);
        $newLocation->setAmenagement($arr['LanguagePackFRV4']['LayoutSimple']);
        $newLocation->setSejourAvecCouchage(null);
        $newLocation->setCoinRepas(null);
        $newLocation->setCuisine(null);
        $newLocation->setSanitaire(null);
        $newLocation->setDistanceMer(null);
        $newLocation->setDistanceLac(null);
        $newLocation->setDistanceRemontees(null);
        $newLocation->setDistanceCentreVille(null);
        $newLocation->setDistanceShopping(null);
        $newLocation->setDistanceOther($_distanceOther);
        $newLocation->setCreationDate($arr['BasicInformationV3']['CreationDate']);

        $curLocation = $em->getRepository('LefDataBundle:Location')->findOneByReference($newLocation->getReference());

        if ($curLocation != null) {
            $curLocation = $newLocation;
            $em->flush();
        }else{
            $curLocation = $newLocation;
            $em->persist($curLocation);
            $em->flush();
        }

        $curLocation = $em->getRepository('LefDataBundle:Location')->findOneByReference($newLocation->getReference());
        
        // Enregistrement des url images dans un tableau
        $medi_arr = $this->jsonSaveAnnonceMedia($arr['MediaV2'][0]['TypeContents']);

        // Ajout des planning dans la base de données
        $this->jsonSaveAnnoncePlanning($arr["AvailabilityPeriodV1"]);

        // Ajout des prix dans la base de données
        foreach ($arr["MinMaxPriceV1"] as $key => $value) {
            var_dump($value);
        }

        // die();

        // Ajout des images dans la base de données
        foreach ($medi_arr as $km_r => $vm_r) {
            $curMedia = $em->getRepository('LefDataBundle:Media')->findOneBy(array('titre'=>$vm_r[0], 'src'=>$vm_r[1]));
            if ($curMedia === null ) {
                $curMedia = new Media();
                $curMedia->setTitre($vm_r[0]);
                $curMedia->setSrc($vm_r[1]);
                $curMedia->setLocation($curLocation);
                $em->persist($curMedia);
                $em->flush();
            }
        }
    }

    public function jsonSaveAnnoncePlanning($arr)
    {
        foreach ($arr as $key => $value) 
        {

            $ArrivalDate = $value['ArrivalDate'];
            $ArrivalTimeFrom = $value['ArrivalTimeFrom'];
            $ArrivalTimeUntil = $value['ArrivalTimeUntil'];
            $DepartureDate = $value['DepartureDate'];
            $DepartureTimeFrom = $value['DepartureTimeFrom'];
            $DepartureTimeUntil = $value['DepartureTimeUntil'];

            $new_planning = new Planning();
            $new_planning->setDateArrivee($ArrivalDate);
            $new_planning->setHeureArrivee($ArrivalTimeFrom);
            $new_planning->setHeureLimitArrivee($ArrivalTimeUntil);
            $new_planning->setDateDepart($DepartureDate);
            $new_planning->setHeureDepart($DepartureTimeFrom);
            $new_planning->setHeureLimitDepart($DepartureTimeUntil);
            $new_planning->setSurDemande((strtolower($value['OnRequest'])=="yes")?true:false);
            $new_planning->setPrix($value['Price']);
            $new_planning->setPrixHtRemise($value['PriceExclDiscount']);
            $new_planning->setLocation($curLocation);
            
            $current_planning = $em->getRepository('LefDataBundle:Planning')->findOneBy(
                array(
                    "dateArrivee" => $ArrivalDate,
                    "heureArrivee" => $ArrivalTimeFrom,
                    "heureLimitArrivee" => $ArrivalTimeUntil,
                    "dateDepart" => $DepartureDate,
                    "heureDepart" => $DepartureTimeFrom,
                    "heureLimitDepart" => $DepartureTimeUntil,
                    "surDemande" => (strtolower($value['OnRequest'])=="yes")?true:false,
                    "prix" => $value['Price'],
                    "location" => $curLocation,
                    )
                );

            if ($current_planning == null) {   
                $current_planning = $new_planning;     
                $em->persist($current_planning);
                $em->flush();    
            }
        }
    }

    public function jsonSaveAnnonceMedia($arr)
    {
        $medi_arr = array();
        foreach ($arr as $key1 => $value1) {
            foreach ($value1['Versions'] as $key2 => $value2) {

                $img_titre = $value1['Tag'];
                $img_src = "http://".$value2['URL'];
                if (count($medi_arr)>=1) {
                    foreach ($medi_arr as $key3 => $value3) {
                        $can_push = false;
                        $current_url_arr = explode('/', $value3[1]);
                        $current_url_str = $value3[1];
                        $new_url = explode('/', $img_src);

                        if ($current_url_arr[4]=="web") {

                            preg_match("/_lsr_\K([^.]*)\./", $img_src, $output_array);
                            preg_match("/_lsr_\K([^.]*)\./", $value3[1], $output_array0);
                            if ($output_array[1] != $output_array0[1]) {
                                $can_push = true;
                            }
                        }else{
                            preg_match("/_lsr-b-\K([^.]*)\./", $img_src, $output_array1);
                            preg_match("/_lsr-b-\K([^.]*)\./", $value3[1], $output_array2);
                            if ((substr($output_array1[1], 0, 2)) != (substr($output_array2[1], 0, 2)) ) {
                                $can_push = true;
                            }
                        }
                    }

                    if ($can_push) {
                        array_push($medi_arr, array($img_titre, $img_src));
                    }
                }else{
                    array_push($medi_arr, array($img_titre, $img_src));
                }
            }
        }
        return $medi_arr;
    }

    // Leisure get data house v
    public function dataOfHouses($partner, $houseCode)
    {
        $rows = array();

        $url = "https://dataofHousesv1.jsonrpc-partner.net/cgi/lars/jsonrpc-partner/jsonrpc.htm";
               /* The JSON method name is part of the URL. use only lowercase characters in the URL */
        $post_data = array(
                'jsonrpc'   => '2.0', 
                'method'    => 'DataOfHousesV1', /* Methodname is part of the URL */
                'params'    => array(
                        'WebpartnerCode'        =>  $partner->getUser(), 
                        'WebpartnerPassword'    =>  $partner->getPassword(), 
                        'HouseCodes'            =>  $houseCode,
                        'Items'                 =>  array(
                            'BasicInformationV3',
                            'MediaV2',
                            'LanguagePackFRV4',
                            'MinMaxPriceV1',
                            'AvailabilityPeriodV1',
                            'PropertiesV1',
                            'CostsOnSiteV1',
                            'LayoutExtendedV2',
                            'DistancesV2',
                            'DistancesV1',
                            ),
                ),
                'id'        =>211897585 /* a unique integer to identify the call for synchronisation */
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type: application/json'));
        curl_setopt($ch, CURLOPT_SSLVERSION,3); /* Due to an OpenSSL issue */ 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);  /* Due to a wildcard certificate */
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_ENCODING, 1); /* If result is gzip then unzip */
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($result = curl_exec($ch)) {
           if ($res = json_decode($result)) {
              $rows[] = $res; 
           } else echo json_last_error();
        } else echo curl_error($ch);
        curl_close($ch);

        return $res;
    }

    public function getEntityAttrs()
    {
        $r = array();
        // Instantiation d'une entité Location pour avoir ces champs
        $alrt = new Alert();
        $be = new BienEtre();
        $eqpmt = new Equipement();
        $lklz = new Localisation();
        $lks = new Location();
        $mdia = new Media();
        $ofr = new Offre();
        $plng = new Planning();
        $czn = new Saison();
        $tem = new Theme();
        $tplks = new TypeLocation();
        $optchld = new OptionEnfant();
        $opthdcp = new OptionHandicapees();
        array_push(
            $r,
            $alrt->getAttrs(),
            $be->getAttrs(),
            $eqpmt->getAttrs(),
            $lklz->getAttrs(),
            $lks->getAttrs(),
            $mdia->getAttrs(),
            $ofr->getAttrs(),
            $plng->getAttrs(),
            $czn->getAttrs(),
            $tem->getAttrs(),
            $tplks->getAttrs(),
            $optchld->getAttrs(),
            $opthdcp->getAttrs()
        );

        return $r;
    }

    public function getConfiguredPathAction($file, $partner){

        // $em = $this->getDoctrine()->getManager();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
        
        // $mapping_repo = self::$em->getRepository('LefDataBundle:Mapping');

        // $mappings = $mapping_repo->findBy(array('filename'=>$file, 'partner'=>$partner));

        // return $mappings;
    }


}

