<?php

namespace Lef\DataBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Lef\DataBundle\Entity\TranchePrix;
use Lef\DataBundle\Form\TranchePrixType;

/**
 * TranchePrix controller.
 *
 */
class TranchePrixController extends Controller
{

    /**
     * Lists all TranchePrix entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LefDataBundle:TranchePrix')->findAll();

        return $this->render('LefDataBundle:TranchePrix:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TranchePrix entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TranchePrix();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('trancheprix_show', array('id' => $entity->getId())));
        }

        return $this->render('LefDataBundle:TranchePrix:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a TranchePrix entity.
    *
    * @param TranchePrix $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(TranchePrix $entity)
    {
        $form = $this->createForm(new TranchePrixType(), $entity, array(
            'action' => $this->generateUrl('trancheprix_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TranchePrix entity.
     *
     */
    public function newAction()
    {
        $entity = new TranchePrix();
        $form   = $this->createCreateForm($entity);

        return $this->render('LefDataBundle:TranchePrix:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TranchePrix entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:TranchePrix')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TranchePrix entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LefDataBundle:TranchePrix:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TranchePrix entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:TranchePrix')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TranchePrix entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LefDataBundle:TranchePrix:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TranchePrix entity.
    *
    * @param TranchePrix $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TranchePrix $entity)
    {
        $form = $this->createForm(new TranchePrixType(), $entity, array(
            'action' => $this->generateUrl('trancheprix_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TranchePrix entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:TranchePrix')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TranchePrix entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('trancheprix_edit', array('id' => $id)));
        }

        return $this->render('LefDataBundle:TranchePrix:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TranchePrix entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LefDataBundle:TranchePrix')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TranchePrix entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('trancheprix'));
    }

    /**
     * Creates a form to delete a TranchePrix entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('trancheprix_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
