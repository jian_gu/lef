<?php 

namespace Lef\DataBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \ZipArchive;

class DownloadController{
	var $session = null;
    public function __construct(){
        $storage = new NativeSessionStorage(array(), new NativeFileSessionHandler());
        $this->session = new Session($storage);
    }

	public function ftp_sync($ftpSrcDir, $dir,$conn_id,$target, $filesToGet){

		ftp_pasv($conn_id, true);

		if ($dir != ".") { 
			if (ftp_chdir($conn_id, $dir) == false) { 
				echo ("Change Dir Failed: $dir<BR>\r\n"); 
				return; 
			} 
			if (!(is_dir($dir))) 
				mkdir($dir); 
			chdir ($dir); 
		} 

		$contents = ftp_nlist($conn_id, $ftpSrcDir);

		$unwantedValue = array('./documentation','./special','./TEST','./xsd');
		foreach ($unwantedValue as $todel) {
			if(($key = array_search($todel,$contents)) !== false) {
				unset($contents[$key]);
			}
		}

		ftp_pasv($conn_id, true);
		foreach ($contents as $file) {
			
			if ($file == '.' || $file == '..'){continue;}

			// Test  si le fichier courant fait partie des fichier à 
			// télécharger sinon on n'en tient pas compte
			if (!in_array($file, $filesToGet, TRUE)) {continue;}

			// On test si le fichier est de type zip ,xml ou json
			if (in_array($this->getExtentionFile($file), array("xml", "zip", 'json'), TRUE)) {
				if (@ftp_chdir($conn_id, $file)) { 
					ftp_chdir ($conn_id, "..");
					$this->ftp_sync($ftpSrcDir, $file,$conn_id,$target, $filesToGet); 
				}
				else{
					if ($ftpSrcDir != "") {
						$fileLocal = str_replace("$ftpSrcDir"."/", "", "$file");
						ftp_get($conn_id, "$target"."$fileLocal", "$file", $this->get_ftp_mode($file));
					}else{
						ftp_get($conn_id, "$target"."$file", "$file", $this->get_ftp_mode($file));
					}
				}
			}
		}
		ftp_chdir ($conn_id, ".."); 
		chdir ("..");
	}

	public function syncFtp($ftpserver, $login, $password, $directory, $target, $filesToGet, $ftpSrcDir){
		foreach ($filesToGet as $key => $value) {
			if ($ftpSrcDir != "") {
				$filesToGet[$key] = $ftpSrcDir.'/'.$value;
			}else{
				$filesToGet[$key] = $ftpSrcDir.$value;
			}
		}
		$conn_id = ftp_connect ($ftpserver) 
			or die("Couldn't connect to $ftp_server"); 
		$login_result = ftp_login($conn_id, $login, $password); 
		if ((!$conn_id) || (!$login_result)) 
			die("FTP Connection Failed"); 
		$this->ftp_sync($ftpSrcDir, $directory,$conn_id,$target, $filesToGet);  
		ftp_close($conn_id); 
	}
	
	public function deleteData($folder){
		$files = glob($folder); // get all file names
		foreach($files as $file){ // iterate files
				if(is_file($file))
					unlink($file); // delete file
		}
	}

	public function unzip($file,$out){
		set_time_limit(3600);
		$zip = new ZipArchive;
		$res = $zip->open($file);
		if ($res === TRUE) {
			$zip->extractTo($out);
			$zip->close();
		} else {
			echo 'ftp error';
		}
	}

	public function getExtentionFile($file){
		$info = new \SplFileInfo($file);
		return $info->getExtension();
	}

	public function delTree($dir) {
		$it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new \RecursiveIteratorIterator($it,
		             \RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files as $file) {
		    if ($file->getFilename() === '.' || $file->getFilename() === '..') {
		        continue;
		    }
		    if ($file->isDir()){
		        rmdir($file->getRealPath());
		    } else {
		        unlink($file->getRealPath());
		    }
		}
		rmdir($dir);
	}

	function get_ftp_mode($file)
	{    
	    $path_parts = pathinfo($file);
	    
	    if (!isset($path_parts['extension'])) return FTP_BINARY;
	    switch (strtolower($path_parts['extension'])) {
	        case 'am':case 'asp':case 'bat':case 'c':case 'cfm':case 'cgi':case 'conf':
	        case 'cpp':case 'css':case 'dhtml':case 'diz':case 'h':case 'hpp':case 'htm':
	        case 'html':case 'in':case 'inc':case 'js':case 'm4':case 'mak':case 'nfs':
	        case 'nsi':case 'pas':case 'patch':case 'php':case 'php3':case 'php4':case 'php5':
	        case 'phtml':case 'pl':case 'po':case 'py':case 'qmail':case 'sh':case 'shtml':
	        case 'sql':case 'tcl':case 'tpl':case 'txt':case 'vbs':case 'xml':case 'xrc':
	            return FTP_ASCII;
	    }
	    return FTP_BINARY;
	}

}
?>