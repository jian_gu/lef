<?php

namespace Lef\DataBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lef\DataBundle\Entity\FileMap;
use Lef\DataBundle\Form\FileMapType;

/**
 * FileMap controller.
 *
 * @Route("/filemap")
 */
class __FileMapController extends Controller
{

    /**
     * Displays a form to edit an existing FileMap entity.
     *
     * @Route("/{id}/edit", name="filemap_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($file, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:FileMap')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FileMap entity.');
        }

        $editForm = $this->createEditForm($entity);

        return array(
            'entity'      => $entity,
            'file'        => $file,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
    * Creates a form to edit a FileMap entity.
    *
    * @param FileMap $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(FileMap $entity)
    {
        $form = $this->createForm(new FileMapType(), $entity, array(
            'action' => $this->generateUrl('partners_config_mapping', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing FileMap entity.
     *
     * @Route("/{id}", name="partners_config_mapping")
     * @Method("PUT")
     * @Template("LefDataBundle:Prtners:mapping.html.twig")
     */
    public function updateAction(Request $request, $id, $file)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:FileMap')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FileMap entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('filemap_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'file'        => $file,
            'edit_form'   => $editForm->createView(),
        );
    }

}
