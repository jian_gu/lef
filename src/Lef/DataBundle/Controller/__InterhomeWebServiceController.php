<?php

namespace Lef\DataBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Entity\MediaPartner;
use Lef\DataBundle\Entity\Alert;

class InterhomeWebServiceController extends Controller
{
	// Set username and password
	private $ih_soap_user ;
	private $ih_soap_pass ;

	// Set soap namespace
	private $ih_soap_namespace;

	// Create new instance of SoapClient with the interhome web service URL
	private $ih_client;

	// Create new soap header and set username and password
	private $ih_header;

	public function __construct(){

		$client_ws = 'https://webservices.interhome.com/partnerV3/WebService.asmx?WSDL';

		$this->ih_soap_user = 'FR700622';
		$this->ih_soap_pass = 'LOUER92';
		$this->ih_soap_namespace = 'http://www.interhome.com/webservice';
		$this->ih_client = new \SoapClient($client_ws);
		$this->ih_header = new \SoapHeader( $this->ih_soap_namespace, 'ServiceAuthHeader',
			array('Username' => $this->ih_soap_user,'Password' => $this->ih_soap_pass),
			true
		);
	}

	// Disponibilité d'une location
    public function availability($AccommodationCode, $CheckIn, $CheckOut)
    {
    	// Prepare parameters
		$params = array('inputValue' => array(
			'AccommodationCode' => $AccommodationCode,
			'CheckIn' 			=> $CheckIn,
			'CheckOut'			=> $CheckOut,
			)
		);

		// Call web service
		$r = $this->ih_client->__soapCall("Availability",array('parameters' => $params), null, $this->ih_header);

		return $r->AvailabilityResult;
    }

    // Details d'une location
    public function accommodationDetail($AccommodationCode, $LanguageCode)
    {
    	// Prepare parameters
		$params = array(
			'inputValue' => array(
				'AccommodationCode' 		=> $AccommodationCode,
				'LanguageCodeuageCode' 		=> $LanguageCode,
				)
			);

		// Call web service
		$r =  $this->ih_client->__soapCall("AccommodationDetail",array('parameters' => $params), null, $this->ih_header);
		return $r->AccommodationDetailResult;
    }

    // Détail d'une location
    public function trips(){}

   	// Service additionnel pour une location
    public function additionalServices($AccommodationCode, $CheckIn, $CheckOut, $LanguageCode, $CurrencyCode, $SalesOfficeCode, $Adults, $Children, $Babies)
    {
    	// Prepare parameters
		$params = array(
			'inputValue' => array(
				'AccommodationCode' 	=> $AccommodationCode,
				'CheckIn' 				=> $CheckIn,
				'CheckOut'				=> $CheckOut,
				'LanguageCode' 			=> $LanguageCode,
				'CurrencyCode'			=> $CurrencyCode,
				'SalesOfficeCode' 		=> $SalesOfficeCode,
				'Adults'				=> $Adults,
				'Children'				=> $Children,
				'Babies'				=> $Babies,
				)
			);

		// Call web service
		$r =  $this->ih_client->__soapCall("AdditionalServices",array('parameters' => $params), null, $this->ih_header);
		return $r->AdditionalServicesResult->AdditionalServices;
    }

    // Recherche sur critère
    public function search(){}

    public function cpCompletName($codeRegion, $codePlace, $codePays, $cp_rows)
    {
    	$cp = $cp2 = array();
    	$cp_rows = json_decode(json_encode($cp_rows), FALSE);
    	foreach ($cp_rows->country as $k1 => $country)
    	{

			$_codePays = $country->code;
			$_namePays = $country->name;
    		# Si le code pays fournis correspond au code pays de notre tableau
			if ($_codePays == $codePays) {
				$cp['codePays'] = $_codePays;
				$cp['nomPays'] = $_namePays;
			}

			if (isset($country->regions))
			# Si on a une ou plusieurs regions dans le pays, sinon on fait rien
			{
				if (isset($country->regions->region->code))
				# Si il y a une seul regions dans le pays
				{
					$_codeRegion = $country->regions->region->code;
					$_nameRegion = $country->regions->region->name;
					# Si le code region fournis correspond au code region courant
					if ($_codePays == $codePays && $_codeRegion == $codeRegion) {
						$cp['codeRegion'] = $_codeRegion;
						$cp['nomRegion'] = $_nameRegion;
					}

					if (isset($country->regions->region->places))
					# Si on a une ou plusieurs places dans la regions, sinon on fait rien
					{
						if (isset($country->regions->region->places->place->code))
						# S'il y a une seul place dans la region
						{
							$_codePlace = $country->regions->region->places->place->code;
							$_namePlace = $country->regions->region->places->place->name;
							# Si le code place fournis correspond au code place courant
							if ($_codePays == $codePays && $_codeRegion == $codeRegion && $_codePlace == $codePlace) {
								$cp['codePlace'] = $_codePlace;
								$cp['nomPlace'] = $_namePlace;
							}

							if (isset($country->regions->region->places->place->subplaces))
							#place avec plusieurs sous-place
							{
								foreach ($country->regions->region->places->place->subplaces->subplace as $k2 => $subplace)
								{
									$_codeSubPlace = $subplace->code;
									$_nameSubPlace = $subplace->name;
									# Si le code sous-place fournis correspond au code sous-place courant
									if ($_codePays == $codePays && $_codeRegion == $codeRegion && $_codeSubPlace == $codePlace) {
										$cp['codePlace'] = $_codeSubPlace;
										$cp['nomPlace'] = $_nameSubPlace;
									}
								}
							}
						}else
						# S'il y a une plusieurs place dans la region
						{
							foreach ($country->regions->region->places->place as $k3 => $place)
							{
								$_codePlace = $place->code;
								$_namePlace = $place->name;
								# Si le code sous-place fournis correspond au code sous-place courant
								if ($_codePays == $codePays && $_codeRegion == $codeRegion && $_codePlace == $codePlace) {
									$cp['codePlace'] = $_codePlace;
									$cp['nomPlace'] = $_namePlace;
								}
							}
						}
					}

				}
				else
				# Ici on a plusieurs régions dans le pays
				{
					foreach ($country->regions->region as $k4 => $region) {
						##############################
						##############################
						##############################
						$_codeRegion = $region->code;
						$_nameRegion = $region->name;
						# Si le code region fournis correspond au code region courant
						if ($_codePays == $codePays && $_codeRegion == $codeRegion) {
							$cp['codeRegion'] = $_codeRegion;
							$cp['nomRegion'] = $_nameRegion;
						}

						if (isset($region->places))
						# Si on a une ou plusieurs places dans la regions, sinon on fait rien
						{
							if (isset($region->places->place->code))
							# S'il y a une seul place dans la region
							{
								$_codePlace = $region->places->place->code;
								$_namePlace = $region->places->place->name;
								# Si le code place fournis correspond au code place courant
								if ($_codePays == $codePays && $_codeRegion == $codeRegion && $_codePlace == $codePlace) {
									$cp['codePlace'] = $_codePlace;
									$cp['nomPlace'] = $_namePlace;
								}

								if (isset($region->places->place->subplaces))
								#place avec plusieurs sous-place
								{
									foreach ($region->places->place->subplaces->subplace as $k2 => $subplace)
									{
										$_codeSubPlace = $subplace->code;
										$_nameSubPlace = $subplace->name;
										# Si le code sous-place fournis correspond au code sous-place courant
										if ($_codePays == $codePays && $_codeRegion == $codeRegion && $_codeSubPlace == $codePlace) {
											$cp['codePlace'] = $_codeSubPlace;
											$cp['nomPlace'] = $_nameSubPlace;
										}
									}
								}
							}else
							# S'il y a une plusieurs place dans la region
							{
								foreach ($region->places->place as $k3 => $place)
								{
									$_codePlace = $place->code;
									$_namePlace = $place->name;
									# Si le code sous-place fournis correspond au code sous-place courant
									if ($_codePays == $codePays && $_codeRegion == $codeRegion && $_codePlace == $codePlace) {
										$cp['codePlace'] = $_codePlace;
										$cp['nomPlace'] = $_namePlace;
									}
								}
							}
						}
						##############################
						##############################
						##############################
					}
				}

			}
		}
		return $cp;
    }

	/**
	 *
	 * @param type $em
	 */
	public function saveAccommodation($em)
	{
		$acommodation =  simplexml_load_file('temp/2/data/accommodation.xml');
		$counrtyplaces = json_decode(json_encode(simplexml_load_file('temp/2/data/countryregionplace_fr.xml')), true);

		$cp_rows = json_decode(json_encode($counrtyplaces), true);

		// Augmentation de la memore allouée
		$this->increaseMemoryAllocated();

		// Convertion du json en tableau
		$locAccs = json_decode(json_encode($acommodation), true);

		foreach ($locAccs['accommodation'] as $key => $locAcc) {

			$cpCompletName = $this->cpCompletName($locAcc['region'],$locAcc['place'],$locAcc['country'], $cp_rows);

			$amenagement ="";
			foreach (current($locAcc['attributes']) as $key => $attributes) {
				$amenagement .= $attributes." ";
			}
			$distances = "";
			foreach (current($locAcc['distances']) as $key => $distance) {
				$distances .= $distance['type']." : ".$distance['value'].", ";
			}

			$location = new Location();
			$location->setReference($locAcc['code']);
			$location->setTitre($locAcc['name']);
			$location->setAdresse($cpCompletName['nomPlace'].', '.$cpCompletName['codePlace'].', '.$cpCompletName['nomPays']);
			$location->setPays($cpCompletName['nomPays']);
			$location->setRegion($cpCompletName['nomRegion']); // Voir le fichier des regions et place xml
			$location->setDepartement(null);
			$location->setVille($cpCompletName['nomPlace']); // Voir le fichier des regions et place xml
			$location->setLatitude($locAcc['geodata']['lat']);
			$location->setLongitude($locAcc['geodata']['lng']);
			$location->setZipPostalCode($locAcc['zip']);
			$location->setNombreDePersonne($locAcc['pax']);
			$location->setNombreEtoiles($locAcc['quality']);
			$location->setSurfaceHabitable($locAcc['sqm']);
			$location->setNombrePiece($locAcc['rooms']);
			$location->setNombreSalleDeBain($locAcc['bathrooms']);
			$location->setNombreChambre($locAcc['bedrooms']);
			$location->setAmenagement($amenagement);
			$location->setDistanceOther($distances);

			$check_location = $em->getRepository('LefDataBundle:Location')->findOneBy(array('reference'=>$locAcc['code']));

			if ($check_location == null) {
				$em->persist($location);
			}else{
				$check_location->setReference($locAcc['code']);
				$check_location->setTitre($locAcc['name']);
				$check_location->setAdresse($cpCompletName['nomPlace'].', '.$cpCompletName['codePlace'].', '.$cpCompletName['nomPays']);
				$check_location->setPays($cpCompletName['nomPays']);
				$check_location->setRegion($cpCompletName['nomRegion']); // Voir le fichier des regions et place xml
				$check_location->setDepartement(null);
				$check_location->setVille($cpCompletName['nomPlace']); // Voir le fichier des regions et place xml
				$check_location->setLatitude($locAcc['geodata']['lat']);
				$check_location->setLongitude($locAcc['geodata']['lng']);
				$check_location->setZipPostalCode($locAcc['zip']);
				$check_location->setNombreDePersonne($locAcc['pax']);
				$check_location->setNombreEtoiles($locAcc['quality']);
				$check_location->setSurfaceHabitable($locAcc['sqm']);
				$check_location->setNombrePiece($locAcc['rooms']);
				$check_location->setNombreSalleDeBain($locAcc['bathrooms']);
				$check_location->setNombreChambre($locAcc['bedrooms']);
				$check_location->setAmenagement($amenagement);
				$check_location->setDistanceOther($distances);
			}
			$em->flush();

		}
	}

	/**
	 *
	 * @param type $em
	 */
	public function saveAlert($em)
	{
		$alerts = json_decode(json_encode(simplexml_load_file('temp/2/data/alert_fr.xml')), true);
		foreach ($alerts['alert'] as $key => $alert) {

			$code = $alert['code'];
			$startdate = $alert['startdate'];
			$enddate = $alert['enddate'];
			$text = $alert['text'];
			$can_persist = false;
			$alert = $em->getRepository('LefDataBundle:Alert')->findOneBy(array("annonceRef" => $code, "description" => $text));
			if ($alert === null) {
				$alert = new Alert();
				$alert->setAnnonceRef($code);
				$can_persist = true;
			}
			$alert->setDescription($text);
			$alert->setEnddate($enddate);
			$alert->setStartdate($startdate);

			if ($can_persist) {
				$em->persist($alert);
			}
			$em->flush();
		}
	}


	/**
	 *
	 * @param type $em
	 */
	public function saveInsidedescription($em)
	{
		$insidedesc =  simplexml_load_file('temp/2/data/insidedescription_fr.xml');

		// Augmentation de la memore allouée
		$this->increaseMemoryAllocated();

		// Conversion du xml en json
		$xml_to_json = json_encode($insidedesc);

		// Convertion du json en tableau
		$locationsDescIn = json_decode($xml_to_json, true);

		foreach ($locationsDescIn['description'] as $key => $locationDescIn) {

			$location = new Location();
			$check_location = $em->getRepository('LefDataBundle:Location')->findOneBy(array('reference'=>$locationDescIn['code']));

			$location->setReference($locationDescIn['code']);
			$location->setDescriptionDetaillee($locationDescIn['text']);

			if ($check_location == null)
			# Ajout d'une nouvelle
			{
				$em->persist($location);
				$em->flush();
				$em->clear();
			}else
			# Mise à jour de l'existante
			{
				$check_location->setDescriptionDetaillee($locationDescIn['text']);
				$em->flush();
				$em->clear();
			}


			# Insertion des images
			$lastInsert = $em->getRepository('LefDataBundle:Location')->findOneByReference($locationDescIn['code']);
			$accoDetail = $this->accommodationDetail($locationDescIn['code'], 'FR');
			if ($accoDetail->Ok)
			{
				$adresse = $accoDetail->Place."".$accoDetail->Zip." ".$accoDetail->Region." ".$accoDetail->Country;
				$lastInsert->setAdresse($adresse);
				$lastInsert->setPays($accoDetail->Country);
				$lastInsert->setRegion($accoDetail->Region);
				$lastInsert->setVille($accoDetail->Place);
				$lastInsert->setZipPostalCode($accoDetail->Zip);
				$lastInsert->setLongitude($accoDetail->GeoLng);
				$lastInsert->setLatitude($accoDetail->GeoLat);
				$typeLoc = ($accoDetail->Type == "a") ? "Appartement" : ($accoDetail->Type == "h") ? 'Maison' : null;
				$lastInsert->setTypePartner($typeLoc);
				$lastInsert->setNombreEtoiles($accoDetail->Quality);

				foreach ($accoDetail->Pictures as $key => $pictures)
				{
					if (is_array($pictures))
					{
						foreach ($pictures as $kp => $picture)
						{
							$check_media = $em->getRepository('LefDataBundle:MediaPartner')->findOneBy(array("location"=>$locationDescIn['code'], 'src'=>$picture));
							if ($check_media == null)
							{
								$mediaP = new MediaPartner($picture, "", $locationDescIn['code']);
								$em->persist($mediaP);
								$em->flush();
							}
						}
					}else{
						$check_media = $em->getRepository('LefDataBundle:MediaPartner')->findOneBy(array("location"=>$locationDescIn['code'], 'src'=>$pictures));
						if ($check_media == null)
						{
							$mediaP = new MediaPartner($pictures, "", $locationDescIn['code']);
							$em->persist($mediaP);
							$em->flush();
						}
					}
				}
				$em->flush();
				$em->clear();
			}
		}
	}

	/**
	 *
	 * @param type $em
	 */
	public function saveOutsidedescription($em)
	{
		$outsidedesc =  simplexml_load_file('temp/2/data/outsidedescription_fr.xml');

		// Augmentation de la memore allouée
		$this->increaseMemoryAllocated();

		// Conversion du xml en json
		$xml_to_json = json_encode($outsidedesc);

		// Convertion du json en tableau
		$locationsDescOut = json_decode($xml_to_json, true);

		foreach ($locationsDescOut['description'] as $key => $locationDescOut) {

			$check_location = $em->getRepository('LefDataBundle:Location')->findOneBy(array('reference'=>$locationDescOut['code']));

			$location = new Location();
			$location->setReference($locationDescOut['code']);
			$location->setDescriptionBreve($locationDescOut['text']);

			if ($check_location == null) {
				$em->persist($location);
				$em->flush();
			}else{
				$check_location->setDescriptionBreve($locationDescOut['text']);
				$em->flush();
			}
		}
	}

	/**
	 *
	 * @param type $em
	 */
	public function savePrice7070eur($em)
	{
		$prices = json_decode(json_encode(simplexml_load_file('temp/2/data/price_7070_eur.xml')), true);
		foreach ($prices['price'] as $key => $price)
		{
			$check_location = $em->getRepository('LefDataBundle:Location')->findOneBy(array('reference'=>$price['code']));
			if ($check_location === null) {
				$location = new Location();
				$location->setReference($price['code']);
				$location->setRentalprice($price['rentalprice']);

				$em->persist($location);
				$em->flush();
			}else{
				$check_location->setRentalprice($price['rentalprice']);
				$em->flush();
			}
		}
	}

	/**
	 *
	 * @param type $em
	*/
	public function savePrice7070eur14($em)
	{
		$price_7070_eur_14 =  simplexml_load_file('temp/2/data/price_7070_eur_14.xml');
	}

	/**
	 *
	 * @param type $em
	*/
	public function savePrice7070eur21($em)
	{
		$price_7070_eur_21 =  simplexml_load_file('temp/2/data/price_7070_eur_21.xml');
	}

	/**
	 *
	 * @param type $em
	*/
	public function savePrice7070eur28($em)
	{
		$price_7070_eur_28 =  simplexml_load_file('temp/2/data/price_7070_eur_28.xml');
	}

	public function getFiles($countryRegion, $accomodation, $alert, $indesc, $outdesc, $prices1, $prices2, $prices3, $prices4, $em)
	{
		$countryRegion = json_decode(json_encode(simplexml_load_file($countryRegion)), true);
		$accomodation = json_decode(json_encode(simplexml_load_file($accomodation)), true);
		$alert = json_decode(json_encode(simplexml_load_file($alert)), true);
		$indesc = json_decode(json_encode(simplexml_load_file($indesc)), true);
		$outdesc = json_decode(json_encode(simplexml_load_file($outdesc)), true);
		$prices1 = json_decode(json_encode(simplexml_load_file($prices1)), true);
		$prices2 = json_decode(json_encode(simplexml_load_file($prices2)), true);
		$prices3 = json_decode(json_encode(simplexml_load_file($prices3)), true);
		$prices4 = json_decode(json_encode(simplexml_load_file($prices4)), true);

		
	}

	public function increaseMemoryAllocated()
	{
		ini_set("user_agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
		ini_set("max_execution_time", 0);
		ini_set("memory_limit", "-1");
	}
}