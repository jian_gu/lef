<?php

// save 11/06 17h15

namespace Lef\DataBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Controller\DownloadController;
use Lef\DataBundle\Controller\InterhomeWebServiceController;
use Lef\DataBundle\Controller\InterchaletWebServiceController;
use Lef\DataBundle\Controller\LeisureWebServiceController;

use Lef\DataBundle\Entity\Alert;
use Lef\DataBundle\Entity\BienEtre;
use Lef\DataBundle\Entity\Equipement;
use Lef\DataBundle\Entity\Localisation;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Entity\Media;
use Lef\DataBundle\Entity\MediaPartner;
use Lef\DataBundle\Entity\Offre;
use Lef\DataBundle\Entity\OptionEnfant;
use Lef\DataBundle\Entity\OptionHandicapees;
use Lef\DataBundle\Entity\Planning;
use Lef\DataBundle\Entity\Saison;
use Lef\DataBundle\Entity\Theme;
use Lef\DataBundle\Entity\TypeLocation;

use Lef\DataBundle\Entity\Partners;
use Lef\DataBundle\Entity\Maping;
use Lef\DataBundle\Entity\Annonces;
use Lef\DataBundle\Entity\FileMap;
use Lef\DataBundle\Form\PartnersType;

/**
 * Partners controller.
 *
 */
class PartnersController extends Controller
{

	private $array_xml_attribute = array();
	private $values_seek = array();
	private $em;

	public function __construct(){

	}

	public function getArray_xml_attribute()
	{
		return $this->array_xml_attribute;
	}

	/**
	 * Lists all Partners entities.
	 *
	 */
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();

		$entities = $em->getRepository('LefDataBundle:Partners')->findAll();

		return $this->render('LefDataBundle:Partners:index.html.twig', array(
			'entities' => $entities,
		));
	}
	/**
	 * Creates a new Partners entity.
	 *
	 */
	public function createAction(Request $request)
	{
		$entity = new Partners();
		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();

			return $this->redirect($this->generateUrl('partners_show', array('id' => $entity->getId())));
		}

		return $this->render('LefDataBundle:Partners:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView(),
		));
	}

	/**
	 * Creates a form to create a Partners entity.
	 *
	 * @param Partners $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	*/
	private function createCreateForm(Partners $entity)
	{
		$form = $this->createForm(new PartnersType(), $entity, array(
			'action' => $this->generateUrl('partners_create'),
			'method' => 'POST',
		));

		$form->add('submit', 'submit', array('label' => 'Create'));

		return $form;
	}

	/**
	 * Displays a form to create a new Partners entity.
	 *
	 */
	public function newAction()
	{
		$entity = new Partners();
		$form   = $this->createCreateForm($entity);

		return $this->render('LefDataBundle:Partners:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView(),
		));
	}

	/**
	 * Finds and displays a Partners entity.
	 *
	 */
	public function showAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('LefDataBundle:Partners')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Partners entity.');
		}

		$deleteForm = $this->createDeleteForm($id);

		return $this->render('LefDataBundle:Partners:show.html.twig', array(
			'entity'      => $entity,
			'delete_form' => $deleteForm->createView(),        ));
	}

	/**
	 * Displays a form to edit an existing Partners entity.
	 *
	 */
	public function editAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('LefDataBundle:Partners')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Partners entity.');
		}

		$editForm = $this->createEditForm($entity);
		$deleteForm = $this->createDeleteForm($id);

		return $this->render('LefDataBundle:Partners:edit.html.twig', array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		));
	}

	/**
	 * Creates a form to edit a Partners entity.
	 *
	 * @param Partners $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	*/
	private function createEditForm(Partners $entity)
	{
		$form = $this->createForm(new PartnersType(), $entity, array(
			'action' => $this->generateUrl('partners_update', array('id' => $entity->getId())),
			'method' => 'PUT',
		));

		$form->add('submit', 'submit', array('label' => 'Update'));

		return $form;
	}
	/**
	 * Edits an existing Partners entity.
	 *
	 */
	public function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('LefDataBundle:Partners')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Partners entity.');
		}

		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {
			$em->flush();

			// return $this->redirect($this->generateUrl('partners'));
			return $this->redirect($this->generateUrl('partners_config', array('id' => $id)));
		}

		return $this->render('LefDataBundle:Partners:edit.html.twig', array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		));
	}
	/**
	 * Deletes a Partners entity.
	 *
	 */
	public function deleteAction(Request $request, $id)
	{
		$form = $this->createDeleteForm($id);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('LefDataBundle:Partners')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Partners entity.');
			}

			$em->remove($entity);
			$em->flush();
		}

		return $this->redirect($this->generateUrl('partners'));
	}

	/**
	 * Creates a form to delete a Partners entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('partners_delete', array('id' => $id)))
			->setMethod('DELETE')
			->add('submit', 'submit', array('label' => 'Supprimer le partner', 'attr'=>array('class'=>'btn btn-danger btn-sm')))
			->getForm()
		;
	}

	/**
	 * Téléchargement des fichier zip a partir du ftp (action controller)
	 * 
	 * @param type $id
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 * @throws type
	 */
	public function syncAction($id, Request $request)
	{

		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('LefDataBundle:Partners')->find($id);
		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Partners entity.');
		}
		$ftpId = $entity->getId();
		$ftpUser = $entity->getUser();
		$ftpHost = $entity->getHost();
		$ftpPassword = $entity->getPassword();
		$ftpSrcDir = $entity->getSrcDirectory();
		$fileToGet = explode(',', $entity->getFiles());

		$targetFtp = new DownloadController();

		if (is_dir("temp/$ftpId/"))
		{
			$targetFtp->delTree("temp/$ftpId/");
		}
		else
		{
			while (!is_dir("temp/$ftpId/")) {
				mkdir("temp/$ftpId/", 0777);
				chmod("temp/$ftpId/", 0777);
			}
			if (is_dir("temp/$ftpId/")) {
				while (!is_dir("temp/$ftpId/zipfiles/")) {
					mkdir("temp/$ftpId/zipfiles/", 0777);
					chmod("temp/$ftpId/zipfiles/", 0777);
					fopen("temp/$ftpId/zipfiles/.htaccess","wb");
				}
				while (!is_dir("temp/$ftpId/data/")) {
					mkdir("temp/$ftpId/data/", 0777);
					chmod("temp/$ftpId/data/", 0777);
					fopen("temp/$ftpId/data/.htaccess","wb");
				}
			}
		}

		// On verifie si le dossier de stockage des fichier zip a bien étais créer
		// puis on lance la téléchagement des fichier
		if(is_dir("temp/$ftpId/zipfiles/")){
			if (in_array($entity->getDataType(), array('XML', 'xml'))) {
				$targetFtp->syncFtpAction($ftpHost,$ftpUser,$ftpPassword,".","temp/$ftpId/zipfiles/", $fileToGet, $ftpSrcDir, $request);
			}else{
				$targetFtp->saveFulx($entity, $path = "temp/$ftpId/zipfiles/");
			}
		}
		else
		{
			$this->syncAction($id, $request);
		}

		return $this->redirect($this->generateUrl('partners_config', array('id'=> $ftpId)));
		// return new Response('true');
	}

	/**
	 * Décompression des fichier zip (action controller)
	 * 
	 * @param type $id
	 * @return type
	 * @throws type
	 */
	public function unpackAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('LefDataBundle:Partners')->find($id);
		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Partners entity.');
		}
		$ftpId = $entity->getId();

		$targetFtp = new DownloadController();
		$tempzips = glob('temp/'.$ftpId.'/zipfiles/*');

		foreach($tempzips as $tempzip)
		{
			$targetFtp->unzip($tempzip,"temp/$ftpId/data/");

			// si on a un fichier xml qui ne sera pas a déziper
			// en le copie dans le dossier de destination
			if ($targetFtp->getExtentionFile($tempzip) == "xml") {
				$srcfile=$tempzip;
				$dstfile=str_replace('zipfiles', 'data', $tempzip);
				copy($srcfile, $dstfile);
			}
		}

		return $this->redirect($this->generateUrl('partners_config', array('id'=> $id)));
		// return new Response('true');
	}

	/**
	 * Téléchargement des fichier zip a partir du ftp
	 * 
	 * @param type $entity
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return boolean
	 */
	public function download($entity, Request $request)
	{
		$ftpId = $entity->getId();
		$ftpUser = $entity->getUser();
		$ftpHost = $entity->getHost();
		$ftpPassword = $entity->getPassword();
		$ftpSrcDir = $entity->getSrcDirectory();
		$fileToGet = explode(',', $entity->getFiles());

		$targetFtp = new DownloadController();

		if (is_dir("temp/$ftpId/"))
		{
			$targetFtp->delTree("temp/$ftpId/");
		}
		else
		{
			while (!is_dir("temp/$ftpId/")) {
				mkdir("temp/$ftpId/", 0777);
				chmod("temp/$ftpId/", 0777);
			}
			if (is_dir("temp/$ftpId/")) {
				while (!is_dir("temp/$ftpId/zipfiles/")) {
					mkdir("temp/$ftpId/zipfiles/", 0777);
					chmod("temp/$ftpId/zipfiles/", 0777);
					fopen("temp/$ftpId/zipfiles/.htaccess","wb");
				}
				while (!is_dir("temp/$ftpId/data/")) {
					mkdir("temp/$ftpId/data/", 0777);
					chmod("temp/$ftpId/data/", 0777);
					fopen("temp/$ftpId/data/.htaccess","wb");
				}
			}
		}

		// On verifie si le dossier de stockage des fichier zip a bien étais créer
		// puis on lance la téléchagement des fichier
		if(is_dir("temp/$ftpId/zipfiles/")){
			if (in_array($entity->getDataType(), array('XML', 'xml'))) {
				$targetFtp->syncFtpAction($ftpHost,$ftpUser,$ftpPassword,".","temp/$ftpId/zipfiles/", $fileToGet, $ftpSrcDir, $request);
			}else{
				$targetFtp->saveFulx($entity, $path = "temp/$ftpId/zipfiles/");
			}
		}
		else
		{
			$this->download($entity, $request);
		}

		return true;

	}

	/**
	 * Décompression des fichier zip
	 * 
	 * @param type $id
	 * @param type $file
	 * @return boolean
	 */
	public function dezip($id, $file)
	{
		$this->increaseMemoryAllocated();

		$targetFtp = new DownloadController();
		$tempzips = glob('temp/'.$id.'/zipfiles/*');

		if (file_exists($file)) {
			$targetFtp->unzip($file,"temp/$id/data/");

			// si on a un fichier xml qui ne sera pas a déziper
			// en le copie dans le dossier de destination
			if ($targetFtp->getExtentionFile($file) == "xml") {
				$srcfile=$file;
				$dstfile=str_replace('zipfiles', 'data', $file);
				copy($srcfile, $dstfile);
			}
		}

		$files = glob('temp/'.$id.'/data/*');
		if (empty($files)) {
			$this->dezip($id, $file);
		}else{
			return true;
		}
	}

	/**
	 * Mise à jour des données (téléchargemnt des fichier et décompression)
	 * 
	 * @param type $id
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 * @throws type
	 */
	public function dataMajAction($id, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$partner = $em->getRepository('LefDataBundle:Partners')->find($id);
		$id = $partner->getId();
		if (!$partner) {
			throw $this->createNotFoundException('Unable to find Partner.');
		}

		// Récupération du contenu des dossier exstant
		$tempzips = glob('temp/'.$id.'/zipfiles/*');
		$files = glob('temp/'.$id.'/data/*');

		if (empty($tempzips)) {
		   // $this->download($partner, $request);
		   // foreach ($tempzips as $key => $file) {
		   //     $this->dezip($id, $file);
		   // }
		}elseif (empty($files)) {
		   // foreach ($tempzips as $key => $file) {
		   //     $this->dezip($id, $file);
		   // }
		}else{
			// $this->download($partner, $request);
			// foreach ($tempzips as $key => $file) {
			//     $this->dezip($id, $file);
			// }
		}

		switch ($id) {

			// Interchalet
			case 1:
				$webService = new InterchaletWebServiceController();

				$loc_file = "temp/1/data/IOBJTXT.xml";
				$img_file = "temp/1/data/IOBJIMG.xml";
				$otr_file = "temp/1/data/IOBPRP.xml";

				$webService->saveIobjtxt1($loc_file, $em);
				// $this->saveIobjimg1($img_file, $em, $loc);
				// $this->saveIobprp1($otr_file, $em, $loc);

				die();
				
				break;

			// Interhome
			// https://www.interhome-partners.com/qs/webservice/ws.php?ihMethod=Prices&ihEnvironment=test&commit=submit&ihSoapUser=&ihSoapPassword=&So=2048&LanguageCode=EN&Currency=EUR&AccommodationCode=DE2981.100.1&CheckIn=2014-08-23&CheckOut=2014-08-30&Duration=7&AdditionalServices=&AccommodationCodes=FR3165.115.1+DE2981.100.1+ES7779.400.1&CountryCode=CH&RegionCode=01&PlaceCode=&SearchPage=1&PageSize=25&Quicksearch=&Adults=2&Children=0&Babies=0&CustomerSalutationType=Mr&CustomerFirstName=TEST+Fname&CustomerName=TEST+Lname+&CustomerAddressStreet=TEST+Street&CustomerAddressZIP=10000&CustomerAddressPlace=TEST-City&CustomerAddressCountryCode=DE&CustomerEmail=mail%40example.com&PaymentType=Invoice&CreditCardType=NotSet&CreditCardNumber=&CreditCardCvc=&CreditCardExpiry=&CreditCardHolder=&BankAccountHolder=&BankCode=&BankAccountNumber=&PartnerID=&PartnerIDZU=&Comment=&BookingID=
			case 2:
				$webService = new InterhomeWebServiceController();

				$accommodationDetail = $webService->accommodationDetail('AD1700.100.2', 'FR');
				$availability = $webService->availability('AD1700.100.2', '2014-06-30', '2014-07-30');
				$additionalServices = $webService->additionalServices('AD1700.100.1', '2014-06-27', '2014-06-30', 'FR', 'EUR', 2048, 2, 1, 0);
 
				var_dump($accommodationDetail);

				die();

				foreach ($files as $key => $file) {
					// Augmentation de la memore allouée
					$this->increaseMemoryAllocated();

					$webService = new LeisureWebServiceController();

					// On récupère le nom du fichier en cours de lecture
					$filename = str_replace(array("temp/$id/data/", "_fr", ".xml"), '', $file);
					$filename = strtolower($filename);

					if ($filename == "countryregionplace"): continue;
					else :
						$saver = "save".ucfirst(str_replace("_", "", $filename))."$id";
						// On charge le fichier
						$xml =  simplexml_load_file($file);
						// Conversion du xml en json
						$xml_to_json = json_encode($xml);
						// Convertion du json en tableau
						$json_to_array = json_decode($xml_to_json, true);

						$this->$saver($json_to_array, $em);

					endif;
				}
				break;
			// Belvilla
			case 3:
				$webService = new LeisureWebServiceController();

				$string = file_get_contents('temp/3/data/houses_code.json');

				$housesCode = explode(",", $string);

				$housesCode = array_slice($housesCode, 0,1);

				foreach ($housesCode as $key => $code) {
					$dataOfHouses = $webService->dataOfHouses($code);
					$webService->jsonSaveAnnonce($dataOfHouses, $em);

					$checkAvailability = $webService->checkAvailability($code, '2015-08-30', '2015-09-01', 168);
				}

				break;

			default:
				break;
		}
		return $this->redirect($this->generateUrl('partners_config', array('id'=> $id)));
	}

	/**
	 * 
	 * @param type $id
	 * @return type
	 * @throws type
	 */
	public function configAction($id)
	{
		$em = $this->getDoctrine()->getManager();
		$partner_repo = $em->getRepository('LefDataBundle:Partners');
		// $mapping_repo = $em->getRepository('LefDataBundle:Mapping');
		$location_repo = $em->getRepository('LefDataBundle:Location');

		$partner = $partner_repo->find($id);

		if (!$partner) {
			throw $this->createNotFoundException('Unable to find Partners partner.');
		}

		$editForm = $this->createEditForm($partner);

		/***********************************************/
		// On recupère les fichier déziper pour le partner courant
		$dataFolder_isEmpty = $zipFolder_isEmpty = true;
		$link = null;
		$files = array();
		if (is_dir('temp/'.$id.'/')) {
			$dataFolderContent = glob('temp/'.$id.'/data/*');
			$zipFolderContent = glob('temp/'.$id.'/zipfiles/*');


			$dataFolder_isEmpty = (empty($dataFolderContent)) ? true : false;
			$zipFolder_isEmpty = (empty($zipFolderContent)) ? true : false;

			if ($zipFolder_isEmpty) {
				$link['name'] = "Télécharger les données";
				$link['action'] = "partners_sync";
				$link['icon'] = "glyphicon-download-alt";
				$link['msg'] = "Aucune données n'a été télécharger";
			} elseif($dataFolder_isEmpty) {
				$link['name'] = "décompréssés les données";
				$link['action'] = "partners_unpack";
				$link['icon'] = "glyphicon-log-out";
				$link['msg'] = "Aucune données n'a été décompréssés";
			}

			foreach ($dataFolderContent as $key => $file) {
				$files[] = str_replace("temp/$id/data/", '', $file);
				$fileMap = $em->getRepository('LefDataBundle:FileMap')->findOneBy(array('fileName'=>$file, 'partnerId'=>$id));
				if ($fileMap == null) {
					$fileMap = new FileMap();
					$fileMap->setPartnerId($id);
					$fileMap->setFileName($file);
					$em->persist($fileMap);
					$em->flush();
				}
			}
		}

		// Augementation du temps limit d'execution et de la memoir limit pour lire les longs fichier xml
		$this->increaseMemoryAllocated();

/*        ------------------------------------------------//
		Sauvegarde des location xml dans la bdd local  //
		------------------------------------------------//
		Pour chaque fichier en fonction du partner
		foreach ($files as $key => $file) :

			$mappingByFilename = $mapping_repo->findBy(array("filename"=>$file));

			// Si mon fichier est mapper
			if (!empty($mappingByFilename)) :

				$xml =  simplexml_load_file('temp/'.$id.'/data/'.$file);
				$xml_to_json = json_encode($xml);
				$json_to_array = json_decode($xml_to_json, true);

				// Pour tout les entrées de Mapping avec le nom du fichier courant
				foreach ($mappingByFilename as $key => $map) :

					$local_Location = $this->arraySeek($json_to_array, $map->getLimiteLocation() );

					// Instanciation d'une location
					$new_loc = new Location();

					// On cherche le tableau avec la cle $map->getLimiteLocation
					// Parcour des annonces dans le fichier XML
					foreach ($local_Location[$map->getLimiteLocation()] as $kll => $vll):

						// On récupère la dénière paramètre dans le path
						$c_xpath = str_replace($file, "", $map->getXpath());
						$c_xpath = str_replace($map->getLimiteLocation(), "", $c_xpath);
						$c_xpath = str_replace(".", "", $c_xpath);

						// On récupère le nom l'attribut de location dans l'entité du mapping courrant
						$refto_exploded = explode(" // ", $map->getRefto());
						$refto = end($refto_exploded);


						if ($refto == "reference") :

							$new_loc1 = $location_repo->findOneByReference( $vll[$c_xpath] );

							if ($new_loc1 != null) :
								// $new_loc1->setReference($vll[$c_xpath]);
							else:
								$new_loc1 = $new_loc;
								// $new_loc1->setReference($vll[$c_xpath]);
							endif;

						endif;

						if($refto != "reference" && isset($new_loc1)):

							$_setter = ucfirst($refto);
							$_setter = 'set'.$_setter;
							$new_loc1->$_setter($vll[$c_xpath]);

							// var_dump($new_loc1);

						endif;

					endforeach;

				endforeach;
			endif;
		endforeach;

		mapping
		foreach ($files as $key => $file)
		{
			// Si les fichier du partner sont en XML
			if (in_array($partner->getDataType(), array('XML', 'xml')))
			{

			}
			else
			{
				$string = file_get_contents('temp/'.$id.'/data/'.$file);

				$houseCode = explode(",", $string);

				$houseCode = array_slice($houseCode, 0,5);

				$dataH = $this->dataOfHouses($partner, $houseCode);

				$dataH = json_encode($dataH);
				$dataH = json_decode($dataH, true);

				foreach ($dataH['result'] as $key => $value)
				{
					$this->jsonSaveAnnonce($value);
				}
			}
		}
*/
		return $this->render('LefDataBundle:Partners:config.html.twig', array(
			'partner'      => $partner,
			'config_form'   => $editForm->createView(),
			'files' => $files,
			'link' => $link,
		));
	}

	/**
	 * Augmentation de la mémoire aloué
	 */
	public function increaseMemoryAllocated()
	{
		ini_set("user_agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
		ini_set("max_execution_time", 0);
		ini_set("memory_limit", "-1");
	}

	/**
	 * 
	 * @param type $array
	 * @param type $key
	 * @return type
	 */
	public function arraySeek($array = array(), $key = null)
	{
		if (array_key_exists($key, $array)) {
			return $array;
		}else{
			foreach ($array as $k => $v) {
				$this->arraySeek($v, $key);
			}
		}
	}

	/**
	 * Fonction pour lister les différents path dans le fichier xml
	 * 
	 * @param type $array
	 * @return array
	 */
	public function setXpathList($array = array())
	{
		$xPath_list = array();

		foreach ($array as $k => $v)
		{
			foreach ($v as $k1 => $v1)
			{
				// Si $v1 est un tableau et qu'il n'est pas vide
				if (is_array($v1) && !empty($v1)) {
					// Si le noeud a des attributs
					if (array_key_exists('attrs', $v1))
					{
						foreach ($v1['attrs'] as $ka2 => $va2)
						{
							// Si le noeud a des noeuds enfant
							if (array_key_exists('noeuds', $v1))
							{
								foreach ($v1['noeuds'] as $kn2 => $vn2)
								{
									if (!in_array("$k1 : @$ka2='$va2' : #$vn2", $xPath_list))
									{
										array_push($xPath_list, "$k1 : @$ka2='$va2' : #$vn2");
									}
								}
							}
							// Si le noeud n'a pas de noeuds enfant
							else{}
						}
					// Si le noeud n'a pas d'attributs
					}else{}
				// Si $v1 n'est pas un tableau
				}
				else
				{
					if (!in_array("$k1", $xPath_list))
					{
						array_push($xPath_list, "$k1");
					}
				}

			}
		}

		// On arrange les valeur par ordre croissant
		sort($xPath_list);
		return $xPath_list;
	}

	/**
	 * Fonction pour lister les niveaux de neouds dans un fichier xml
	 * 
	 * @param type $array
	 * @return array
	 */
	public function setXpathList2($array = array())
	{
		$xPath_list = array();

		foreach ($array as $k => $v)
		{
			foreach ($v as $k1 => $v1)
			{
				if (!in_array("$k1", $xPath_list))
				{
					array_push($xPath_list, "$k1");
				}

			}
		}

		// On arrange les valeur par ordre croissant
		sort($xPath_list);
		return $xPath_list;
	}

	/**
	 * Liste des attributs dans un neouds xml données
	 * 
	 * @param type $object
	 * @param type $attribute
	 * @return type
	 */
	public function xml_attribute($object, $attribute)
	{
		if(isset($object[$attribute]))
			return (string) $object[$attribute];
	}

	/**
	 * Récuparation des neouds enfants dans un fichiers xml
	 * 
	 * @param type $parent
	 * @param type $xml
	 * @return int
	 */
	public function getXmlChild($parent=null, $xml)
	{
		$parent_row[$parent] = array();
		if (is_object($xml)) {
			$attrs_object = $xml->attributes();

			// Conveersion de l'objet en tableau
			$attrs_array = (array) $attrs_object;

			// On test si le tableau d'attributes n'est pas vide
			if (!empty($attrs_array['@attributes'])) {

				$attrs_kv = array();

				foreach ($attrs_array['@attributes'] as $key => $value)
				{
					$attrs_kv[$key] = $value;

				}

				if (!empty($attrs_kv))
				{
					$parent_row[$parent]['attrs'] =  $attrs_kv;
					$key_s = array();
					foreach($xml as $key => $value)
					{
						if (!in_array($key, $key_s))
						{
							array_push($key_s, $key);
						}
					}
					$parent_row[$parent]['noeuds'] = $key_s;
					if (!in_array($parent_row, $this->array_xml_attribute))
					{
						array_push($this->array_xml_attribute, $parent_row);
					}

				}
			}
			else{
				if (!in_array($parent_row, $this->array_xml_attribute))
				{
					array_push($this->array_xml_attribute, $parent_row);
				}
			}
		}

		$child_count = 0;
		foreach($xml as $key => $value)
		{
			$child_count++;
			$this->getXmlChild($parent.".".$key, $value);
		}

		return $child_count;
	}

	/**
	 * Récupration des valeur d'un noeud xml avec un path données
	 * 
	 * @param type $parent
	 * @param type $mappingEnity
	 * @param type $xml
	 * @return int
	 */
	public function getXmlFileValue($parent, $mappingEnity, $xml)
	{

		// Explode pour avoir l'entité et l'attribut réferencé
		$entity_ref_explode = explode(" // ", $mappingEnity->getRefto());
		$entity_refereced = trim($entity_ref_explode[0]);
		// $entity_ref = trim($entity_ref_explode[1]);
		$ucf_entity_ref = ucfirst(trim($entity_ref_explode[1]));

		// $partner_mappings = $mapping_repo->findOneBy(array("Reference"=>"Location // Reference", "filename"=>$file));
		$instance_entity_ref = new Location();

		$f_getter = "get$ucf_entity_ref";
		$f_setter = "set$ucf_entity_ref";


		$attrs_wanted = $mappingEnity->getOptions();
		$xpath = $mappingEnity->getXpath();
		$refto = $mappingEnity->getRefto();

		if ($refto != null)
		{
			$child_count = 0;
			$i = 0;
			foreach($xml as $key => $value)
			{
				$child_count++;
				$i++;
				$r_count = $this->getXmlFileValue($parent.".".$key, $mappingEnity, $value);
				$xpath_explode = explode(" : ", $xpath);

				// $parent = chemin xml
				// Si le chamin xml courant  = chemin xml de mappingEntyti

				if ($parent.".".$key == $xpath_explode[0])
				{
					// Récuparation des attribut du noeud xml actuel
					$attrs_array = (array)$xml->attributes();

					// Si le noeud actuel a des attributs
					if (is_array(current($attrs_array)))
					{
						foreach (current($attrs_array) as $k => $v)
						{
							// Si les attribut du neud actuelle correspo,
							if (isset($xpath_explode[1]) && $xpath_explode[1] == "@$k='$v'")
							{
								switch ($attrs_wanted)
								{
									case 'balise':
										// var_dump(trim((string)$value));
										break;
									default:
										// var_dump("$v");
										break;
								}
							}else
							{
								// Code
							}
						}
					// Si le noeud actuel n'a psa d'attributs
					}else
					{
						if( $r_count == 0)
						{
							// var_dump($parent,$xpath_explode[0]);
							// var_dump(trim((string)$value));
							// var_dump($parent);
						}

					}
				}
			}
			return $child_count;
		}else{
			var_dump("le fichier n'est ");
		}
	}

	/**
	 * 
	 * @param type $file
	 * @param type $partner
	 */
	public function getConfiguredPathAction($file, $partner){

		// $em = $this->getDoctrine()->getManager();

		// $mapping_repo = self::$em->getRepository('LefDataBundle:Mapping');

		// $mappings = $mapping_repo->findBy(array('filename'=>$file, 'partner'=>$partner));

		// return $mappings;
	}


	/**
	 * 
	 * @return array
	 */
	public function getEntityAttrs()
	{
		$r = array();
		// Instantiation d'une entité Location pour avoir ces champs
		$alrt = new Alert();
		$be = new BienEtre();
		$eqpmt = new Equipement();
		$lklz = new Localisation();
		$lks = new Location();
		$mdia = new Media();
		$ofr = new Offre();
		$plng = new Planning();
		$czn = new Saison();
		$tem = new Theme();
		$tplks = new TypeLocation();
		$optchld = new OptionEnfant();
		$opthdcp = new OptionHandicapees();
		array_push(
			$r,
			$alrt->getAttrs(),
			$be->getAttrs(),
			$eqpmt->getAttrs(),
			$lklz->getAttrs(),
			$lks->getAttrs(),
			$mdia->getAttrs(),
			$ofr->getAttrs(),
			$plng->getAttrs(),
			$czn->getAttrs(),
			$tem->getAttrs(),
			$tplks->getAttrs(),
			$optchld->getAttrs(),
			$opthdcp->getAttrs()
		);

		return $r;
	}



}


