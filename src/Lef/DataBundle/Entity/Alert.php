<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alert
 *
 * @ORM\Table(name="alert")
 * @ORM\Entity
 */
class Alert
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="annonce_ref", type="text", nullable=false)
     */
    private $annonceRef;

    /**
     * @var string
     *
     * @ORM\Column(name="enddate", type="text", nullable=false)
     */
    private $enddate;

    /**
     * @var string
     *
     * @ORM\Column(name="startdate", type="text", nullable=false)
     */
    private $startdate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Location", mappedBy="alert")
     */
    private $location;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->location = new \Doctrine\Common\Collections\ArrayCollection();
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Alert
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set annonceRef
     *
     * @param string $annonceRef
     * @return Alert
     */
    public function setAnnonceRef($annonceRef)
    {
        $this->annonceRef = $annonceRef;
    
        return $this;
    }

    /**
     * Get annonceRef
     *
     * @return string 
     */
    public function getAnnonceRef()
    {
        return $this->annonceRef;
    }

    /**
     * Set enddate
     *
     * @param string $enddate
     * @return Alert
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;
    
        return $this;
    }

    /**
     * Get enddate
     *
     * @return string 
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set startdate
     *
     * @param string $startdate
     * @return Alert
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;
    
        return $this;
    }

    /**
     * Get startdate
     *
     * @return string 
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Add location
     *
     * @param \Lef\DataBundle\Entity\Location $location
     * @return Alert
     */
    public function addLocation(\Lef\DataBundle\Entity\Location $location)
    {
        $this->location[] = $location;
    
        return $this;
    }

    /**
     * Remove location
     *
     * @param \Lef\DataBundle\Entity\Location $location
     */
    public function removeLocation(\Lef\DataBundle\Entity\Location $location)
    {
        $this->location->removeElement($location);
    }

    /**
     * Get location
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocation()
    {
        return $this->location;
    }
}