<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Equipement
 *
 * @ORM\Table(name="equipement")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\EquipementRepository")
 */
class Equipement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    
    /**
     * 
     * @return type
     */
    public function __toString() {
        return $this->titre;
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Equipement
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set locations
     *
     * @param \Lef\DataBundle\Entity\Location $locations
     * @return Equipement
     */
    public function setLocations(\Lef\DataBundle\Entity\Location $locations = null)
    {
        $this->locations = $locations;
    
        return $this;
    }

    /**
     * Get locations
     *
     * @return \Lef\DataBundle\Entity\Location 
     */
    public function getLocations()
    {
        return $this->locations;
    }
}