<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Facture
 *
 * @ORM\Table(name="facture")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\FactureRepository")
 */
class Facture {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string")
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="montant", type="float")
     */
    private $montant;

    /**
     * ref annonceur manytoone unidirectional
     * @ORM\ManyToOne(targetEntity="\Lef\BoBundle\Entity\User")
     * @ORM\JoinColumn(name="annonceur_id", referencedColumnName="id")
     */
    private $annonceur;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Facture
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    
        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Facture
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set montant
     *
     * @param string $montant
     * @return Facture
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    
        return $this;
    }

    /**
     * Get montant
     *
     * @return string 
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set annonceur
     *
     * @param \Lef\BoBundle\Entity\User $annonceur
     * @return Facture
     */
    public function setAnnonceur(\Lef\BoBundle\Entity\User $annonceur = null)
    {
        $this->annonceur = $annonceur;
    
        return $this;
    }

    /**
     * Get annonceur
     *
     * @return \Lef\BoBundle\Entity\User 
     */
    public function getAnnonceur()
    {
        return $this->annonceur;
    }
}