<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FileMap
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\FileMapRepository")
 */
class FileMap
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="partnerId", type="integer")
     */
    private $partnerId;

    /**
     * @var string
     *
     * @ORM\Column(name="fileName", type="string", length=255)
     */
    private $fileName;

    /**
     * @var string
     *
     * @ORM\Column(name="mapping", type="text", nullable=true)
     */
    private $mapping;


    /**
     * @var string $is_mapped
     *
     * @ORM\Column(name="is_mapped", type="boolean", nullable=true)
     */
    private $is_mapped;


    /**
     * @var string
     *
     * @ORM\Column(name="fileAttrubuts", type="text", nullable=true)
     */
    private $fileAttrubuts;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set partnerId
     *
     * @param integer $partnerId
     * @return FileMap
     */
    public function setPartnerId($partnerId)
    {
        $this->partnerId = $partnerId;
    
        return $this;
    }

    /**
     * Get partnerId
     *
     * @return integer 
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return FileMap
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    
        return $this;
    }

    /**
     * Get fileName
     *
     * @return string 
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set mapping
     *
     * @param string $mapping
     * @return FileMap
     */
    public function setMapping($mapping)
    {
        $this->mapping = $mapping;
    
        return $this;
    }

    /**
     * Get mapping
     *
     * @return string 
     */
    public function getMapping()
    {
        return $this->mapping;
    }

    /**
     * Set is_mapped
     *
     * @param boolean $isMapped
     * @return FileMap
     */
    public function setIsMapped($isMapped)
    {
        $this->is_mapped = $isMapped;
    
        return $this;
    }

    /**
     * Get is_mapped
     *
     * @return boolean 
     */
    public function getIsMapped()
    {
        return $this->is_mapped;
    }

    /**
     * Set fileAttrubuts
     *
     * @param string $fileAttrubuts
     * @return FileMap
     */
    public function setFileAttrubuts($fileAttrubuts)
    {
        $this->fileAttrubuts = $fileAttrubuts;
    
        return $this;
    }

    /**
     * Get fileAttrubuts
     *
     * @return string 
     */
    public function getFileAttrubuts()
    {
        return $this->fileAttrubuts;
    }
}