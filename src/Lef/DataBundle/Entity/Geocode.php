<?php
//
// Stock geocode with country (region, ville) name(s)
//
//
namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Geocode
 *
 * @ORM\Table(name="geocode")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\GeocodeRepository")
 */
class Geocode {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=255, nullable=false)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="full_addr", type="string", length=255, nullable=true)
     */
    private $fullAddr;

    /**
     * @var float
     *
     * @ORM\Column(name="center_lng", type="float", nullable=false)
     */
    private $centerLng;

    /**
     * @var float
     *
     * @ORM\Column(name="center_lat", type="float", nullable=false)
     */
    private $centerLat;

    /**
     * @var float
     *
     * @ORM\Column(name="bound_n", type="float", nullable=false)
     */
    private $boundN;

    /**
     * @var float
     *
     * @ORM\Column(name="bound_e", type="float", nullable=false)
     */
    private $boundE;

    /**
     * @var float
     *
     * @ORM\Column(name="bound_s", type="float", nullable=false)
     */
    private $boundS;

    /**
     * @var float
     *
     * @ORM\Column(name="bound_w", type="float", nullable=false)
     */
    private $boundW;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pays
     *
     * @param string $pays
     * @return Geocode
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    
        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Geocode
     */
    public function setRegion($region)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Geocode
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    
        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set fullAddr
     *
     * @param string $fullAddr
     * @return Geocode
     */
    public function setFullAddr($fullAddr)
    {
        $this->fullAddr = $fullAddr;
    
        return $this;
    }

    /**
     * Get fullAddr
     *
     * @return string 
     */
    public function getFullAddr()
    {
        return $this->fullAddr;
    }

    /**
     * Set centerLng
     *
     * @param float $centerLng
     * @return Geocode
     */
    public function setCenterLng($centerLng)
    {
        $this->centerLng = $centerLng;
    
        return $this;
    }

    /**
     * Get centerLng
     *
     * @return float 
     */
    public function getCenterLng()
    {
        return $this->centerLng;
    }

    /**
     * Set centerLat
     *
     * @param float $centerLat
     * @return Geocode
     */
    public function setCenterLat($centerLat)
    {
        $this->centerLat = $centerLat;
    
        return $this;
    }

    /**
     * Get centerLat
     *
     * @return float 
     */
    public function getCenterLat()
    {
        return $this->centerLat;
    }

    /**
     * Set boundN
     *
     * @param float $boundN
     * @return Geocode
     */
    public function setBoundN($boundN)
    {
        $this->boundN = $boundN;
    
        return $this;
    }

    /**
     * Get boundN
     *
     * @return float 
     */
    public function getBoundN()
    {
        return $this->boundN;
    }

    /**
     * Set boundE
     *
     * @param float $boundE
     * @return Geocode
     */
    public function setBoundE($boundE)
    {
        $this->boundE = $boundE;
    
        return $this;
    }

    /**
     * Get boundE
     *
     * @return float 
     */
    public function getBoundE()
    {
        return $this->boundE;
    }

    /**
     * Set boundS
     *
     * @param float $boundS
     * @return Geocode
     */
    public function setBoundS($boundS)
    {
        $this->boundS = $boundS;
    
        return $this;
    }

    /**
     * Get boundS
     *
     * @return float 
     */
    public function getBoundS()
    {
        return $this->boundS;
    }

    /**
     * Set boundW
     *
     * @param float $boundW
     * @return Geocode
     */
    public function setBoundW($boundW)
    {
        $this->boundW = $boundW;
    
        return $this;
    }

    /**
     * Get boundW
     *
     * @return float 
     */
    public function getBoundW()
    {
        return $this->boundW;
    }
}