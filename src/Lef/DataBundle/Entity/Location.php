<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Location
 *
 * @ORM\Table(name="location")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\LocationRepository")
 * 
 */
class Location {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=false)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=255, nullable=true)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="departement", type="string", length=255, nullable=true)
     */
    private $departement;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="ZipPostalCode", type="string", length=255, nullable=true)
     */
    private $ZipPostalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreDePersonne", type="string", length=255, nullable=true)
     */
    private $nombreDePersonne;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreEtoiles", type="string", length=255, nullable=true)
     */
    private $nombreEtoiles;

    /**
     * @var float
     *
     * @ORM\Column(name="surfaceHabitable", type="float", nullable=true, options={"default" = 0})
     */
    private $surfaceHabitable;

    /**
     * @var string
     *
     * @ORM\Column(name="nombrePiece", type="string", length=255, nullable=true)
     */
    private $nombrePiece;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreSalleDeBain", type="string", length=255, nullable=true)
     */
    private $nombreSalleDeBain;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreChambre", type="string", length=255, nullable=true)
     */
    private $nombreChambre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="animauxDomestiques", type="boolean", nullable=true)
     */
    private $animauxDomestiques;

    /**
     * @var boolean
     *
     * @ORM\Column(name="piscine", type="boolean", nullable=true)
     */
    private $piscine;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionBreve", type="text", nullable=true)
     */
    private $descriptionBreve;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionDetaillee", type="text", nullable=true)
     */
    private $descriptionDetaillee;

    /**
     * @var string
     *
     * @ORM\Column(name="caracteristiques", type="text", nullable=true)
     */
    private $caracteristiques;

    /**
     * @var string
     *
     * @ORM\Column(name="amenagement", type="text", nullable=true)
     */
    private $amenagement;

    /**
     * @var string
     *
     * @ORM\Column(name="sejourAvecCouchage", type="text", nullable=true)
     */
    private $sejourAvecCouchage;

    /**
     * @var string
     *
     * @ORM\Column(name="coinRepas", type="text", nullable=true)
     */
    private $coinRepas;

    /**
     * @var string
     *
     * @ORM\Column(name="cuisine", type="text", nullable=true)
     */
    private $cuisine;

    /**
     * @var string
     *
     * @ORM\Column(name="sanitaire", type="text", nullable=true)
     */
    private $sanitaire;

    /**
     * @var string
     *
     * @ORM\Column(name="distanceOther", type="text", nullable=true)
     */
    private $distanceOther;

    /**
     * @var float
     *
     * @ORM\Column(name="distanceMer", type="float", nullable=true)
     */
    private $distanceMer;

    /**
     * @var float
     *
     * @ORM\Column(name="distanceLac", type="float", nullable=true)
     */
    private $distanceLac;

    /**
     * @var float
     *
     * @ORM\Column(name="distanceSki", type="float", nullable=true)
     */
    private $distanceSki;

    /**
     * @var float
     *
     * @ORM\Column(name="distancePublicTransport", type="float", nullable=true)
     */
    private $distancePublicTransport;

    /**
     * @var float
     *
     * @ORM\Column(name="distanceGolf", type="float", nullable=true)
     */
    private $distanceGolf;

    /**
     * @var float
     *
     * @ORM\Column(name="distanceRemontees", type="float", nullable=true)
     */
    private $distanceRemontees;

    /**
     * @var float
     *
     * @ORM\Column(name="distanceCentreVille", type="float", nullable=true)
     */
    private $distanceCentreVille;

    /**
     * @var float
     *
     * @ORM\Column(name="distanceShopping", type="float", nullable=true)
     */
    private $distanceShopping;

    /**
     * @var float
     *
     * @ORM\Column(name="rentalprice", type="float", nullable=true, options={"default" = 0})
     */
    private $rentalPrice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modificationDate", type="datetime", nullable=true)
     */
    private $modificationDate;

    /**
     * 
     * @var boolean
     * @ORM\Column(name="ADM", type="boolean", nullable=true)
     */
    private $adm;

    /**
     * @var boolean
     *
     * @ORM\Column(name="APM", type="boolean", nullable=true)
     */
    private $apm;

    /**
     * @var boolean
     * @ORM\Column(name="lastMinute", type="boolean", nullable=true)
     */
    private $lastMinute;

    /**
     * @var boolean
     *
     * @ORM\Column(name="promo", type="boolean", nullable=true)
     */
    private $promo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean", nullable=true)
     */
    private $locked;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_vus_all", type="integer", nullable=true)
     */
    private $nbrVusAll;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_vus_30j", type="integer", nullable=true)
     */
    private $nbrVus30j;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_resa_all", type="integer", nullable=true)
     */
    private $nbrResaAll;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_resa_30j", type="integer", nullable=true)
     */
    private $nbrResa30j;

    /**
     * @var string
     *
     * @ORM\Column(name="equipementsPartner", type="text", nullable=true)
     */
    private $equipementsPartner;

    /**
     * @var string
     *
     * @ORM\Column(name="typePartner", type="text", nullable=true)
     */
    private $typePartner;

    /**
     * @var string
     *
     * @ORM\Column(name="themePartner", type="text", nullable=true)
     */
    private $themePartner;

    /**
     * @var integer
     *
     * @ORM\Column(name="etage", type="integer", nullable=true)
     */
    private $etage;

    /**
     * @var string
     *
     * @ORM\Column(name="brandPartner", type="text", nullable=true)
     */
    private $brandPartner;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datedispodebut", type="date", nullable=false)
     */
    private $dateDispoDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datedispofin", type="date", nullable=false)
     */
    private $dateDispoFin;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Alert", inversedBy="location",cascade={"persist"})
     * @ORM\JoinTable(name="location_alerts",
     *   joinColumns={
     *     @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="alert_id", referencedColumnName="id")
     *   }
     * )
     */
    private $alert;

    /**
     * @var ArrayCollection $medias
     * 
     * @ORM\ManyToMany(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="location_medias",
     *      joinColumns={@ORM\JoinColumn(name="location_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="media_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     * 
     * @Assert\Count(min = 3, minMessage = "Vous devez ajouter au moins 3 images.",
     *      max = 8, maxMessage = "Vous ne pouvez ajouter que 8 images au max.")
     * 
     */
    private $medias;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Offre", inversedBy="location",cascade={"persist"})
     * @ORM\JoinTable(name="location_offres",
     *   joinColumns={
     *     @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="offre_id", referencedColumnName="id")
     *   }
     * )
     */
    private $offre;

//    /**
//     * @var \Doctrine\Common\Collections\Collection
//     *
//     * @ORM\ManyToMany(targetEntity="Planning", inversedBy="location",cascade={"persist", "remove"})
//     * @ORM\JoinTable(name="location_plannings",
//     *   joinColumns={
//     *     @ORM\JoinColumn(name="location_id", referencedColumnName="id")
//     *   },
//     *   inverseJoinColumns={
//     *     @ORM\JoinColumn(name="planning_id", referencedColumnName="id")
//     *   }
//     * )
//     */
//    private $planning;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="BienEtre", inversedBy="location",cascade={"persist"})
     * @ORM\JoinTable(name="locations_bien_etres",
     *   joinColumns={
     *     @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="bien_etre_id", referencedColumnName="id")
     *   }
     * )
     */
    private $bienEtres;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Equipement", inversedBy="location",cascade={"persist"})
     * @ORM\JoinTable(name="locations_equipements",
     *   joinColumns={
     *     @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="equipement_id", referencedColumnName="id")
     *   }
     * )
     */
    private $equipements;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Localisation", inversedBy="location",cascade={"persist"})
     * @ORM\JoinTable(name="locations_localisations",
     *   joinColumns={
     *     @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="localisation_id", referencedColumnName="id")
     *   }
     * )
     */
    private $localisations;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="OptionEnfant", inversedBy="location",cascade={"persist"})
     * @ORM\JoinTable(name="locations_options_enfants",
     *   joinColumns={
     *     @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="option_enfnt_id", referencedColumnName="id")
     *   }
     * )
     */
    private $optionEnfants;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="OptionHandicapees", inversedBy="location",cascade={"persist"})
     * @ORM\JoinTable(name="locations_options_handicapees",
     *   joinColumns={
     *     @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="option_handicapee_id", referencedColumnName="id")
     *   }
     * )
     */
    private $optionHandicapees;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Saison", inversedBy="location",cascade={"persist"})
     * @ORM\JoinTable(name="locations_saisons",
     *   joinColumns={
     *     @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="saison_id", referencedColumnName="id")
     *   }
     * )
     */
    private $saisons;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Theme", inversedBy="location",cascade={"persist"})
     * @ORM\JoinTable(name="locations_themes",
     *   joinColumns={
     *     @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="theme_id", referencedColumnName="id")
     *   }
     * )
     */
    private $themes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Type", inversedBy="location",cascade={"persist"})
     * @ORM\JoinTable(name="locations_types",
     *   joinColumns={
     *     @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     *   }
     * )
     */
    private $types;
    //=====================================
    //======ManyToOne Unidirectional=======
    //=====================================  
    /**
     * ref annonceur manytoone unidirectional
     * @ORM\ManyToOne(targetEntity="\Lef\BoBundle\Entity\User")
     * @ORM\JoinColumn(name="annonceur_id", referencedColumnName="id")
     */
    private $annonceur;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TranchePrix", inversedBy="location",cascade={"persist", "remove"})
     * @ORM\JoinTable(name="location_trancheprix",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_location", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_tranche_prix", referencedColumnName="id")
     *   }
     * )
     */
    private $tranchePrix;

    /**
     * Constructor
     */
    public function __construct() {
        $this->alert = new \Doctrine\Common\Collections\ArrayCollection();
        $this->medias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->offre = new \Doctrine\Common\Collections\ArrayCollection();
        $this->planning = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bienEtres = new \Doctrine\Common\Collections\ArrayCollection();
        $this->equipement = new \Doctrine\Common\Collections\ArrayCollection();
        $this->localisations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->optionEnfants = new \Doctrine\Common\Collections\ArrayCollection();
        $this->optionHandicapee = new \Doctrine\Common\Collections\ArrayCollection();
        $this->saisons = new \Doctrine\Common\Collections\ArrayCollection();
        $this->themes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->types = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Location
     */
    public function setReference($reference) {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference() {
        return $this->reference;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Location
     */
    public function setTitre($titre) {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre() {
        return $this->titre;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Location
     */
    public function setAdresse($adresse) {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse() {
        return $this->adresse;
    }

    /**
     * Set pays
     *
     * @param string $pays
     * @return Location
     */
    public function setPays($pays) {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays() {
        return $this->pays;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Location
     */
    public function setRegion($region) {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * Set departement
     *
     * @param string $departement
     * @return Location
     */
    public function setDepartement($departement) {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return string 
     */
    public function getDepartement() {
        return $this->departement;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Location
     */
    public function setVille($ville) {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille() {
        return $this->ville;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Location
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Location
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * Set zippostalcode
     *
     * @param string $zippostalcode
     * @return Location
     */
    public function setZipPostalCode($ZipPostalCode) {
        $this->ZipPostalCode = $ZipPostalCode;

        return $this;
    }

    /**
     * Get zippostalcode
     *
     * @return string 
     */
    public function getZipPostalCode() {
        return $this->ZipPostalCode;
    }

    /**
     * Set nombreDePersonne
     *
     * @param string $nombreDePersonne
     * @return Location
     */
    public function setNombreDePersonne($nombredepersonne) {
        $this->nombreDePersonne = $nombredepersonne;

        return $this;
    }

    /**
     * Get nombreDePersonne
     *
     * @return string 
     */
    public function getNombreDePersonne() {
        return $this->nombreDePersonne;
    }

    /**
     * Set nombreEtoiles
     *
     * @param string $nombreEtoiles
     * @return Location
     */
    public function setNombreEtoiles($nombreetoiles) {
        $this->nombreEtoiles = $nombreetoiles;

        return $this;
    }

    /**
     * Get nombreEtoiles
     *
     * @return string 
     */
    public function getNombreEtoiles() {
        return $this->nombreEtoiles;
    }

    /**
     * Set surfaceHabitable
     *
     * @param float $surfaceHabitable
     * @return Location
     */
    public function setSurfaceHabitable($surfacehabitable) {
        $this->surfaceHabitable = $surfacehabitable;

        return $this;
    }

    /**
     * Get surfaceHabitable
     *
     * @return float 
     */
    public function getSurfaceHabitable() {
        return $this->surfaceHabitable;
    }

    /**
     * Set nombrepiece
     *
     * @param string $nombrepiece
     * @return Location
     */
    public function setNombrePiece($nombrePiece) {
        $this->nombrePiece = $nombrePiece;

        return $this;
    }

    /**
     * Get nombrepiece
     *
     * @return string 
     */
    public function getNombrePiece() {
        return $this->nombrePiece;
    }

    /**
     * Set nombresalledebain
     *
     * @param string $nombresalledebain
     * @return Location
     */
    public function setNombreSalleDeBain($nombreSalleDeBain) {
        $this->nombreSalleDeBain = $nombreSalleDeBain;

        return $this;
    }

    /**
     * Get nombresalledebain
     *
     * @return string 
     */
    public function getNombreSalleDeBain() {
        return $this->nombreSalleDeBain;
    }

    /**
     * Set nombrechambre
     *
     * @param string $nombrechambre
     * @return Location
     */
    public function setNombreChambre($nombreChambre) {
        $this->nombreChambre = $nombreChambre;

        return $this;
    }

    /**
     * Get nombrechambre
     *
     * @return string 
     */
    public function getNombreChambre() {
        return $this->nombreChambre;
    }

    /**
     * Set animauxdomestiques
     *
     * @param boolean $animauxdomestiques
     * @return Location
     */
    public function setAnimauxDomestiques($animauxDomestiques) {
        $this->animauxDomestiques = $animauxDomestiques;

        return $this;
    }

    /**
     * Get animauxdomestiques
     *
     * @return boolean 
     */
    public function getAnimauxDomestiques() {
        return $this->animauxDomestiques;
    }

    /**
     * Set piscine
     *
     * @param boolean $piscine
     * @return Location
     */
    public function setPiscine($piscine) {
        $this->piscine = $piscine;

        return $this;
    }

    /**
     * Get piscine
     *
     * @return boolean 
     */
    public function getPiscine() {
        return $this->piscine;
    }

    /**
     * Set descriptionbreve
     *
     * @param string $descriptionbreve
     * @return Location
     */
    public function setDescriptionBreve($descriptionBreve) {
        $this->descriptionBreve = $descriptionBreve;

        return $this;
    }

    /**
     * Get descriptionbreve
     *
     * @return string 
     */
    public function getDescriptionBreve() {
        return $this->descriptionBreve;
    }

    /**
     * Set descriptiondetaillee
     *
     * @param string $descriptiondetaillee
     * @return Location
     */
    public function setDescriptionDetaillee($descriptionDetaillee) {
        $this->descriptionDetaillee = $descriptionDetaillee;

        return $this;
    }

    /**
     * Get descriptiondetaillee
     *
     * @return string 
     */
    public function getDescriptionDetaillee() {
        return $this->descriptionDetaillee;
    }

    /**
     * Set caracteristiques
     *
     * @param string $caracteristiques
     * @return Location
     */
    public function setCaracteristiques($caracteristiques) {
        $this->caracteristiques = $caracteristiques;

        return $this;
    }

    /**
     * Get caracteristiques
     *
     * @return string 
     */
    public function getCaracteristiques() {
        return $this->caracteristiques;
    }

    /**
     * Set amenagement
     *
     * @param string $amenagement
     * @return Location
     */
    public function setAmenagement($amenagement) {
        $this->amenagement = $amenagement;

        return $this;
    }

    /**
     * Get amenagement
     *
     * @return string 
     */
    public function getAmenagement() {
        return $this->amenagement;
    }

    /**
     * Set sejouraveccouchage
     *
     * @param string $sejouraveccouchage
     * @return Location
     */
    public function setSejourAvecCouchage($sejourAvecCouchage) {
        $this->sejourAvecCouchage = $sejourAvecCouchage;

        return $this;
    }

    /**
     * Get sejouraveccouchage
     *
     * @return string 
     */
    public function getSejourAvecCouchage() {
        return $this->sejourAvecCouchage;
    }

    /**
     * Set coinrepas
     *
     * @param string $coinrepas
     * @return Location
     */
    public function setCoinRepas($coinRepas) {
        $this->coinRepas = $coinRepas;

        return $this;
    }

    /**
     * Get coinrepas
     *
     * @return string 
     */
    public function getCoinRepas() {
        return $this->coinRepas;
    }

    /**
     * Set cuisine
     *
     * @param string $cuisine
     * @return Location
     */
    public function setCuisine($cuisine) {
        $this->cuisine = $cuisine;

        return $this;
    }

    /**
     * Get cuisine
     *
     * @return string 
     */
    public function getCuisine() {
        return $this->cuisine;
    }

    /**
     * Set sanitaire
     *
     * @param string $sanitaire
     * @return Location
     */
    public function setSanitaire($sanitaire) {
        $this->sanitaire = $sanitaire;

        return $this;
    }

    /**
     * Get sanitaire
     *
     * @return string 
     */
    public function getSanitaire() {
        return $this->sanitaire;
    }

    /**
     * Set distanceother
     *
     * @param string $distanceother
     * @return Location
     */
    public function setDistanceOther($distanceOther) {
        $this->distanceOther = $distanceOther;

        return $this;
    }

    /**
     * Get distanceother
     *
     * @return string 
     */
    public function getDistanceOther() {
        return $this->distanceOther;
    }

    /**
     * Set distancemer
     *
     * @param float $distancemer
     * @return Location
     */
    public function setDistanceMer($distanceMer) {
        $this->distanceMer = $distanceMer;

        return $this;
    }

    /**
     * Get distancemer
     *
     * @return float 
     */
    public function getDistanceMer() {
        return $this->distanceMer;
    }

    /**
     * Set distancelac
     *
     * @param float $distancelac
     * @return Location
     */
    public function setDistanceLac($distanceLac) {
        $this->distanceLac = $distanceLac;

        return $this;
    }

    /**
     * Get distancelac
     *
     * @return float 
     */
    public function getDistanceLac() {
        return $this->distanceLac;
    }

    /**
     * Set distanceski
     *
     * @param float $distanceski
     * @return Location
     */
    public function setDistanceSki($distanceSki) {
        $this->distanceSki = $distanceSki;

        return $this;
    }

    /**
     * Get distanceski
     *
     * @return float 
     */
    public function getDistanceSki() {
        return $this->distanceSki;
    }

    /**
     * Set distancepublictransport
     *
     * @param float $distancepublictransport
     * @return Location
     */
    public function setDistancePublicTransport($distancePublicTransport) {
        $this->distancePublicTransport = $distancePublicTransport;

        return $this;
    }

    /**
     * Get distancepublictransport
     *
     * @return float 
     */
    public function getDistancePublicTransport() {
        return $this->distancePublicTransport;
    }

    /**
     * Set distancegolf
     *
     * @param float $distancegolf
     * @return Location
     */
    public function setDistanceGolf($distanceGolf) {
        $this->distanceGolf = $distanceGolf;

        return $this;
    }

    /**
     * Get distancegolf
     *
     * @return float 
     */
    public function getDistanceGolf() {
        return $this->distanceGolf;
    }

    /**
     * Set distanceremontees
     *
     * @param float $distanceRemontees
     * @return Location
     */
    public function setDistanceRemontees($distanceRemontees) {
        $this->distanceRemontees = $distanceRemontees;

        return $this;
    }

    /**
     * Get distanceRemontees
     *
     * @return float 
     */
    public function getDistanceRemontees() {
        return $this->distanceRemontees;
    }

    /**
     * Set distancecentreville
     *
     * @param float $distancecentreville
     * @return Location
     */
    public function setDistanceCentreVille($distanceCentreVille) {
        $this->distanceCentreVille = $distanceCentreVille;

        return $this;
    }

    /**
     * Get distancecentreville
     *
     * @return float 
     */
    public function getDistanceCentreVille() {
        return $this->distanceCentreVille;
    }

    /**
     * Set distanceshopping
     *
     * @param float $distanceshopping
     * @return Location
     */
    public function setDistanceShopping($distanceShopping) {
        $this->distanceShopping = $distanceShopping;

        return $this;
    }

    /**
     * Get distanceshopping
     *
     * @return float 
     */
    public function getDistanceShopping() {
        return $this->distanceShopping;
    }

    /**
     * Set rentalprice
     *
     * @param float $rentalprice
     * @return Location
     */
    public function setRentalPrice($rentalPrice) {
        $this->rentalPrice = $rentalPrice;

        return $this;
    }

    /**
     * Get rentalprice
     *
     * @return float 
     */
    public function getRentalPrice() {
        return $this->rentalPrice;
    }

    /**
     * Set creationdate
     *
     * @param \DateTime $creationdate
     * @return Location
     */
    public function setCreationDate($creationDate) {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationdate
     *
     * @return \DateTime 
     */
    public function getCreationDate() {
        return $this->creationDate;
    }

    /**
     * Set modificationdate
     *
     * @param \DateTime $modificationdate
     * @return Location
     */
    public function setModificationDate($modificationDate) {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationdate
     *
     * @return \DateTime 
     */
    public function getModificationDate() {
        return $this->modificationDate;
    }

    /**
     * Set adm
     *
     * @param boolean $adm
     * @return Location
     */
    public function setAdm($adm) {
        $this->adm = $adm;

        return $this;
    }

    /**
     * Get adm
     *
     * @return boolean 
     */
    public function getAdm() {
        return $this->adm;
    }

    /**
     * Set apm
     *
     * @param boolean $apm
     * @return Location
     */
    public function setApm($apm) {
        $this->apm = $apm;

        return $this;
    }

    /**
     * Get apm
     *
     * @return boolean 
     */
    public function getApm() {
        return $this->apm;
    }

    /**
     * Set lastminute
     *
     * @param boolean $lastminute
     * @return Location
     */
    public function setLastMinute($lastminute) {
        $this->lastMinute = $lastminute;

        return $this;
    }

    /**
     * Get lastminute
     *
     * @return boolean 
     */
    public function getMinute() {
        return $this->lastMinute;
    }

    /**
     * Set promo
     *
     * @param boolean $promo
     * @return Location
     */
    public function setPromo($promo) {
        $this->promo = $promo;

        return $this;
    }

    /**
     * Get promo
     *
     * @return boolean 
     */
    public function getPromo() {
        return $this->promo;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     * @return Location
     */
    public function setLocked($locked) {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean 
     */
    public function getLocked() {
        return $this->locked;
    }

    /**
     * Set nbrVusAll
     *
     * @param integer $nbrVusAll
     * @return Location
     */
    public function setNbrVusAll($nbrVusAll) {
        $this->nbrVusAll = $nbrVusAll;

        return $this;
    }

    /**
     * Get nbrVusAll
     *
     * @return integer 
     */
    public function getNbrVusAll() {
        return $this->nbrVusAll;
    }

    /**
     * Set nbrVus30j
     *
     * @param integer $nbrVus30j
     * @return Location
     */
    public function setNbrVus30j($nbrVus30j) {
        $this->nbrVus30j = $nbrVus30j;

        return $this;
    }

    /**
     * Get nbrVus30j
     *
     * @return integer 
     */
    public function getNbrVus30j() {
        return $this->nbrVus30j;
    }

    /**
     * Set nbrResaAll
     *
     * @param integer $nbrResaAll
     * @return Location
     */
    public function setNbrResaAll($nbrResaAll) {
        $this->nbrResaAll = $nbrResaAll;

        return $this;
    }

    /**
     * Get nbrResaAll
     *
     * @return integer 
     */
    public function getNbrResaAll() {
        return $this->nbrResaAll;
    }

    /**
     * Set nbrResa30j
     *
     * @param integer $nbrResa30j
     * @return Location
     */
    public function setNbrResa30j($nbrResa30j) {
        $this->nbrResa30j = $nbrResa30j;

        return $this;
    }

    /**
     * Get nbrResa30j
     *
     * @return integer 
     */
    public function getNbrResa30j() {
        return $this->nbrResa30j;
    }

    /**
     * Set equipementspartner
     *
     * @param string $equipementspartner
     * @return Location
     */
    public function setEquipementsPartner($equipementsPartner) {
        $this->equipementsPartner = $equipementsPartner;

        return $this;
    }

    /**
     * Get equipementspartner
     *
     * @return string 
     */
    public function getEquipementsPartner() {
        return $this->equipementsPartner;
    }

    /**
     * Set typepartner
     *
     * @param string $typepartner
     * @return Location
     */
    public function setTypePartner($typePartner) {
        $this->typePartner = $typePartner;

        return $this;
    }

    /**
     * Get typepartner
     *
     * @return string 
     */
    public function getTypePartner() {
        return $this->typePartner;
    }

    /**
     * Set themepartner
     *
     * @param string $themepartner
     * @return Location
     */
    public function setThemePartner($themePartner) {
        $this->themePartner = $themePartner;

        return $this;
    }

    /**
     * Get themepartner
     *
     * @return string 
     */
    public function getThemePartner() {
        return $this->themePartner;
    }

    /**
     * Set etage
     *
     * @param integer $etage
     * @return Location
     */
    public function setEtage($etage) {
        $this->etage = $etage;

        return $this;
    }

    /**
     * Get etage
     *
     * @return integer 
     */
    public function getEtage() {
        return $this->etage;
    }

    /**
     * Set brandpartner
     *
     * @param string $brandpartner
     * @return Location
     */
    public function setBrandPartner($brandPartner) {
        $this->brandPartner = $brandPartner;

        return $this;
    }

    /**
     * Get brandpartner
     *
     * @return string 
     */
    public function getBrandPartner() {
        return $this->brandPartner;
    }

    /**
     * Set dateDispoDebut
     *
     * @param \DateTime $dateDispoDebut
     * @return Location
     */
    public function setDateDispoDebut($dateDispoDebut) {
        $this->dateDispoDebut = $dateDispoDebut;

        return $this;
    }

    /**
     * Get dateDispoDebut
     *
     * @return \DateTime 
     */
    public function getDateDispoDebut() {
        return $this->dateDispoDebut;
    }

    /**
     * Set dateDispoFin
     *
     * @param \DateTime $dateDispoFin
     * @return Location
     */
    public function setDateDispoFin($dateDispoFin) {
        $this->dateDispoFin = $dateDispoFin;

        return $this;
    }

    /**
     * Get dateDispoFin
     *
     * @return \DateTime 
     */
    public function getDateDispoFin() {
        return $this->dateDispoFin;
    }

    /**
     * Add alert
     *
     * @param \Lef\DataBundle\Entity\Alert $alert
     * @return Location
     */
    public function addAlert(\Lef\DataBundle\Entity\Alert $alert) {
        $this->alert[] = $alert;

        return $this;
    }

    /**
     * Remove alert
     *
     * @param \Lef\DataBundle\Entity\Alert $alert
     */
    public function removeAlert(\Lef\DataBundle\Entity\Alert $alert) {
        $this->alert->removeElement($alert);
    }

    /**
     * Get alert
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAlert() {
        return $this->alert;
    }

    /**
     * Add media
     *
     * @param \Lef\DataBundle\Entity\Media $media
     * @return Location
     */
    public function addMedia(\Lef\DataBundle\Entity\Media $medias) {
        $this->medias[] = $medias;

        return $this;
    }

    /**
     * Remove media
     *
     * @param \Lef\DataBundle\Entity\Media $media
     */
    public function removeMedia(\Lef\DataBundle\Entity\Media $medias) {
        $this->medias->removeElement($medias);
    }

    /**
     * Get media
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMedias() {
        return $this->medias;
    }

    /**
     * Add offre
     *
     * @param \Lef\DataBundle\Entity\Offre $offre
     * @return Location
     */
    public function addOffre(\Lef\DataBundle\Entity\Offre $offres) {
        $this->offre[] = $offres;

        return $this;
    }

    /**
     * Remove offre
     *
     * @param \Lef\DataBundle\Entity\Offre $offre
     */
    public function removeOffre(\Lef\DataBundle\Entity\Offre $offre) {
        $this->offre->removeElement($offre);
    }

    /**
     * Get offre
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOffre() {
        return $this->offre;
    }

//    /**
//     * Add planning
//     *
//     * @param \Lef\DataBundle\Entity\Planning $planning
//     * @return Location
//     */
//    public function addPlanning(\Lef\DataBundle\Entity\Planning $planning) {
//        $this->planning[] = $planning;
//
//        return $this;
//    }
//    /**
//     * Remove planning
//     *
//     * @param \Lef\DataBundle\Entity\Planning $planning
//     */
//    public function removePlanning(\Lef\DataBundle\Entity\Planning $planning) {
//        $this->planning->removeElement($planning);
//    }
//    /**
//     * Get planning
//     *
//     * @return \Doctrine\Common\Collections\Collection 
//     */
//    public function getPlanning() {
//        return $this->planning;
//    }

    /**
     * Add bienEtre
     *
     * @param \Lef\DataBundle\Entity\BienEtre $bienEtres
     * @return Location
     */
    public function addBienEtre(\Lef\DataBundle\Entity\BienEtre $bienEtres) {
        $this->bienEtres[] = $bienEtres;

        return $this;
    }

    /**
     * Remove bienEtre
     *
     * @param \Lef\DataBundle\Entity\BienEtre $bienEtre
     */
    public function removeBienEtre(\Lef\DataBundle\Entity\BienEtre $bienEtres) {
        $this->bienEtres->removeElement($bienEtres);
    }

    /**
     * Get bienEtre
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBienEtres() {
        return $this->bienEtres;
    }

    /**
     * Get bienEtre
     *
     * @param Description\Doctrine\Common\Collections\Collection $bienEtres
     */
    public function setBienEtres(\Doctrine\Common\Collections\Collection $bienEtres) {
        return $this->bienEtres = $bienEtres;
    }

    /**
     * Add equipement
     *
     * @param \Lef\DataBundle\Entity\Equipement $equipements
     * @return Location
     */
    public function addEquipement(\Lef\DataBundle\Entity\Equipement $equipements) {
        $this->equipement[] = $equipements;

        return $this;
    }

    /**
     * Remove equipement
     *
     * @param \Lef\DataBundle\Entity\Equipement $equipement
     */
    public function removeEquipement(\Lef\DataBundle\Entity\Equipement $equipements) {
        $this->equipements->removeElement($equipements);
    }

    /**
     * Get equipement
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEquipements() {
        return $this->equipements;
    }

    /**
     * Add localisation
     *
     * @param \Lef\DataBundle\Entity\Localisation $localisation
     * @return Location
     */
    public function addLocalisation(\Lef\DataBundle\Entity\Localisation $localisation) {
        $this->localisations[] = $localisation;

        return $this;
    }

    /**
     * Remove localisation
     *
     * @param \Lef\DataBundle\Entity\Localisation $localisation
     */
    public function removeLocalisation(\Lef\DataBundle\Entity\Localisation $localisation) {
        $this->localisations->removeElement($localisation);
    }

    /**
     * Get localisation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocalisations() {
        return $this->localisations;
    }

    /**
     * Add optionEnfnt
     *
     * @param \Lef\DataBundle\Entity\OptionsEnfant $optionEnfnt
     * @return Location
     */
    public function addOptionEnfant(\Lef\DataBundle\Entity\OptionsEnfant $optionEnfnt) {
        $this->optionEnfants[] = $optionEnfnt;

        return $this;
    }

    /**
     * Remove optionEnfnt
     *
     * @param \Lef\DataBundle\Entity\OptionsEnfant $optionEnfnt
     */
    public function removeOptionEnfant(\Lef\DataBundle\Entity\OptionsEnfant $optionEnfnt) {
        $this->optionEnfants->removeElement($optionEnfnt);
    }

    /**
     * Get optionEnfants
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOptionEnfants() {
        return $this->optionEnfants;
    }

    /**
     * Add optionHandicapees
     *
     * @param \Lef\DataBundle\Entity\OptionHandicapees $optionHandicapees
     * @return Location
     */
    public function addOptionHandicapee(\Lef\DataBundle\Entity\OptionHandicapees $optionHandicapees) {
        $this->optionHandicapees[] = $optionHandicapees;

        return $this;
    }

    /**
     * Remove optionHandicapees
     *
     * @param \Lef\DataBundle\Entity\OptionHandicapees $optionHandicapees
     */
    public function removeOptionHandicapee(\Lef\DataBundle\Entity\OptionHandicapees $optionHandicapees) {
        $this->optionHandicapees->removeElement($optionHandicapees);
    }

    /**
     * Get optionHandicapees
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOptionHandicapees() {
        return $this->optionHandicapees;
    }

    /**
     * Add saison
     *
     * @param \Lef\DataBundle\Entity\Saison $saison
     * @return Location
     */
    public function addSaison(\Lef\DataBundle\Entity\Saison $saison) {
        $this->saisons[] = $saison;

        return $this;
    }

    /**
     * Remove saison
     *
     * @param \Lef\DataBundle\Entity\Saison $saison
     */
    public function removeSaison(\Lef\DataBundle\Entity\Saison $saison) {
        $this->saisons->removeElement($saison);
    }

    /**
     * Get saison
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSaisons() {
        return $this->saisons;
    }

    /**
     * Add theme
     *
     * @param \Lef\DataBundle\Entity\Theme $theme
     * @return Location
     */
    public function addTheme(\Lef\DataBundle\Entity\Theme $theme) {
        $this->themes[] = $theme;

        return $this;
    }

    /**
     * Remove theme
     *
     * @param \Lef\DataBundle\Entity\Theme $theme
     */
    public function removeTheme(\Lef\DataBundle\Entity\Theme $theme) {
        $this->themes->removeElement($theme);
    }

    /**
     * Get theme
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getThemes() {
        return $this->themes;
    }

    /**
     * Add type
     *
     * @param \Lef\DataBundle\Entity\Type $type
     * @return Location
     */
    public function addType(\Lef\DataBundle\Entity\Type $type) {
        $this->types[] = $type;

        return $this;
    }

    /**
     * Remove type
     *
     * @param \Lef\DataBundle\Entity\Type $type
     */
    public function removeType(\Lef\DataBundle\Entity\Type $type) {
        $this->types->removeElement($type);
    }

    /**
     * Get type
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTypes() {
        return $this->types;
    }

    /**
     * Set annonceur
     *
     * @param @param \Lef\BoBundle\Entity\User  $annonceur
     * @return Location
     */
    public function setAnnonceur(\Lef\BoBundle\Entity\User $annonceur = null) {
        $this->annonceur = $annonceur;

        return $this;
    }

    /**
     * Get annonceur
     *
     * @return \Lef\DataBundle\Entity\FosUserUser 
     */
    public function getAnnonceur() {
        return $this->annonceur;
    }

    /**
     * Add tranchePrix
     *
     * @param \Lef\DataBundle\Entity\TranchePrix $tranchePrix
     * @return Location
     */
    public function addTranchePrix(\Lef\DataBundle\Entity\TranchePrix $tranchePrix) {
        $this->tranchePrix[] = $tranchePrix;

        return $this;
    }

    /**
     * Remove tranchePrix
     *
     * @param \Lef\DataBundle\Entity\TranchePrix $tranchePrix
     */
    public function removeTranchePrix(\Lef\DataBundle\Entity\TranchePrix $tranchePrix) {
        $this->tranchePrix->removeElement($tranchePrix);
    }

    /**
     * Get trancheDePrix
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranchePrix() {
        return $this->tranchePrix;
    }
}
