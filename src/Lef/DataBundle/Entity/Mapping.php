<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mapping
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\MappingRepository")
 */
class Mapping
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="xpath", type="string", length=255)
     */
    private $xpath;

    /**
     * @var string
     *
     * @ORM\Column(name="refto", type="string", length=255)
     */
    private $refto;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="limite_location", type="string", length=255)
     */
    private $limite_location;

    /**
     * @var string
     *
     * @ORM\Column(name="partner", type="string", length=255)
     */
    private $partner;

    /**
     * @var string
     *
     * @ORM\Column(name="options", type="string", length=255)
     */
    private $options;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set xpath
     *
     * @param string $xpath
     * @return Mapping
     */
    public function setXpath($xpath)
    {
        $this->xpath = $xpath;
    
        return $this;
    }

    /**
     * Get xpath
     *
     * @return string 
     */
    public function getXpath()
    {
        return $this->xpath;
    }

    /**
     * Set refto
     *
     * @param string $refto
     * @return Mapping
     */
    public function setRefto($refto)
    {
        $this->refto = $refto;
    
        return $this;
    }

    /**
     * Get refto
     *
     * @return string 
     */
    public function getRefto()
    {
        return $this->refto;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return Mapping
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    
        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set partner
     *
     * @param string $partner
     * @return Mapping
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
    
        return $this;
    }

    /**
     * Get partner
     *
     * @return string 
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * Set options
     *
     * @param string $options
     * @return Mapping
     */
    public function setOptions($options)
    {
        $this->options = $options;
    
        return $this;
    }

    /**
     * Get options
     *
     * @return string 
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set limite_location
     *
     * @param string $limiteLocation
     * @return Mapping
     */
    public function setLimiteLocation($limiteLocation)
    {
        $this->limite_location = $limiteLocation;
    
        return $this;
    }

    /**
     * Get limite_location
     *
     * @return string 
     */
    public function getLimiteLocation()
    {
        return $this->limite_location;
    }
}