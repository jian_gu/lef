<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Media
 *
 * @ORM\Table(name="media")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\MediaRepository")
 */
class Media {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=255)
     */
    private $src;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;

//    /**
//     * @ORM\ManyToOne(targetEntity="Location", inversedBy="medias")
//     * @ORM\JoinColumn(name="location_id", referencedColumnName="id")
//     */
//    private $location;

    /**
     *  @Assert\File( maxSize="2M")
     */
    protected $file;

    // ===================
    // == Saving files ===
    // ===================
    //    ========== File uploading ==========
//    public function getAbsolutePathMedia() {
//        return null === $this->src ? null : $this->getUploadRootDir() . '/picto/' . $this->getFile()->getClientOriginalName();
//    }
//
//    public function getWebPathMedia() {
//        return null === $this->src ? null : $this->getUploadDir() . '/picto/' . $this->getFile()->getClientOriginalName();
//    }

    protected function getUploadRootDir($basepath) {
        // the absolute directory path where uploaded documents should be saved
        return $basepath . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return '';
    }

    public function upload($basepath) {
        // the file property can be empty if the field is not required
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the target filename to move to
        $this->file->move($this->getUploadRootDir($basepath), $this->file->getClientOriginalName());

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * Set file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    // ===================
    // =Saving files fin==
    // ===================

    public function __toString() {
        return $this->getTitre();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set src
     *
     * @param string $src
     * @return Media
     */
    public function setSrc($src) {
        $this->src = $src;

        return $this;
    }

    /**
     * Get src
     *
     * @return string 
     */
    public function getSrc() {
        return $this->src;
    }

    /**

     * Set titre
     *
     * @param string $titre
     * @return Media
     */
    public function setTitre($titre) {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre() {
        return $this->titre;
    }


}