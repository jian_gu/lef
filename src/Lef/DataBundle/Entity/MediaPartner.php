<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Media
 *
 * @ORM\Table(name="media_partner")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\MediaPartnerRepository")
 */
class MediaPartner {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=255, nullable=false)
     */
    private $src;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="location_ref", type="string", length=255, nullable=true)
     */
    private $locationRef;

    /**
     * 
     * @param string $src
     * @param string $titre
     * @param string $location !!Ici, location est la référence, format string, n'est pas un objet
     */
    public function __construct($src = null, $titre = null, $locationRef = null)
    {
        $this->src = $src;
        $this->titre = $titre;
        $this->locationRef = $locationRef;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    } 

    /**
     * Set src
     *
     * @param string $src
     * @return MediaPartner
     */
    public function setSrc($src)
    {
        $this->src = $src;
    
        return $this;
    }

    /**
     * Get src
     *
     * @return string 
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return MediaPartner
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set locationRef
     *
     * @param string $locationRef
     * @return MediaPartner
     */
    public function setLocationRef($locationRef)
    {
        $this->locationRef = $locationRef;
    
        return $this;
    }

    /**
     * Get locationRef
     *
     * @return string 
     */
    public function getLocationRef()
    {
        return $this->locationRef;
    }
}