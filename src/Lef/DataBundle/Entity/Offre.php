<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Offre
 *
 * @ORM\Table(name="offre")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\OffreRepository")
 */
class Offre {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var float
     *
     * @ORM\Column(name="prix_min", type="float")
     */
    private $prixMin;

    /**
     * @var float
     *
     * @ORM\Column(name="prix_max", type="float")
     */
    private $prixMax;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Offre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Offre
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    
        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**

     * Set prixMin
     *
     * @param float $prixMin
     * @return Offre
     */
    public function setPrixMin($prixMin)
    {
        $this->prixMin = $prixMin;
    
        return $this;
    }

    /**
     * Get prixMin
     *
     * @return float 
     */
    public function getPrixMin()
    {
        return $this->prixMin;
    }

    /**
     * Set prixMax
     *
     * @param float $prixMax
     * @return Offre
     */
    public function setPrixMax($prixMax)
    {
        $this->prixMax = $prixMax;
    
        return $this;
    }

    /**
     * Get prixMax
     *
     * @return float 
     */
    public function getPrixMax()
    {
        return $this->prixMax;
    }
}