<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OptionEnfant
 *
 * @ORM\Table(name="options_enfant")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\OptionEnfantRepository")
 */
class OptionEnfant
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;
    
    /**
     * 
     * @return type
     */
    public function __toString() {
        return $this->getTitre();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return OptionEnfant
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }
}