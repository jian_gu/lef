<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Partners
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\PartnersRepository")
 */
class Partners
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="host", type="string", length=255, nullable=false)
     */
    private $host;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=255, nullable=true)
     */
    private $user;

    /**
     * @var text
     *
     * @ORM\Column(name="src_directory", type="text", nullable=true)
     */
    private $srcDirectory;

    /**
     * @var text
     *
     * @ORM\Column(name="files", type="text", nullable=true)
     */
    private $files;


//    /**
//     * @var text
//     *
//     * @ORM\Column(name="mapping", type="text", nullable=true)
//     */
//    private $mapping;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="frameUrl", type="string", length=255, nullable=true)
//     */
//    private $frameUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="dataType", type="string", length=255, nullable=true)
     */
    private $dataType;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var integer
     *
     * @ORM\Column(name="port", type="integer", nullable=true)
     */
    private $port;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set host
     *
     * @param string $host
     * @return Partners
     */
    public function setHost($host)
    {
        $this->host = $host;
    
        return $this;
    }

    /**
     * Get host
     *
     * @return string 
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Partners
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set user
     *
     * @param string $user
     * @return Partners
     */
    public function setUser($user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return string 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set files
     *
     * @param string $files
     * @return Partners
     */
    public function setFiles($files)
    {
        $this->files = $files;
    
        return $this;
    }

    /**
     * Get files
     *
     * @return string 
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set dataType
     *
     * @param string $dataType
     * @return Partners
     */
    public function setDataType($dataType)
    {
        $this->dataType = $dataType;
    
        return $this;
    }

    /**
     * Get dataType
     *
     * @return string 
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Partners
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set port
     *
     * @param integer $port
     * @return Partners
     */
    public function setPort($port)
    {
        $this->port = $port;
    
        return $this;
    }

    /**
     * Get port
     *
     * @return integer 
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Set srcDirectory
     *
     * @param string $srcDirectory
     * @return Partners
     */
    public function setSrcDirectory($srcDirectory)
    {
        $this->srcDirectory = $srcDirectory;
    
        return $this;
    }

    /**
     * Get srcDirectory
     *
     * @return string 
     */
    public function getSrcDirectory()
    {
        return $this->srcDirectory;
    }
}