<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Planning
 *
 * @ORM\Table(name="planning")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\PlanningRepository")
 */
class Planning {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="dispo", type="text", nullable=false)
     */
    private $dispo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isPartner", type="boolean", nullable=false)
     */
    private $ispartner;

    /**
     * @var \Location
     *
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_location", referencedColumnName="id")
     * })
     */
    private $idLocation;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Planning
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set dispo
     *
     * @param string $dispo
     * @return Planning
     */
    public function setDispo($dispo)
    {
        $this->dispo = $dispo;
    
        return $this;
    }

    /**
     * Get dispo
     *
     * @return string 
     */
    public function getDispo()
    {
        return $this->dispo;
    }

    /**
     * Set ispartner
     *
     * @param boolean $ispartner
     * @return Planning
     */
    public function setIspartner($ispartner)
    {
        $this->ispartner = $ispartner;
    
        return $this;
    }

    /**
     * Get ispartner
     *
     * @return boolean 
     */
    public function getIspartner()
    {
        return $this->ispartner;
    }

    /**
     * Set idLocation
     *
     * @param \Lef\DataBundle\Entity\Location $idLocation
     * @return Planning
     */
    public function setIdLocation(\Lef\DataBundle\Entity\Location $idLocation = null)
    {
        $this->idLocation = $idLocation;
    
        return $this;
    }

    /**
     * Get idLocation
     *
     * @return \Lef\DataBundle\Entity\Location 
     */
    public function getIdLocation()
    {
        return $this->idLocation;
    }
}