<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawIcGeb
 *
 * @ORM\Table(name="raw_ic_dispo")
 * @ORM\Entity
 */
class RawIcDispo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="location_ref", type="string", length=16, nullable=true)
     */
    private $locationRef;

    /**
     * @var string
     *
     * @ORM\Column(name="firstDate", type="date", nullable=true)
     */
    private $firstDate;

    /**
     * @var string
     *
     * @ORM\Column(name="availability", type="text", nullable=true)
     */
    private $availability;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locationRef
     *
     * @param string $locationRef
     * @return RawIcDispo
     */
    public function setLocationRef($locationRef)
    {
        $this->locationRef = $locationRef;
    
        return $this;
    }

    /**
     * Get locationRef
     *
     * @return string 
     */
    public function getLocationRef()
    {
        return $this->locationRef;
    }

    /**
     * Set firstDate
     *
     * @param \DateTime $firstDate
     * @return RawIcDispo
     */
    public function setFirstDate($firstDate)
    {
        $this->firstDate = $firstDate;
    
        return $this;
    }

    /**
     * Get firstDate
     *
     * @return \DateTime 
     */
    public function getFirstDate()
    {
        return $this->firstDate;
    }

    /**
     * Set availability
     *
     * @param string $availability
     * @return RawIcDispo
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;
    
        return $this;
    }

    /**
     * Get availability
     *
     * @return string 
     */
    public function getAvailability()
    {
        return $this->availability;
    }
}