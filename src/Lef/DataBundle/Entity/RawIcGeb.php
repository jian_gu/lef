<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawIcGeb
 *
 * @ORM\Table(name="raw_ic_geb")
 * @ORM\Entity
 */
class RawIcGeb
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codePays", type="string", length=8)
     */
    private $codePays;

    /**
     * @var string
     *
     * @ORM\Column(name="codeRegion", type="string", length=16)
     */
    private $codeRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="nameRegion", type="string", length=128)
     */
    private $nameRegion;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codePays
     *
     * @param string $codePays
     * @return RawIcGeb
     */
    public function setCodePays($codePays)
    {
        $this->codePays = $codePays;
    
        return $this;
    }

    /**
     * Get codePays
     *
     * @return string 
     */
    public function getCodePays()
    {
        return $this->codePays;
    }

    /**
     * Set codeRegion
     *
     * @param string $codeRegion
     * @return RawIcGeb
     */
    public function setCodeRegion($codeRegion)
    {
        $this->codeRegion = $codeRegion;
    
        return $this;
    }

    /**
     * Get codeRegion
     *
     * @return string 
     */
    public function getCodeRegion()
    {
        return $this->codeRegion;
    }

    /**
     * Set nameRegion
     *
     * @param string $nameRegion
     * @return RawIcGeb
     */
    public function setNameRegion($nameRegion)
    {
        $this->nameRegion = $nameRegion;
    
        return $this;
    }

    /**
     * Get nameRegion
     *
     * @return string 
     */
    public function getNameRegion()
    {
        return $this->nameRegion;
    }
}