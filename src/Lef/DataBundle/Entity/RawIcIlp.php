<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawIcIlp
 *
 * @ORM\Table(name="raw_ic_ilp")
 * @ORM\Entity
 */
class RawIcIlp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="location_ref", type="string", length=16, nullable=true)
     */
    private $locationRef;

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=255, nullable=true)
     */
    private $src;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="datetime", type="datetime", nullable=true)
//     */
//    private $datetime;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locationRef
     *
     * @param string $locationRef
     * @return RawIcIlp
     */
    public function setLocationRef($locationRef)
    {
        $this->locationRef = $locationRef;
    
        return $this;
    }

    /**
     * Get locationRef
     *
     * @return string 
     */
    public function getLocationRef()
    {
        return $this->locationRef;
    }

    /**
     * Set src
     *
     * @param string $src
     * @return RawIcIlp
     */
    public function setSrc($src)
    {
        $this->src = $src;
    
        return $this;
    }

    /**
     * Get src
     *
     * @return string 
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return RawIcIlp
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }
}