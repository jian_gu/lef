<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawIcLand
 *
 * @ORM\Table(name="raw_ic_land")
 * @ORM\Entity
 */
class RawIcLand
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codePays", type="string", length=8)
     */
    private $codePays;

    /**
     * @var string
     *
     * @ORM\Column(name="namePays", type="string", length=128)
     */
    private $namePays;

    /**
     * @var string
     *
     * @ORM\Column(name="isoPays", type="string", length=8)
     */
    private $isoPays;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codePays
     *
     * @param string $codePays
     * @return RawIcLand
     */
    public function setCodePays($codePays)
    {
        $this->codePays = $codePays;
    
        return $this;
    }

    /**
     * Get codePays
     *
     * @return string 
     */
    public function getCodePays()
    {
        return $this->codePays;
    }

    /**
     * Set namePays
     *
     * @param string $namePays
     * @return RawIcLand
     */
    public function setNamePays($namePays)
    {
        $this->namePays = $namePays;
    
        return $this;
    }

    /**
     * Get namePays
     *
     * @return string 
     */
    public function getNamePays()
    {
        return $this->namePays;
    }

    /**
     * Set isoPays
     *
     * @param string $isoPays
     * @return RawIcLand
     */
    public function setIsoPays($isoPays)
    {
        $this->isoPays = $isoPays;
    
        return $this;
    }

    /**
     * Get isoPays
     *
     * @return string 
     */
    public function getIsoPays()
    {
        return $this->isoPays;
    }
}