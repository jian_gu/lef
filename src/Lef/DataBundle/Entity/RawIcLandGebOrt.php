<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawIcOrt
 *
 * @ORM\Table(name = "raw_ic_land_geb_ort")
 * @ORM\Entity
 */
class RawIcLandGebOrt
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codePays", type="string", length=8, nullable=true)
     */
    private $codePays;

    /**
     * @var string
     *
     * @ORM\Column(name="namePays", type="string", length=128, nullable=true)
     */
    private $namePays;

    /**
     * @var string
     *
     * @ORM\Column(name="codeRegion", type="string", length=8, nullable=true)
     */
    private $codeRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="nameRegion", type="string", length=128, nullable=true)
     */
    private $nameRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="codeVille", type="string", length=8, nullable=true)
     */
    private $codeVille;

    /**
     * @var string
     *
     * @ORM\Column(name="nameVille", type="string", length=128, nullable=true)
     */
    private $nameVille;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codePays
     *
     * @param string $codePays
     * @return RawIcLandGebOrt
     */
    public function setCodePays($codePays)
    {
        $this->codePays = $codePays;
    
        return $this;
    }

    /**
     * Get codePays
     *
     * @return string 
     */
    public function getCodePays()
    {
        return $this->codePays;
    }

    /**
     * Set namePays
     *
     * @param string $namePays
     * @return RawIcLandGebOrt
     */
    public function setNamePays($namePays)
    {
        $this->namePays = $namePays;
    
        return $this;
    }

    /**
     * Get namePays
     *
     * @return string 
     */
    public function getNamePays()
    {
        return $this->namePays;
    }

    /**
     * Set codeRegion
     *
     * @param string $codeRegion
     * @return RawIcLandGebOrt
     */
    public function setCodeRegion($codeRegion)
    {
        $this->codeRegion = $codeRegion;
    
        return $this;
    }

    /**
     * Get codeRegion
     *
     * @return string 
     */
    public function getCodeRegion()
    {
        return $this->codeRegion;
    }

    /**
     * Set nameRegion
     *
     * @param string $nameRegion
     * @return RawIcLandGebOrt
     */
    public function setNameRegion($nameRegion)
    {
        $this->nameRegion = $nameRegion;
    
        return $this;
    }

    /**
     * Get nameRegion
     *
     * @return string 
     */
    public function getNameRegion()
    {
        return $this->nameRegion;
    }

    /**
     * Set codeVille
     *
     * @param string $codeVille
     * @return RawIcLandGebOrt
     */
    public function setCodeVille($codeVille)
    {
        $this->codeVille = $codeVille;
    
        return $this;
    }

    /**
     * Get codeVille
     *
     * @return string 
     */
    public function getCodeVille()
    {
        return $this->codeVille;
    }

    /**
     * Set nameVille
     *
     * @param string $nameVille
     * @return RawIcLandGebOrt
     */
    public function setNameVille($nameVille)
    {
        $this->nameVille = $nameVille;
    
        return $this;
    }

    /**
     * Get nameVille
     *
     * @return string 
     */
    public function getNameVille()
    {
        return $this->nameVille;
    }
}