<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawIcTranchePrix
 *
 * @ORM\Table(name = "raw_ic_tranche_prix")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\RawIcTranchePrixRepository")
 */
class RawIcTranchePrix
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="locationRef", type="string", length=32, nullable = true)
     */
    private $locationRef;

    /**
     * @var \Date
     *
     * @ORM\Column(name="startDate", type="date", nullable = true)
     */
    private $startDate;

    /**
     * @var \Date
     *
     * @ORM\Column(name="endDate", type="date", nullable = true)
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="pricePerTW", type="string", length=1, nullable = true)
     */
    private $pricePerTW;

    /**
     * @var string
     *
     * @ORM\Column(name="pricePerOP", type="string", length=1, nullable = true)
     */
    private $pricePerOP;

    /**
     * @var integer
     *
     * @ORM\Column(name="minStay", type="integer", length=2, nullable = true)
     */
    private $minStay;

    /**
     * @var string
     *
     * @ORM\Column(name="dayArrival", type="string", length=2, nullable = true)
     */
    private $dayArrival;
    
    /**
     * @var float
     *
     * @ORM\Column(name="rentalPrice", type="float", nullable = true)
     */
    private $rentalPrice;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locationRef
     *
     * @param string $locationRef
     * @return RawIcTranchePrix
     */
    public function setLocationRef($locationRef)
    {
        $this->locationRef = $locationRef;
    
        return $this;
    }

    /**
     * Get locationRef
     *
     * @return string 
     */
    public function getLocationRef()
    {
        return $this->locationRef;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return RawIcTranchePrix
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return RawIcTranchePrix
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    
        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set pricePerTW
     *
     * @param string $pricePerTW
     * @return RawIcTranchePrix
     */
    public function setPricePerTW($pricePerTW)
    {
        $this->pricePerTW = $pricePerTW;
    
        return $this;
    }

    /**
     * Get pricePerTW
     *
     * @return string 
     */
    public function getPricePerTW()
    {
        return $this->pricePerTW;
    }

    /**
     * Set pricePerOP
     *
     * @param string $pricePerOP
     * @return RawIcTranchePrix
     */
    public function setPricePerOP($pricePerOP)
    {
        $this->pricePerOP = $pricePerOP;
    
        return $this;
    }

    /**
     * Get pricePerOP
     *
     * @return string 
     */
    public function getPricePerOP()
    {
        return $this->pricePerOP;
    }

    /**
     * Set minStay
     *
     * @param integer $minStay
     * @return RawIcTranchePrix
     */
    public function setMinStay($minStay)
    {
        $this->minStay = $minStay;
    
        return $this;
    }

    /**
     * Get minStay
     *
     * @return integer 
     */
    public function getMinStay()
    {
        return $this->minStay;
    }

    /**
     * Set dayArrival
     *
     * @param string $dayArrival
     * @return RawIcTranchePrix
     */
    public function setDayArrival($dayArrival)
    {
        $this->dayArrival = $dayArrival;
    
        return $this;
    }

    /**
     * Get dayArrival
     *
     * @return string 
     */
    public function getDayArrival()
    {
        return $this->dayArrival;
    }

    /**
     * Set rentalPrice
     *
     * @param float $rentalPrice
     * @return RawIcTranchePrix
     */
    public function setRentalPrice($rentalPrice)
    {
        $this->rentalPrice = $rentalPrice;
    
        return $this;
    }

    /**
     * Get rentalPrice
     *
     * @return float 
     */
    public function getRentalPrice()
    {
        return $this->rentalPrice;
    }
}