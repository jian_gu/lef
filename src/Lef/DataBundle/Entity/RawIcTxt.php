<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawIhText
 *
 * @ORM\Table(name = "raw_ic_txt")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\RawIcTxtRepository")
 */
class RawIcTxt {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=16, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="codeVille", type="string", length=8, nullable=true)
     */
    private $codeVille;

    /**
     * @var string
     *
     * @ORM\Column(name="langCode", type="string", length=4, nullable=true)
     */
    private $langCode;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modificationDate", type="datetime", nullable=true)
     */
    private $modificationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreDePersonne", type="string", length=32, nullable=true)
     */
    private $nombreDePersonne;

    /**
     * @var float
     *
     * @ORM\Column(name="surfaceHabitable", type="float", nullable=true)
     */
    private $surfaceHabitable;

    /**
     * @var string
     *
     * @ORM\Column(name="nombrePiece", type="string", length=32, nullable=true)
     */
    private $nombrePiece;

    /**
     * @var float
     *
     * @ORM\Column(name="nombreChambre", type="float", nullable=true)
     */
    private $nombreChambre;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionDetaillee", type="text", nullable=true)
     */
    private $descriptionDetaillee;

    /**
     * @var string
     *
     * @ORM\Column(name="caracteristiques", type="text", nullable=true)
     */
    private $caracteristiques;

    /**
     * @var string
     *
     * @ORM\Column(name="amenagement", type="text", nullable=true)
     */
    private $amenagement;

    /**
     * @var boolean
     *
     * @ORM\Column(name="piscine", type="boolean", nullable=true)
     */
    private $piscine;

    /**
     * @var boolean
     *
     * @ORM\Column(name="animauxDomestiques", type="boolean", nullable=true)
     */
    private $animauxDomestiques;

    /**
     * @var string
     *
     * @ORM\Column(name="equipementsPartner", type="text", nullable=true)
     */
    private $equipementsPartner;

    /**
     * @var string
     *
     * @ORM\Column(name="sejourAvecCouchage", type="text", nullable=true)
     */
    private $sejourAvecCouchage;

    /**
     * @var string
     *
     * @ORM\Column(name="coinRepas", type="text", nullable=true)
     */
    private $coinRepas;

    /**
     * @var string
     *
     * @ORM\Column(name="cuisine", type="text", nullable=true)
     */
    private $cuisine;

    /**
     * @var string
     *
     * @ORM\Column(name="sanitaire", type="text", nullable=true)
     */
    private $sanitaire;

    /**
     * @var string
     *
     * @ORM\Column(name="typePartner", type="text", nullable=true)
     */
    private $typePartner;

    /**
     * @var string
     *
     * @ORM\Column(name="distanceOther", type="text", nullable=true)
     */
    private $distanceOther;

    /**
     * @var float
     *
     * @ORM\Column(name="distanceMer", type="float", nullable=true)
     */
    private $distanceMer;

    /**
     * @var float
     *
     * @ORM\Column(name="distanceShopping", type="float", nullable=true)
     */
    private $distanceShopping;

    /**
     * @var float
     *
     * @ORM\Column(name="distanceRemontee", type="float", nullable=true)
     */
    private $distanceRemontee;

    /**
     * @var float
     *
     * @ORM\Column(name="distanceCentreVille", type="float", nullable=true)
     */
    private $distanceCentreVille;

    /**
     * @var float
     *
     * @ORM\Column(name="distancePublicTransport", type="float", nullable=true)
     */
    private $distancePublicTransport;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return RawIcTxt
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set codeVille
     *
     * @param string $codeVille
     * @return RawIcTxt
     */
    public function setCodeVille($codeVille)
    {
        $this->codeVille = $codeVille;
    
        return $this;
    }

    /**
     * Get codeVille
     *
     * @return string 
     */
    public function getCodeVille()
    {
        return $this->codeVille;
    }

    /**
     * Set langCode
     *
     * @param string $langCode
     * @return RawIcTxt
     */
    public function setLangCode($langCode)
    {
        $this->langCode = $langCode;
    
        return $this;
    }

    /**
     * Get langCode
     *
     * @return string 
     */
    public function getLangCode()
    {
        return $this->langCode;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return RawIcTxt
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return RawIcTxt
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;
    
        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime 
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Set nombreDePersonne
     *
     * @param string $nombreDePersonne
     * @return RawIcTxt
     */
    public function setNombreDePersonne($nombreDePersonne)
    {
        $this->nombreDePersonne = $nombreDePersonne;
    
        return $this;
    }

    /**
     * Get nombreDePersonne
     *
     * @return string 
     */
    public function getNombreDePersonne()
    {
        return $this->nombreDePersonne;
    }

    /**
     * Set nombreChambre
     *
     * @param float $nombreChambre
     * @return RawIcTxt
     */
    public function setNombreChambre($nombreChambre)
    {
        $this->nombreChambre = $nombreChambre;
    
        return $this;
    }

    /**
     * Get nombreChambre
     *
     * @return float 
     */
    public function getNombreChambre()
    {
        return $this->nombreChambre;
    }

    /**
     * Set descriptionDetaillee
     *
     * @param string $descriptionDetaillee
     * @return RawIcTxt
     */
    public function setDescriptionDetaillee($descriptionDetaillee)
    {
        $this->descriptionDetaillee = $descriptionDetaillee;
    
        return $this;
    }

    /**
     * Get descriptionDetaillee
     *
     * @return string 
     */
    public function getDescriptionDetaillee()
    {
        return $this->descriptionDetaillee;
    }

    /**
     * Set caracteristiques
     *
     * @param string $caracteristiques
     * @return RawIcTxt
     */
    public function setCaracteristiques($caracteristiques)
    {
        $this->caracteristiques = $caracteristiques;
    
        return $this;
    }

    /**
     * Get caracteristiques
     *
     * @return string 
     */
    public function getCaracteristiques()
    {
        return $this->caracteristiques;
    }

    /**
     * Set piscine
     *
     * @param boolean $piscine
     * @return RawIcTxt
     */
    public function setPiscine($piscine)
    {
        $this->piscine = $piscine;
    
        return $this;
    }

    /**
     * Get piscine
     *
     * @return boolean 
     */
    public function getPiscine()
    {
        return $this->piscine;
    }

    /**
     * Set animauxDomestiques
     *
     * @param boolean $animauxDomestiques
     * @return RawIcTxt
     */
    public function setAnimauxDomestiques($animauxDomestiques)
    {
        $this->animauxDomestiques = $animauxDomestiques;
    
        return $this;
    }

    /**
     * Get animauxDomestiques
     *
     * @return boolean 
     */
    public function getAnimauxDomestiques()
    {
        return $this->animauxDomestiques;
    }

    /**
     * Set equipementsPartner
     *
     * @param string $equipementsPartner
     * @return RawIcTxt
     */
    public function setEquipementsPartner($equipementsPartner)
    {
        $this->equipementsPartner = $equipementsPartner;
    
        return $this;
    }

    /**
     * Get equipementsPartner
     *
     * @return string 
     */
    public function getEquipementsPartner()
    {
        return $this->equipementsPartner;
    }

    /**
     * Set sejourAvecCouchage
     *
     * @param string $sejourAvecCouchage
     * @return RawIcTxt
     */
    public function setSejourAvecCouchage($sejourAvecCouchage)
    {
        $this->sejourAvecCouchage = $sejourAvecCouchage;
    
        return $this;
    }

    /**
     * Get sejourAvecCouchage
     *
     * @return string 
     */
    public function getSejourAvecCouchage()
    {
        return $this->sejourAvecCouchage;
    }

    /**
     * Set coinRepas
     *
     * @param string $coinRepas
     * @return RawIcTxt
     */
    public function setCoinRepas($coinRepas)
    {
        $this->coinRepas = $coinRepas;
    
        return $this;
    }

    /**
     * Get coinRepas
     *
     * @return string 
     */
    public function getCoinRepas()
    {
        return $this->coinRepas;
    }

    /**
     * Set cuisine
     *
     * @param string $cuisine
     * @return RawIcTxt
     */
    public function setCuisine($cuisine)
    {
        $this->cuisine = $cuisine;
    
        return $this;
    }

    /**
     * Get cuisine
     *
     * @return string 
     */
    public function getCuisine()
    {
        return $this->cuisine;
    }

    /**
     * Set sanitaire
     *
     * @param string $sanitaire
     * @return RawIcTxt
     */
    public function setSanitaire($sanitaire)
    {
        $this->sanitaire = $sanitaire;
    
        return $this;
    }

    /**
     * Get sanitaire
     *
     * @return string 
     */
    public function getSanitaire()
    {
        return $this->sanitaire;
    }

    /**
     * Set typePartner
     *
     * @param string $typePartner
     * @return RawIcTxt
     */
    public function setTypePartner($typePartner)
    {
        $this->typePartner = $typePartner;
    
        return $this;
    }

    /**
     * Get typePartner
     *
     * @return string 
     */
    public function getTypePartner()
    {
        return $this->typePartner;
    }

    /**
     * Set distanceOther
     *
     * @param string $distanceOther
     * @return RawIcTxt
     */
    public function setDistanceOther($distanceOther)
    {
        $this->distanceOther = $distanceOther;
    
        return $this;
    }

    /**
     * Get distanceOther
     *
     * @return string 
     */
    public function getDistanceOther()
    {
        return $this->distanceOther;
    }

    /**
     * Set distanceMer
     *
     * @param float $distanceMer
     * @return RawIcTxt
     */
    public function setDistanceMer($distanceMer)
    {
        $this->distanceMer = $distanceMer;
    
        return $this;
    }

    /**
     * Get distanceMer
     *
     * @return float 
     */
    public function getDistanceMer()
    {
        return $this->distanceMer;
    }

    /**
     * Set distanceRemontee
     *
     * @param float $distanceRemontee
     * @return RawIcTxt
     */
    public function setDistanceRemontee($distanceRemontee)
    {
        $this->distanceRemontee = $distanceRemontee;
    
        return $this;
    }

    /**
     * Get distanceRemontee
     *
     * @return float 
     */
    public function getDistanceRemontee()
    {
        return $this->distanceRemontee;
    }

    /**
     * Set distanceCentreVille
     *
     * @param float $distanceCentreVille
     * @return RawIcTxt
     */
    public function setDistanceCentreVille($distanceCentreVille)
    {
        $this->distanceCentreVille = $distanceCentreVille;
    
        return $this;
    }

    /**
     * Get distanceCentreVille
     *
     * @return float 
     */
    public function getDistanceCentreVille()
    {
        return $this->distanceCentreVille;
    }

    /**
     * Set distancePublicTransport
     *
     * @param float $distancePublicTransport
     * @return RawIcTxt
     */
    public function setDistancePublicTransport($distancePublicTransport)
    {
        $this->distancePublicTransport = $distancePublicTransport;
    
        return $this;
    }

    /**
     * Get distancePublicTransport
     *
     * @return float 
     */
    public function getDistancePublicTransport()
    {
        return $this->distancePublicTransport;
    }

    /**
     * Set surfaceHabitable
     *
     * @param float $surfaceHabitable
     * @return RawIcTxt
     */
    public function setSurfaceHabitable($surfaceHabitable)
    {
        $this->surfaceHabitable = $surfaceHabitable;
    
        return $this;
    }

    /**
     * Get surfaceHabitable
     *
     * @return float 
     */
    public function getSurfaceHabitable()
    {
        return $this->surfaceHabitable;
    }

    /**
     * Set nombrePiece
     *
     * @param string $nombrePiece
     * @return RawIcTxt
     */
    public function setNombrePiece($nombrePiece)
    {
        $this->nombrePiece = $nombrePiece;
    
        return $this;
    }

    /**
     * Get nombrePiece
     *
     * @return string 
     */
    public function getNombrePiece()
    {
        return $this->nombrePiece;
    }

    /**
     * Set amenagement
     *
     * @param string $amenagement
     * @return RawIcTxt
     */
    public function setAmenagement($amenagement)
    {
        $this->amenagement = $amenagement;
    
        return $this;
    }

    /**
     * Get amenagement
     *
     * @return string 
     */
    public function getAmenagement()
    {
        return $this->amenagement;
    }

    /**
     * Set distanceShopping
     *
     * @param float $distanceShopping
     * @return RawIcTxt
     */
    public function setDistanceShopping($distanceShopping)
    {
        $this->distanceShopping = $distanceShopping;
    
        return $this;
    }

    /**
     * Get distanceShopping
     *
     * @return float 
     */
    public function getDistanceShopping()
    {
        return $this->distanceShopping;
    }
}