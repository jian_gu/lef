<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawIhAccommodation
 *
 * @ORM\Table(name="raw_ih_accommodation")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\RawIhAccommodationRepository")
 */
class RawIhAccommodation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=18, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=15, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=50, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=50, nullable=true)
     */
    private $place;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=20, nullable=true)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=1, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="details", type="string", length=255, nullable=true)
     */
    private $details;

    /**
     * @var integer
     *
     * @ORM\Column(name="quality", type="integer", nullable=true)
     */
    private $quality;

    /**
     * @var string
     *
     * @ORM\Column(name="brand", type="string", length=3, nullable=true)
     */
    private $brand;

    /**
     * @var string
     *
     * @ORM\Column(name="pax", type="decimal", nullable=true)
     */
    private $pax;

    /**
     * @var string
     *
     * @ORM\Column(name="sqm", type="decimal", nullable=true)
     */
    private $sqm;

    /**
     * @var string
     *
     * @ORM\Column(name="floor", type="decimal", nullable=true)
     */
    private $floor;

    /**
     * @var string
     *
     * @ORM\Column(name="rooms", type="decimal", nullable=true)
     */
    private $rooms;

    /**
     * @var string
     *
     * @ORM\Column(name="bedroorooms", type="decimal", nullable=true)
     */
    private $bedroorooms;

    /**
     * @var string
     *
     * @ORM\Column(name="toilets", type="decimal", nullable=true)
     */
    private $toilets;

    /**
     * @var string
     *
     * @ORM\Column(name="bathrooms", type="decimal", nullable=true)
     */
    private $bathrooms;

    /**
     * @var array
     *
     * @ORM\Column(name="geodata", type="array", nullable=true)
     */
    private $geodata;

    /**
     * @var array
     *
     * @ORM\Column(name="attributes", type="array", nullable=true)
     */
    private $attributes;

    /**
     * @var array
     *
     * @ORM\Column(name="distances", type="array", nullable=true)
     */
    private $distances;

    /**
     * @var array
     *
     * @ORM\Column(name="themes", type="array", nullable=true)
     */
    private $Themes;

    /**
     * @var array
     *
     * @ORM\Column(name="pictures", type="array", nullable=true)
     */
    private $pictures;

    /**
     * @var datetime
     *
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return RawIhAccommodation
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RawIhAccommodation
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return RawIhAccommodation
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return RawIhAccommodation
     */
    public function setRegion($region)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set place
     *
     * @param string $place
     * @return RawIhAccommodation
     */
    public function setPlace($place)
    {
        $this->place = $place;
    
        return $this;
    }

    /**
     * Get place
     *
     * @return string 
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return RawIhAccommodation
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    
        return $this;
    }

    /**
     * Get zip
     *
     * @return string 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return RawIhAccommodation
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return RawIhAccommodation
     */
    public function setDetails($details)
    {
        $this->details = $details;
    
        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set quality
     *
     * @param integer $quality
     * @return RawIhAccommodation
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;
    
        return $this;
    }

    /**
     * Get quality
     *
     * @return integer 
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * Set brand
     *
     * @param string $brand
     * @return RawIhAccommodation
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    
        return $this;
    }

    /**
     * Get brand
     *
     * @return string 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set pax
     *
     * @param string $pax
     * @return RawIhAccommodation
     */
    public function setPax($pax)
    {
        $this->pax = $pax;
    
        return $this;
    }

    /**
     * Get pax
     *
     * @return string 
     */
    public function getPax()
    {
        return $this->pax;
    }

    /**
     * Set sqm
     *
     * @param string $sqm
     * @return RawIhAccommodation
     */
    public function setSqm($sqm)
    {
        $this->sqm = $sqm;
    
        return $this;
    }

    /**
     * Get sqm
     *
     * @return string 
     */
    public function getSqm()
    {
        return $this->sqm;
    }

    /**
     * Set floor
     *
     * @param string $floor
     * @return RawIhAccommodation
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;
    
        return $this;
    }

    /**
     * Get floor
     *
     * @return string 
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set rooms
     *
     * @param string $rooms
     * @return RawIhAccommodation
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
    
        return $this;
    }

    /**
     * Get rooms
     *
     * @return string 
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set bedroorooms
     *
     * @param string $bedroorooms
     * @return RawIhAccommodation
     */
    public function setBedroorooms($bedroorooms)
    {
        $this->bedroorooms = $bedroorooms;
    
        return $this;
    }

    /**
     * Get bedroorooms
     *
     * @return string 
     */
    public function getBedroorooms()
    {
        return $this->bedroorooms;
    }

    /**
     * Set toilets
     *
     * @param string $toilets
     * @return RawIhAccommodation
     */
    public function setToilets($toilets)
    {
        $this->toilets = $toilets;
    
        return $this;
    }

    /**
     * Get toilets
     *
     * @return string 
     */
    public function getToilets()
    {
        return $this->toilets;
    }

    /**
     * Set bathrooms
     *
     * @param string $bathrooms
     * @return RawIhAccommodation
     */
    public function setBathrooms($bathrooms)
    {
        $this->bathrooms = $bathrooms;
    
        return $this;
    }

    /**
     * Get bathrooms
     *
     * @return string 
     */
    public function getBathrooms()
    {
        return $this->bathrooms;
    }

    /**
     * Set geodata
     *
     * @param array $geodata
     * @return RawIhAccommodation
     */
    public function setGeodata($geodata)
    {
        $this->geodata = $geodata;
    
        return $this;
    }

    /**
     * Get geodata
     *
     * @return array 
     */
    public function getGeodata()
    {
        return $this->geodata;
    }

    /**
     * Set attributes
     *
     * @param array $attributes
     * @return RawIhAccommodation
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    
        return $this;
    }

    /**
     * Get attributes
     *
     * @return array 
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set distances
     *
     * @param array $distances
     * @return RawIhAccommodation
     */
    public function setDistances($distances)
    {
        $this->distances = $distances;
    
        return $this;
    }

    /**
     * Get distances
     *
     * @return array 
     */
    public function getDistances()
    {
        return $this->distances;
    }

    /**
     * Set Themes
     *
     * @param array $Themes
     * @return RawIhAccommodation
     */
    public function setThemes($Themes)
    {
        $this->Themes = $Themes;
    
        return $this;
    }

    /**
     * Get Themes
     *
     * @return array 
     */
    public function getThemes()
    {
        return $this->Themes;
    }

    /**
     * Set pictures
     *
     * @param array $pictures
     * @return RawIhAccommodation
     */
    public function setPictures($pictures)
    {
        $this->pictures = $pictures;
    
        return $this;
    }

    /**
     * Get pictures
     *
     * @return array 
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return RawIhAccommodation
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    
        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }
}