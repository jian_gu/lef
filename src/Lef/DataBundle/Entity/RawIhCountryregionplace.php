<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawIhCountryregionplace
 *
 * @ORM\Table(name="raw_ih_countryregionplace")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\RawIhCountryregionplaceRepository")
 */
class RawIhCountryregionplace {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="country_code", type="string", length=10,  nullable=true)
     */
    private $countryCode;

    /**
     * @var array
     *
     * @ORM\Column(name="country_name", type="string", length=15, nullable=true)
     */
    private $countryName;

    /**
     * @var array
     *
     * @ORM\Column(name="region_code", type="string", length=10, nullable=true)
     */
    private $regionCode;

    /**
     * @var array
     *
     * @ORM\Column(name="region_name", type="string", length=50,  nullable=true)
     */
    private $regionName;

    /**
     * @var array
     *
     * @ORM\Column(name="subregion_code", type="string", length=10, nullable=true)
     */
    private $subregionCode;

    /**
     * @var array
     *
     * @ORM\Column(name="subregion_name", type="string", length=50, nullable=true)
     */
    private $subregionName;

    /**
     * @var array
     *
     * @ORM\Column(name="place_code", type="string", length=10, nullable=true)
     */
    private $placeCode;

    /**
     * @var array
     *
     * @ORM\Column(name="place_name", type="string", length=50,  nullable=true)
     */
    private $placeName;

    /**
     * @var array
     *
     * @ORM\Column(name="subplace_code", type="string", length=10, nullable=true)
     */
    private $subplaceCode;

    /**
     * @var array
     *
     * @ORM\Column(name="subplace_name", type="string", length=50, nullable=true)
     */
    private $subplaceName;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     * @return RawIhCountryregionplace
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    
        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string 
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set countryName
     *
     * @param string $countryName
     * @return RawIhCountryregionplace
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;
    
        return $this;
    }

    /**
     * Get countryName
     *
     * @return string 
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * Set regionCode
     *
     * @param string $regionCode
     * @return RawIhCountryregionplace
     */
    public function setRegionCode($regionCode)
    {
        $this->regionCode = $regionCode;
    
        return $this;
    }

    /**
     * Get regionCode
     *
     * @return string 
     */
    public function getRegionCode()
    {
        return $this->regionCode;
    }

    /**
     * Set regionName
     *
     * @param string $regionName
     * @return RawIhCountryregionplace
     */
    public function setRegionName($regionName)
    {
        $this->regionName = $regionName;
    
        return $this;
    }

    /**
     * Get regionName
     *
     * @return string 
     */
    public function getRegionName()
    {
        return $this->regionName;
    }

    /**
     * Set subregionCode
     *
     * @param string $subregionCode
     * @return RawIhCountryregionplace
     */
    public function setSubregionCode($subregionCode)
    {
        $this->subregionCode = $subregionCode;
    
        return $this;
    }

    /**
     * Get subregionCode
     *
     * @return string 
     */
    public function getSubregionCode()
    {
        return $this->subregionCode;
    }

    /**
     * Set subregionName
     *
     * @param string $subregionName
     * @return RawIhCountryregionplace
     */
    public function setSubregionName($subregionName)
    {
        $this->subregionName = $subregionName;
    
        return $this;
    }

    /**
     * Get subregionName
     *
     * @return string 
     */
    public function getSubregionName()
    {
        return $this->subregionName;
    }

    /**
     * Set placeCode
     *
     * @param string $placeCode
     * @return RawIhCountryregionplace
     */
    public function setPlaceCode($placeCode)
    {
        $this->placeCode = $placeCode;
    
        return $this;
    }

    /**
     * Get placeCode
     *
     * @return string 
     */
    public function getPlaceCode()
    {
        return $this->placeCode;
    }

    /**
     * Set placeName
     *
     * @param string $placeName
     * @return RawIhCountryregionplace
     */
    public function setPlaceName($placeName)
    {
        $this->placeName = $placeName;
    
        return $this;
    }

    /**
     * Get placeName
     *
     * @return string 
     */
    public function getPlaceName()
    {
        return $this->placeName;
    }

    /**
     * Set subplaceCode
     *
     * @param string $subplaceCode
     * @return RawIhCountryregionplace
     */
    public function setSubplaceCode($subplaceCode)
    {
        $this->subplaceCode = $subplaceCode;
    
        return $this;
    }

    /**
     * Get subplaceCode
     *
     * @return string 
     */
    public function getSubplaceCode()
    {
        return $this->subplaceCode;
    }

    /**
     * Set subplaceName
     *
     * @param string $subplaceName
     * @return RawIhCountryregionplace
     */
    public function setSubplaceName($subplaceName)
    {
        $this->subplaceName = $subplaceName;
    
        return $this;
    }

    /**
     * Get subplaceName
     *
     * @return string 
     */
    public function getSubplaceName()
    {
        return $this->subplaceName;
    }
}