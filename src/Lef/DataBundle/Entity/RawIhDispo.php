<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawIhDispo
 *
 * @ORM\Table(name="raw_ih_dispo")
 * @ORM\Entity
 */
class RawIhDispo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="location_ref", type="string", length=16, nullable=true)
     */
    private $locationRef;

    /**
     * @var string
     *
     * @ORM\Column(name="firstDate", type="date", nullable=true)
     */
    private $firstDate;

    /**
     * @var string
     *
     * @ORM\Column(name="availability", type="text", nullable=true)
     */
    private $availability;

    /**
     * @var string
     *
     * @ORM\Column(name="minstay", type="string", length=768, nullable=true)
     */
    private $minstay;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locationRef
     *
     * @param string $locationRef
     * @return RawIhDispo
     */
    public function setLocationRef($locationRef)
    {
        $this->locationRef = $locationRef;
    
        return $this;
    }

    /**
     * Get locationRef
     *
     * @return string 
     */
    public function getLocationRef()
    {
        return $this->locationRef;
    }

    /**
     * Set firstDate
     *
     * @param \DateTime $firstDate
     * @return RawIhDispo
     */
    public function setFirstDate($firstDate)
    {
        $this->firstDate = $firstDate;
    
        return $this;
    }

    /**
     * Get firstDate
     *
     * @return \DateTime 
     */
    public function getFirstDate()
    {
        return $this->firstDate;
    }

    /**
     * Set availability
     *
     * @param string $availability
     * @return RawIhDispo
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;
    
        return $this;
    }

    /**
     * Get availability
     *
     * @return string 
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * Set minstay
     *
     * @param integer $minstay
     * @return RawIhDispo
     */
    public function setMinstay($minstay)
    {
        $this->minstay = $minstay;
    
        return $this;
    }

    /**
     * Get minstay
     *
     * @return integer 
     */
    public function getMinstay()
    {
        return $this->minstay;
    }
}