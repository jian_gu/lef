<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawIhImg
 *
 * @ORM\Table(name="raw_ih_img")
 * @ORM\Entity
 */
class RawIhImg
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="location_ref", type="string", length=16, nullable=true)
     */
    private $locationRef;

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=255, nullable=true)
     */
    private $src;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="season", type="string", length=16, nullable=true)
     */
    private $season;

    /**
     * @var string
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=true)
     */
    private $datetime;
    
   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locationRef
     *
     * @param string $locationRef
     * @return RawIhImg
     */
    public function setLocationRef($locationRef)
    {
        $this->locationRef = $locationRef;
    
        return $this;
    }

    /**
     * Get locationRef
     *
     * @return string 
     */
    public function getLocationRef()
    {
        return $this->locationRef;
    }

    /**
     * Set src
     *
     * @param string $src
     * @return RawIhImg
     */
    public function setSrc($src)
    {
        $this->src = $src;
    
        return $this;
    }

    /**
     * Get src
     *
     * @return string 
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return RawIhImg
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set season
     *
     * @param string $season
     * @return RawIhImg
     */
    public function setSeason($season)
    {
        $this->season = $season;
    
        return $this;
    }

    /**
     * Get season
     *
     * @return string 
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     * @return RawIhImg
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    
        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }
}