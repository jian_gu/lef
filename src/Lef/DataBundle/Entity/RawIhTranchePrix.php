<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawIhPrice
 *
 * @ORM\Table(name="raw_ih_tranche_prix")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\RawIhTranchePrixRepository")
 */
class RawIhTranchePrix
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="locationRef", type="string", length=18)
     */
    private $locationRef;

    /**
     * @var Date
     *
     * @ORM\Column(name="startDate", type="date", nullable=true)
     */
    private $startDate;

    /**
     * @var Date
     *
     * @ORM\Column(name="endDate", type="date", length=10, nullable=true)
     */
    private $endDate;

    /**
     * @var float
     *
     * @ORM\Column(name="day", type="float", nullable=true)
     */
    private $day;

    /**
     * @var float
     *
     * @ORM\Column(name="weekend", type="float", nullable=true)
     */
    private $weekend;

    /**
     * @var float
     *
     * @ORM\Column(name="week", type="float", nullable=true)
     */
    private $week;

    /**
     * @var float
     *
     * @ORM\Column(name="week2", type="float", nullable=true)
     */
    private $week2;
    /**
     * @var float
     *
     * @ORM\Column(name="week3", type="float", nullable=true)
     */
    private $week3;
    /**
     * @var float
     *
     * @ORM\Column(name="month", type="float", nullable=true)
     */
    private $month;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locationRef
     *
     * @param string $locationRef
     * @return RawIhTranchePrix
     */
    public function setLocationRef($locationRef)
    {
        $this->locationRef = $locationRef;
    
        return $this;
    }

    /**
     * Get locationRef
     *
     * @return string 
     */
    public function getLocationRef()
    {
        return $this->locationRef;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return RawIhTranchePrix
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return RawIhTranchePrix
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    
        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set day
     *
     * @param float $day
     * @return RawIhTranchePrix
     */
    public function setDay($day)
    {
        $this->day = $day;
    
        return $this;
    }

    /**
     * Get day
     *
     * @return float 
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set weekend
     *
     * @param float $weekend
     * @return RawIhTranchePrix
     */
    public function setWeekend($weekend)
    {
        $this->weekend = $weekend;
    
        return $this;
    }

    /**
     * Get weekend
     *
     * @return float 
     */
    public function getWeekend()
    {
        return $this->weekend;
    }

    /**
     * Set week
     *
     * @param float $week
     * @return RawIhTranchePrix
     */
    public function setWeek($week)
    {
        $this->week = $week;
    
        return $this;
    }

    /**
     * Get week
     *
     * @return float 
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * Set week2
     *
     * @param float $week2
     * @return RawIhTranchePrix
     */
    public function setWeek2($week2)
    {
        $this->week2 = $week2;
    
        return $this;
    }

    /**
     * Get week2
     *
     * @return float 
     */
    public function getWeek2()
    {
        return $this->week2;
    }

    /**
     * Set week3
     *
     * @param float $week3
     * @return RawIhTranchePrix
     */
    public function setWeek3($week3)
    {
        $this->week3 = $week3;
    
        return $this;
    }

    /**
     * Get week3
     *
     * @return float 
     */
    public function getWeek3()
    {
        return $this->week3;
    }

    /**
     * Set month
     *
     * @param float $month
     * @return RawIhTranchePrix
     */
    public function setMonth($month)
    {
        $this->month = $month;
    
        return $this;
    }

    /**
     * Get month
     *
     * @return float 
     */
    public function getMonth()
    {
        return $this->month;
    }
}