<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawLeisureLoc
 *
 * @ORM\Table(name="raw_leisure_loc")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\RawLeisureLocRepository")
 */
class RawLeisureLoc
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=16)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=128, nullable=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=32, nullable=true)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=64, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=32, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="zipPostalCode", type="string", length=16, nullable=true)
     */
    private $zipPostalCode;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreDePersonne", type="integer", nullable=true)
     */
    private $nombreDePersonne;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreEtoiles", type="integer", nullable=true)
     */
    private $nombreEtoiles;

    /**
     * @var integer
     *
     * @ORM\Column(name="surfaceHabitable", type="integer", nullable=true)
     */
    private $surfaceHabitable;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreSalleDeBain", type="integer", nullable=true)
     */
    private $nombreSalleDeBain;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreChambre", type="integer", nullable=true)
     */
    private $nombreChambre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="animauxDomestiques", type="integer", nullable=true)
     */
    private $animauxDomestiques;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionBreve", type="text", nullable=true)
     */
    private $descriptionBreve;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionDetaillee", type="text", nullable=true)
     */
    private $descriptionDetaillee;

    /**
     * @var string
     *
     * @ORM\Column(name="caracteristiques", type="text", nullable=true)
     */
    private $caracteristiques;

    /**
     * @var string
     *
     * @ORM\Column(name="amenagement", type="text", nullable=true)
     */
    private $amenagement;

    /**
     * @var string
     *
     * @ORM\Column(name="distanceMer", type="integer", nullable=true)
     */
    private $distanceMer;

    /**
     * @var string
     *
     * @ORM\Column(name="distanceLac", type="integer", nullable=true)
     */
    private $distanceLac;

    /**
     * @var string
     *
     * @ORM\Column(name="distanceCentreVille", type="integer", nullable=true)
     */
    private $distanceCentreVille;

    /**
     * @var string
     *
     * @ORM\Column(name="distanceShopping", type="integer", nullable=true)
     */
    private $distanceShopping;

    /**
     * @var string
     *
     * @ORM\Column(name="distancePublicTransport", type="integer", nullable=true)
     */
    private $distancePublicTransport;

    /**
     * @var string
     *
     * @ORM\Column(name="distanceOther", type="text", nullable=true)
     */
    private $distanceOther;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="date", nullable=true)
     */
    private $creationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="rentalprice", type="integer", nullable=true)
     */
    private $rentalprice;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return RawLeisureLoc
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    
        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return RawLeisureLoc
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set pays
     *
     * @param string $pays
     * @return RawLeisureLoc
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    
        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return RawLeisureLoc
     */
    public function setRegion($region)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return RawLeisureLoc
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    
        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set zipPostalCode
     *
     * @param string $zipPostalCode
     * @return RawLeisureLoc
     */
    public function setZipPostalCode($zipPostalCode)
    {
        $this->zipPostalCode = $zipPostalCode;
    
        return $this;
    }

    /**
     * Get zipPostalCode
     *
     * @return string 
     */
    public function getZipPostalCode()
    {
        return $this->zipPostalCode;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return RawLeisureLoc
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    
        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return RawLeisureLoc
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    
        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set nombreDePersonne
     *
     * @param string $nombreDePersonne
     * @return RawLeisureLoc
     */
    public function setNombreDePersonne($nombreDePersonne)
    {
        $this->nombreDePersonne = $nombreDePersonne;
    
        return $this;
    }

    /**
     * Get nombreDePersonne
     *
     * @return string 
     */
    public function getNombreDePersonne()
    {
        return $this->nombreDePersonne;
    }

    /**
     * Set nombreEtoiles
     *
     * @param string $nombreEtoiles
     * @return RawLeisureLoc
     */
    public function setNombreEtoiles($nombreEtoiles)
    {
        $this->nombreEtoiles = $nombreEtoiles;
    
        return $this;
    }

    /**
     * Get nombreEtoiles
     *
     * @return string 
     */
    public function getNombreEtoiles()
    {
        return $this->nombreEtoiles;
    }

    /**
     * Set surfaceHabitable
     *
     * @param integer $surfaceHabitable
     * @return RawLeisureLoc
     */
    public function setSurfaceHabitable($surfaceHabitable)
    {
        $this->surfaceHabitable = $surfaceHabitable;
    
        return $this;
    }

    /**
     * Get surfaceHabitable
     *
     * @return integer 
     */
    public function getSurfaceHabitable()
    {
        return $this->surfaceHabitable;
    }

    /**
     * Set nombreSalleDeBain
     *
     * @param string $nombreSalleDeBain
     * @return RawLeisureLoc
     */
    public function setNombreSalleDeBain($nombreSalleDeBain)
    {
        $this->nombreSalleDeBain = $nombreSalleDeBain;
    
        return $this;
    }

    /**
     * Get nombreSalleDeBain
     *
     * @return string 
     */
    public function getNombreSalleDeBain()
    {
        return $this->nombreSalleDeBain;
    }

    /**
     * Set nombreChambre
     *
     * @param string $nombreChambre
     * @return RawLeisureLoc
     */
    public function setNombreChambre($nombreChambre)
    {
        $this->nombreChambre = $nombreChambre;
    
        return $this;
    }

    /**
     * Get nombreChambre
     *
     * @return string 
     */
    public function getNombreChambre()
    {
        return $this->nombreChambre;
    }

    /**
     * Set animauxDomestiques
     *
     * @param boolean $animauxDomestiques
     * @return RawLeisureLoc
     */
    public function setAnimauxDomestiques($animauxDomestiques)
    {
        $this->animauxDomestiques = $animauxDomestiques;
    
        return $this;
    }

    /**
     * Get animauxDomestiques
     *
     * @return boolean 
     */
    public function getAnimauxDomestiques()
    {
        return $this->animauxDomestiques;
    }

    /**
     * Set descriptionBreve
     *
     * @param string $descriptionBreve
     * @return RawLeisureLoc
     */
    public function setDescriptionBreve($descriptionBreve)
    {
        $this->descriptionBreve = $descriptionBreve;
    
        return $this;
    }

    /**
     * Get descriptionBreve
     *
     * @return string 
     */
    public function getDescriptionBreve()
    {
        return $this->descriptionBreve;
    }

    /**
     * Set descriptionDetaillee
     *
     * @param string $descriptionDetaillee
     * @return RawLeisureLoc
     */
    public function setDescriptionDetaillee($descriptionDetaillee)
    {
        $this->descriptionDetaillee = $descriptionDetaillee;
    
        return $this;
    }

    /**
     * Get descriptionDetaillee
     *
     * @return string 
     */
    public function getDescriptionDetaillee()
    {
        return $this->descriptionDetaillee;
    }

    /**
     * Set caracteristiques
     *
     * @param string $caracteristiques
     * @return RawLeisureLoc
     */
    public function setCaracteristiques($caracteristiques)
    {
        $this->caracteristiques = $caracteristiques;
    
        return $this;
    }

    /**
     * Get caracteristiques
     *
     * @return string 
     */
    public function getCaracteristiques()
    {
        return $this->caracteristiques;
    }

    /**
     * Set amenagement
     *
     * @param string $amenagement
     * @return RawLeisureLoc
     */
    public function setAmenagement($amenagement)
    {
        $this->amenagement = $amenagement;
    
        return $this;
    }

    /**
     * Get amenagement
     *
     * @return string 
     */
    public function getAmenagement()
    {
        return $this->amenagement;
    }

    /**
     * Set distanceMer
     *
     * @param string $distanceMer
     * @return RawLeisureLoc
     */
    public function setDistanceMer($distanceMer)
    {
        $this->distanceMer = $distanceMer;
    
        return $this;
    }

    /**
     * Get distanceMer
     *
     * @return string 
     */
    public function getDistanceMer()
    {
        return $this->distanceMer;
    }

    /**
     * Set distanceLac
     *
     * @param string $distanceLac
     * @return RawLeisureLoc
     */
    public function setDistanceLac($distanceLac)
    {
        $this->distanceLac = $distanceLac;
    
        return $this;
    }

    /**
     * Get distanceLac
     *
     * @return string 
     */
    public function getDistanceLac()
    {
        return $this->distanceLac;
    }

    /**
     * Set distanceCentreVille
     *
     * @param string $distanceCentreVille
     * @return RawLeisureLoc
     */
    public function setDistanceCentreVille($distanceCentreVille)
    {
        $this->distanceCentreVille = $distanceCentreVille;
    
        return $this;
    }

    /**
     * Get distanceCentreVille
     *
     * @return string 
     */
    public function getDistanceCentreVille()
    {
        return $this->distanceCentreVille;
    }

    /**
     * Set distanceShopping
     *
     * @param string $distanceShopping
     * @return RawLeisureLoc
     */
    public function setDistanceShopping($distanceShopping)
    {
        $this->distanceShopping = $distanceShopping;
    
        return $this;
    }

    /**
     * Get distanceShopping
     *
     * @return string 
     */
    public function getDistanceShopping()
    {
        return $this->distanceShopping;
    }

    /**
     * Set distancePublicTransport
     *
     * @param string $distancePublicTransport
     * @return RawLeisureLoc
     */
    public function setDistancePublicTransport($distancePublicTransport)
    {
        $this->distancePublicTransport = $distancePublicTransport;
    
        return $this;
    }

    /**
     * Get distancePublicTransport
     *
     * @return string 
     */
    public function getDistancePublicTransport()
    {
        return $this->distancePublicTransport;
    }

    /**
     * Set distanceOther
     *
     * @param string $distanceOther
     * @return RawLeisureLoc
     */
    public function setDistanceOther($distanceOther)
    {
        $this->distanceOther = $distanceOther;
    
        return $this;
    }

    /**
     * Get distanceOther
     *
     * @return string 
     */
    public function getDistanceOther()
    {
        return $this->distanceOther;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return RawLeisureLoc
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set rentalprice
     *
     * @param string $rentalprice
     * @return RawLeisureLoc
     */
    public function setRentalprice($rentalprice)
    {
        $this->rentalprice = $rentalprice;
    
        return $this;
    }

    /**
     * Get rentalprice
     *
     * @return string 
     */
    public function getRentalprice()
    {
        return $this->rentalprice;
    }
}
