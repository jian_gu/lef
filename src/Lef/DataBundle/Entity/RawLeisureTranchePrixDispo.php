<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RawLeisureTranchePrix
 *
 * @ORM\Table(name="raw_leisure_tranche_prix_dispo")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\RawLeisureTranchePrixDispoRepository")
 */
class RawLeisureTranchePrixDispo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var String
     *
     * @ORM\Column(name="locationRef", type="string", length=16, nullable=false)
     */
    private $locationRef;

    /**
     * @var \Date
     *
     * @ORM\Column(name="startDate", type="date", nullable=true)
     */
    private $startDate;

//    /**
//     * @var \DateTime
//     *
//     * @ORM\Column(name="heureArrivee", type="time", nullable=true)
//     */
//    private $heureArrivee;

//    /**
//     * @var \DateTime
//     *
//     * @ORM\Column(name="heureLimitArrivee", type="time", nullable=true)
//     */
//    private $heureLimitArrivee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="date", nullable=true)
     */
    private $endDate;

//    /**
//     * @var \DateTime
//     *
//     * @ORM\Column(name="heureDepart", type="time", nullable=true)
//     */
//    private $heureDepart;

//    /**
//     * @var \DateTime
//     *
//     * @ORM\Column(name="heureLimitDepart", type="time", nullable=true)
//     */
//    private $heureLimitDepart;

    /**
     * @var boolean
     *
     * @ORM\Column(name="surDemande", type="boolean", nullable=true)
     */
    private $surDemande;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", nullable=true)
     */
    private $prix;

    /**
     * @var float
     *
     * @ORM\Column(name="rentalPrice", type="float", nullable=true)
     */
    private $rentalPrice;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locationRef
     *
     * @param string $locationRef
     * @return RawLeisureTranchePrixDispo
     */
    public function setLocationRef($locationRef)
    {
        $this->locationRef = $locationRef;
    
        return $this;
    }

    /**
     * Get locationRef
     *
     * @return string 
     */
    public function getLocationRef()
    {
        return $this->locationRef;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return RawLeisureTranchePrixDispo
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return RawLeisureTranchePrixDispo
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    
        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set surDemande
     *
     * @param boolean $surDemande
     * @return RawLeisureTranchePrixDispo
     */
    public function setSurDemande($surDemande)
    {
        $this->surDemande = $surDemande;
    
        return $this;
    }

    /**
     * Get surDemande
     *
     * @return boolean 
     */
    public function getSurDemande()
    {
        return $this->surDemande;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return RawLeisureTranchePrixDispo
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    
        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set rentalPrice
     *
     * @param float $rentalPrice
     * @return RawLeisureTranchePrixDispo
     */
    public function setRentalPrice($rentalPrice)
    {
        $this->rentalPrice = $rentalPrice;
    
        return $this;
    }

    /**
     * Get rentalPrice
     *
     * @return float 
     */
    public function getRentalPrice()
    {
        return $this->rentalPrice;
    }
}