<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StatConsultLocation
 *
 * @author Jian
 * StatResaLocation
 *
 * @ORM\Table(name="stat_resa_location")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\StatResaLocationRepository")
 */
class StatResaLocation {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumn(name="ref_location", referencedColumnName="id")
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="\Lef\BoBundle\Entity\User")
     * @ORM\JoinColumn(name="ref_visiteur", referencedColumnName="id", nullable=true)
     */
    private $visiteur;
    
    /**
     *
     * @var datetime 
     * 
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return StatResaLocation
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set location
     *
     * @param \Lef\DataBundle\Entity\Location $location
     * @return StatResaLocation
     */
    public function setLocation(\Lef\DataBundle\Entity\Location $location = null)
    {
        $this->location = $location;
    
        return $this;
    }

    /**
     * Get location
     *
     * @return \Lef\DataBundle\Entity\Location 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set visiteur
     *
     * @param \Lef\BoBundle\Entity\User $visiteur
     * @return StatResaLocation
     */
    public function setVisiteur(\Lef\BoBundle\Entity\User $visiteur = null)
    {
        $this->visiteur = $visiteur;
    
        return $this;
    }

    /**
     * Get visiteur
     *
     * @return \Lef\BoBundle\Entity\User 
     */
    public function getVisiteur()
    {
        return $this->visiteur;
    }
}