<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TranchePrix
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\TranchePrixRepository")
 */
class TranchePrix {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=128, nullable=true)
     */
    private $titre;

    /**
     * @var float
     *
     * @ORM\Column(name="day", type="float", nullable=true)
     */
    private $day;

    /**
     * @var float
     *
     * @ORM\Column(name="weekend", type="float", nullable=true)
     */
    private $weekend;

    /**
     * @var float
     *
     * @ORM\Column(name="week", type="float", nullable=true)
     */
    private $week;

    /**
     * @var float
     *
     * @ORM\Column(name="weeks2", type="float", nullable=true)
     */
    private $weeks2;

    /**
     * @var float
     *
     * @ORM\Column(name="weeks3", type="float", nullable=true)
     */
    private $weeks3;

    /**
     * @var float
     *
     * @ORM\Column(name="month", type="float", nullable=true)
     */
    private $month;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="date", nullable=true)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="date", nullable=true)
     */
    private $dateFin;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return TranchePrix
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set day
     *
     * @param float $day
     * @return TranchePrix
     */
    public function setDay($day)
    {
        $this->day = $day;
    
        return $this;
    }

    /**
     * Get day
     *
     * @return float 
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set weekend
     *
     * @param float $weekend
     * @return TranchePrix
     */
    public function setWeekend($weekend)
    {
        $this->weekend = $weekend;
    
        return $this;
    }

    /**
     * Get weekend
     *
     * @return float 
     */
    public function getWeekend()
    {
        return $this->weekend;
    }

    /**
     * Set week
     *
     * @param float $week
     * @return TranchePrix
     */
    public function setWeek($week)
    {
        $this->week = $week;
    
        return $this;
    }

    /**
     * Get week
     *
     * @return float 
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * Set weeks2
     *
     * @param float $weeks2
     * @return TranchePrix
     */
    public function setWeeks2($weeks2)
    {
        $this->weeks2 = $weeks2;
    
        return $this;
    }

    /**
     * Get weeks2
     *
     * @return float 
     */
    public function getWeeks2()
    {
        return $this->weeks2;
    }

    /**
     * Set weeks3
     *
     * @param float $weeks3
     * @return TranchePrix
     */
    public function setWeeks3($weeks3)
    {
        $this->weeks3 = $weeks3;
    
        return $this;
    }

    /**
     * Get weeks3
     *
     * @return float 
     */
    public function getWeeks3()
    {
        return $this->weeks3;
    }

    /**
     * Set month
     *
     * @param float $month
     * @return TranchePrix
     */
    public function setMonth($month)
    {
        $this->month = $month;
    
        return $this;
    }

    /**
     * Get month
     *
     * @return float 
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return TranchePrix
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    
        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return TranchePrix
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    
        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }
}