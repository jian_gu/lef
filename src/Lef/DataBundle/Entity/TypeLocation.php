<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeLocation
 *
 * @ORM\Table(name="location_type")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\TypeLocationRepository")
 */
class TypeLocation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return TypeLocation
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    public function getAttrs()
    {
        $attributs = array();
        foreach (get_object_vars($this) as $key => $value) {
            if ($key == "id") {continue;}
            $attributs[] = $key;
        }
        $className_array = explode("\\", get_class($this));
        $className = end($className_array);
        return array( $className => $attributs);
    }

    /**
     * 
     * @return type
     */
    public function __toString() {
        return $this->titre;
    }

}