<?php

namespace Lef\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Annonces
 *
 * @ORM\Table(name="annonces")
 * @ORM\Entity(repositoryClass="Lef\DataBundle\Entity\AnnoncesRepository")
 */
class Annonces
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IMMOID", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $immoid;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROPID", type="bigint", nullable=false)
     */
    private $propid;

    /**
     * @var string
     *
     * @ORM\Column(name="SITELINK", type="string", length=150, nullable=false)
     */
    private $sitelink;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPLOU", type="string", length=50, nullable=false)
     */
    private $typlou;

    /**
     * @var string
     *
     * @ORM\Column(name="CATTYPLOU", type="string", length=35, nullable=false)
     */
    private $cattyplou;

    /**
     * @var string
     *
     * @ORM\Column(name="PAYS", type="string", length=5, nullable=false)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="ADRESSE", type="text", nullable=false)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="CP", type="string", length=8, nullable=false)
     */
    private $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="VILLE", type="string", length=35, nullable=false)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="REGION", type="string", length=35, nullable=false)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="SITE", type="string", length=50, nullable=false)
     */
    private $site;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=15, nullable=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="LABELTXT", type="string", length=35, nullable=false)
     */
    private $labeltxt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="EPI", type="boolean", nullable=false)
     */
    private $epi;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CLE", type="boolean", nullable=false)
     */
    private $cle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="LABELCAP", type="boolean", nullable=false)
     */
    private $labelcap;

    /**
     * @var boolean
     *
     * @ORM\Column(name="LABELCAPMAX", type="boolean", nullable=false)
     */
    private $labelcapmax;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPELOG", type="string", length=20, nullable=false)
     */
    private $typelog;

    /**
     * @var string
     *
     * @ORM\Column(name="SURFLOG", type="string", length=5, nullable=false)
     */
    private $surflog;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPLOUTXT", type="string", length=35, nullable=false)
     */
    private $typloutxt;

    /**
     * @var string
     *
     * @ORM\Column(name="SITCHBREHOTE", type="string", length=40, nullable=false)
     */
    private $sitchbrehote;

    /**
     * @var string
     *
     * @ORM\Column(name="SITUATION", type="string", length=40, nullable=false)
     */
    private $situation;

    /**
     * @var string
     *
     * @ORM\Column(name="SITTYPLOG", type="string", length=40, nullable=false)
     */
    private $sittyplog;

    /**
     * @var string
     *
     * @ORM\Column(name="VUE", type="string", length=40, nullable=false)
     */
    private $vue;

    /**
     * @var string
     *
     * @ORM\Column(name="VUETXT", type="string", length=35, nullable=false)
     */
    private $vuetxt;

    /**
     * @var string
     *
     * @ORM\Column(name="SITADIST1", type="string", length=5, nullable=false)
     */
    private $sitadist1;

    /**
     * @var string
     *
     * @ORM\Column(name="SITALIEU1", type="string", length=20, nullable=false)
     */
    private $sitalieu1;

    /**
     * @var string
     *
     * @ORM\Column(name="SITADIST12", type="string", length=5, nullable=false)
     */
    private $sitadist12;

    /**
     * @var string
     *
     * @ORM\Column(name="SITALIEU2", type="string", length=20, nullable=false)
     */
    private $sitalieu2;

    /**
     * @var integer
     *
     * @ORM\Column(name="CARSEJSURF", type="integer", nullable=false)
     */
    private $carsejsurf;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CARSEJOPTMEZ", type="boolean", nullable=false)
     */
    private $carsejoptmez;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CARSEJOPTCHEM", type="boolean", nullable=false)
     */
    private $carsejoptchem;

    /**
     * @var string
     *
     * @ORM\Column(name="CARSEJOPTCUIS", type="string", length=20, nullable=false)
     */
    private $carsejoptcuis;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CARNBCHAMB", type="boolean", nullable=false)
     */
    private $carnbchamb;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CARNBSDB", type="boolean", nullable=false)
     */
    private $carnbsdb;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CARNBSDEAU", type="boolean", nullable=false)
     */
    private $carnbsdeau;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CARNBWC", type="boolean", nullable=false)
     */
    private $carnbwc;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CARWCIND", type="boolean", nullable=false)
     */
    private $carwcind;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CARCOULITDB", type="boolean", nullable=false)
     */
    private $carcoulitdb;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CARCOULITSIMP", type="boolean", nullable=false)
     */
    private $carcoulitsimp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CARCOULITCONV", type="boolean", nullable=false)
     */
    private $carcoulitconv;

    /**
     * @var string
     *
     * @ORM\Column(name="EQUIP", type="string", length=30, nullable=false)
     */
    private $equip;

    /**
     * @var string
     *
     * @ORM\Column(name="EQUIPEXT", type="string", length=10, nullable=false)
     */
    private $equipext;

    /**
     * @var string
     *
     * @ORM\Column(name="ELEXJARDIN", type="string", length=30, nullable=false)
     */
    private $elexjardin;

    /**
     * @var string
     *
     * @ORM\Column(name="ELEXJARDINSURF", type="string", length=5, nullable=false)
     */
    private $elexjardinsurf;

    /**
     * @var string
     *
     * @ORM\Column(name="ELEXPARKING", type="string", length=30, nullable=false)
     */
    private $elexparking;

    /**
     * @var string
     *
     * @ORM\Column(name="ELEXPARKINGNB", type="string", length=5, nullable=false)
     */
    private $elexparkingnb;

    /**
     * @var string
     *
     * @ORM\Column(name="ELEXGARAGE", type="string", length=30, nullable=false)
     */
    private $elexgarage;

    /**
     * @var string
     *
     * @ORM\Column(name="ELEXGARAGENB", type="string", length=5, nullable=false)
     */
    private $elexgaragenb;

    /**
     * @var string
     *
     * @ORM\Column(name="ELEXPISCINE", type="string", length=30, nullable=false)
     */
    private $elexpiscine;

    /**
     * @var string
     *
     * @ORM\Column(name="ELEXPISCINESURF", type="string", length=5, nullable=false)
     */
    private $elexpiscinesurf;

    /**
     * @var string
     *
     * @ORM\Column(name="ELEXTENNIS", type="string", length=30, nullable=false)
     */
    private $elextennis;

    /**
     * @var string
     *
     * @ORM\Column(name="ELEX", type="string", length=10, nullable=false)
     */
    private $elex;

    /**
     * @var boolean
     *
     * @ORM\Column(name="SERVDIVAH", type="boolean", nullable=false)
     */
    private $servdivah;

    /**
     * @var boolean
     *
     * @ORM\Column(name="SERVDIVPD", type="boolean", nullable=false)
     */
    private $servdivpd;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVDIVMENAGE", type="string", length=30, nullable=false)
     */
    private $servdivmenage;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVDIVLINGE", type="string", length=30, nullable=false)
     */
    private $servdivlinge;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVDIVDRAPS", type="string", length=30, nullable=false)
     */
    private $servdivdraps;

    /**
     * @var boolean
     *
     * @ORM\Column(name="SERVDIVANIM", type="boolean", nullable=false)
     */
    private $servdivanim;

    /**
     * @var string
     *
     * @ORM\Column(name="ACTIV", type="string", length=30, nullable=false)
     */
    private $activ;

    /**
     * @var string
     *
     * @ORM\Column(name="ACTIVAUT1", type="string", length=30, nullable=false)
     */
    private $activaut1;

    /**
     * @var string
     *
     * @ORM\Column(name="ACTIVAUT1DIST", type="string", length=35, nullable=false)
     */
    private $activaut1dist;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ACTIVAUT1VAL", type="boolean", nullable=false)
     */
    private $activaut1val;

    /**
     * @var string
     *
     * @ORM\Column(name="ACTIVAUT2", type="string", length=30, nullable=false)
     */
    private $activaut2;

    /**
     * @var string
     *
     * @ORM\Column(name="ACTIVAUT2DIST", type="string", length=35, nullable=false)
     */
    private $activaut2dist;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ACTIVAUT2VAL", type="boolean", nullable=false)
     */
    private $activaut2val;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCFR", type="text", nullable=false)
     */
    private $descfr;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCANG", type="text", nullable=false)
     */
    private $descang;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFNTTHS", type="integer", nullable=false)
     */
    private $tarifntths;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFNTHS", type="integer", nullable=false)
     */
    private $tarifnths;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFNTMS", type="integer", nullable=false)
     */
    private $tarifntms;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFNTBS", type="integer", nullable=false)
     */
    private $tarifntbs;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFWETHS", type="integer", nullable=false)
     */
    private $tarifweths;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFWEHS", type="integer", nullable=false)
     */
    private $tarifwehs;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFWEMS", type="integer", nullable=false)
     */
    private $tarifwems;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFWEBS", type="integer", nullable=false)
     */
    private $tarifwebs;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFSETHS", type="integer", nullable=false)
     */
    private $tarifseths;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFSEHS", type="integer", nullable=false)
     */
    private $tarifsehs;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFSEMS", type="integer", nullable=false)
     */
    private $tarifsems;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFSEBS", type="integer", nullable=false)
     */
    private $tarifsebs;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFQZTHS", type="integer", nullable=false)
     */
    private $tarifqzths;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFQZHS", type="integer", nullable=false)
     */
    private $tarifqzhs;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFQZMS", type="integer", nullable=false)
     */
    private $tarifqzms;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFQZBS", type="integer", nullable=false)
     */
    private $tarifqzbs;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFMTTHS", type="integer", nullable=false)
     */
    private $tarifmtths;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFMTHS", type="integer", nullable=false)
     */
    private $tarifmths;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFMTMS", type="integer", nullable=false)
     */
    private $tarifmtms;

    /**
     * @var integer
     *
     * @ORM\Column(name="TARIFMTBS", type="integer", nullable=false)
     */
    private $tarifmtbs;

    /**
     * @var string
     *
     * @ORM\Column(name="THSDEB1", type="string", length=10, nullable=false)
     */
    private $thsdeb1;

    /**
     * @var string
     *
     * @ORM\Column(name="THSFIN1", type="string", length=10, nullable=false)
     */
    private $thsfin1;

    /**
     * @var string
     *
     * @ORM\Column(name="THSDEB2", type="string", length=10, nullable=false)
     */
    private $thsdeb2;

    /**
     * @var string
     *
     * @ORM\Column(name="THSFIN2", type="string", length=10, nullable=false)
     */
    private $thsfin2;

    /**
     * @var string
     *
     * @ORM\Column(name="HSDEB1", type="string", length=10, nullable=false)
     */
    private $hsdeb1;

    /**
     * @var string
     *
     * @ORM\Column(name="HSFIN1", type="string", length=10, nullable=false)
     */
    private $hsfin1;

    /**
     * @var string
     *
     * @ORM\Column(name="HSDEB2", type="string", length=10, nullable=false)
     */
    private $hsdeb2;

    /**
     * @var string
     *
     * @ORM\Column(name="HSFIN2", type="string", length=10, nullable=false)
     */
    private $hsfin2;

    /**
     * @var string
     *
     * @ORM\Column(name="MSDEB1", type="string", length=10, nullable=false)
     */
    private $msdeb1;

    /**
     * @var string
     *
     * @ORM\Column(name="MSFIN1", type="string", length=10, nullable=false)
     */
    private $msfin1;

    /**
     * @var string
     *
     * @ORM\Column(name="MSDEB2", type="string", length=10, nullable=false)
     */
    private $msdeb2;

    /**
     * @var string
     *
     * @ORM\Column(name="MSFIN2", type="string", length=10, nullable=false)
     */
    private $msfin2;

    /**
     * @var string
     *
     * @ORM\Column(name="BSDEB1", type="string", length=10, nullable=false)
     */
    private $bsdeb1;

    /**
     * @var string
     *
     * @ORM\Column(name="BSFIN1", type="string", length=10, nullable=false)
     */
    private $bsfin1;

    /**
     * @var string
     *
     * @ORM\Column(name="BSDEB2", type="string", length=10, nullable=false)
     */
    private $bsdeb2;

    /**
     * @var string
     *
     * @ORM\Column(name="BSFIN2", type="string", length=10, nullable=false)
     */
    private $bsfin2;

    /**
     * @var string
     *
     * @ORM\Column(name="BTHSDEB1", type="string", length=10, nullable=false)
     */
    private $bthsdeb1;

    /**
     * @var string
     *
     * @ORM\Column(name="BTHSDEB2", type="string", length=10, nullable=false)
     */
    private $bthsdeb2;

    /**
     * @var string
     *
     * @ORM\Column(name="BTHSFIN1", type="string", length=10, nullable=false)
     */
    private $bthsfin1;

    /**
     * @var string
     *
     * @ORM\Column(name="BTHSFIN2", type="string", length=10, nullable=false)
     */
    private $bthsfin2;

    /**
     * @var string
     *
     * @ORM\Column(name="BHSDEB1", type="string", length=10, nullable=false)
     */
    private $bhsdeb1;

    /**
     * @var string
     *
     * @ORM\Column(name="BHSDEB2", type="string", length=10, nullable=false)
     */
    private $bhsdeb2;

    /**
     * @var string
     *
     * @ORM\Column(name="BHSFIN1", type="string", length=10, nullable=false)
     */
    private $bhsfin1;

    /**
     * @var string
     *
     * @ORM\Column(name="BHSFIN2", type="string", length=10, nullable=false)
     */
    private $bhsfin2;

    /**
     * @var string
     *
     * @ORM\Column(name="BMSDEB1", type="string", length=10, nullable=false)
     */
    private $bmsdeb1;

    /**
     * @var string
     *
     * @ORM\Column(name="BMSDEB2", type="string", length=10, nullable=false)
     */
    private $bmsdeb2;

    /**
     * @var string
     *
     * @ORM\Column(name="BMSFIN1", type="string", length=10, nullable=false)
     */
    private $bmsfin1;

    /**
     * @var string
     *
     * @ORM\Column(name="BMSFIN2", type="string", length=10, nullable=false)
     */
    private $bmsfin2;

    /**
     * @var string
     *
     * @ORM\Column(name="BBSDEB1", type="string", length=10, nullable=false)
     */
    private $bbsdeb1;

    /**
     * @var string
     *
     * @ORM\Column(name="BBSDEB2", type="string", length=10, nullable=false)
     */
    private $bbsdeb2;

    /**
     * @var string
     *
     * @ORM\Column(name="BBSFIN1", type="string", length=10, nullable=false)
     */
    private $bbsfin1;

    /**
     * @var string
     *
     * @ORM\Column(name="BBSFIN2", type="string", length=10, nullable=false)
     */
    private $bbsfin2;

    /**
     * @var string
     *
     * @ORM\Column(name="CALANCOUR", type="text", nullable=false)
     */
    private $calancour;

    /**
     * @var string
     *
     * @ORM\Column(name="CALANSUIV", type="text", nullable=false)
     */
    private $calansuiv;

    /**
     * @var string
     *
     * @ORM\Column(name="PLANNINGA", type="text", nullable=false)
     */
    private $planninga;

    /**
     * @var string
     *
     * @ORM\Column(name="PLANNINGB", type="text", nullable=false)
     */
    private $planningb;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATECREA", type="date", nullable=false)
     */
    private $datecrea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATEECHEANCE", type="date", nullable=false)
     */
    private $dateecheance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATEMAJ", type="date", nullable=false)
     */
    private $datemaj;

    /**
     * @var string
     *
     * @ORM\Column(name="PHOTO1", type="string", length=250, nullable=false)
     */
    private $photo1;

    /**
     * @var string
     *
     * @ORM\Column(name="PHOTO2", type="string", length=250, nullable=false)
     */
    private $photo2;

    /**
     * @var string
     *
     * @ORM\Column(name="PHOTO3", type="string", length=250, nullable=false)
     */
    private $photo3;

    /**
     * @var string
     *
     * @ORM\Column(name="PHOTO4", type="string", length=250, nullable=false)
     */
    private $photo4;

    /**
     * @var string
     *
     * @ORM\Column(name="TAGCCOEUR", type="string", length=250, nullable=false)
     */
    private $tagccoeur;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VISIBLE", type="boolean", nullable=false)
     */
    private $visible;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ADMVISIBLE", type="boolean", nullable=false)
     */
    private $admvisible;

    /**
     * @var integer
     *
     * @ORM\Column(name="STAT", type="bigint", nullable=false)
     */
    private $stat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ADM", type="boolean", nullable=false)
     */
    private $adm;

    /**
     * @var string
     *
     * @ORM\Column(name="ADMRESUME", type="text", nullable=false)
     */
    private $admresume;

    /**
     * @var boolean
     *
     * @ORM\Column(name="DMINACTIVE", type="boolean", nullable=false)
     */
    private $dminactive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="DMINRB1", type="boolean", nullable=false)
     */
    private $dminrb1;

    /**
     * @var integer
     *
     * @ORM\Column(name="DMINTAUX", type="integer", nullable=false)
     */
    private $dmintaux;

    /**
     * @var integer
     *
     * @ORM\Column(name="DMINPRIX", type="integer", nullable=false)
     */
    private $dminprix;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DMINDATED", type="date", nullable=false)
     */
    private $dmindated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DMINDATEF", type="date", nullable=false)
     */
    private $dmindatef;

    /**
     * @var boolean
     *
     * @ORM\Column(name="DMINNUIT", type="boolean", nullable=false)
     */
    private $dminnuit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="DMINWE", type="boolean", nullable=false)
     */
    private $dminwe;

    /**
     * @var boolean
     *
     * @ORM\Column(name="DMINSEM", type="boolean", nullable=false)
     */
    private $dminsem;

    /**
     * @var boolean
     *
     * @ORM\Column(name="DMINQZ", type="boolean", nullable=false)
     */
    private $dminqz;

    /**
     * @var boolean
     *
     * @ORM\Column(name="DMINMOIS", type="boolean", nullable=false)
     */
    private $dminmois;

    /**
     * @var float
     *
     * @ORM\Column(name="LAT", type="float", nullable=false)
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="LNG", type="float", nullable=false)
     */
    private $lng;



    /**
     * Get immoid
     *
     * @return integer 
     */
    public function getImmoid()
    {
        return $this->immoid;
    }

    /**
     * Set propid
     *
     * @param integer $propid
     * @return Annonces
     */
    public function setPropid($propid)
    {
        $this->propid = $propid;
    
        return $this;
    }

    /**
     * Get propid
     *
     * @return integer 
     */
    public function getPropid()
    {
        return $this->propid;
    }

    /**
     * Set sitelink
     *
     * @param string $sitelink
     * @return Annonces
     */
    public function setSitelink($sitelink)
    {
        $this->sitelink = $sitelink;
    
        return $this;
    }

    /**
     * Get sitelink
     *
     * @return string 
     */
    public function getSitelink()
    {
        return $this->sitelink;
    }

    /**
     * Set typlou
     *
     * @param string $typlou
     * @return Annonces
     */
    public function setTyplou($typlou)
    {
        $this->typlou = $typlou;
    
        return $this;
    }

    /**
     * Get typlou
     *
     * @return string 
     */
    public function getTyplou()
    {
        return $this->typlou;
    }

    /**
     * Set cattyplou
     *
     * @param string $cattyplou
     * @return Annonces
     */
    public function setCattyplou($cattyplou)
    {
        $this->cattyplou = $cattyplou;
    
        return $this;
    }

    /**
     * Get cattyplou
     *
     * @return string 
     */
    public function getCattyplou()
    {
        return $this->cattyplou;
    }

    /**
     * Set pays
     *
     * @param string $pays
     * @return Annonces
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    
        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Annonces
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    
        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return Annonces
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    
        return $this;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Annonces
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    
        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Annonces
     */
    public function setRegion($region)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set site
     *
     * @param string $site
     * @return Annonces
     */
    public function setSite($site)
    {
        $this->site = $site;
    
        return $this;
    }

    /**
     * Get site
     *
     * @return string 
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Annonces
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set labeltxt
     *
     * @param string $labeltxt
     * @return Annonces
     */
    public function setLabeltxt($labeltxt)
    {
        $this->labeltxt = $labeltxt;
    
        return $this;
    }

    /**
     * Get labeltxt
     *
     * @return string 
     */
    public function getLabeltxt()
    {
        return $this->labeltxt;
    }

    /**
     * Set epi
     *
     * @param boolean $epi
     * @return Annonces
     */
    public function setEpi($epi)
    {
        $this->epi = $epi;
    
        return $this;
    }

    /**
     * Get epi
     *
     * @return boolean 
     */
    public function getEpi()
    {
        return $this->epi;
    }

    /**
     * Set cle
     *
     * @param boolean $cle
     * @return Annonces
     */
    public function setCle($cle)
    {
        $this->cle = $cle;
    
        return $this;
    }

    /**
     * Get cle
     *
     * @return boolean 
     */
    public function getCle()
    {
        return $this->cle;
    }

    /**
     * Set labelcap
     *
     * @param boolean $labelcap
     * @return Annonces
     */
    public function setLabelcap($labelcap)
    {
        $this->labelcap = $labelcap;
    
        return $this;
    }

    /**
     * Get labelcap
     *
     * @return boolean 
     */
    public function getLabelcap()
    {
        return $this->labelcap;
    }

    /**
     * Set labelcapmax
     *
     * @param boolean $labelcapmax
     * @return Annonces
     */
    public function setLabelcapmax($labelcapmax)
    {
        $this->labelcapmax = $labelcapmax;
    
        return $this;
    }

    /**
     * Get labelcapmax
     *
     * @return boolean 
     */
    public function getLabelcapmax()
    {
        return $this->labelcapmax;
    }

    /**
     * Set typelog
     *
     * @param string $typelog
     * @return Annonces
     */
    public function setTypelog($typelog)
    {
        $this->typelog = $typelog;
    
        return $this;
    }

    /**
     * Get typelog
     *
     * @return string 
     */
    public function getTypelog()
    {
        return $this->typelog;
    }

    /**
     * Set surflog
     *
     * @param string $surflog
     * @return Annonces
     */
    public function setSurflog($surflog)
    {
        $this->surflog = $surflog;
    
        return $this;
    }

    /**
     * Get surflog
     *
     * @return string 
     */
    public function getSurflog()
    {
        return $this->surflog;
    }

    /**
     * Set typloutxt
     *
     * @param string $typloutxt
     * @return Annonces
     */
    public function setTyploutxt($typloutxt)
    {
        $this->typloutxt = $typloutxt;
    
        return $this;
    }

    /**
     * Get typloutxt
     *
     * @return string 
     */
    public function getTyploutxt()
    {
        return $this->typloutxt;
    }

    /**
     * Set sitchbrehote
     *
     * @param string $sitchbrehote
     * @return Annonces
     */
    public function setSitchbrehote($sitchbrehote)
    {
        $this->sitchbrehote = $sitchbrehote;
    
        return $this;
    }

    /**
     * Get sitchbrehote
     *
     * @return string 
     */
    public function getSitchbrehote()
    {
        return $this->sitchbrehote;
    }

    /**
     * Set situation
     *
     * @param string $situation
     * @return Annonces
     */
    public function setSituation($situation)
    {
        $this->situation = $situation;
    
        return $this;
    }

    /**
     * Get situation
     *
     * @return string 
     */
    public function getSituation()
    {
        return $this->situation;
    }

    /**
     * Set sittyplog
     *
     * @param string $sittyplog
     * @return Annonces
     */
    public function setSittyplog($sittyplog)
    {
        $this->sittyplog = $sittyplog;
    
        return $this;
    }

    /**
     * Get sittyplog
     *
     * @return string 
     */
    public function getSittyplog()
    {
        return $this->sittyplog;
    }

    /**
     * Set vue
     *
     * @param string $vue
     * @return Annonces
     */
    public function setVue($vue)
    {
        $this->vue = $vue;
    
        return $this;
    }

    /**
     * Get vue
     *
     * @return string 
     */
    public function getVue()
    {
        return $this->vue;
    }

    /**
     * Set vuetxt
     *
     * @param string $vuetxt
     * @return Annonces
     */
    public function setVuetxt($vuetxt)
    {
        $this->vuetxt = $vuetxt;
    
        return $this;
    }

    /**
     * Get vuetxt
     *
     * @return string 
     */
    public function getVuetxt()
    {
        return $this->vuetxt;
    }

    /**
     * Set sitadist1
     *
     * @param string $sitadist1
     * @return Annonces
     */
    public function setSitadist1($sitadist1)
    {
        $this->sitadist1 = $sitadist1;
    
        return $this;
    }

    /**
     * Get sitadist1
     *
     * @return string 
     */
    public function getSitadist1()
    {
        return $this->sitadist1;
    }

    /**
     * Set sitalieu1
     *
     * @param string $sitalieu1
     * @return Annonces
     */
    public function setSitalieu1($sitalieu1)
    {
        $this->sitalieu1 = $sitalieu1;
    
        return $this;
    }

    /**
     * Get sitalieu1
     *
     * @return string 
     */
    public function getSitalieu1()
    {
        return $this->sitalieu1;
    }

    /**
     * Set sitadist12
     *
     * @param string $sitadist12
     * @return Annonces
     */
    public function setSitadist12($sitadist12)
    {
        $this->sitadist12 = $sitadist12;
    
        return $this;
    }

    /**
     * Get sitadist12
     *
     * @return string 
     */
    public function getSitadist12()
    {
        return $this->sitadist12;
    }

    /**
     * Set sitalieu2
     *
     * @param string $sitalieu2
     * @return Annonces
     */
    public function setSitalieu2($sitalieu2)
    {
        $this->sitalieu2 = $sitalieu2;
    
        return $this;
    }

    /**
     * Get sitalieu2
     *
     * @return string 
     */
    public function getSitalieu2()
    {
        return $this->sitalieu2;
    }

    /**
     * Set carsejsurf
     *
     * @param integer $carsejsurf
     * @return Annonces
     */
    public function setCarsejsurf($carsejsurf)
    {
        $this->carsejsurf = $carsejsurf;
    
        return $this;
    }

    /**
     * Get carsejsurf
     *
     * @return integer 
     */
    public function getCarsejsurf()
    {
        return $this->carsejsurf;
    }

    /**
     * Set carsejoptmez
     *
     * @param boolean $carsejoptmez
     * @return Annonces
     */
    public function setCarsejoptmez($carsejoptmez)
    {
        $this->carsejoptmez = $carsejoptmez;
    
        return $this;
    }

    /**
     * Get carsejoptmez
     *
     * @return boolean 
     */
    public function getCarsejoptmez()
    {
        return $this->carsejoptmez;
    }

    /**
     * Set carsejoptchem
     *
     * @param boolean $carsejoptchem
     * @return Annonces
     */
    public function setCarsejoptchem($carsejoptchem)
    {
        $this->carsejoptchem = $carsejoptchem;
    
        return $this;
    }

    /**
     * Get carsejoptchem
     *
     * @return boolean 
     */
    public function getCarsejoptchem()
    {
        return $this->carsejoptchem;
    }

    /**
     * Set carsejoptcuis
     *
     * @param string $carsejoptcuis
     * @return Annonces
     */
    public function setCarsejoptcuis($carsejoptcuis)
    {
        $this->carsejoptcuis = $carsejoptcuis;
    
        return $this;
    }

    /**
     * Get carsejoptcuis
     *
     * @return string 
     */
    public function getCarsejoptcuis()
    {
        return $this->carsejoptcuis;
    }

    /**
     * Set carnbchamb
     *
     * @param boolean $carnbchamb
     * @return Annonces
     */
    public function setCarnbchamb($carnbchamb)
    {
        $this->carnbchamb = $carnbchamb;
    
        return $this;
    }

    /**
     * Get carnbchamb
     *
     * @return boolean 
     */
    public function getCarnbchamb()
    {
        return $this->carnbchamb;
    }

    /**
     * Set carnbsdb
     *
     * @param boolean $carnbsdb
     * @return Annonces
     */
    public function setCarnbsdb($carnbsdb)
    {
        $this->carnbsdb = $carnbsdb;
    
        return $this;
    }

    /**
     * Get carnbsdb
     *
     * @return boolean 
     */
    public function getCarnbsdb()
    {
        return $this->carnbsdb;
    }

    /**
     * Set carnbsdeau
     *
     * @param boolean $carnbsdeau
     * @return Annonces
     */
    public function setCarnbsdeau($carnbsdeau)
    {
        $this->carnbsdeau = $carnbsdeau;
    
        return $this;
    }

    /**
     * Get carnbsdeau
     *
     * @return boolean 
     */
    public function getCarnbsdeau()
    {
        return $this->carnbsdeau;
    }

    /**
     * Set carnbwc
     *
     * @param boolean $carnbwc
     * @return Annonces
     */
    public function setCarnbwc($carnbwc)
    {
        $this->carnbwc = $carnbwc;
    
        return $this;
    }

    /**
     * Get carnbwc
     *
     * @return boolean 
     */
    public function getCarnbwc()
    {
        return $this->carnbwc;
    }

    /**
     * Set carwcind
     *
     * @param boolean $carwcind
     * @return Annonces
     */
    public function setCarwcind($carwcind)
    {
        $this->carwcind = $carwcind;
    
        return $this;
    }

    /**
     * Get carwcind
     *
     * @return boolean 
     */
    public function getCarwcind()
    {
        return $this->carwcind;
    }

    /**
     * Set carcoulitdb
     *
     * @param boolean $carcoulitdb
     * @return Annonces
     */
    public function setCarcoulitdb($carcoulitdb)
    {
        $this->carcoulitdb = $carcoulitdb;
    
        return $this;
    }

    /**
     * Get carcoulitdb
     *
     * @return boolean 
     */
    public function getCarcoulitdb()
    {
        return $this->carcoulitdb;
    }

    /**
     * Set carcoulitsimp
     *
     * @param boolean $carcoulitsimp
     * @return Annonces
     */
    public function setCarcoulitsimp($carcoulitsimp)
    {
        $this->carcoulitsimp = $carcoulitsimp;
    
        return $this;
    }

    /**
     * Get carcoulitsimp
     *
     * @return boolean 
     */
    public function getCarcoulitsimp()
    {
        return $this->carcoulitsimp;
    }

    /**
     * Set carcoulitconv
     *
     * @param boolean $carcoulitconv
     * @return Annonces
     */
    public function setCarcoulitconv($carcoulitconv)
    {
        $this->carcoulitconv = $carcoulitconv;
    
        return $this;
    }

    /**
     * Get carcoulitconv
     *
     * @return boolean 
     */
    public function getCarcoulitconv()
    {
        return $this->carcoulitconv;
    }

    /**
     * Set equip
     *
     * @param string $equip
     * @return Annonces
     */
    public function setEquip($equip)
    {
        $this->equip = $equip;
    
        return $this;
    }

    /**
     * Get equip
     *
     * @return string 
     */
    public function getEquip()
    {
        return $this->equip;
    }

    /**
     * Set equipext
     *
     * @param string $equipext
     * @return Annonces
     */
    public function setEquipext($equipext)
    {
        $this->equipext = $equipext;
    
        return $this;
    }

    /**
     * Get equipext
     *
     * @return string 
     */
    public function getEquipext()
    {
        return $this->equipext;
    }

    /**
     * Set elexjardin
     *
     * @param string $elexjardin
     * @return Annonces
     */
    public function setElexjardin($elexjardin)
    {
        $this->elexjardin = $elexjardin;
    
        return $this;
    }

    /**
     * Get elexjardin
     *
     * @return string 
     */
    public function getElexjardin()
    {
        return $this->elexjardin;
    }

    /**
     * Set elexjardinsurf
     *
     * @param string $elexjardinsurf
     * @return Annonces
     */
    public function setElexjardinsurf($elexjardinsurf)
    {
        $this->elexjardinsurf = $elexjardinsurf;
    
        return $this;
    }

    /**
     * Get elexjardinsurf
     *
     * @return string 
     */
    public function getElexjardinsurf()
    {
        return $this->elexjardinsurf;
    }

    /**
     * Set elexparking
     *
     * @param string $elexparking
     * @return Annonces
     */
    public function setElexparking($elexparking)
    {
        $this->elexparking = $elexparking;
    
        return $this;
    }

    /**
     * Get elexparking
     *
     * @return string 
     */
    public function getElexparking()
    {
        return $this->elexparking;
    }

    /**
     * Set elexparkingnb
     *
     * @param string $elexparkingnb
     * @return Annonces
     */
    public function setElexparkingnb($elexparkingnb)
    {
        $this->elexparkingnb = $elexparkingnb;
    
        return $this;
    }

    /**
     * Get elexparkingnb
     *
     * @return string 
     */
    public function getElexparkingnb()
    {
        return $this->elexparkingnb;
    }

    /**
     * Set elexgarage
     *
     * @param string $elexgarage
     * @return Annonces
     */
    public function setElexgarage($elexgarage)
    {
        $this->elexgarage = $elexgarage;
    
        return $this;
    }

    /**
     * Get elexgarage
     *
     * @return string 
     */
    public function getElexgarage()
    {
        return $this->elexgarage;
    }

    /**
     * Set elexgaragenb
     *
     * @param string $elexgaragenb
     * @return Annonces
     */
    public function setElexgaragenb($elexgaragenb)
    {
        $this->elexgaragenb = $elexgaragenb;
    
        return $this;
    }

    /**
     * Get elexgaragenb
     *
     * @return string 
     */
    public function getElexgaragenb()
    {
        return $this->elexgaragenb;
    }

    /**
     * Set elexpiscine
     *
     * @param string $elexpiscine
     * @return Annonces
     */
    public function setElexpiscine($elexpiscine)
    {
        $this->elexpiscine = $elexpiscine;
    
        return $this;
    }

    /**
     * Get elexpiscine
     *
     * @return string 
     */
    public function getElexpiscine()
    {
        return $this->elexpiscine;
    }

    /**
     * Set elexpiscinesurf
     *
     * @param string $elexpiscinesurf
     * @return Annonces
     */
    public function setElexpiscinesurf($elexpiscinesurf)
    {
        $this->elexpiscinesurf = $elexpiscinesurf;
    
        return $this;
    }

    /**
     * Get elexpiscinesurf
     *
     * @return string 
     */
    public function getElexpiscinesurf()
    {
        return $this->elexpiscinesurf;
    }

    /**
     * Set elextennis
     *
     * @param string $elextennis
     * @return Annonces
     */
    public function setElextennis($elextennis)
    {
        $this->elextennis = $elextennis;
    
        return $this;
    }

    /**
     * Get elextennis
     *
     * @return string 
     */
    public function getElextennis()
    {
        return $this->elextennis;
    }

    /**
     * Set elex
     *
     * @param string $elex
     * @return Annonces
     */
    public function setElex($elex)
    {
        $this->elex = $elex;
    
        return $this;
    }

    /**
     * Get elex
     *
     * @return string 
     */
    public function getElex()
    {
        return $this->elex;
    }

    /**
     * Set servdivah
     *
     * @param boolean $servdivah
     * @return Annonces
     */
    public function setServdivah($servdivah)
    {
        $this->servdivah = $servdivah;
    
        return $this;
    }

    /**
     * Get servdivah
     *
     * @return boolean 
     */
    public function getServdivah()
    {
        return $this->servdivah;
    }

    /**
     * Set servdivpd
     *
     * @param boolean $servdivpd
     * @return Annonces
     */
    public function setServdivpd($servdivpd)
    {
        $this->servdivpd = $servdivpd;
    
        return $this;
    }

    /**
     * Get servdivpd
     *
     * @return boolean 
     */
    public function getServdivpd()
    {
        return $this->servdivpd;
    }

    /**
     * Set servdivmenage
     *
     * @param string $servdivmenage
     * @return Annonces
     */
    public function setServdivmenage($servdivmenage)
    {
        $this->servdivmenage = $servdivmenage;
    
        return $this;
    }

    /**
     * Get servdivmenage
     *
     * @return string 
     */
    public function getServdivmenage()
    {
        return $this->servdivmenage;
    }

    /**
     * Set servdivlinge
     *
     * @param string $servdivlinge
     * @return Annonces
     */
    public function setServdivlinge($servdivlinge)
    {
        $this->servdivlinge = $servdivlinge;
    
        return $this;
    }

    /**
     * Get servdivlinge
     *
     * @return string 
     */
    public function getServdivlinge()
    {
        return $this->servdivlinge;
    }

    /**
     * Set servdivdraps
     *
     * @param string $servdivdraps
     * @return Annonces
     */
    public function setServdivdraps($servdivdraps)
    {
        $this->servdivdraps = $servdivdraps;
    
        return $this;
    }

    /**
     * Get servdivdraps
     *
     * @return string 
     */
    public function getServdivdraps()
    {
        return $this->servdivdraps;
    }

    /**
     * Set servdivanim
     *
     * @param boolean $servdivanim
     * @return Annonces
     */
    public function setServdivanim($servdivanim)
    {
        $this->servdivanim = $servdivanim;
    
        return $this;
    }

    /**
     * Get servdivanim
     *
     * @return boolean 
     */
    public function getServdivanim()
    {
        return $this->servdivanim;
    }

    /**
     * Set activ
     *
     * @param string $activ
     * @return Annonces
     */
    public function setActiv($activ)
    {
        $this->activ = $activ;
    
        return $this;
    }

    /**
     * Get activ
     *
     * @return string 
     */
    public function getActiv()
    {
        return $this->activ;
    }

    /**
     * Set activaut1
     *
     * @param string $activaut1
     * @return Annonces
     */
    public function setActivaut1($activaut1)
    {
        $this->activaut1 = $activaut1;
    
        return $this;
    }

    /**
     * Get activaut1
     *
     * @return string 
     */
    public function getActivaut1()
    {
        return $this->activaut1;
    }

    /**
     * Set activaut1dist
     *
     * @param string $activaut1dist
     * @return Annonces
     */
    public function setActivaut1dist($activaut1dist)
    {
        $this->activaut1dist = $activaut1dist;
    
        return $this;
    }

    /**
     * Get activaut1dist
     *
     * @return string 
     */
    public function getActivaut1dist()
    {
        return $this->activaut1dist;
    }

    /**
     * Set activaut1val
     *
     * @param boolean $activaut1val
     * @return Annonces
     */
    public function setActivaut1val($activaut1val)
    {
        $this->activaut1val = $activaut1val;
    
        return $this;
    }

    /**
     * Get activaut1val
     *
     * @return boolean 
     */
    public function getActivaut1val()
    {
        return $this->activaut1val;
    }

    /**
     * Set activaut2
     *
     * @param string $activaut2
     * @return Annonces
     */
    public function setActivaut2($activaut2)
    {
        $this->activaut2 = $activaut2;
    
        return $this;
    }

    /**
     * Get activaut2
     *
     * @return string 
     */
    public function getActivaut2()
    {
        return $this->activaut2;
    }

    /**
     * Set activaut2dist
     *
     * @param string $activaut2dist
     * @return Annonces
     */
    public function setActivaut2dist($activaut2dist)
    {
        $this->activaut2dist = $activaut2dist;
    
        return $this;
    }

    /**
     * Get activaut2dist
     *
     * @return string 
     */
    public function getActivaut2dist()
    {
        return $this->activaut2dist;
    }

    /**
     * Set activaut2val
     *
     * @param boolean $activaut2val
     * @return Annonces
     */
    public function setActivaut2val($activaut2val)
    {
        $this->activaut2val = $activaut2val;
    
        return $this;
    }

    /**
     * Get activaut2val
     *
     * @return boolean 
     */
    public function getActivaut2val()
    {
        return $this->activaut2val;
    }

    /**
     * Set descfr
     *
     * @param string $descfr
     * @return Annonces
     */
    public function setDescfr($descfr)
    {
        $this->descfr = $descfr;
    
        return $this;
    }

    /**
     * Get descfr
     *
     * @return string 
     */
    public function getDescfr()
    {
        return $this->descfr;
    }

    /**
     * Set descang
     *
     * @param string $descang
     * @return Annonces
     */
    public function setDescang($descang)
    {
        $this->descang = $descang;
    
        return $this;
    }

    /**
     * Get descang
     *
     * @return string 
     */
    public function getDescang()
    {
        return $this->descang;
    }

    /**
     * Set tarifntths
     *
     * @param integer $tarifntths
     * @return Annonces
     */
    public function setTarifntths($tarifntths)
    {
        $this->tarifntths = $tarifntths;
    
        return $this;
    }

    /**
     * Get tarifntths
     *
     * @return integer 
     */
    public function getTarifntths()
    {
        return $this->tarifntths;
    }

    /**
     * Set tarifnths
     *
     * @param integer $tarifnths
     * @return Annonces
     */
    public function setTarifnths($tarifnths)
    {
        $this->tarifnths = $tarifnths;
    
        return $this;
    }

    /**
     * Get tarifnths
     *
     * @return integer 
     */
    public function getTarifnths()
    {
        return $this->tarifnths;
    }

    /**
     * Set tarifntms
     *
     * @param integer $tarifntms
     * @return Annonces
     */
    public function setTarifntms($tarifntms)
    {
        $this->tarifntms = $tarifntms;
    
        return $this;
    }

    /**
     * Get tarifntms
     *
     * @return integer 
     */
    public function getTarifntms()
    {
        return $this->tarifntms;
    }

    /**
     * Set tarifntbs
     *
     * @param integer $tarifntbs
     * @return Annonces
     */
    public function setTarifntbs($tarifntbs)
    {
        $this->tarifntbs = $tarifntbs;
    
        return $this;
    }

    /**
     * Get tarifntbs
     *
     * @return integer 
     */
    public function getTarifntbs()
    {
        return $this->tarifntbs;
    }

    /**
     * Set tarifweths
     *
     * @param integer $tarifweths
     * @return Annonces
     */
    public function setTarifweths($tarifweths)
    {
        $this->tarifweths = $tarifweths;
    
        return $this;
    }

    /**
     * Get tarifweths
     *
     * @return integer 
     */
    public function getTarifweths()
    {
        return $this->tarifweths;
    }

    /**
     * Set tarifwehs
     *
     * @param integer $tarifwehs
     * @return Annonces
     */
    public function setTarifwehs($tarifwehs)
    {
        $this->tarifwehs = $tarifwehs;
    
        return $this;
    }

    /**
     * Get tarifwehs
     *
     * @return integer 
     */
    public function getTarifwehs()
    {
        return $this->tarifwehs;
    }

    /**
     * Set tarifwems
     *
     * @param integer $tarifwems
     * @return Annonces
     */
    public function setTarifwems($tarifwems)
    {
        $this->tarifwems = $tarifwems;
    
        return $this;
    }

    /**
     * Get tarifwems
     *
     * @return integer 
     */
    public function getTarifwems()
    {
        return $this->tarifwems;
    }

    /**
     * Set tarifwebs
     *
     * @param integer $tarifwebs
     * @return Annonces
     */
    public function setTarifwebs($tarifwebs)
    {
        $this->tarifwebs = $tarifwebs;
    
        return $this;
    }

    /**
     * Get tarifwebs
     *
     * @return integer 
     */
    public function getTarifwebs()
    {
        return $this->tarifwebs;
    }

    /**
     * Set tarifseths
     *
     * @param integer $tarifseths
     * @return Annonces
     */
    public function setTarifseths($tarifseths)
    {
        $this->tarifseths = $tarifseths;
    
        return $this;
    }

    /**
     * Get tarifseths
     *
     * @return integer 
     */
    public function getTarifseths()
    {
        return $this->tarifseths;
    }

    /**
     * Set tarifsehs
     *
     * @param integer $tarifsehs
     * @return Annonces
     */
    public function setTarifsehs($tarifsehs)
    {
        $this->tarifsehs = $tarifsehs;
    
        return $this;
    }

    /**
     * Get tarifsehs
     *
     * @return integer 
     */
    public function getTarifsehs()
    {
        return $this->tarifsehs;
    }

    /**
     * Set tarifsems
     *
     * @param integer $tarifsems
     * @return Annonces
     */
    public function setTarifsems($tarifsems)
    {
        $this->tarifsems = $tarifsems;
    
        return $this;
    }

    /**
     * Get tarifsems
     *
     * @return integer 
     */
    public function getTarifsems()
    {
        return $this->tarifsems;
    }

    /**
     * Set tarifsebs
     *
     * @param integer $tarifsebs
     * @return Annonces
     */
    public function setTarifsebs($tarifsebs)
    {
        $this->tarifsebs = $tarifsebs;
    
        return $this;
    }

    /**
     * Get tarifsebs
     *
     * @return integer 
     */
    public function getTarifsebs()
    {
        return $this->tarifsebs;
    }

    /**
     * Set tarifqzths
     *
     * @param integer $tarifqzths
     * @return Annonces
     */
    public function setTarifqzths($tarifqzths)
    {
        $this->tarifqzths = $tarifqzths;
    
        return $this;
    }

    /**
     * Get tarifqzths
     *
     * @return integer 
     */
    public function getTarifqzths()
    {
        return $this->tarifqzths;
    }

    /**
     * Set tarifqzhs
     *
     * @param integer $tarifqzhs
     * @return Annonces
     */
    public function setTarifqzhs($tarifqzhs)
    {
        $this->tarifqzhs = $tarifqzhs;
    
        return $this;
    }

    /**
     * Get tarifqzhs
     *
     * @return integer 
     */
    public function getTarifqzhs()
    {
        return $this->tarifqzhs;
    }

    /**
     * Set tarifqzms
     *
     * @param integer $tarifqzms
     * @return Annonces
     */
    public function setTarifqzms($tarifqzms)
    {
        $this->tarifqzms = $tarifqzms;
    
        return $this;
    }

    /**
     * Get tarifqzms
     *
     * @return integer 
     */
    public function getTarifqzms()
    {
        return $this->tarifqzms;
    }

    /**
     * Set tarifqzbs
     *
     * @param integer $tarifqzbs
     * @return Annonces
     */
    public function setTarifqzbs($tarifqzbs)
    {
        $this->tarifqzbs = $tarifqzbs;
    
        return $this;
    }

    /**
     * Get tarifqzbs
     *
     * @return integer 
     */
    public function getTarifqzbs()
    {
        return $this->tarifqzbs;
    }

    /**
     * Set tarifmtths
     *
     * @param integer $tarifmtths
     * @return Annonces
     */
    public function setTarifmtths($tarifmtths)
    {
        $this->tarifmtths = $tarifmtths;
    
        return $this;
    }

    /**
     * Get tarifmtths
     *
     * @return integer 
     */
    public function getTarifmtths()
    {
        return $this->tarifmtths;
    }

    /**
     * Set tarifmths
     *
     * @param integer $tarifmths
     * @return Annonces
     */
    public function setTarifmths($tarifmths)
    {
        $this->tarifmths = $tarifmths;
    
        return $this;
    }

    /**
     * Get tarifmths
     *
     * @return integer 
     */
    public function getTarifmths()
    {
        return $this->tarifmths;
    }

    /**
     * Set tarifmtms
     *
     * @param integer $tarifmtms
     * @return Annonces
     */
    public function setTarifmtms($tarifmtms)
    {
        $this->tarifmtms = $tarifmtms;
    
        return $this;
    }

    /**
     * Get tarifmtms
     *
     * @return integer 
     */
    public function getTarifmtms()
    {
        return $this->tarifmtms;
    }

    /**
     * Set tarifmtbs
     *
     * @param integer $tarifmtbs
     * @return Annonces
     */
    public function setTarifmtbs($tarifmtbs)
    {
        $this->tarifmtbs = $tarifmtbs;
    
        return $this;
    }

    /**
     * Get tarifmtbs
     *
     * @return integer 
     */
    public function getTarifmtbs()
    {
        return $this->tarifmtbs;
    }

    /**
     * Set thsdeb1
     *
     * @param string $thsdeb1
     * @return Annonces
     */
    public function setThsdeb1($thsdeb1)
    {
        $this->thsdeb1 = $thsdeb1;
    
        return $this;
    }

    /**
     * Get thsdeb1
     *
     * @return string 
     */
    public function getThsdeb1()
    {
        return $this->thsdeb1;
    }

    /**
     * Set thsfin1
     *
     * @param string $thsfin1
     * @return Annonces
     */
    public function setThsfin1($thsfin1)
    {
        $this->thsfin1 = $thsfin1;
    
        return $this;
    }

    /**
     * Get thsfin1
     *
     * @return string 
     */
    public function getThsfin1()
    {
        return $this->thsfin1;
    }

    /**
     * Set thsdeb2
     *
     * @param string $thsdeb2
     * @return Annonces
     */
    public function setThsdeb2($thsdeb2)
    {
        $this->thsdeb2 = $thsdeb2;
    
        return $this;
    }

    /**
     * Get thsdeb2
     *
     * @return string 
     */
    public function getThsdeb2()
    {
        return $this->thsdeb2;
    }

    /**
     * Set thsfin2
     *
     * @param string $thsfin2
     * @return Annonces
     */
    public function setThsfin2($thsfin2)
    {
        $this->thsfin2 = $thsfin2;
    
        return $this;
    }

    /**
     * Get thsfin2
     *
     * @return string 
     */
    public function getThsfin2()
    {
        return $this->thsfin2;
    }

    /**
     * Set hsdeb1
     *
     * @param string $hsdeb1
     * @return Annonces
     */
    public function setHsdeb1($hsdeb1)
    {
        $this->hsdeb1 = $hsdeb1;
    
        return $this;
    }

    /**
     * Get hsdeb1
     *
     * @return string 
     */
    public function getHsdeb1()
    {
        return $this->hsdeb1;
    }

    /**
     * Set hsfin1
     *
     * @param string $hsfin1
     * @return Annonces
     */
    public function setHsfin1($hsfin1)
    {
        $this->hsfin1 = $hsfin1;
    
        return $this;
    }

    /**
     * Get hsfin1
     *
     * @return string 
     */
    public function getHsfin1()
    {
        return $this->hsfin1;
    }

    /**
     * Set hsdeb2
     *
     * @param string $hsdeb2
     * @return Annonces
     */
    public function setHsdeb2($hsdeb2)
    {
        $this->hsdeb2 = $hsdeb2;
    
        return $this;
    }

    /**
     * Get hsdeb2
     *
     * @return string 
     */
    public function getHsdeb2()
    {
        return $this->hsdeb2;
    }

    /**
     * Set hsfin2
     *
     * @param string $hsfin2
     * @return Annonces
     */
    public function setHsfin2($hsfin2)
    {
        $this->hsfin2 = $hsfin2;
    
        return $this;
    }

    /**
     * Get hsfin2
     *
     * @return string 
     */
    public function getHsfin2()
    {
        return $this->hsfin2;
    }

    /**
     * Set msdeb1
     *
     * @param string $msdeb1
     * @return Annonces
     */
    public function setMsdeb1($msdeb1)
    {
        $this->msdeb1 = $msdeb1;
    
        return $this;
    }

    /**
     * Get msdeb1
     *
     * @return string 
     */
    public function getMsdeb1()
    {
        return $this->msdeb1;
    }

    /**
     * Set msfin1
     *
     * @param string $msfin1
     * @return Annonces
     */
    public function setMsfin1($msfin1)
    {
        $this->msfin1 = $msfin1;
    
        return $this;
    }

    /**
     * Get msfin1
     *
     * @return string 
     */
    public function getMsfin1()
    {
        return $this->msfin1;
    }

    /**
     * Set msdeb2
     *
     * @param string $msdeb2
     * @return Annonces
     */
    public function setMsdeb2($msdeb2)
    {
        $this->msdeb2 = $msdeb2;
    
        return $this;
    }

    /**
     * Get msdeb2
     *
     * @return string 
     */
    public function getMsdeb2()
    {
        return $this->msdeb2;
    }

    /**
     * Set msfin2
     *
     * @param string $msfin2
     * @return Annonces
     */
    public function setMsfin2($msfin2)
    {
        $this->msfin2 = $msfin2;
    
        return $this;
    }

    /**
     * Get msfin2
     *
     * @return string 
     */
    public function getMsfin2()
    {
        return $this->msfin2;
    }

    /**
     * Set bsdeb1
     *
     * @param string $bsdeb1
     * @return Annonces
     */
    public function setBsdeb1($bsdeb1)
    {
        $this->bsdeb1 = $bsdeb1;
    
        return $this;
    }

    /**
     * Get bsdeb1
     *
     * @return string 
     */
    public function getBsdeb1()
    {
        return $this->bsdeb1;
    }

    /**
     * Set bsfin1
     *
     * @param string $bsfin1
     * @return Annonces
     */
    public function setBsfin1($bsfin1)
    {
        $this->bsfin1 = $bsfin1;
    
        return $this;
    }

    /**
     * Get bsfin1
     *
     * @return string 
     */
    public function getBsfin1()
    {
        return $this->bsfin1;
    }

    /**
     * Set bsdeb2
     *
     * @param string $bsdeb2
     * @return Annonces
     */
    public function setBsdeb2($bsdeb2)
    {
        $this->bsdeb2 = $bsdeb2;
    
        return $this;
    }

    /**
     * Get bsdeb2
     *
     * @return string 
     */
    public function getBsdeb2()
    {
        return $this->bsdeb2;
    }

    /**
     * Set bsfin2
     *
     * @param string $bsfin2
     * @return Annonces
     */
    public function setBsfin2($bsfin2)
    {
        $this->bsfin2 = $bsfin2;
    
        return $this;
    }

    /**
     * Get bsfin2
     *
     * @return string 
     */
    public function getBsfin2()
    {
        return $this->bsfin2;
    }

    /**
     * Set bthsdeb1
     *
     * @param string $bthsdeb1
     * @return Annonces
     */
    public function setBthsdeb1($bthsdeb1)
    {
        $this->bthsdeb1 = $bthsdeb1;
    
        return $this;
    }

    /**
     * Get bthsdeb1
     *
     * @return string 
     */
    public function getBthsdeb1()
    {
        return $this->bthsdeb1;
    }

    /**
     * Set bthsdeb2
     *
     * @param string $bthsdeb2
     * @return Annonces
     */
    public function setBthsdeb2($bthsdeb2)
    {
        $this->bthsdeb2 = $bthsdeb2;
    
        return $this;
    }

    /**
     * Get bthsdeb2
     *
     * @return string 
     */
    public function getBthsdeb2()
    {
        return $this->bthsdeb2;
    }

    /**
     * Set bthsfin1
     *
     * @param string $bthsfin1
     * @return Annonces
     */
    public function setBthsfin1($bthsfin1)
    {
        $this->bthsfin1 = $bthsfin1;
    
        return $this;
    }

    /**
     * Get bthsfin1
     *
     * @return string 
     */
    public function getBthsfin1()
    {
        return $this->bthsfin1;
    }

    /**
     * Set bthsfin2
     *
     * @param string $bthsfin2
     * @return Annonces
     */
    public function setBthsfin2($bthsfin2)
    {
        $this->bthsfin2 = $bthsfin2;
    
        return $this;
    }

    /**
     * Get bthsfin2
     *
     * @return string 
     */
    public function getBthsfin2()
    {
        return $this->bthsfin2;
    }

    /**
     * Set bhsdeb1
     *
     * @param string $bhsdeb1
     * @return Annonces
     */
    public function setBhsdeb1($bhsdeb1)
    {
        $this->bhsdeb1 = $bhsdeb1;
    
        return $this;
    }

    /**
     * Get bhsdeb1
     *
     * @return string 
     */
    public function getBhsdeb1()
    {
        return $this->bhsdeb1;
    }

    /**
     * Set bhsdeb2
     *
     * @param string $bhsdeb2
     * @return Annonces
     */
    public function setBhsdeb2($bhsdeb2)
    {
        $this->bhsdeb2 = $bhsdeb2;
    
        return $this;
    }

    /**
     * Get bhsdeb2
     *
     * @return string 
     */
    public function getBhsdeb2()
    {
        return $this->bhsdeb2;
    }

    /**
     * Set bhsfin1
     *
     * @param string $bhsfin1
     * @return Annonces
     */
    public function setBhsfin1($bhsfin1)
    {
        $this->bhsfin1 = $bhsfin1;
    
        return $this;
    }

    /**
     * Get bhsfin1
     *
     * @return string 
     */
    public function getBhsfin1()
    {
        return $this->bhsfin1;
    }

    /**
     * Set bhsfin2
     *
     * @param string $bhsfin2
     * @return Annonces
     */
    public function setBhsfin2($bhsfin2)
    {
        $this->bhsfin2 = $bhsfin2;
    
        return $this;
    }

    /**
     * Get bhsfin2
     *
     * @return string 
     */
    public function getBhsfin2()
    {
        return $this->bhsfin2;
    }

    /**
     * Set bmsdeb1
     *
     * @param string $bmsdeb1
     * @return Annonces
     */
    public function setBmsdeb1($bmsdeb1)
    {
        $this->bmsdeb1 = $bmsdeb1;
    
        return $this;
    }

    /**
     * Get bmsdeb1
     *
     * @return string 
     */
    public function getBmsdeb1()
    {
        return $this->bmsdeb1;
    }

    /**
     * Set bmsdeb2
     *
     * @param string $bmsdeb2
     * @return Annonces
     */
    public function setBmsdeb2($bmsdeb2)
    {
        $this->bmsdeb2 = $bmsdeb2;
    
        return $this;
    }

    /**
     * Get bmsdeb2
     *
     * @return string 
     */
    public function getBmsdeb2()
    {
        return $this->bmsdeb2;
    }

    /**
     * Set bmsfin1
     *
     * @param string $bmsfin1
     * @return Annonces
     */
    public function setBmsfin1($bmsfin1)
    {
        $this->bmsfin1 = $bmsfin1;
    
        return $this;
    }

    /**
     * Get bmsfin1
     *
     * @return string 
     */
    public function getBmsfin1()
    {
        return $this->bmsfin1;
    }

    /**
     * Set bmsfin2
     *
     * @param string $bmsfin2
     * @return Annonces
     */
    public function setBmsfin2($bmsfin2)
    {
        $this->bmsfin2 = $bmsfin2;
    
        return $this;
    }

    /**
     * Get bmsfin2
     *
     * @return string 
     */
    public function getBmsfin2()
    {
        return $this->bmsfin2;
    }

    /**
     * Set bbsdeb1
     *
     * @param string $bbsdeb1
     * @return Annonces
     */
    public function setBbsdeb1($bbsdeb1)
    {
        $this->bbsdeb1 = $bbsdeb1;
    
        return $this;
    }

    /**
     * Get bbsdeb1
     *
     * @return string 
     */
    public function getBbsdeb1()
    {
        return $this->bbsdeb1;
    }

    /**
     * Set bbsdeb2
     *
     * @param string $bbsdeb2
     * @return Annonces
     */
    public function setBbsdeb2($bbsdeb2)
    {
        $this->bbsdeb2 = $bbsdeb2;
    
        return $this;
    }

    /**
     * Get bbsdeb2
     *
     * @return string 
     */
    public function getBbsdeb2()
    {
        return $this->bbsdeb2;
    }

    /**
     * Set bbsfin1
     *
     * @param string $bbsfin1
     * @return Annonces
     */
    public function setBbsfin1($bbsfin1)
    {
        $this->bbsfin1 = $bbsfin1;
    
        return $this;
    }

    /**
     * Get bbsfin1
     *
     * @return string 
     */
    public function getBbsfin1()
    {
        return $this->bbsfin1;
    }

    /**
     * Set bbsfin2
     *
     * @param string $bbsfin2
     * @return Annonces
     */
    public function setBbsfin2($bbsfin2)
    {
        $this->bbsfin2 = $bbsfin2;
    
        return $this;
    }

    /**
     * Get bbsfin2
     *
     * @return string 
     */
    public function getBbsfin2()
    {
        return $this->bbsfin2;
    }

    /**
     * Set calancour
     *
     * @param string $calancour
     * @return Annonces
     */
    public function setCalancour($calancour)
    {
        $this->calancour = $calancour;
    
        return $this;
    }

    /**
     * Get calancour
     *
     * @return string 
     */
    public function getCalancour()
    {
        return $this->calancour;
    }

    /**
     * Set calansuiv
     *
     * @param string $calansuiv
     * @return Annonces
     */
    public function setCalansuiv($calansuiv)
    {
        $this->calansuiv = $calansuiv;
    
        return $this;
    }

    /**
     * Get calansuiv
     *
     * @return string 
     */
    public function getCalansuiv()
    {
        return $this->calansuiv;
    }

    /**
     * Set planninga
     *
     * @param string $planninga
     * @return Annonces
     */
    public function setPlanninga($planninga)
    {
        $this->planninga = $planninga;
    
        return $this;
    }

    /**
     * Get planninga
     *
     * @return string 
     */
    public function getPlanninga()
    {
        return $this->planninga;
    }

    /**
     * Set planningb
     *
     * @param string $planningb
     * @return Annonces
     */
    public function setPlanningb($planningb)
    {
        $this->planningb = $planningb;
    
        return $this;
    }

    /**
     * Get planningb
     *
     * @return string 
     */
    public function getPlanningb()
    {
        return $this->planningb;
    }

    /**
     * Set datecrea
     *
     * @param \DateTime $datecrea
     * @return Annonces
     */
    public function setDatecrea($datecrea)
    {
        $this->datecrea = $datecrea;
    
        return $this;
    }

    /**
     * Get datecrea
     *
     * @return \DateTime 
     */
    public function getDatecrea()
    {
        return $this->datecrea;
    }

    /**
     * Set dateecheance
     *
     * @param \DateTime $dateecheance
     * @return Annonces
     */
    public function setDateecheance($dateecheance)
    {
        $this->dateecheance = $dateecheance;
    
        return $this;
    }

    /**
     * Get dateecheance
     *
     * @return \DateTime 
     */
    public function getDateecheance()
    {
        return $this->dateecheance;
    }

    /**
     * Set datemaj
     *
     * @param \DateTime $datemaj
     * @return Annonces
     */
    public function setDatemaj($datemaj)
    {
        $this->datemaj = $datemaj;
    
        return $this;
    }

    /**
     * Get datemaj
     *
     * @return \DateTime 
     */
    public function getDatemaj()
    {
        return $this->datemaj;
    }

    /**
     * Set photo1
     *
     * @param string $photo1
     * @return Annonces
     */
    public function setPhoto1($photo1)
    {
        $this->photo1 = $photo1;
    
        return $this;
    }

    /**
     * Get photo1
     *
     * @return string 
     */
    public function getPhoto1()
    {
        return $this->photo1;
    }

    /**
     * Set photo2
     *
     * @param string $photo2
     * @return Annonces
     */
    public function setPhoto2($photo2)
    {
        $this->photo2 = $photo2;
    
        return $this;
    }

    /**
     * Get photo2
     *
     * @return string 
     */
    public function getPhoto2()
    {
        return $this->photo2;
    }

    /**
     * Set photo3
     *
     * @param string $photo3
     * @return Annonces
     */
    public function setPhoto3($photo3)
    {
        $this->photo3 = $photo3;
    
        return $this;
    }

    /**
     * Get photo3
     *
     * @return string 
     */
    public function getPhoto3()
    {
        return $this->photo3;
    }

    /**
     * Set photo4
     *
     * @param string $photo4
     * @return Annonces
     */
    public function setPhoto4($photo4)
    {
        $this->photo4 = $photo4;
    
        return $this;
    }

    /**
     * Get photo4
     *
     * @return string 
     */
    public function getPhoto4()
    {
        return $this->photo4;
    }

    /**
     * Set tagccoeur
     *
     * @param string $tagccoeur
     * @return Annonces
     */
    public function setTagccoeur($tagccoeur)
    {
        $this->tagccoeur = $tagccoeur;
    
        return $this;
    }

    /**
     * Get tagccoeur
     *
     * @return string 
     */
    public function getTagccoeur()
    {
        return $this->tagccoeur;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Annonces
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set admvisible
     *
     * @param boolean $admvisible
     * @return Annonces
     */
    public function setAdmvisible($admvisible)
    {
        $this->admvisible = $admvisible;
    
        return $this;
    }

    /**
     * Get admvisible
     *
     * @return boolean 
     */
    public function getAdmvisible()
    {
        return $this->admvisible;
    }

    /**
     * Set stat
     *
     * @param integer $stat
     * @return Annonces
     */
    public function setStat($stat)
    {
        $this->stat = $stat;
    
        return $this;
    }

    /**
     * Get stat
     *
     * @return integer 
     */
    public function getStat()
    {
        return $this->stat;
    }

    /**
     * Set adm
     *
     * @param boolean $adm
     * @return Annonces
     */
    public function setAdm($adm)
    {
        $this->adm = $adm;
    
        return $this;
    }

    /**
     * Get adm
     *
     * @return boolean 
     */
    public function getAdm()
    {
        return $this->adm;
    }

    /**
     * Set admresume
     *
     * @param string $admresume
     * @return Annonces
     */
    public function setAdmresume($admresume)
    {
        $this->admresume = $admresume;
    
        return $this;
    }

    /**
     * Get admresume
     *
     * @return string 
     */
    public function getAdmresume()
    {
        return $this->admresume;
    }

    /**
     * Set dminactive
     *
     * @param boolean $dminactive
     * @return Annonces
     */
    public function setDminactive($dminactive)
    {
        $this->dminactive = $dminactive;
    
        return $this;
    }

    /**
     * Get dminactive
     *
     * @return boolean 
     */
    public function getDminactive()
    {
        return $this->dminactive;
    }

    /**
     * Set dminrb1
     *
     * @param boolean $dminrb1
     * @return Annonces
     */
    public function setDminrb1($dminrb1)
    {
        $this->dminrb1 = $dminrb1;
    
        return $this;
    }

    /**
     * Get dminrb1
     *
     * @return boolean 
     */
    public function getDminrb1()
    {
        return $this->dminrb1;
    }

    /**
     * Set dmintaux
     *
     * @param integer $dmintaux
     * @return Annonces
     */
    public function setDmintaux($dmintaux)
    {
        $this->dmintaux = $dmintaux;
    
        return $this;
    }

    /**
     * Get dmintaux
     *
     * @return integer 
     */
    public function getDmintaux()
    {
        return $this->dmintaux;
    }

    /**
     * Set dminprix
     *
     * @param integer $dminprix
     * @return Annonces
     */
    public function setDminprix($dminprix)
    {
        $this->dminprix = $dminprix;
    
        return $this;
    }

    /**
     * Get dminprix
     *
     * @return integer 
     */
    public function getDminprix()
    {
        return $this->dminprix;
    }

    /**
     * Set dmindated
     *
     * @param \DateTime $dmindated
     * @return Annonces
     */
    public function setDmindated($dmindated)
    {
        $this->dmindated = $dmindated;
    
        return $this;
    }

    /**
     * Get dmindated
     *
     * @return \DateTime 
     */
    public function getDmindated()
    {
        return $this->dmindated;
    }

    /**
     * Set dmindatef
     *
     * @param \DateTime $dmindatef
     * @return Annonces
     */
    public function setDmindatef($dmindatef)
    {
        $this->dmindatef = $dmindatef;
    
        return $this;
    }

    /**
     * Get dmindatef
     *
     * @return \DateTime 
     */
    public function getDmindatef()
    {
        return $this->dmindatef;
    }

    /**
     * Set dminnuit
     *
     * @param boolean $dminnuit
     * @return Annonces
     */
    public function setDminnuit($dminnuit)
    {
        $this->dminnuit = $dminnuit;
    
        return $this;
    }

    /**
     * Get dminnuit
     *
     * @return boolean 
     */
    public function getDminnuit()
    {
        return $this->dminnuit;
    }

    /**
     * Set dminwe
     *
     * @param boolean $dminwe
     * @return Annonces
     */
    public function setDminwe($dminwe)
    {
        $this->dminwe = $dminwe;
    
        return $this;
    }

    /**
     * Get dminwe
     *
     * @return boolean 
     */
    public function getDminwe()
    {
        return $this->dminwe;
    }

    /**
     * Set dminsem
     *
     * @param boolean $dminsem
     * @return Annonces
     */
    public function setDminsem($dminsem)
    {
        $this->dminsem = $dminsem;
    
        return $this;
    }

    /**
     * Get dminsem
     *
     * @return boolean 
     */
    public function getDminsem()
    {
        return $this->dminsem;
    }

    /**
     * Set dminqz
     *
     * @param boolean $dminqz
     * @return Annonces
     */
    public function setDminqz($dminqz)
    {
        $this->dminqz = $dminqz;
    
        return $this;
    }

    /**
     * Get dminqz
     *
     * @return boolean 
     */
    public function getDminqz()
    {
        return $this->dminqz;
    }

    /**
     * Set dminmois
     *
     * @param boolean $dminmois
     * @return Annonces
     */
    public function setDminmois($dminmois)
    {
        $this->dminmois = $dminmois;
    
        return $this;
    }

    /**
     * Get dminmois
     *
     * @return boolean 
     */
    public function getDminmois()
    {
        return $this->dminmois;
    }

    /**
     * Set lat
     *
     * @param float $lat
     * @return Annonces
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    
        return $this;
    }

    /**
     * Get lat
     *
     * @return float 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param float $lng
     * @return Annonces
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    
        return $this;
    }

    /**
     * Get lng
     *
     * @return float 
     */
    public function getLng()
    {
        return $this->lng;
    }

   public function getObjectVars() {
        $attributs = array();
        foreach (get_object_vars($this) as $key => $value) {
            $attributs[] = $key;
        }
        return $attributs;
    }
}