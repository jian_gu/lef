<?php

namespace Lef\DataBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
use Lef\DataBundle\Form\MediaType;
use Lef\DataBundle\Form\TranchePrixType;

class LocationType extends AbstractType {

    private function nomsPays() {
        return Intl::getRegionBundle()->getCountryNames('fr');
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
//                ->add('reference')
                ->add('titre')
                ->add('adresse')
                ->add('pays', 'country', array('preferred_choices' => array('FR'),))
                ->add('region')
                ->add('departement')
                ->add('ville')
                ->add('dateDispoDebut', 'date', array(
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'format' => 'dd/MM/yyyy',
                ))
                ->add('dateDispoFin', 'date', array(
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'format' => 'dd/MM/yyyy',
                ))
                ->add('longitude')
                ->add('latitude')
                ->add('ZipPostalCode')
                ->add('nombreDePersonne')
                ->add('nombreEtoiles', 'choice', array(
                    'choices' => array(
                        0 => '0',
                        1 => '1',
                        2 => '2',
                        3 => '3',
                        4 => '4',
                        5 => '5',
                    ),
                    'empty_value' => 'Nombre étoile',
                    'multiple' => FALSE,
                ))
                ->add('surfaceHabitable')
                ->add('nombrePiece')
                ->add('nombreSalleDeBain')
                ->add('nombreChambre')
                ->add('animauxDomestiques')
                ->add('piscine')
                ->add('descriptionBreve')
                ->add('descriptionDetaillee')
                ->add('caracteristiques')
                ->add('amenagement')
                ->add('sejourAvecCouchage')
                ->add('coinRepas')
                ->add('cuisine')
                ->add('sanitaire')
                ->add('distanceOther')
                ->add('distanceMer')
                ->add('distanceLac')
                ->add('distanceRemontees')
                ->add('distanceCentreVille')
                ->add('distanceShopping')
//                ->add('creationDate')
                // ==== with association tables ====
                // ==== ManyToMany BiDirec ====
//                ->add('alerts')
                ->add('medias', 'collection', array(
                    'type' => new MediaType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,))
//                ->add('offres')
//                ->add('plannings')
                // ==== OneToMany UniDirec ====
//                ->add('bienEtres', 'collection', array('type' => new BienEtreType()))
                ->add('bienEtres', 'entity', array(
                    'class' => 'LefDataBundle:BienEtre',
                    'property' => 'titre',
                    'multiple' => true,
                    'expanded' => true,
                    'attr' => array('class' => 'inputBox')))
                ->add('equipements', 'entity', array(
                    'class' => 'LefDataBundle:Equipement',
                    'property' => 'titre',
                    'multiple' => true,
                    'expanded' => true))
                ->add('localisations')
                ->add('optionEnfants', 'entity', array(
                    'class' => 'LefDataBundle:OptionEnfant',
                    'property' => 'titre',
                    'multiple' => true,
                    'expanded' => true))
                ->add('optionHandicapees', 'entity', array(
                    'class' => 'LefDataBundle:OptionHandicapees',
                    'property' => 'titre',
                    'multiple' => true,
                    'expanded' => true))
                ->add('saisons')
                ->add('themes')
                ->add('tranchePrix', 'collection', array(
                    'type' => new TranchePrixType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                ))
//                ->add('tranchePrix', new TranchePrixType())
                ->add('rentalprice', null, array('required' => true))
                ->add('types', 'entity', array('class' => 'LefDataBundle:Type',
                    'multiple' => TRUE,
                    'expanded' => FALSE))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {

        $resolver->setDefaults(array(
            'data_class' => 'Lef\DataBundle\Entity\Location'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'lef_databundle_location';
    }

}
