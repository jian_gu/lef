<?php

namespace Lef\DataBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Lef\DataBundle\Controller\PartnersController as PartnersController;
use Lef\DataBundle\Entity\Mapping as Mapping;

class MappingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $partnersController = new PartnersController();

        $mapping = new Mapping();

        $mapping = $options['data'];
        $filename = $mapping->getFilename();
        $partner = $mapping->getPartner();

        $xml = simplexml_load_file('temp/'.$partner.'/data/'.$filename); 

        $partnersController->getXmlChild($filename, $xml);

        // Tableau qui contindra les attributs xml 
        $xPath_list = $xPath_list2 = array();
        $xPath_list = $partnersController->setXpathList($partnersController->getArray_xml_attribute());
        foreach ($xPath_list as $key => $value) {
            $xPath_list2[$value] = $value;
        }

        // Tableau qui contindra les attributs xml 
        $limit_anonce = $limit_anonce2 = array();
        $limit_anonce = $partnersController->setXpathList2($partnersController->getArray_xml_attribute());
        foreach ($limit_anonce as $key => $value) {
            $value = str_replace($filename, "", $value);
            $array_val = explode('.', $value);
            foreach ($array_val as $k => $v) {
                if ($v != '') {
                    $limit_anonce2[$v] = $v;
                }
            }
        }

        $entities_attrs_list1 = array();
        $entities_attrs_list1 = $partnersController->getEntityAttrs();
        foreach ($entities_attrs_list1 as $key => $value) {
            foreach ($value as $entity_name => $value1) {
                foreach ($value1 as $key1 => $entity_attrs) {
                    $entities_attrs_list2["$entity_name // $entity_attrs"] = "$entity_name // $entity_attrs";
                }
            }
        }

        // Récupération des xpath qui sont deja dans la bdd
        $configured_path = $partnersController->getConfiguredPathAction( $filename, $partner );
        // ================================
        // Ici ill faudrat supprimer les element qui sont déja enregistrer en BDD
        // ================================
                
        // Récupération de l'entity manager qui va nous permettre de gérer les entités.
        // $entityManager = $this->get('doctrine.orm.entity_manager');   

        // $mapping_list = $entityManager->getRepository('Lef\DataBundle\Entity\Mapping')->findAll();

        // foreach ($mapping_list as $key => $value) {
        //     var_dump($key);
        // }

        // $key1 = array_search($del_val_file, $xPath_list);
        //     if (false !== $key1) 
        //     {
        //         unset($xPath_list[$key1]);
        //     }

        $builder
            ->add('xpath', 'choice', array(
                'choices'   => $xPath_list2,
                'empty_value' => 'Choisissez un chemin dans le fichier XML',
                'empty_data'  => null
            ))
            ->add('refto', 'choice', array(
                'choices'   => $entities_attrs_list2,
                'empty_value' => 'Choisissez la réference dans la table location',
                'empty_data'  => null
            ))
            ->add('options', 'choice', array(
                'choices'   => array("attribut"=>"Valeur des attributs", "balise"=>"Valeur de balise"),
                'empty_data'  => null
            ))
            ->add('limite_location', 'choice', array(
                'choices'   => $limit_anonce2,
                'empty_value' => 'Choisissez un niveau de limite',
                'empty_data'  => null
            ))
            ->add('filename')
            ->add('partner')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lef\DataBundle\Entity\Mapping'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lef_databundle_mapping';
    }
}
