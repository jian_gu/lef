<?php

namespace Lef\DataBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MediaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('file', 'file', array('label' => 'Fichier d\'image',
                    'required' => FALSE, 'attr' => array(
                        'class' => 'form-control'
            )))
//                ->add('src')
                ->add('titre', null, array('required' => FALSE,
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Lef\DataBundle\Entity\Media'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'lef_databundle_media';
    }

}
