<?php

namespace Lef\DataBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PartnersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('host')
            ->add('user')
            ->add('password')

            ->add('srcDirectory', null, array(
                'label' => "Dossier source sur le serveur",
                'attr'=>array('placeholder'=>'Laissez vide si les sources sont à la racine du serveur  sinon précisez le chemin, "annonces/blabla"',)))

            ->add('dataType', null, array(
                'label' => "Type de données",
                'attr'=>array('placeholder'=>'JSON ou XML',)))

            ->add('frameUrl', null, array(
                'label' => "Url du frame",
                'attr'=>array('placeholder'=>'Url du frame à générer (pour les partenaire)',)))

            ->add('files', null, array(
                'label' => "Fichiers à télécharger du serveur",
                'attr'=>array('placeholder'=>'Ajouter les noms des fichier à la suite en les séparant par des virgule',)))
            ->add('port')

            // ->add('mapping', null, array('label'=>"Mapping XPath"))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lef\DataBundle\Entity\Partners'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lef_databundle_partners';
    }
}
