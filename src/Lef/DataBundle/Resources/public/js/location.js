/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    $(".chosen-select").chosen({
        placeholder_text_multiple: 'Sélectionner des options'
    });
    
    var $chosenRequired = $(".chosen-select").attr('required');
    if($chosenRequired === 'required'){
        $(".chosen-select").removeAttr('required');
    }
});

