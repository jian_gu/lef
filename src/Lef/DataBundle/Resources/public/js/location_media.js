/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    // ===========================
    // == Définitions variables ==
    // ===========================
    var $mediaCollectionHolder;
    var $addTagLink = $('<a href="#" class="a-add_media">Ajout une autre image</a>');
    var $newLinkLi = $('<li class="li-link-add-media"></li>').append($addTagLink);
    var $counterImage = 2;                                                        // 3 photo min
    var $maxCountImage = 7;                                                     // 8 photo max


    // =========================
    // == Définitions actions ==
    // =========================

    // Get the ul that holds the collection of mesias
    $mediaCollectionHolder = $('ul.media');
    // add the "add a media" anchor and li to the media ul
    $mediaCollectionHolder.append($newLinkLi);

    $addTagLink.on('click', function(e) {
        // count the current form inputs we have (e.g. 3), use that as the new
        // index when inserting a new item (e.g. 3)
        $mediaCollectionHolder.data('index', $mediaCollectionHolder.find(':input[type="file"]').length);
        // prevent the link from creating a "#" on the URL
        e.preventDefault();
        // add a new media form (see next code block)
        addMediaForm($mediaCollectionHolder, $newLinkLi);
    });


    // ===========================
    // == Définitions fonctions ==
    // ===========================
    function addMediaForm($mediaCollectionHolder, $newLinkLi) {
        $counterImage++;
        if ($counterImage > $maxCountImage) {
            return;
        } else {
            // Get the data-prototype explained earlier
            var prototype = $mediaCollectionHolder.data('prototype');
            // get the new index
            var index = $mediaCollectionHolder.data('index');
            // Replace '__name__' in the prototype's HTML to
            // instead be a number based on how many items we have
            var newForm = prototype.replace(/__name__/g, index);
            // increase the index with one for the next item
            $mediaCollectionHolder.data('index', index + 1);
            // Display the form in the page in an li, before the "Add a media" link li
            var $newFormLi = $('<li></li>').append(newForm);
            // Define our style
            _defineStyle($newFormLi);


            // prepend media form before add-link
            $newLinkLi.before($newFormLi);
            // hide or show the add-link
            hideShowLink($counterImage);
            ifAddDelLink();
        }

    }

    function ifAddDelLink() {
        // add a delete link to all of the existing media form li elements
        $mediaCollectionHolder.find('li').each(function() {
            if ($(this).hasClass('li-link-add-media') === false) {
                // remove links if they exist
                if ($(this).find('a.a-del-media').length) {
                    $(this).find('a.a-del-media').remove();
                }
                // add links
                var $id = $(this).find('input[type="file"]').attr('id');
//                console.log($id);
                var $_index = $id.match(/[media_]\d[_file]/)[0];
                var $index = $_index.match(/\d/)[0];

                if ($.inArray($index, ['0', '1', '2']) < 0) {
                    addTagFormDeleteLink($(this), $mediaCollectionHolder);
                }
            }


        });
    }
    function addTagFormDeleteLink($mediaFormLi, $mediaCollectionHolder) {
        var $removeFormA = $('<a href="#" class="a-del-media">Supprimer</a>');
        $mediaFormLi.append($removeFormA);
        // get the new index
        var index = $mediaCollectionHolder.data('index');

        $removeFormA.on('click', function(e) {
            // count the current form inputs we have (e.g. 3), use that as the new
            // index when inserting a new item (e.g. 3)
            $mediaCollectionHolder.data('index', $mediaCollectionHolder.find(':input[type="file"]').length);
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            // remove the li for the media form
            $mediaFormLi.remove();

            $counterImage--;
            // hide or show the add-link
            hideShowLink($counterImage);
        });
    }

    /**
     * Cacher ou afficher le lien pour télécharger photo
     * @param {type} $counter
     * @returns {undefined}
     */
    function hideShowLink($counter) {
        if ($counter < $maxCountImage) {
            $addLink = $mediaCollectionHolder.find('li.li-link-add-media')
            // if hidden, show it
            if ($addLink.hasClass('hidden')) {
                $newLinkLi.removeClass('hidden');
            }
        } else {
            // if exists, remove it
            if ($newLinkLi.length) {
                $newLinkLi.addClass('hidden');
            }
        }
    }

    /**
     * Function define style to our new appends
     * @param {Html element} $form
     * @returns {non}
     */
    function _defineStyle($form) {
        $form.find('div[id]').addClass('form-group row');
        $form.find('div > div').addClass('col-md-3 col-sm-3');
        $form.find('div > div >label').remove();
        $form.find('div > div > input[type="text"]').attr('placeholder', 'titre image');
    }
});
