<?php

namespace Lef\DataBundle\Validator;

use Symfony\Component\Validator\ExecutionContextInterface;
use Lef\DataBundle\Entity\Location;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomLocationCallback
 *
 * @author Jian
 * 
 */
class CustomLocationCallback {

    public static function tranchePrix(Location $location, ExecutionContextInterface $context) {
        // get different prices
        $arrayPrice = array(
            'day' => $location->getTranchePrix()->getDay(),
            'weedend' => $location->getTranchePrix()->getWeekend(),
            'week' => $location->getTranchePrix()->getWeek(),
            'weeks2' => $location->getTranchePrix()->getWeeks2(),
            'weeks3' => $location->getTranchePrix()->getWeeks3(),
            'month' => $location->getTranchePrix()->getMonth(),
        );

        $testValString = '';
        foreach ($arrayPrice as $val) {
            $testValString .= trim($val);
        }

        if (trim($testValString) == '') {
//            $context->addViolation('Vous devez enseigner au moin un prix.');
            $paramMsg = array('class' => 'error');
            $context->addViolation('Vous devez enseigner au moin un prix.', $paramMsg);
        }
        // ...
    }

}
