<?php

namespace Lef\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Locale\Locale;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ComparaisonController
 *
 * @author Jian
 */
class ComparaisonController extends Controller {

    public function feedAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $container = $request->query->get('container');
        $idsStr = $this->_getItemsCompa();
        $idsArry = explode(',', $idsStr);
        $entities = $em->getRepository('LefDataBundle:Location')->findById($idsArry);

//        var_dump(count($entities));die();
        switch ($container) {
            case 'panier':
                return $this->render('LefFoBundle:Comparaison:panier.html.twig', array('items' => $entities));
                break;
            case 'modal':
                return $this->render('LefFoBundle:Comparaison:modal.html.twig', array('items' => $entities));
            default:
                break;
        }
    }

    private function _getItemsCompa() {
        $request = $this->get('request');
        $cookies = $request->cookies;

        if (!$cookies->has('items_compa')) {
            return NULL;
        }
        return $cookies->get('items_compa');
    }

}
