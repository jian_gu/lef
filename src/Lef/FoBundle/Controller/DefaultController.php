<?php

namespace Lef\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;                                   // Added le 120814 par Jian
use Lef\FoBundle\Form\Type\ContactEmailType;                                    // Added le 120814 par Jian

class DefaultController extends Controller {

    /**
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities1 = $em->getRepository('LefDataBundle:Location')->findLimit(0, 4);
        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findMedaiByRef($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $entities2 = $em->getRepository('LefDataBundle:Location')->findLimit(5, 4);
        foreach ($entities2 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findMedaiByRef($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        return array(
            'entities1' => $entities1,
            'entities2' => $entities2,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $nbAnnonce,
        );
    }

    public function phpinfoAction() {
        phpinfo();
    }

    public function staticAction($page) {
        return $this->render('LefFoBundle:Default:' . $page . '.html.twig', array(null));
    }

    /**
     * Contactez-nou
     * Modified le 120814 par Jian
     * @return type
     */
    public function contactAction(Request $request) {
        // ===== Form ======
        $form = $this->_createContactForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            // call function with raw data
            $this->_sendEmailMessage($form);
            // Create flash message
            $this->_createFlashMessage('success', 'Votre demande a été envoyée, veuillez vérifier dans votre boîte courriel.');
            // stay on the current page
            return $this->redirect($this->generateUrl('contact'));
        }
        // ====== end form ======

        return $this->render('LefFoBundle:Default:contact.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    ////////////////////////////////
    ////// private functions ///////
    ////////////////////////////////
    /**
     * Create contact form
     * added le 120814 par Jian
     * @return type
     */
    private function _createContactForm() {
        $form = $this->createForm(new ContactEmailType(), null, array(
            'action' => $this->generateUrl('contact'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Envoyer la demande'));

        return $form;
    }

    /**
     * Create flash message
     * added le 120814 par Jian
     * @param string $type
     * @param string $contents
     */
    private function _createFlashMessage($type, $contents) {
        $this->get('session')->getFlashBag()->add(
                $type, $contents
        );
    }

    /**
     * Send email with mailer service
     * added le 120814 par Jian
     * @param type $nbrCredit
     * @param type $prix
     * @param type $token
     */
    private function _sendEmailMessage($formData) {
        $context = array();
        // prepare to send email
        // get demande detail
        $context['nom'] = $formData["nom"]->getData();
        $context['prenom'] = $formData["prenom"]->getData();
        $context['societe'] = $formData["societe"]->getData()? : NULL;
        $context['phone'] = $formData["phone"]->getData()? : NULL;
        $context['email'] = $formData["email"]->getData()? : NULL;
        $context['subject'] = $formData["subject"]->getData()? : NULL;
        $context['contents'] = $formData["contents"]->getData()? : NULL;

        $mailer = $this->get('lef_bo.mailer.twig_swift');

        $mailer->sendContactNousEmailMessage($context);
    }

}

