<?php

namespace Lef\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\EntityManager;
use Lef\DataBundle\Entity\Geocode;

class GeocodeController extends Controller {

    // EntityManager
    protected $em;

    public function __construct(EntityManager $entityManager) {
        $this->em = $entityManager;
    }

    /**
     * @Route("/geocode/index")
     * @Template()
     */
    public function IndexAction() {
        return array(
                // ...
        );
    }

    /**
     * Get and return geocode if it exists, otherwise, return false
     * @param string $pays
     * @param string $region
     * @param string $ville
     * @return type
     */
    public function findGeoCodeFromDb($pays, $region = NULL, $ville = NULL) {
        /* Construct condtion array to be passed to query */
        $conditionArray = array('pays' => $pays, 'region' => $region, 'ville' => $ville);
        $em = $this->em;
        $record = $em->getRepository('LefDataBundle:Geocode')->findOneBy($conditionArray);
        $count = count($record);
        return $count > 0 ? $record : NULL;
    }

    /**
     * Saving process
     * @param type $pays
     * @param type $region
     * @param type $ville
     * @param type $centerLng
     * @param type $centerLat
     * @param type $boundN
     * @param type $boundE
     * @param type $boundS
     * @param type $boundW
     */
    public function insertGeocode($pays, $region, $ville, $centerLng, $centerLat, $boundN, $boundE, $boundS, $boundW) {
        /* instancier object */
        $entity = new Geocode();
        // TODO: bind values to object
        $entity->setPays($pays);
        $entity->setRegion($region);
        $entity->setVille($ville);
        $entity->setCenterLng($centerLng);
        $entity->setCenterLat($centerLat);
        $entity->setBoundN($boundN);
        $entity->setBoundE($boundE);
        $entity->setBoundS($boundS);
        $entity->setBoundW($boundW);
        //
        /* get entity manager */
        $em = $this->em;
        /* persist object */
        $em->persist($entity);
        /* insertion */
        $em->flush();
    }

    /**
     * Save geocode into database
     * @param type $geodata
     */
    public function saveGeocode($geodata) {
        /* Grab administration level names */
        $pays = $geodata->pays == NULL ? NULL : $geodata->pays;
        $region = $geodata->region == NULL ? NULL : $geodata->region;
        $ville = $geodata->ville == NULL ? NULL : $geodata->ville;
        /* Test if code is already exist */
        $record = $this->findGeoCodeFromDb($pays, $region, $ville);
        /* If not exists, we save it */
        if ($record == NULL) {
//            var_dump($geodata);
            $centerLng = $geodata->center->lng;
            $centerLat = $geodata->center->lat;
            $boundN = $geodata->bounds->northeast->lat;
            $boundE = $geodata->bounds->northeast->lng;
            $boundS = $geodata->bounds->southwest->lat;
            $boundW = $geodata->bounds->southwest->lng;
            $this->insertGeocode($pays, $region, $ville, $centerLng, $centerLat, $boundN, $boundE, $boundS, $boundW);
        }
    }

    /**
     * Get geodata from either DB or webservice, and return it
     * @param type $pays
     * @param type $region
     * @param type $ville
     * @param type $adresse
     * @return \stdClass|null
     */
    public function getGeocodeGeneral($pays = '', $region = '', $ville = '', $adresse = '') {
        /* If exists in BDD */
        // Nullfy params if it is empty
        $paysENF = $pays == '' ? NULL : $pays;
        $regionENF = $region == '' ? NULL : $region;
        $villeENF = $ville == '' ? NULL : $ville;
        $dbRecord = $this->findGeoCodeFromDb($paysENF, $regionENF, $villeENF);

        /* If result exists in BDD */
        if (count($dbRecord) > 0) {
            $geodata = new \stdClass();
            $geodata->pays = $dbRecord->getPays();
            $geodata->region = $dbRecord->getRegion();
            $geodata->ville = $dbRecord->getVille();
            $geodata->center = new \stdClass();
            $geodata->center->lng = $dbRecord->getCenterLng();
            $geodata->center->lat = $dbRecord->getCenterLat();
            $geodata->bounds = new \stdClass();
            $geodata->bounds->northeast = new \stdClass();
            $geodata->bounds->southwest = new \stdClass();
            $geodata->bounds->northeast->lat = $dbRecord->getBoundN();
            $geodata->bounds->northeast->lng = $dbRecord->getBoundE();
            $geodata->bounds->southwest->lat = $dbRecord->getBoundS();
            $geodata->bounds->southwest->lng = $dbRecord->getBoundW();
            return $geodata;
        } else if (count($dbRecord) <= 0) {
            /* Get from web service */
            $geodata = $this->getGeocodeByWebService($pays, $region, $ville, $adresse);
            /* Save record to BDD */
            if ($geodata != NULL) {
                $formatedGeodata = $this->_formatGeodata($geodata);
                $this->saveGeocode($formatedGeodata);
                /* Return retrieved values */
                return $formatedGeodata;
            } else {
                return NULL;
            }
        }
    }

    /**
     * Call web service to get geodata
     * @param string $pays
     * @param string $region
     * @param string $ville
     * @param string $adresse
     * @return null
     */
    public function getGeocodeByWebService($pays = '', $region = '', $ville = '', $adresse = '') {
        /* Prepare params */
        $pays = strpos(trim($pays), ' ') > 0 ? str_replace(' ', '+', trim($pays)) : trim($pays);
        $region = strpos(trim($region), ' ') > 0 ? str_replace(' ', '+', trim($region)) : trim($region);
        $ville = strpos(trim($ville), ' ') > 0 ? str_replace(' ', '+', trim($ville)) : trim($ville);
        $adresse = strpos(trim($adresse), ' ') > 0 ? str_replace(' ', '+', trim($adresse)) : trim($adresse);
        /* Call web service */
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?language=fr&address=' . $adresse . '&components=country:' . $pays . '|administrative_area:' . $region . '|locality:' . $ville;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        /* JSON decode object */
        $dataObject = json_decode($data);
        if (is_object($dataObject) && $dataObject->status == 'OK') {
            return $geodata = $dataObject->results[0];
        } else {
            return NULL;
        }
    }

    /**
     * Format geodata retrieved from web service
     * @param type $geodata
     * @return \stdClass
     */
    private function _formatGeodata($geodata) {
        /* Array paire for translation */
        $typesArray = array('street_address' => 'adresse', 'route' => 'rue', 'postal_code' => 'cp',
            'locality' => 'ville', 'administrative_area3' => 'ville',
            'administrative_area_level_1' => 'region', 'country' => 'pays');
        /* Initiate formated data to be returned */
        $formatedGeodata = new \stdClass();
        /*  */
        $address_components = $geodata->address_components;
        $geometry = $geodata->geometry;
        /*  */
        $formatedGeodata->type = array_key_exists($geodata->types[0], $typesArray) ?
                $typesArray[$geodata->types[0]] : NULL;
        $formatedGeodata->center = $geometry->location;
        $formatedGeodata->fulladdr = $geodata->formatted_address;
        // Bounds exist
        if (property_exists($geometry, 'bounds')) {
            $bounds = $geometry->bounds;
            $formatedGeodata->bounds = $bounds;
        } else {
            $formatedGeodata->bounds->northeast = $formatedGeodata->center;
            $formatedGeodata->bounds->southwest = $formatedGeodata->center;
        }
        /* Initiate adminstration level names */
        $formatedGeodata->pays = NULL;
        $formatedGeodata->region = NULL;
        $formatedGeodata->ville = NULL;
        $formatedGeodata->cp = NULL;
        /* Loop and assign adminstration level names */
        foreach ($address_components as $component) {
            $type = $component->types[0];
            $name = $component->long_name;
            switch ($type) {
                case 'country':
                    $formatedGeodata->pays = $name;
                    break;
                case 'administrative_area_level_1':
                    $formatedGeodata->region = $name;
                    break;
                case 'locality':
                case 'administrative_area3':
                    $formatedGeodata->ville = $name;
                    break;
                case 'cp':
                    $formatedGeodata->cp = $name;
                    break;
                default:
                    break;
            }
        }

//        echo('<pre>');
//        var_dump($formatedGeodata);
//        echo('<pre>');

        return $formatedGeodata;
    }

}
