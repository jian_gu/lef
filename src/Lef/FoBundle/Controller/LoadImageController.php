<?php
/**
 * Controller load image by reference, called by ajax
 */
namespace Lef\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class LoadImageController extends Controller {

    public function byRefAction() {
        $media = null;
        $request = $this->container->get('request');
        $reference = $request->request->get('ref');

        $em = $this->getDoctrine()->getManager();
        $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findMedaiByRef($reference);
        if (!empty($mediasP)) {
            shuffle($mediasP);
            $media = $mediasP[0];
        }
        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new
            JsonEncoder()));
        $resultJsonFy = $serializer->serialize($media, 'json');
        $resultUtf8Fy = utf8_encode($resultJsonFy);
//        var_dump($resultUtf8Fy);die();
        return new Response($resultUtf8Fy);
    }

}
