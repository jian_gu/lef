<?php

namespace Lef\FoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Session\Session;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Form\LocationType;
use Symfony\Component\Locale\Locale;
use Lef\DataBundle\Controller\InterhomeWebServiceController;
use Lef\DataBundle\Controller\InterchaletWebServiceController;
use Lef\DataBundle\Controller\LeisureWebServiceController;
use Lef\FoBundle\Controller\GeocodeController;

/**
 * Location controller.
 *
 * @Route("/location")
 */
class LocationController extends Controller {

    protected static $SRCH_GEO_ADRESSE;
    // Radius in meter, radius_coded = radius / 100000
    protected static $SRCH_GEO_RADIUS = 0;
    protected static $SRCH_DATE_ARRIVEE;
    protected static $SRCH_DATE_DEPART;
    protected static $SRCH_DUREE;
    protected static $SRCH_NBR_PERSO;
    protected static $SRCH_NBR_CHAMBRE;
    protected static $SRCH_NBR_ETOILE;
    protected static $SRCH_NBR_SDB;
    protected static $SRCH_SURFACE_MIN;
    protected static $SRCH_SURFACE_MAX;
    protected static $SRCH_PRIX_MIN;
    protected static $SRCH_PRIX_MAX;
    protected static $SRCH_TYPE_LOGE;
    protected static $SRCH_PISCINE;
    protected static $SRCH_DISTANCE_MER;
    protected static $SRCH_DISTANCE_LAC;
    protected static $SRCH_DISTANCE_REMONTEE;
    protected static $SRCH_DISTANCE_CENTRE_VILLE;
    protected static $SRCH_CARAC;                                               // Ce-ci est un tableau
    protected static $SRCH_RESULTS_BOUNDS;                                      // Ce-ci est un tableau
    protected static $SRCH_RESULTS_CENTER;                                      // Ce-ci est un tableau

    /**
     * Lists all Location entities.
     *
     * @Template()
     */

    public function indexAction(Request $request) {
        // Get url get params. Assign protect static variables
        $this->_getUrlParams($request);
        $session = $this->get('session');
        /* SQL string, initiate with id > 0, ready to added other conditions */
        $dqlMore = "WHERE lc.id > 0 AND lc.pays IS NOT NULL AND lc.region IS NOT NULL AND lc.ville IS NOT NULL ";
        /* SQL for join table */
        $dqlMoreJoin = '';
        /* SQL for range conditions */
        $dqlMoreRange = '';
        /* SQL for disponibilité */
        $dqlMoreDispo = '';
        // Get all post data from request
        $postData = $request->request->all();

        /////////////// GEO DATA ////////////////////
        /////////////////////////////////////////////
        /* If no adresse is used, clear geodata in session */
        if (self::$SRCH_GEO_ADRESSE == NULL && array_key_exists('geodata', $postData)) {
            /* If geodata is already set in session */
            if ($session->has('geodata')) {
                $session->remove('geodata');
            }
        }
        // If Geodata exists in post params
        if (array_key_exists('geodata', $postData)) {
            $geoDataStr = $postData['geodata'];

            $geoData = json_decode($geoDataStr);

            /* Get geodata Type if it exists */
            if (property_exists($geoData, 'type')) {
                $geoType = $geoData->type;
            }
            /* If type in geodata is defined in level pays-region-ville */
            if (!in_array($geoType, array('pays', 'region', 'ville')) && $geoType != NULL) {
                $session->set('geodata', $geoData);
            }
        }

        if (isset($session) && $session->has('geodata')) {
            $geoDataInSession = $session->get('geodata');
            /* Get 4 params for border */
            /* Assign them to the statac variable */
            $e = self::$SRCH_RESULTS_BOUNDS['n'] = (double) $geoDataInSession->bounds->northeast->lat + self::$SRCH_GEO_RADIUS / 100000;
            $n = self::$SRCH_RESULTS_BOUNDS['e'] = (double) $geoDataInSession->bounds->northeast->lng + self::$SRCH_GEO_RADIUS / 100000;
            $w = self::$SRCH_RESULTS_BOUNDS['s'] = (double) $geoDataInSession->bounds->southwest->lat - self::$SRCH_GEO_RADIUS / 100000;
            $s = self::$SRCH_RESULTS_BOUNDS['w'] = (double) $geoDataInSession->bounds->southwest->lng - self::$SRCH_GEO_RADIUS / 100000;
            /* Get country name */
            $countryName = $geoDataInSession->pays;
            /* Get center */
            self::$SRCH_RESULTS_CENTER['lng'] = $geoDataInSession->center->lng;
            self::$SRCH_RESULTS_CENTER['lat'] = $geoDataInSession->center->lat;
            /* Construct sql query string */
            $dqlMore = $dqlMore . "AND lc.latitude > $w AND lc.latitude < $e AND lc.longitude > $s AND lc.longitude < $n ";
            $dqlMore = $dqlMore . "AND lc.pays = '$countryName' ";
        }

        /////////////// SQL MORE ////////////////////
        /////////////////////////////////////////////
        if (self::$SRCH_DATE_ARRIVEE != NULL) {
            if (self::$SRCH_DATE_DEPART != NULL) {
                // Chercher location qui correspondent aux conditions
                $locsDipsoArray = $this->GetLocationsDispo(self::$SRCH_DATE_ARRIVEE, self::$SRCH_DATE_DEPART);
                $dqlMoreJoin = $dqlMoreJoin . "JOIN lc.tranchePrix tp WITH (CASE WHEN tp.dateDebut == '" . self::$SRCH_DATE_ARRIVEE . "' THEN tp.dateDebut = '" . self::$SRCH_DATE_ARRIVEE . "' ELSE tp.dateDebut < '" . self::$SRCH_DATE_ARRIVEE . "' END) AND 
                        (CASE WHEN tp.dateFin == '" . self::$SRCH_DATE_DEPART . "' THEN tp.dateFin = '" . self::$SRCH_DATE_DEPART . "' ELSE tp.dateFin > '" . self::$SRCH_DATE_DEPART . "' END) ";
            } else {
                // Chercher location qui correspondent aux conditions
                $locsDipsoArray = $this->GetLocationsDispo(self::$SRCH_DATE_ARRIVEE);
                $dqlMoreJoin = $dqlMoreJoin . "JOIN lc.tranchePrix tp WITH (CASE WHEN tp.dateDebut = '" . self::$SRCH_DATE_ARRIVEE . "' THEN tp.dateDebut = '" . self::$SRCH_DATE_ARRIVEE . "' ELSE tp.dateDebut < '" . self::$SRCH_DATE_ARRIVEE . "' END) ";
            }
            if (count($locsDipsoArray) <= 0) {
                $locsDipsoArray = [0];
            }
            $dqlMoreDispo = $dqlMoreDispo . "AND lc.id IN (" . implode('  ', $locsDipsoArray) . ") ";
        }
        if (self::$SRCH_NBR_PERSO != NULL) {
            $dqlMore = $dqlMore . "AND lc.nombreDePersonne = " . self::$SRCH_NBR_PERSO . " ";
        }
        if (self::$SRCH_PRIX_MIN != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.rentalPrice >= " . self::$SRCH_PRIX_MIN . " ";
        }
        if (self::$SRCH_PRIX_MAX != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.rentalPrice <= " . self::$SRCH_PRIX_MAX . " ";
        }
        if (self::$SRCH_SURFACE_MIN != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.surfaceHabitable >= " . self::$SRCH_SURFACE_MIN . " ";
        }
        if (self::$SRCH_SURFACE_MAX != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.surfaceHabitable <= " . self::$SRCH_SURFACE_MAX . " ";
        }
        if (self::$SRCH_TYPE_LOGE != NULL) {
            $dqlMore = $dqlMore . "AND lc.typePartner LIKE '%" . self::$SRCH_TYPE_LOGE . "%' ";
        }
        if (self::$SRCH_NBR_ETOILE != NULL) {
            if (self::$SRCH_NBR_ETOILE == 0) {
                $dqlMore = $dqlMore . "AND lc.nombreEtoiles = null ";
            } else {
                $dqlMore = $dqlMore . "AND lc.nombreEtoiles = " . self::$SRCH_NBR_ETOILE . " ";
            }
        }
        if (self::$SRCH_NBR_CHAMBRE != NULL) {
            $dqlMore = $dqlMore . "AND lc.nombreChambre >= " . self::$SRCH_NBR_CHAMBRE . " ";
        }
        if (self::$SRCH_NBR_SDB != NULL) {
            $dqlMore = $dqlMore . "AND lc.nombreSalleDeBain >= " . self::$SRCH_NBR_SDB . " ";
        }
        if (self::$SRCH_PISCINE != NULL) {
            $dqlMore = $dqlMore . "AND (lc.piscine = 1 OR lc.descriptionBreve LIKE '%piscine%' "
                    . "OR lc.descriptionDetaillee LIKE '%piscine%' "
                    . "OR lc.caracteristiques LIKE '%piscine%') ";
        }
        if (self::$SRCH_DISTANCE_MER != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceMer <= " . self::$SRCH_DISTANCE_MER . " ";
        }
        if (self::$SRCH_DISTANCE_LAC != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceLac <= " . self::$SRCH_DISTANCE_LAC . " ";
        }
        if (self::$SRCH_DISTANCE_REMONTEE != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceRemontees <= " . self::$SRCH_DISTANCE_REMONTEE . " ";
        }
        if (self::$SRCH_DISTANCE_CENTRE_VILLE != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceCentreVille <= " . self::$SRCH_DISTANCE_CENTRE_VILLE . " ";
        }
        if (self::$SRCH_CARAC != NULL && count(self::$SRCH_CARAC) > 0) {
            foreach (self::$SRCH_CARAC as $carac) {
                $dqlMore = $dqlMore . "AND (lc.descriptionBreve LIKE '%" . $carac . "%' "
                        . "OR lc.descriptionDetaillee LIKE '%" . $carac . "%' "
                        . "OR lc.caracteristiques LIKE '%" . $carac . "%' "
                        . "OR lc.themePartner LIKE '%" . $carac . "%') ";
            }
        }
        /////////////////////////////////////////////
        /////////////////////////////////////////////
//        echo('<pre>');
//        var_dump($postData);
//        echo('</pre>');
//        die();

        $nbrLocPerPage = $this->container->getParameter('max_articles_on_listepage');
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // Initiate entity manager, deal with queries
        $em = $this->getDoctrine()->getManager();
        $this->_increaseMemoryAllocate();
        $dql = "SELECT lc 
                FROM LefDataBundle:Location lc ";
        // Concatener sql-more and range conditions
        $dql = $dql . $dqlMoreJoin . $dqlMore . $dqlMoreDispo . $dqlMoreRange;
        $dql = $dql . "ORDER BY lc.modificationDate";
        $query = $em->createQuery($dql);

        // ========= range slider =============
        // Sql to get ranges  (min - max) for price and surface
        $dqlRange = "SELECT MIN(lc.rentalPrice) AS prix_min, MAX(lc.rentalPrice) AS prix_max,"
                . "MIN(lc.surfaceHabitable) AS surface_min, MAX(lc.surfaceHabitable) AS surface_max "
                . "FROM LefDataBundle:Location lc ";
        // Here we do not concatene range conditions
        $dqlRange = $dqlRange . $dqlMore;
        $queryRange = $em->createQuery($dqlRange);
        $sliderRangesRaw = $queryRange->getResult();
        $sliderRanges = $sliderRangesRaw[0];
        // ====================================
        // Use KNP paginator
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $this->get('request')->query->get('page', 1)/* page number */, $nbrLocPerPage/* limit per page */
        );

        $nbrTotalLocs = $pagination->getTotalItemCount();

        return array(
            'entities' => $pagination,
            'sliderRanges' => $sliderRanges,
            'totalRecord' => $nbrTotalLocs,
            'adresse' => self::$SRCH_GEO_ADRESSE,
            'resultsCenter' => json_encode(self::$SRCH_RESULTS_CENTER),
            'resultsBounds' => json_encode(self::$SRCH_RESULTS_BOUNDS),
            'nbrItemCompa' => $nombreItemPanierCompa,
        );
    }

    /**
     * Recuperer parametres dans le lien, affecter les variables self
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    private function _getUrlParams(Request $request) {
        // Usable params
        $valideParams = array();
        // Retrieve get data
        $queryData = $request->query->all();
        // Loop get data, and re-construct params which are not blank
        foreach ($queryData as $key => $value) {
            // If an array is present: a cluster values of a param
            if (is_array($value) && count($value) > 0) {
                $valideParams[$key] = $value;
            } else if (trim($value) != '') {
                // If value of param is not blank
                $valideParams[$key] = $value;
            }
        }
        // Loop re-constructed params, and assign them to static variable
        foreach ($valideParams as $key => $value) {
            switch ($key) {
                case 'datearrivee':
                    self::$SRCH_DATE_ARRIVEE = $value;
                    break;
                case 'datedepart':
                    self::$SRCH_DATE_DEPART = $value;
                    break;
                case 'duree':
                    self::$SRCH_DUREE = (integer) $value;
                    break;
                case 'nbrperso':
                    self::$SRCH_NBR_PERSO = (integer) $value;
                    break;
                case 'prixmin':
                    self::$SRCH_PRIX_MIN = $value;
                    break;
                case 'prixmax':
                    self::$SRCH_PRIX_MAX = $value;
                    break;
                case 'surfacemin':
                    self::$SRCH_SURFACE_MIN = $value;
                    break;
                case 'surfacemax':
                    self::$SRCH_SURFACE_MAX = $value;
                    break;
                case 'adresse':
                    self::$SRCH_GEO_ADRESSE = $value;
                    break;
                case 'radius':
                    self::$SRCH_GEO_RADIUS = $value;
                    break;
                case 'nbrchambre':
                    self::$SRCH_NBR_CHAMBRE = (integer) $value;
                    break;
                case 'nbretoile':
                    self::$SRCH_NBR_ETOILE = (integer) $value;
                    break;
                case 'nbrsdb':
                    self::$SRCH_NBR_SDB = (integer) $value;
                    break;
                // surface et prix here
                case 'typeloge':
                    self::$SRCH_TYPE_LOGE = (string) $value;
                    break;
                case 'piscine':
                    self::$SRCH_PISCINE = (string) $value;
                    break;
                case 'distancemer':
                    self::$SRCH_DISTANCE_MER = (integer) $value;
                    break;
                case 'distancelac':
                    self::$SRCH_DISTANCE_LAC = (integer) $value;
                    break;
                case 'distanceremontee':
                    self::$SRCH_DISTANCE_REMONTEE = (integer) $value;
                    break;
                case 'distancecentreville':
                    self::$SRCH_DISTANCE_CENTRE_VILLE = (integer) $value;
                    break;
                case 'carac':
                    self::$SRCH_CARAC = (array) $value;
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Recuperer an array of available locations
     * @param string $dateArrivee
     * @param string $dateDepart 
     * @return array
     */
    private function GetLocationsDispo($dateArrivee = NULL, $dateDepart = NULL) {
        $idsDipso = array();

        $datetimeArrivee = strtotime($dateArrivee);
        $datetimeDepart = strtotime($dateDepart);
        $dql = "SELECT lc.id AS id, pln.date AS date, pln.dispo AS dispo 
                FROM LefDataBundle:Planning pln 
                JOIN LefDataBundle:Location lc
                WITH pln.idLocation = lc.id 
                WHERE lc.brandPartner != 'leirure' 
                AND pln.date < '" . $dateArrivee . "' ";
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery($dql);
        $resultRaw = $query->getResult();
        if (count($resultRaw) <= 0) {
            return;
        }
//        var_dump($resultRaw);
        foreach ($resultRaw as $result) {
            $date = $result['date'];
            $datetime = strtotime($date->format('Y-m-d'));
            $dispoStr = $result['dispo'];
            $isDispo = $this->GetLocationDispoTreat($datetimeArrivee, $datetimeDepart, $datetime, $dispoStr);
            if ($isDispo) {
                $idsDipso[] = $result['id'];
            }
        }
        $resultRaw = NULL;
        gc_collect_cycles();
        return $idsDipso;
    }

    /**
     * Find out whether a location is available
     * @param datetime $datetimeArrivee
     * @param datetime $datetimeDepart
     * @param datetime $datetime start date in planning
     * @param string $dispoStr
     * @return boolean
     */
    private function GetLocationDispoTreat($datetimeArrivee, $datetimeDepart, $datetime, $dispoStr) {
        $isDispo = TRUE;
        $duree = ($datetimeDepart - $datetimeArrivee) / (60 * 60 * 24);
        $intervalD = ($datetimeArrivee - $datetime) / (60 * 60 * 24);
        $strDaysDispo = substr($dispoStr, $intervalD, $duree);
//        var_dump($interval);
//        var_dump($dispoStr);
//        var_dump($strDaysDispo);
//        die();
        if (is_string(strchr($strDaysDispo, '0')) || is_string(strchr($strDaysDispo, '2'))) {
            $isDispo = FALSE;
        }
        return $isDispo;
    }

    /**
     * Lists  Locations by pays.
     *
     * @Template()
     */
    public function locByPaysAction(Request $request) {
        // Get url get params
        $this->_getUrlParams($request);

        /* SQL string, initiate with id > 0, ready to added other conditions */
        $dqlMore = "WHERE lc.id > 0 AND lc.pays IS NOT NULL AND lc.region IS NOT NULL AND lc.ville IS NOT NULL ";
        /* SQL for range conditions */
        $dqlMoreRange = '';
        /* SQL for disponibilité */
        $dqlMoreDispo = '';
        // Get all post data from request
        $postData = $request->request->all();
        /* Get pays name attribute from post */
        $nomPays = $request->attributes->get('pays');


        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /* Call our serice, retrieve geodata from if exists the DB, otherwise by web service */
        $geocodeController = $this->container->get('lef_fo.geocode');
        $geodata = $geocodeController->getGeocodeGeneral($nomPays);
        /////////////////////////////////////////////
        /////////////////////////////////////////////

        if ($geodata != NULL) {
            /* Get 4 params for border */
            /* Assign them to the statac variable */
            $e = self::$SRCH_RESULTS_BOUNDS['n'] = (double) $geodata->bounds->northeast->lat + self::$SRCH_GEO_RADIUS / 100000;
            $n = self::$SRCH_RESULTS_BOUNDS['e'] = (double) $geodata->bounds->northeast->lng + self::$SRCH_GEO_RADIUS / 100000;
            $w = self::$SRCH_RESULTS_BOUNDS['s'] = (double) $geodata->bounds->southwest->lat - self::$SRCH_GEO_RADIUS / 100000;
            $s = self::$SRCH_RESULTS_BOUNDS['w'] = (double) $geodata->bounds->southwest->lng - self::$SRCH_GEO_RADIUS / 100000;
            /* Get country name */
            $countryName = $geodata->pays;
            /* Get center */
            self::$SRCH_RESULTS_CENTER['lng'] = $geodata->center->lng;
            self::$SRCH_RESULTS_CENTER['lat'] = $geodata->center->lat;
            /* Assign country name to variable */
            $nomPays = $countryName;
            /* Construct sql query string */
            $dqlMore = $dqlMore . "AND lc.latitude > $w AND lc.latitude < $e AND lc.longitude > $s AND lc.longitude < $n ";
            $dqlMore = $dqlMore . "AND lc.pays = '$countryName' ";
        }

        /////////////// SQL MORE ////////////////////
        /////////////////////////////////////////////
        if (self::$SRCH_DATE_ARRIVEE != NULL) {
            if (self::$SRCH_DUREE != NULL) {
                // Chercher location qui correspondent aux conditions
                $locsDipsoArray = $this->GetLocationsDispo(self::$SRCH_DATE_ARRIVEE, self::$SRCH_DUREE);
            } else {
                // Chercher location qui correspondent aux conditions
                $locsDipsoArray = $this->GetLocationsDispo(self::$SRCH_DATE_ARRIVEE);
            }
            if (count($locsDipsoArray) <= 0) {
                $locsDipsoArray = [0];
            }
            $dqlMoreDispo = $dqlMoreDispo . "AND lc.id IN (" . implode('  ', $locsDipsoArray) . ") ";
        }
        if (self::$SRCH_NBR_PERSO != NULL) {
            $dqlMore = $dqlMore . "AND lc.nombreDePersonne = " . self::$SRCH_NBR_PERSO . " ";
        }
        if (self::$SRCH_PRIX_MIN != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.rentalPrice >= " . self::$SRCH_PRIX_MIN . " ";
        }
        if (self::$SRCH_PRIX_MAX != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.rentalPrice <= " . self::$SRCH_PRIX_MAX . " ";
        }
        if (self::$SRCH_SURFACE_MIN != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.surfaceHabitable >= " . self::$SRCH_SURFACE_MIN . " ";
        }
        if (self::$SRCH_SURFACE_MAX != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.surfaceHabitable <= " . self::$SRCH_SURFACE_MAX . " ";
        }
        if (self::$SRCH_TYPE_LOGE != NULL) {
            $dqlMore = $dqlMore . "AND lc.typePartner LIKE '%" . self::$SRCH_TYPE_LOGE . "%' ";
        }
        if (self::$SRCH_NBR_ETOILE != NULL) {
            if (self::$SRCH_NBR_ETOILE == 0) {
                $dqlMore = $dqlMore . "AND lc.nombreEtoiles = null ";
            } else {
                $dqlMore = $dqlMore . "AND lc.nombreEtoiles = " . self::$SRCH_NBR_ETOILE . " ";
            }
        }
        if (self::$SRCH_NBR_CHAMBRE != NULL) {
            $dqlMore = $dqlMore . "AND lc.nombreChambre >= " . self::$SRCH_NBR_CHAMBRE . " ";
        }
        if (self::$SRCH_NBR_SDB != NULL) {
            $dqlMore = $dqlMore . "AND lc.nombreSalleDeBain >= " . self::$SRCH_NBR_SDB . " ";
        }
        if (self::$SRCH_PISCINE != NULL) {
            $dqlMore = $dqlMore . "AND (lc.piscine = 1 OR lc.descriptionBreve LIKE '%piscine%' "
                    . "OR lc.descriptionDetaillee LIKE '%piscine%' "
                    . "OR lc.caracteristiques LIKE '%piscine%') ";
        }
        if (self::$SRCH_DISTANCE_MER != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceMer <= " . self::$SRCH_DISTANCE_MER . " ";
        }
        if (self::$SRCH_DISTANCE_LAC != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceLac <= " . self::$SRCH_DISTANCE_LAC . " ";
        }
        if (self::$SRCH_DISTANCE_REMONTEE != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceRemontees <= " . self::$SRCH_DISTANCE_REMONTEE . " ";
        }
        if (self::$SRCH_DISTANCE_CENTRE_VILLE != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceCentreVille <= " . self::$SRCH_DISTANCE_CENTRE_VILLE . " ";
        }
        if (self::$SRCH_CARAC != NULL && count(self::$SRCH_CARAC) > 0) {
            foreach (self::$SRCH_CARAC as $carac) {
                $dqlMore = $dqlMore . "AND (lc.descriptionBreve LIKE '%" . $carac . "%' "
                        . "OR lc.descriptionDetaillee LIKE '%" . $carac . "%' "
                        . "OR lc.caracteristiques LIKE '%" . $carac . "%' "
                        . "OR lc.themePartner LIKE '%" . $carac . "%') ";
            }
        }
        /////////////////////////////////////////////
        /////////////////////////////////////////////

        $nbrLocPerPage = $this->container->getParameter('max_articles_on_listepage');
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // Initiate entity manager, deal with queries
        $em = $this->getDoctrine()->getManager();
        $this->_increaseMemoryAllocate();
        $dql = "SELECT lc 
                FROM LefDataBundle:Location lc ";
        // Concatener sql-more and range conditions
        $dql = $dql . $dqlMore . $dqlMoreDispo . $dqlMoreRange;
        $dql = $dql . "ORDER BY lc.modificationDate";
        $query = $em->createQuery($dql);

        // ========= range slider =============
        // Sql to get ranges  (min - max) for price and surface
        $dqlRange = "SELECT MIN(lc.rentalPrice) AS prix_min, MAX(lc.rentalPrice) AS prix_max,"
                . "MIN(lc.surfaceHabitable) AS surface_min, MAX(lc.surfaceHabitable) AS surface_max "
                . "FROM LefDataBundle:Location lc ";
        $dqlRange = $dqlRange . $dqlMore;
        $queryRange = $em->createQuery($dqlRange);
        $sliderRangesRaw = $queryRange->getResult();
        $sliderRanges = $sliderRangesRaw[0];
        // ====================================
        // Use KNP paginator
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $this->get('request')->query->get('page', 1)/* page number */, $nbrLocPerPage/* limit per page */
        );

        $nbrTotalLocs = $pagination->getTotalItemCount();

        return $this->render('LefFoBundle:Location:index.html.twig', array(
                    'entities' => $pagination,
                    'sliderRanges' => $sliderRanges,
                    'totalRecord' => $nbrTotalLocs,
                    'pays' => $nomPays,
                    'resultsCenter' => json_encode(self::$SRCH_RESULTS_CENTER),
                    'resultsBounds' => json_encode(self::$SRCH_RESULTS_BOUNDS),
                    'nbrItemCompa' => $nombreItemPanierCompa,
        ));
    }

    /**
     * Lists  Locations by region.
     *
     * @Template()
     */
    public function locByRegionAction(Request $request) {
        // Get url get params
        $this->_getUrlParams($request);

        /* SQL string, initiate with id > 0, ready to added other conditions */
        $dqlMore = "WHERE lc.id > 0 AND lc.pays IS NOT NULL AND lc.region IS NOT NULL AND lc.ville IS NOT NULL ";
        /* SQL for range conditions */
        $dqlMoreRange = '';
        /* SQL for disponibilité */
        $dqlMoreDispo = '';
        // Get all post data from request
        $postData = $request->request->all();
        /* Get pays name attribute from post */
        $nomPays = $request->attributes->get('pays');
        /* Get region name attribute from post */
        $nomRegion = $request->attributes->get('region');


        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /* Call our serice, retrieve geodata from if exists the DB, otherwise by web service */
        $geocodeController = $this->container->get('lef_fo.geocode');
        $geodata = $geocodeController->getGeocodeGeneral($nomPays, $nomRegion);
        /////////////////////////////////////////////
        /////////////////////////////////////////////

        if ($geodata != NULL) {
            /* Get 4 params for border */
            /* Assign them to the statac variable */
            $e = self::$SRCH_RESULTS_BOUNDS['n'] = (double) $geodata->bounds->northeast->lat + self::$SRCH_GEO_RADIUS / 100000;
            $n = self::$SRCH_RESULTS_BOUNDS['e'] = (double) $geodata->bounds->northeast->lng + self::$SRCH_GEO_RADIUS / 100000;
            $w = self::$SRCH_RESULTS_BOUNDS['s'] = (double) $geodata->bounds->southwest->lat - self::$SRCH_GEO_RADIUS / 100000;
            $s = self::$SRCH_RESULTS_BOUNDS['w'] = (double) $geodata->bounds->southwest->lng - self::$SRCH_GEO_RADIUS / 100000;
            /* Get country name */
            $countryName = $geodata->pays;
            /* Get center */
            self::$SRCH_RESULTS_CENTER['lng'] = $geodata->center->lng;
            self::$SRCH_RESULTS_CENTER['lat'] = $geodata->center->lat;
            /* Assign country name to variable */
            $nomPays = $countryName;
            /* Construct sql query string */
            $dqlMore = $dqlMore . "AND lc.latitude > $w AND lc.latitude < $e AND lc.longitude > $s AND lc.longitude < $n ";
            $dqlMore = $dqlMore . "AND lc.pays = '$countryName' ";
        }

        /////////////// SQL MORE ////////////////////
        /////////////////////////////////////////////
        if (self::$SRCH_DATE_ARRIVEE != NULL) {
            if (self::$SRCH_DUREE != NULL) {
                // Chercher location qui correspondent aux conditions
                $locsDipsoArray = $this->GetLocationsDispo(self::$SRCH_DATE_ARRIVEE, self::$SRCH_DUREE);
            } else {
                // Chercher location qui correspondent aux conditions
                $locsDipsoArray = $this->GetLocationsDispo(self::$SRCH_DATE_ARRIVEE);
            }
            if (count($locsDipsoArray) <= 0) {
                $locsDipsoArray = [0];
            }
            $dqlMoreDispo = $dqlMoreDispo . "AND lc.id IN (" . implode('  ', $locsDipsoArray) . ") ";
        }
        if (self::$SRCH_NBR_PERSO != NULL) {
            $dqlMore = $dqlMore . "AND lc.nombreDePersonne = " . self::$SRCH_NBR_PERSO . " ";
        }
        if (self::$SRCH_PRIX_MIN != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.rentalPrice >= " . self::$SRCH_PRIX_MIN . " ";
        }
        if (self::$SRCH_PRIX_MAX != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.rentalPrice <= " . self::$SRCH_PRIX_MAX . " ";
        }
        if (self::$SRCH_SURFACE_MIN != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.surfaceHabitable >= " . self::$SRCH_SURFACE_MIN . " ";
        }
        if (self::$SRCH_SURFACE_MAX != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.surfaceHabitable <= " . self::$SRCH_SURFACE_MAX . " ";
        }
        if (self::$SRCH_TYPE_LOGE != NULL) {
            $dqlMore = $dqlMore . "AND lc.typePartner LIKE '%" . self::$SRCH_TYPE_LOGE . "%' ";
        }
        if (self::$SRCH_NBR_ETOILE != NULL) {
            if (self::$SRCH_NBR_ETOILE == 0) {
                $dqlMore = $dqlMore . "AND lc.nombreEtoiles = null ";
            } else {
                $dqlMore = $dqlMore . "AND lc.nombreEtoiles = " . self::$SRCH_NBR_ETOILE . " ";
            }
        }
        if (self::$SRCH_NBR_CHAMBRE != NULL) {
            $dqlMore = $dqlMore . "AND lc.nombreChambre >= " . self::$SRCH_NBR_CHAMBRE . " ";
        }
        if (self::$SRCH_NBR_SDB != NULL) {
            $dqlMore = $dqlMore . "AND lc.nombreSalleDeBain >= " . self::$SRCH_NBR_SDB . " ";
        }
        if (self::$SRCH_PISCINE != NULL) {
            $dqlMore = $dqlMore . "AND (lc.piscine = 1 OR lc.descriptionBreve LIKE '%piscine%' "
                    . "OR lc.descriptionDetaillee LIKE '%piscine%' "
                    . "OR lc.caracteristiques LIKE '%piscine%') ";
        }
        if (self::$SRCH_DISTANCE_MER != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceMer <= " . self::$SRCH_DISTANCE_MER . " ";
        }
        if (self::$SRCH_DISTANCE_LAC != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceLac <= " . self::$SRCH_DISTANCE_LAC . " ";
        }
        if (self::$SRCH_DISTANCE_REMONTEE != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceRemontees <= " . self::$SRCH_DISTANCE_REMONTEE . " ";
        }
        if (self::$SRCH_DISTANCE_CENTRE_VILLE != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceCentreVille <= " . self::$SRCH_DISTANCE_CENTRE_VILLE . " ";
        }
        if (self::$SRCH_CARAC != NULL && count(self::$SRCH_CARAC) > 0) {
            foreach (self::$SRCH_CARAC as $carac) {
                $dqlMore = $dqlMore . "AND (lc.descriptionBreve LIKE '%" . $carac . "%' "
                        . "OR lc.descriptionDetaillee LIKE '%" . $carac . "%' "
                        . "OR lc.caracteristiques LIKE '%" . $carac . "%' "
                        . "OR lc.themePartner LIKE '%" . $carac . "%') ";
            }
        }
        /////////////////////////////////////////////
        /////////////////////////////////////////////

        $nbrLocPerPage = $this->container->getParameter('max_articles_on_listepage');
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // Initiate entity manager, deal with queries
        $em = $this->getDoctrine()->getManager();
        $this->_increaseMemoryAllocate();
        $dql = "SELECT lc 
                FROM LefDataBundle:Location lc ";
        // Concatener sql-more and range conditions
        $dql = $dql . $dqlMore . $dqlMoreDispo . $dqlMoreRange;
        $dql = $dql . "ORDER BY lc.modificationDate";
        $query = $em->createQuery($dql);

        // ========= range slider =============
        // Sql to get ranges  (min - max) for price and surface
        $dqlRange = "SELECT MIN(lc.rentalPrice) AS prix_min, MAX(lc.rentalPrice) AS prix_max,"
                . "MIN(lc.surfaceHabitable) AS surface_min, MAX(lc.surfaceHabitable) AS surface_max "
                . "FROM LefDataBundle:Location lc ";
        $dqlRange = $dqlRange . $dqlMore;
        $queryRange = $em->createQuery($dqlRange);
        $sliderRangesRaw = $queryRange->getResult();
        $sliderRanges = $sliderRangesRaw[0];
        // ====================================
        // Use KNP paginator
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $this->get('request')->query->get('page', 1)/* page number */, $nbrLocPerPage/* limit per page */
        );

        $nbrTotalLocs = $pagination->getTotalItemCount();

        return $this->render('LefFoBundle:Location:index.html.twig', array(
                    'entities' => $pagination,
                    'sliderRanges' => $sliderRanges,
                    'totalRecord' => $nbrTotalLocs,
                    'pays' => $nomPays,
                    'region' => $nomRegion,
                    'resultsCenter' => json_encode(self::$SRCH_RESULTS_CENTER),
                    'resultsBounds' => json_encode(self::$SRCH_RESULTS_BOUNDS),
                    'nbrItemCompa' => $nombreItemPanierCompa,
        ));
    }

    /**
     * Lists  Locations by ville.
     *
     * @Template()
     */
    public function locByVilleAction(Request $request) {
        // Get url get params
        $this->_getUrlParams($request);

        /* SQL string, initiate with id > 0, ready to added other conditions */
        $dqlMore = "WHERE lc.id > 0 AND lc.pays IS NOT NULL AND lc.region IS NOT NULL AND lc.ville IS NOT NULL ";
        /* SQL for range conditions */
        $dqlMoreRange = '';
        /* SQL for disponibilité */
        $dqlMoreDispo = '';
        // Get all post data from request
        $postData = $request->request->all();
        /* Get pays name attribute from post */
        $nomPays = $request->attributes->get('pays');
        /* Get region name attribute from post */
        $nomRegion = $request->attributes->get('region');
        /* Get ville name attribute from post */
        $nomVille = $request->attributes->get('ville');


        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /* Call our serice, retrieve geodata from if exists the DB, otherwise by web service */
        $geocodeController = $this->container->get('lef_fo.geocode');
        $geodata = $geocodeController->getGeocodeGeneral($nomPays, $nomRegion, $nomVille);
        /////////////////////////////////////////////
        /////////////////////////////////////////////

        if ($geodata != NULL) {
            /* Get 4 params for border */
            /* Assign them to the statac variable */
            $e = self::$SRCH_RESULTS_BOUNDS['n'] = (double) $geodata->bounds->northeast->lat + self::$SRCH_GEO_RADIUS / 100000;
            $n = self::$SRCH_RESULTS_BOUNDS['e'] = (double) $geodata->bounds->northeast->lng + self::$SRCH_GEO_RADIUS / 100000;
            $w = self::$SRCH_RESULTS_BOUNDS['s'] = (double) $geodata->bounds->southwest->lat - self::$SRCH_GEO_RADIUS / 100000;
            $s = self::$SRCH_RESULTS_BOUNDS['w'] = (double) $geodata->bounds->southwest->lng - self::$SRCH_GEO_RADIUS / 100000;
            /* Get country name */
            $countryName = $geodata->pays;
            /* Get center */
            self::$SRCH_RESULTS_CENTER['lng'] = $geodata->center->lng;
            self::$SRCH_RESULTS_CENTER['lat'] = $geodata->center->lat;
            /* Assign country name to variable */
            $nomPays = $countryName;
            /* Construct sql query string */
            $dqlMore = $dqlMore . "AND lc.latitude > $w AND lc.latitude < $e AND lc.longitude > $s AND lc.longitude < $n ";
            $dqlMore = $dqlMore . "AND lc.pays = '$countryName' ";
        }

        /////////////// SQL MORE ////////////////////
        /////////////////////////////////////////////
        if (self::$SRCH_DATE_ARRIVEE != NULL) {
            if (self::$SRCH_DUREE != NULL) {
                // Chercher location qui correspondent aux conditions
                $locsDipsoArray = $this->GetLocationsDispo(self::$SRCH_DATE_ARRIVEE, self::$SRCH_DUREE);
            } else {
                // Chercher location qui correspondent aux conditions
                $locsDipsoArray = $this->GetLocationsDispo(self::$SRCH_DATE_ARRIVEE);
            }
            if (count($locsDipsoArray) <= 0) {
                $locsDipsoArray = [0];
            }
            $dqlMoreDispo = $dqlMoreDispo . "AND lc.id IN (" . implode('  ', $locsDipsoArray) . ") ";
        }
        if (self::$SRCH_NBR_PERSO != NULL) {
            $dqlMore = $dqlMore . "AND lc.nombreDePersonne = " . self::$SRCH_NBR_PERSO . " ";
        }
        if (self::$SRCH_PRIX_MIN != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.rentalPrice >= " . self::$SRCH_PRIX_MIN . " ";
        }
        if (self::$SRCH_PRIX_MAX != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.rentalPrice <= " . self::$SRCH_PRIX_MAX . " ";
        }
        if (self::$SRCH_SURFACE_MIN != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.surfaceHabitable >= " . self::$SRCH_SURFACE_MIN . " ";
        }
        if (self::$SRCH_SURFACE_MAX != NULL) {
            $dqlMoreRange = $dqlMoreRange . "AND lc.surfaceHabitable <= " . self::$SRCH_SURFACE_MAX . " ";
        }
        if (self::$SRCH_TYPE_LOGE != NULL) {
            $dqlMore = $dqlMore . "AND lc.typePartner LIKE '%" . self::$SRCH_TYPE_LOGE . "%' ";
        }
        if (self::$SRCH_NBR_ETOILE != NULL) {
            if (self::$SRCH_NBR_ETOILE == 0) {
                $dqlMore = $dqlMore . "AND lc.nombreEtoiles = null ";
            } else {
                $dqlMore = $dqlMore . "AND lc.nombreEtoiles = " . self::$SRCH_NBR_ETOILE . " ";
            }
        }
        if (self::$SRCH_NBR_CHAMBRE != NULL) {
            $dqlMore = $dqlMore . "AND lc.nombreChambre >= " . self::$SRCH_NBR_CHAMBRE . " ";
        }
        if (self::$SRCH_NBR_SDB != NULL) {
            $dqlMore = $dqlMore . "AND lc.nombreSalleDeBain >= " . self::$SRCH_NBR_SDB . " ";
        }
        if (self::$SRCH_PISCINE != NULL) {
            $dqlMore = $dqlMore . "AND (lc.piscine = 1 OR lc.descriptionBreve LIKE '%piscine%' "
                    . "OR lc.descriptionDetaillee LIKE '%piscine%' "
                    . "OR lc.caracteristiques LIKE '%piscine%') ";
        }
        if (self::$SRCH_DISTANCE_MER != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceMer <= " . self::$SRCH_DISTANCE_MER . " ";
        }
        if (self::$SRCH_DISTANCE_LAC != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceLac <= " . self::$SRCH_DISTANCE_LAC . " ";
        }
        if (self::$SRCH_DISTANCE_REMONTEE != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceRemontees <= " . self::$SRCH_DISTANCE_REMONTEE . " ";
        }
        if (self::$SRCH_DISTANCE_CENTRE_VILLE != NULL) {
            $dqlMore = $dqlMore . "AND lc.distanceCentreVille <= " . self::$SRCH_DISTANCE_CENTRE_VILLE . " ";
        }
        if (self::$SRCH_CARAC != NULL && count(self::$SRCH_CARAC) > 0) {
            foreach (self::$SRCH_CARAC as $carac) {
                $dqlMore = $dqlMore . "AND (lc.descriptionBreve LIKE '%" . $carac . "%' "
                        . "OR lc.descriptionDetaillee LIKE '%" . $carac . "%' "
                        . "OR lc.caracteristiques LIKE '%" . $carac . "%' "
                        . "OR lc.themePartner LIKE '%" . $carac . "%') ";
            }
        }
        /////////////////////////////////////////////
        /////////////////////////////////////////////

        $nbrLocPerPage = $this->container->getParameter('max_articles_on_listepage');
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // Initiate entity manager, deal with queries
        $em = $this->getDoctrine()->getManager();
        $this->_increaseMemoryAllocate();
        $dql = "SELECT lc 
                FROM LefDataBundle:Location lc ";
        // Concatener sql-more and range conditions
        $dql = $dql . $dqlMore . $dqlMoreDispo . $dqlMoreRange;
        $dql = $dql . "ORDER BY lc.modificationDate";
        $query = $em->createQuery($dql);

        // ========= range slider =============
        // Sql to get ranges  (min - max) for price and surface
        $dqlRange = "SELECT MIN(lc.rentalPrice) AS prix_min, MAX(lc.rentalPrice) AS prix_max,"
                . "MIN(lc.surfaceHabitable) AS surface_min, MAX(lc.surfaceHabitable) AS surface_max "
                . "FROM LefDataBundle:Location lc ";
        $dqlRange = $dqlRange . $dqlMore;
        $queryRange = $em->createQuery($dqlRange);
        $sliderRangesRaw = $queryRange->getResult();
        $sliderRanges = $sliderRangesRaw[0];
        // ====================================
        // Use KNP paginator
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $this->get('request')->query->get('page', 1)/* page number */, $nbrLocPerPage/* limit per page */
        );

        $nbrTotalLocs = $pagination->getTotalItemCount();

        return $this->render('LefFoBundle:Location:index.html.twig', array(
                    'entities' => $pagination,
                    'sliderRanges' => $sliderRanges,
                    'totalRecord' => $nbrTotalLocs,
                    'pays' => $nomPays,
                    'region' => $nomRegion,
                    'ville' => $nomVille,
                    'resultsCenter' => json_encode(self::$SRCH_RESULTS_CENTER),
                    'resultsBounds' => json_encode(self::$SRCH_RESULTS_BOUNDS),
                    'nbrItemCompa' => $nombreItemPanierCompa,
        ));
    }

    /**
     * Finds and displays a Location entity.
     *
     * @Template()
     */
    public function showAction($reference, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Location')->findOneByReference($reference);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Location entity.');
        }

        $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findMedaiByRef($entity->getReference());

        $arrivee = $request->query->get('datearrivee');
        if (isset($arrivee)) {
            $dateArrivee = $arrivee;
        } else {
            $dateArrivee = date('Y-m-d');
        }

        $depart = $request->query->get('datedepart');
        if (isset($depart)) {
            $dateDepart = $depart;
        } else {
            $dateDepart = date('Y-m-d', strtotime($dateArrivee . "+7 days"));
        }

        //////////////////////////// A 110814 Jian ///////////////////////////////
        $canCheckAvailability = FALSE;                                          //
        $availability = "";                                                     //
        $isPartner = FALSE;                                                     //
        //////////////////////////////////////////////////////////////////////////
        switch ($entity->getBrandPartner()) {
            case 'interchalet':
//                $webService = new InterchaletWebServiceController();
                $webService = $this->container->get('lef_data.web_service.interchalet');
                $canCheckAvailability = false;
                $availability = "";
                ////////////// A 110814 Jian /////////////
                $isPartner = TRUE;                      //
                //////////////////////////////////////////
                break;

            case 'interhome':
//                $webService = new InterhomeWebServiceController();
                $webService = $this->container->get('lef_data.web_service.interhome');
                $checkAvailability = $webService->availability($entity->getReference(), $dateArrivee, $dateDepart);
                $canCheckAvailability = true;
                if ($checkAvailability->Ok) {
                    $availability = substr($checkAvailability->State, -1);
                    $availability = str_replace(array('N', 'Y', 'Q'), array('Occupée', 'Disponible', 'Sur demande'), $availability);
                }
                ////////////// A 110814 Jian /////////////
                $isPartner = TRUE;                      //
                //////////////////////////////////////////
                break;

            case 'leisure':
//                $webService = new LeisureWebServiceController();
                $webService = $this->container->get('lef_data.web_service.leisure');
                $checkAvailability = $webService->checkAvailability($entity->getReference(), $dateArrivee, $dateDepart, 168);
                $canCheckAvailability = true;
                $availability = "";
                ////////////// A 110814 Jian /////////////
                $isPartner = TRUE;                      //
                //////////////////////////////////////////
                break;

            default:
                ////////////// A 110814 Jian /////////////
                $isPartner = FALSE;                     //
                //////////////////////////////////////////
                break;
        }
        shuffle($mediasP);
        // $entity->medias_p = $mediasP;
        $entity->medias_p = array_slice($mediasP, 0, 10);
        $currentUrl = $this->getRequest()->getUri();

        if (!$isPartner) {
            
        }

        return array(
            'entity' => $entity,
            'annonceur' => $entity->getAnnonceur(),
            'dateArrivee' => $dateArrivee,
            'dateDepart' => $dateDepart,
            'canCheckAvailability' => $canCheckAvailability,
            'availability' => $availability,
            ////////////// A 110814 Jian /////////////
            'isPartner' => $isPartner, //
            //////////////////////////////////////////
            'partnerBrand' => $entity->getBrandPartner(),
        );
    }

    ////////////////////////////////////////////////////////////<<<<<<<<<<<<<<<<<<
    //
    //////////////////////////// A 130814 par Jian ////////////////>>>>>>>>>>>>>>>
    /**
     * Lists favoris entities.
     *
     * @Template()
     */
    public function favorisAction($page) {
        $page = str_replace("page:", "", $page);
        $references = array();
        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request');
        $cookies = $request->cookies;

        if ($cookies->has('wish_list')) {
            $listStr = $cookies->get('wish_list');
            $references = strpos($listStr, ',') > -1 ? explode(',', $listStr) : array($listStr);
        }

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getFavoris($page, $references, null, null);
        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');

        $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getFavoris($page, $references, $articles_per_page, $max_articles_on_listepage);


        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findMedaiByRef($entity->getReference());
            shuffle($mediasP);
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $nbAnnonce = count($entities1);
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        return array(
            'entities' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $nbAnnonce,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
        );
    }

    /**
     * Pagination pour favoris
     * @Template()
     */
    public function __paginationFavorisAction($page) {
        $page = str_replace("page:", "", $page);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
        $references = array();

        $request = $this->get('request');
        $cookies = $request->cookies;

        if ($cookies->has('wish_list')) {
            $listStr = $cookies->get('wish_list');
            $references = strpos($listStr, ',') > -1 ? explode(',', $listStr) : array($listStr);
        }

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getFavoris($page, $references, null, null);
        /* total of résultat */
        $total_articles = count($total);
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;


        return array(
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
        );
    }

    ////////////////////////////////////////////////////////////<<<<<<<<<<<<<<<<<<
    //
    //
    // ===========================
    // ==== Private functions ====
    // ===========================
    /**
     * function get current connected user
     * @return object
     */
    private function _getConnectedUser() {
        if (!$this->get('security.context')->getToken()->getUser()) {
            throw $this->createNotFoundException('Utilisateur non connecté.');
        }
        return $this->get('security.context')->getToken()->getUser();
    }

    /**
     * Create flash message
     * @param string $type (notice, success...)
     * @param text $contents
     */
    private function _createFlashMessage($type, $contents) {
        $this->get('session')->getFlashBag()->add(
                $type, $contents
        );
    }

    /**
     * Function get all locations who have attribute brandpartner not null
     * A le 170914 par Jian
     * @return array
     */
    private function _getAllLocationsPartner() {
        $em = $this->getDoctrine()->getManager();
        // Limit request to 20
        $locations = $em->getRepository('LefDataBundle:Location')->getAllLocPartner(20);
        return $locations;
    }

    private function _increaseMemoryAllocate() {
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
    }

}
