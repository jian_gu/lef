<?php

namespace Lef\FoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Session\Session;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Form\LocationType;
use Symfony\Component\Locale\Locale;
use Lef\DataBundle\Controller\InterhomeWebServiceController;
use Lef\DataBundle\Controller\InterchaletWebServiceController;
use Lef\DataBundle\Controller\LeisureWebServiceController;
use Lef\FoBundle\Controller\GeocodeController;

/**
 * Location controller.
 *
 * @Route("/location")
 */
class LocationController extends Controller {

    // Radius in meter, radius_coded = radius / 1000000
    protected static $SRCH_GEO_RADIUS;
    protected static $SRCH_DUREE;
    protected static $SRCH_DATE_ARRIVEE;
    protected static $SRCH_NBR_PERSO;

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function indexAction(Request $request) {
        $session = $this->get('session');
        /* If geodata is already set in session */
        if ($session->has('geodata')) {
            $session->remove('geodata');
        }
        /* SQL string, initiate with id > 0, ready to added other conditions */
        $dqlMore = "WHERE lc.id > 0 ";
        // Get all post data from request
        $postData = $request->request->all();
        // If Geodata exists in post params
        if (array_key_exists('geodata', $postData)) {
            $geoDataStr = $postData['geodata'];

            $geoData = json_decode($geoDataStr);

            /* Get geodata Type if it exists */
            if (property_exists($geoData, 'type')) {
                $getoType = $geoData->type;
            }
            /* If type in geodata is defined in level pays-region-ville */
            if (in_array($getoType, ['pays', 'region', 'ville']) || $getoType == NULL) {
//                $session = $this->get('session');
                $session->set('geodata', $geoData);
                if ($getoType != NULL) {
                    $geocodeController = $this->container->get('lef_fo.geocode');
                    $geocodeController->saveGeocode($geoData);
                }
            }
        }

        if (isset($session) && $session->has('geodata')) {
            $geoDataInSession = $session->get('geodata');
            /* Get 4 params for border */
            $e = (double) $geoDataInSession->bounds_r->northeast->lat;
            $n = (double) $geoDataInSession->bounds_r->northeast->lng;
            $w = (double) $geoDataInSession->bounds_r->southwest->lat;
            $s = (double) $geoDataInSession->bounds_r->southwest->lng;
            /* Get country name */
            $countryName = $geoDataInSession->pays;
            /* Construct sql query string */
            $dqlMore = $dqlMore . "AND lc.latitude > $w AND lc.latitude < $e AND lc.longitude > $s AND lc.longitude < $n ";
            $dqlMore = $dqlMore . "AND lc.pays = '$countryName' ";
        }

//        echo('<pre>');
//        var_dump($geoData->type);
//        echo('</pre>');
//        die();

        $nbrLocPerPage = $this->container->getParameter('max_articles_on_listepage');
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // Initiate entity manager, deal with queries
        $em = $this->getDoctrine()->getManager();
        $this->_increaseMemoryAllocate();
        $dql = "SELECT lc 
                FROM LefDataBundle:Location lc ";
        $dql = $dql . $dqlMore;
        $dql = $dql . "ORDER BY lc.modificationDate";
        $query = $em->createQuery($dql);

        // Use KNP paginator
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $this->get('request')->query->get('page', 1)/* page number */, $nbrLocPerPage/* limit per page */
        );

        $nbrTotalLocs = $pagination->getTotalItemCount();

        return array(
            'entities' => $pagination,
            'totalRecord' => $nbrTotalLocs,
            'nbrItemCompa' => $nombreItemPanierCompa,
        );
    }

    private function _getUrlParams(Request $request) {
        // Usable params
        $valideParams = array();
        // Retrieve get data
        $getData = $request->query->all();
        // Loop get data, and re-construct params which are not blank
        foreach ($getData as $key => $value) {
            // re-construct params which are not blank
            if (trim($value) != '') {
                $valideParams[$key] = $value;
            }
        }
        // Loop re-constructed params, and assign them to static variable
        foreach ($valideParams as $key => $value) {
            switch ($key) {
                case 'radius':
                    self::$SRCH_GEO_RADIUS = $value;
                    break;
                case 'datearrivee':
                    self::$SRCH_DATE_ARRIVEE = $value;
                    break;
                case 'duree':
                    self::$SRCH_DUREE = $value;
                    break;
                case 'nbrperso':
                    self::$SRCH_NBR_PERSO = $value;
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Lists  Locations by pays.
     *
     * @Template()
     */
    public function locByPaysAction(Request $request) {
        $this->_getUrlParams($request);
//        $nomPays = NULL;
//        $session = $this->get('session');
//        /* If geodata is already set in session */
//        if ($session->has('geodata')) {
//            $session->remove('geodata');
//        }
        /* SQL string, initiate with id > 0, ready to added other conditions */
        $dqlMore = "WHERE lc.id > 0 ";
        // Get all post data from request
        $postData = $request->request->all();
        /* Get pays name attribute from post */
        $nomPays = $request->attributes->get('pays');


        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /* Get geodata by web service */
        $geocodeController = $this->container->get('lef_fo.geocode');
        $geodata = $geocodeController->getGeocodeGeneral($nomPays);
        /////////////////////////////////////////////
        /////////////////////////////////////////////
//        // If Geodata exists in post params
//        if (array_key_exists('geodata', $postData)) {
//            $geoDataStr = $postData['geodata'];
//
//            $geoData = json_decode($geoDataStr);
//
//            /* Get geodata Type if it exists */
//            if (property_exists($geoData, 'type')) {
//                $getoType = $geoData->type;
//            }
//            /* If type in geodata is defined in level pays-region-ville */
//            if (in_array($getoType, ['pays', 'region', 'ville']) || $getoType == NULL) {
////                $session = $this->get('session');
//                $session->set('geodata', $geoData);
//                if ($getoType != NULL) {
//                    /////////////////////////////////////////////
//                    /////////////////////////////////////////////
//                    $geocodeController = $this->container->get('lef_fo.geocode');
//                    $geocodeController->saveGeocode($geoData);
//                    /////////////////////////////////////////////
//                    /////////////////////////////////////////////
//                }
//            }
//        }

        if ($geodata != NULL) {
            /* Get 4 params for border */
            $e = (double) $geodata->bounds->northeast->lat + self::$SRCH_GEO_RADIUS/1000000;
            $n = (double) $geodata->bounds->northeast->lng + self::$SRCH_GEO_RADIUS/1000000;
            $w = (double) $geodata->bounds->southwest->lat - self::$SRCH_GEO_RADIUS/1000000;
            $s = (double) $geodata->bounds->southwest->lng - self::$SRCH_GEO_RADIUS/1000000;
            /* Get country name */
            $countryName = $geodata->pays;
            /* Assign country name to variable */
            $nomPays = $countryName;
            /* Construct sql query string */
            $dqlMore = $dqlMore . "AND lc.latitude > $w AND lc.latitude < $e AND lc.longitude > $s AND lc.longitude < $n ";
            $dqlMore = $dqlMore . "AND lc.pays = '$countryName' ";
        }

        $nbrLocPerPage = $this->container->getParameter('max_articles_on_listepage');
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // Initiate entity manager, deal with queries
        $em = $this->getDoctrine()->getManager();
        $this->_increaseMemoryAllocate();
        $dql = "SELECT lc 
                FROM LefDataBundle:Location lc ";
        $dql = $dql . $dqlMore;
        $dql = $dql . "ORDER BY lc.modificationDate";
        $query = $em->createQuery($dql);

        // Use KNP paginator
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $this->get('request')->query->get('page', 1)/* page number */, $nbrLocPerPage/* limit per page */
        );

        $nbrTotalLocs = $pagination->getTotalItemCount();

        return $this->render('LefFoBundle:Location:index.html.twig', array(
                    'entities' => $pagination,
                    'totalRecord' => $nbrTotalLocs,
                    'pays' => $nomPays,
                    'nbrItemCompa' => $nombreItemPanierCompa,
        ));
    }

    /**
     * Lists  Locations by region.
     *
     * @Template()
     */
    public function locByRegionAction(Request $request) {
        $nomPays = NULL;
        $nomRegion = NULL;
        $session = $this->get('session');
        /* If geodata is already set in session */
        if ($session->has('geodata')) {
            $session->remove('geodata');
        }
        /* SQL string, initiate with id > 0, ready to added other conditions */
        $dqlMore = "WHERE lc.id > 0 ";
        // Get all post data from request
        $postData = $request->request->all();
        // If Geodata exists in post params
        if (array_key_exists('geodata', $postData)) {
            $geoDataStr = $postData['geodata'];

            $geoData = json_decode($geoDataStr);

            /* Get geodata Type if it exists */
            if (property_exists($geoData, 'type')) {
                $getoType = $geoData->type;
            }
            /* If type in geodata is defined in level pays-region-ville */
            if (in_array($getoType, ['pays', 'region', 'ville']) || $getoType == NULL) {
//                $session = $this->get('session');
                $session->set('geodata', $geoData);
                if ($getoType != NULL) {
                    $geocodeController = $this->container->get('lef_fo.geocode');
                    $geocodeController->saveGeocode($geoData);
                }
            }
        }

        if (isset($session) && $session->has('geodata')) {
            $geoDataInSession = $session->get('geodata');
            /* Get 4 params for border */
            $e = (double) $geoDataInSession->bounds_r->northeast->lat;
            $n = (double) $geoDataInSession->bounds_r->northeast->lng;
            $w = (double) $geoDataInSession->bounds_r->southwest->lat;
            $s = (double) $geoDataInSession->bounds_r->southwest->lng;
            /* Get country name */
            $countryName = $geoDataInSession->pays;
            /* Get region name */
            $regionName = $geoDataInSession->region;
            /* Assign country name to variable */
            $nomPays = $countryName;
            /* Assign region name to variable */
            $nomRegion = $regionName;
            /* Construct sql query string */
            $dqlMore = $dqlMore . "AND lc.latitude > $w AND lc.latitude < $e AND lc.longitude > $s AND lc.longitude < $n ";
            $dqlMore = $dqlMore . "AND lc.pays = '$countryName' ";
        }

        $nbrLocPerPage = $this->container->getParameter('max_articles_on_listepage');
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // Initiate entity manager, deal with queries
        $em = $this->getDoctrine()->getManager();
        $this->_increaseMemoryAllocate();
        $dql = "SELECT lc 
                FROM LefDataBundle:Location lc ";
        $dql = $dql . $dqlMore;
        $dql = $dql . "ORDER BY lc.modificationDate";
        $query = $em->createQuery($dql);

        // Use KNP paginator
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $this->get('request')->query->get('page', 1)/* page number */, $nbrLocPerPage/* limit per page */
        );

        $nbrTotalLocs = $pagination->getTotalItemCount();

        return $this->render('LefFoBundle:Location:index.html.twig', array(
                    'entities' => $pagination,
                    'totalRecord' => $nbrTotalLocs,
                    'pays' => $nomPays,
                    'region' => $nomRegion,
                    'nbrItemCompa' => $nombreItemPanierCompa,
        ));
    }

    /**
     * Lists  Locations by ville.
     *
     * @Template()
     */
    public function locByVilleAction(Request $request) {
        $nomPays = NULL;
        $nomRegion = NULL;
        $nomVille = NULL;
        $session = $this->get('session');
        /* If geodata is already set in session */
        if ($session->has('geodata')) {
            $session->remove('geodata');
        }
        /* SQL string, initiate with id > 0, ready to added other conditions */
        $dqlMore = "WHERE lc.id > 0 ";
        // Get all post data from request
        $postData = $request->request->all();
        // If Geodata exists in post params
        if (array_key_exists('geodata', $postData)) {
            $geoDataStr = $postData['geodata'];

            $geoData = json_decode($geoDataStr);

            /* Get geodata Type if it exists */
            if (property_exists($geoData, 'type')) {
                $getoType = $geoData->type;
            }
            /* If type in geodata is defined in level pays-region-ville */
            if (in_array($getoType, ['pays', 'region', 'ville']) || $getoType == NULL) {
//                $session = $this->get('session');
                $session->set('geodata', $geoData);
                if ($getoType != NULL) {
                    $geocodeController = $this->container->get('lef_fo.geocode');
                    $geocodeController->saveGeocode($geoData);
                }
            }
        }

        if (isset($session) && $session->has('geodata')) {
            $geoDataInSession = $session->get('geodata');
            /* Get 4 params for border */
            $e = (double) $geoDataInSession->bounds_r->northeast->lat;
            $n = (double) $geoDataInSession->bounds_r->northeast->lng;
            $w = (double) $geoDataInSession->bounds_r->southwest->lat;
            $s = (double) $geoDataInSession->bounds_r->southwest->lng;
            /* Get country name */
            $countryName = $geoDataInSession->pays;
            /* Get region name */
            $regionName = $geoDataInSession->region;
            /* Get ville name */
            $villeName = $geoDataInSession->ville;
            /* Assign country name to variable */
            $nomPays = $countryName;
            /* Assign region name to variable */
            $nomRegion = $regionName;
            /* Assign ville name to variable */
            $nomVille = $villeName;
            /* Construct sql query string */
            $dqlMore = $dqlMore . "AND lc.latitude > $w AND lc.latitude < $e AND lc.longitude > $s AND lc.longitude < $n ";
            $dqlMore = $dqlMore . "AND lc.pays = '$countryName' ";
        }

        $nbrLocPerPage = $this->container->getParameter('max_articles_on_listepage');
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // Initiate entity manager, deal with queries
        $em = $this->getDoctrine()->getManager();
        $this->_increaseMemoryAllocate();
        $dql = "SELECT lc 
                FROM LefDataBundle:Location lc ";
        $dql = $dql . $dqlMore;
        $dql = $dql . "ORDER BY lc.modificationDate";
        $query = $em->createQuery($dql);

        // Use KNP paginator
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $this->get('request')->query->get('page', 1)/* page number */, $nbrLocPerPage/* limit per page */
        );

        $nbrTotalLocs = $pagination->getTotalItemCount();

        return $this->render('LefFoBundle:Location:index.html.twig', array(
                    'entities' => $pagination,
                    'totalRecord' => $nbrTotalLocs,
                    'pays' => $nomPays,
                    'region' => $nomRegion,
                    'ville' => $nomVille,
                    'nbrItemCompa' => $nombreItemPanierCompa,
        ));
    }

    /**
     * Finds and displays a Location entity.
     *
     * @Template()
     */
    public function showAction($reference, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Location')->findOneByReference($reference);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Location entity.');
        }

        $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findMedaiByRef($entity->getReference());

        $arrivee = $request->query->get('arrivee');
        if (isset($arrivee)) {
            $dateArrivee = $arrivee;
        } else {
            $dateArrivee = date('Y-m-d');
        }

        $depart = $request->query->get('depart');
        if (isset($depart)) {
            $dateDepart = $depart;
        } else {
            $dateDepart = date('Y-m-d', strtotime("+7 days"));
        }

        //////////////////////////// A 110814 Jian ///////////////////////////////
        $canCheckAvailability = FALSE;                                          //
        $availability = "";                                                     //
        $isPartner = FALSE;                                                     //
        //////////////////////////////////////////////////////////////////////////
        switch ($entity->getBrandPartner()) {
            case 'interhome':
//                $webService = new InterhomeWebServiceController();
                $webService = $this->container->get('lef_data.web_service.interhome');
                $checkAvailability = $webService->availability($entity->getReference(), $dateArrivee, $dateDepart);
                $canCheckAvailability = true;
                if ($checkAvailability->Ok) {
                    $availability = substr($checkAvailability->State, -1);
                    $availability = str_replace(array('N', 'Y', 'Q'), array('Occupée', 'Disponible', 'Sur demande'), $availability);
                }
                ////////////// A 110814 Jian /////////////
                $isPartner = TRUE;                      //
                //////////////////////////////////////////
                break;

            case 'interchalet':
//                $webService = new InterchaletWebServiceController();
                $webService = $this->container->get('lef_data.web_service.interchalet');
                $canCheckAvailability = false;
                $availability = "";
                ////////////// A 110814 Jian /////////////
                $isPartner = TRUE;                      //
                //////////////////////////////////////////
                break;

            case 'leisure':
//                $webService = new LeisureWebServiceController();
                $webService = $this->container->get('lef_data.web_service.leisure');
                $checkAvailability = $webService->checkAvailability($code, $dateArrivee, $dateDepart, 168);
                $canCheckAvailability = true;
                $availability = "";
                ////////////// A 110814 Jian /////////////
                $isPartner = TRUE;                      //
                //////////////////////////////////////////
                break;

            default:
                ////////////// A 110814 Jian /////////////
                $isPartner = FALSE;                     //
                //////////////////////////////////////////
                break;
        }
        shuffle($mediasP);
        // $entity->medias_p = $mediasP;
        $entity->medias_p = array_slice($mediasP, 0, 10);
        $currentUrl = $this->getRequest()->getUri();

        if (!$isPartner) {
            
        }

        return array(
            'entity' => $entity,
            'annonceur' => $entity->getAnnonceur(),
            'dateArrivee' => $dateArrivee,
            'dateDepart' => $dateDepart,
            'canCheckAvailability' => $canCheckAvailability,
            'availability' => $availability,
            ////////////// A 110814 Jian /////////////
            'isPartner' => $isPartner, //
            //////////////////////////////////////////
            'partnerBrand' => $entity->getBrandPartner(),
        );
    }

    //////////////////////////// A 110814 Jian ////////////////////>>>>>>>>>>>>>>>
    /**
     * display annonceur details
     * @param type $reference
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return type
     * @Template()
     */
    public function showDetailsAction($reference, Request $request) {
        // ==============================
        // If user is not connected
        // ==============================
        if ($this->_getConnectedUser() == 'anon.') {
            // set flash message
            $this->_createFlashMessage
                    ('info', 'Connectez-vous pour consulter des détails d\'une annonce.');
            // get current path
            $current_path = $this->container->get('request')->getPathInfo();
            // set current path into session
            $this->container->get('session')->set('refererPath', $current_path);
            // return to login page
            //            return $this->redirect(
            //                            $this->generateUrl('sonata_user_security_login')
            //            );

            return array(
                'requireConnection' => TRUE,
            );
        }
        // ==============================
        // If user is connected
        // ==============================
        $em = $this->getDoctrine()->getManager();

        $location = $em->getRepository('LefDataBundle:Location')->findOneByReference($reference);
        $annonceur = $location->getAnnonceur();
        $statVu = new \Lef\DataBundle\Entity\StatVusLocation();

        // ==============================
        // Record visit: ignore meme visiteur - meme location pendant 30 Minutes
        // ==============================
        $visited = $em->getRepository('LefDataBundle:StatVusLocation')->isConsultedIn30Min($this->_getConnectedUser(), $location);
        if (!$visited) {
            $statVu->setVisiteur($this->_getConnectedUser());
            $statVu->setLocation($location);
            $statVu->setDate(new \DateTime());

            $em->persist($statVu);
            $em->flush();
        }

        return array(
            'requireConnection' => FALSE,
            'annonceur' => $annonceur, // Attention, faut mieux que ne pas metter tous les champs
        );
    }

    ////////////////////////////////////////////////////////////<<<<<<<<<<<<<<<<<<
    //
    //////////////////////////// A 130814 par Jian ////////////////>>>>>>>>>>>>>>>
    /**
     * Lists favoris entities.
     *
     * @Template()
     */
    public function favorisAction($page) {
        $page = str_replace("page:", "", $page);
        $references = array();
        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request');
        $cookies = $request->cookies;

        if ($cookies->has('wish_list')) {
            $listStr = $cookies->get('wish_list');
            $references = strpos($listStr, ',') > -1 ? explode(',', $listStr) : array($listStr);
        }

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getFavoris($page, $references, null, null);
        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');

        $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getFavoris($page, $references, $articles_per_page, $max_articles_on_listepage);


        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findMedaiByRef($entity->getReference());
            shuffle($mediasP);
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $nbAnnonce = count($entities1);
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        return array(
            'entities' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $nbAnnonce,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
        );
    }

    /**
     * Pagination pour favoris
     * @Template()
     */
    public function __paginationFavorisAction($page) {
        $page = str_replace("page:", "", $page);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
        $references = array();

        $request = $this->get('request');
        $cookies = $request->cookies;

        if ($cookies->has('wish_list')) {
            $listStr = $cookies->get('wish_list');
            $references = strpos($listStr, ',') > -1 ? explode(',', $listStr) : array($listStr);
        }

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getFavoris($page, $references, null, null);
        /* total of résultat */
        $total_articles = count($total);
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;


        return array(
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
        );
    }

    ////////////////////////////////////////////////////////////<<<<<<<<<<<<<<<<<<
    //
    //
    // ===========================
    // ==== Private functions ====
    // ===========================
    /**
     * function get current connected user
     * @return object
     */
    private function _getConnectedUser() {
        if (!$this->get('security.context')->getToken()->getUser()) {
            throw $this->createNotFoundException('Utilisateur non connecté.');
        }
        return $this->get('security.context')->getToken()->getUser();
    }

    /**
     * Create flash message
     * @param string $type (notice, success...)
     * @param text $contents
     */
    private function _createFlashMessage($type, $contents) {
        $this->get('session')->getFlashBag()->add(
                $type, $contents
        );
    }

    private function _getRangeSqm() {
        $em = $this->getDoctrine()->getManager();
        $range = array();

        $entity = $em->getRepository('LefDataBundle:Location')->getRangeSqm();
        $prefill = $this->_prepareLeftSearchSqmRange();

        $range['min'] = $entity[0][1];
        $range['max'] = $entity[0][2];
        foreach ($prefill as $key => $node) {
            switch ($key) {
                case 0:
                    $nk = 'left';
                    break;
                case 1:
                    $nk = 'right';
                    break;
                default:
                    break;
            }
            if ($node !== null) {
                $range[$nk] = $node;
            } else {
                $range[$nk] = $entity[0][$key + 1];
            }
        }

        return $range;
    }

    // ============================================================
    // ====== use webservice to get  availability for filter ======
    // ============================================================
    /**
     * Function get excluded(locations) liste
     * A le 170914 par Jian
     * @param integer $duree days
     * @param date $dateArrivee arrival date
     */
    private function _getLocPartnerNonAvailable($duree, $dateArrivee = NULL) {
        $excludeList = array();
        if ($dateArrivee == NULL) {
            return $excludeList;
        }
        $dateArriveeStandard = $dateArrivee == NULL ? date('Y-m-d') : \DateTime::createFromFormat('d/m/Y', $dateArrivee)->format('Y-m-d');
        $dateDepartStandard = defined($duree) && $duree != NULL ? date('Y-m-d', strtotime($dateArriveeStandard . '+ ' . $duree . ' days')) : date('Y-m-d', strtotime($dateArriveeStandard . '+ 7 days'));
        $allLocPartner = $this->_getAllLocationsPartner();

        foreach ($allLocPartner as $loc) {
            $availability = $this->_getPartnerAvailabilityByWebservice($loc['reference'], $loc['brandPartner'], $dateArriveeStandard, $dateDepartStandard, $loc['rentalprice']);
            // if not available
            if ($availability != NULL && $availability == 'N') {
                $excludeList[] = $loc['id'];
            }
        }
        return $excludeList;
    }

    /**
     * Function get all locations who have attribute brandpartner not null
     * A le 170914 par Jian
     * @return array
     */
    private function _getAllLocationsPartner() {
        $em = $this->getDoctrine()->getManager();
        // Limit request to 20
        $locations = $em->getRepository('LefDataBundle:Location')->getAllLocPartner(20);
        return $locations;
    }

    /**
     * Function get availability by using webservice
     * A le 170914 par Jian
     * @param string $reference
     * @param string $partnerBrand
     * @param date $dateArrivee
     * @param date $dateDepart
     * @return string
     */
    private function _getPartnerAvailabilityByWebservice($reference, $partnerBrand, $dateArrivee, $dateDepart, $price) {
        $availability = NULL;
        switch ($partnerBrand) {
            case 'interhome':
//                $webService = new InterhomeWebServiceController();
                $webService = $this->container->get('lef_data.web_service.interhome');
                $checkAvailability = $webService->availability($reference, $dateArrivee, $dateDepart);
                $canCheckAvailability = true;
                if ($checkAvailability->Ok) {
                    $availability = substr($checkAvailability->State, -1);
//                    $availability = str_replace(array('N', 'Y', 'Q'), array('Occupée', 'Disponible', 'Sur demande'), $availability);
                }
                break;

            case 'interchalet':
                $webService = new InterchaletWebServiceController();
                $canCheckAvailability = false;
                $availability = NULL;
                break;

            case 'leisure':
//                $webService = new LeisureWebServiceController();
                $webService = $this->container->get('lef_data.web_service.leisure');
                $checkAvailability = $webService->checkAvailability($reference, $dateArrivee, $dateDepart, $price);
                $canCheckAvailability = true;
                $availability = property_exists($checkAvailability, 'Available') ? $checkAvailability->Available : NULL;
                $availability = $availability == NULL ? NULL : str_replace(array('No', 'Yes'), array('N', 'Y'), $availability);
                break;

            default:
                break;
        }
        return $availability;
    }

    private function _increaseMemoryAllocate() {
        ini_set("max_execution_time", 0);
        ini_set("memory_limit", "-1");
    }

}
