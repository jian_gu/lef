<?php

namespace Lef\FoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lef\DataBundle\Entity\Location;
use Lef\FoBundle\Form\Type\SearchFormLeftType;

/**
 * Location controller.
 *
 * @Route("/location")
 */
class LocationController extends Controller {

    // Fields used in left search box
    protected static $checkboxFields = array('types', 'equipements', 'bienEtres');

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function indexAction($page) {
        $page = str_replace("page:", "", $page);
        $em = $this->getDoctrine()->getManager();
        // Prepare left search form: clean or prefilled
        $leftSearchForm = $this->_prepareLeftSearchForm();

        // get entities if search form is used
        $this->_findLocationsIfUseSearchForm();
        $useSrchFormLeftDeactor = $this->_findLocationsIfUseSearchForm();

        if ($useSrchFormLeftDeactor['if']) {
            $entities = $useSrchFormLeftDeactor['entities'];
        } else {
            $entities = $em->getRepository('LefDataBundle:Location')->findAll();
        }


        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');


        if ($useSrchFormLeftDeactor['if']) {

            $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormLeft($useSrchFormLeftDeactor['surface'], $useSrchFormLeftDeactor['conditions'], $page, null, null, null, null, null);
            /* total of résultat */
            $total_articles = count($total);
            $nbAnnonce = $total_articles;
            $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
            $last_page = ceil($total_articles / $articles_per_page);
            $previous_page = $page > 1 ? $page - 1 : 1;
            $next_page = $page < $last_page ? $page + 1 : $last_page;

            /* résultat  à afficher */
            $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormLeft($useSrchFormLeftDeactor['surface'], $useSrchFormLeftDeactor['conditions'], $page, $articles_per_page, $max_articles_on_listepage);

            foreach ($entities1 as $ke => $entity) {
                $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
                if (!empty($mediasP)) {
                    $entity->medias_p = $mediasP;
                } else {
                    $entity->medias_p = null;
                }
            }
        } else {
            // $entities = $em->getRepository('LefDataBundle:Location')->findAll();
            $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, null, null, null);
            /* total of résultat */
            $total_articles = count($total);
            $nbAnnonce = $total_articles;
            $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
            $last_page = ceil($total_articles / $articles_per_page);
            $previous_page = $page > 1 ? $page - 1 : 1;
            $next_page = $page < $last_page ? $page + 1 : $last_page;

            /* résultat  à afficher */
            $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage);

            foreach ($entities1 as $ke => $entity) {
                $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
                if (!empty($mediasP)) {
                    $entity->medias_p = $mediasP;
                } else {
                    $entity->medias_p = null;
                }
            }
        }

//        var_dump($this->getRequest()->get);
//        $url = $this->generateUrl('home', array('filter'=>'tt'), FALSE);
//return $this->redirect($url);
        $this->getRequest()->attributes->set('mykey', 'myvalue');
        return array(
            'entities' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $nbAnnonce,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'niveauLocalisation' => null,
            'pays' => null,
            'region' => null,
            'ville' => null,
            'leftSearchForm' => $leftSearchForm->createView(),
            'rangeSqm' => $this->_getRangeSqm(),
            'rangePrix' => $this->_getRangePrix(),
        );
    }

    /**
     *
     * @Template()
     */
    public function __paginationAction($page, $niveauLocalisation = null, $pays = null, $region = null, $ville = null) {
        $page = str_replace("page:", "", $page);
        $useSrchFormLeftDeactor = $this->_findLocationsIfUseSearchForm();

        if ($useSrchFormLeftDeactor['if']) {
            $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormLeft($useSrchFormLeftDeactor['surface'], $useSrchFormLeftDeactor['conditions'], $page, null, null, null, null, null);
        } else {
            $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, $region, $ville);
        }
        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        return array(
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'niveauLocalisation' => $niveauLocalisation,
            'pays' => $pays,
            'region' => $region,
            'ville' => $ville,
        );
    }

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function locByLoacalisationAction($page, $pays, $region, $ville) {
        $page = str_replace("page:", "", $page);
        $pays = str_replace("pays:", "", $pays);
        $region = str_replace("region:", "", $region);
        $ville = str_replace("ville:", "", $ville);
        $em = $this->getDoctrine()->getManager();

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, $region, $ville);

        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');

        $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage, $pays, $region, $ville);

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        return array(
            'entities1' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $nbAnnonce,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'pays' => $pays,
            'region' => $region,
            'ville' => $ville,
        );
    }

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function locByVilleAction($page, $pays, $region, $ville) {
        // die();
        $page = str_replace("page:", "", $page);
        $pays = str_replace("pays:", "", $pays);
        $region = str_replace("region:", "", $region);
        $ville = str_replace("ville:", "", $ville);
        $em = $this->getDoctrine()->getManager();

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, $region, $ville);

        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
        $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage, $pays, $region, $ville);

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $niveauLocalisation = 'ville';

        return $this->render('LefFoBundle:Location:locByLoacalisation.html.twig', array(
                    'entities1' => $entities1,
                    'nbrItemCompa' => $nombreItemPanierCompa,
                    'nbAnnonce' => $nbAnnonce,
                    'last_page' => $last_page,
                    'previous_page' => $previous_page,
                    'current_page' => $page,
                    'next_page' => $next_page,
                    'total_articles' => $total_articles,
                    'pays' => $pays,
                    'region' => $region,
                    'ville' => $ville,
                    'niveauLocalisation' => $niveauLocalisation,
        ));
    }

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function locByRegionAction($page, $pays, $region) {
        // die();
        $page = str_replace("page:", "", $page);
        $pays = str_replace("pays:", "", $pays);
        $region = str_replace("region:", "", $region);
        $em = $this->getDoctrine()->getManager();

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, $region, null);

        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
        $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage, $pays, $region);

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $niveauLocalisation = 'region';

        return $this->render('LefFoBundle:Location:locByLoacalisation.html.twig', array(
                    'entities1' => $entities1,
                    'nbrItemCompa' => $nombreItemPanierCompa,
                    'nbAnnonce' => $nbAnnonce,
                    'last_page' => $last_page,
                    'previous_page' => $previous_page,
                    'current_page' => $page,
                    'next_page' => $next_page,
                    'total_articles' => $total_articles,
                    'pays' => $pays,
                    'region' => $region,
                    'ville' => null,
                    'niveauLocalisation' => $niveauLocalisation,
        ));
    }

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function locByPaysAction($page, $pays) {
        $page = str_replace("page:", "", $page);
        $pays = str_replace("pays:", "", $pays);
        $em = $this->getDoctrine()->getManager();

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, null, null);

        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
        $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage, $pays);

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $niveauLocalisation = 'pays';

        return array(
            'entities1' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $nbAnnonce,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'pays' => $pays,
            'region' => null,
            'ville' => null,
            'niveauLocalisation' => $niveauLocalisation,
        );
    }

    /**
     * Lists Location vacs entities.
     *
     * @Template()
     */
    public function locsPromoAction($type_location) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        return array(
            'entities' => $entities,
            'type_location' => $type_location,
        );
    }

    /**
     * Lists Location vacs entities.
     *
     * @Template()
     */
    public function locsVacsAction($type_location) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        return array(
            'entities' => $entities,
            'type_location' => $type_location,
        );
    }

    /**
     * Finds and displays a Location entity.
     *
     * @Template()
     */
    public function showAction($reference) {
        // ==============================
        // If user is not connected
        // ==============================
        if ($this->_getConnectedUser() == 'anon.') {
            // set flash message
            $this->_createFlashMessage('info', 'Connectez-vous pour consulter des détails d\'une annonce.');
            // get current path
            $current_path = $this->container->get('request')->getPathInfo();
            // set current path into session
            $this->container->get('session')->set('refererPath', $current_path);
            // return to login page
            return $this->redirect($this->generateUrl('sonata_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Location')->findOneByReference($reference);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Location entity.');
        }

        $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
        $entity->medias_p = $mediasP;

        $currentUrl = $this->getRequest()->getUri();

        return array(
            'entity' => $entity
        );
    }

    // ********************************
    // ****** private functions *******
    // ********************************
    /**
     * Create sear form (côté gauche)
     * @param \Lef\DataBundle\Entity\Location $entity
     * @return type
     */
    private function _creatSearchFormLeft(Location $entity) {
        $route = $this->getRequest()->get('_route');
        $params = $this->getRequest()->request->get('filters');
        $url = $this->generateUrl($route, array($params));
        
        $form = $this->createForm(new SearchFormLeftType(), $entity, array(
//            'action' => $this->generateUrl('location', array('tt'=>'tt')),
            'action' => $url,
            'method' => 'GET',
            'attr' => array('class' => 'form-horizontal')
        ));


        $form->add('submit', 'submit', array('label' => 'Submit', 'attr' => array('value' => 'submit')));

        return $form;
    }

    /**
     * function get current connected user
     * @return object
     */
    private function _getConnectedUser() {
        if (!$this->get('security.context')->getToken()->getUser()) {
            throw $this->createNotFoundException('Utilisateur non connecté.');
        }
        return $this->get('security.context')->getToken()->getUser();
    }

    /**
     * Create flash message
     * @param string $type (notice, success...)
     * @param text $contents
     */
    private function _createFlashMessage($type, $contents) {
        $this->get('session')->getFlashBag()->add(
                $type, $contents
        );
    }

    private function _getRangeSqm() {
        $em = $this->getDoctrine()->getManager();
        $range = array();

        $entity = $em->getRepository('LefDataBundle:Location')->getRangeSqm();
        $prefill = $this->_prepareLeftSearchSqmRange();

        $range['min'] = $entity[0][1];
        $range['max'] = $entity[0][2];
        foreach ($prefill as $key => $node) {
            switch ($key) {
                case 0:
                    $nk = 'left';
                    break;
                case 1:
                    $nk = 'right';
                    break;
                default:
                    break;
            }
            if ($node !== null) {
                $range[$nk] = $node;
            } else {
                $range[$nk] = $entity[0][$key + 1];
            }
        }

        return $range;
    }

    private function _getRangePrix() {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Location')->getRangePrix('day');
        return $entity[0];
    }

    /**
     * Function prepare search form checkboxes, get values from session if exist
     * @return form
     */
    private function _prepareLeftSearchForm() {
        // Clean search form created
        $leftSearchForm = $this->_creatSearchFormLeft(new Location());
        // Returned search form with data
        $leftSearchFormRqst = $this->getRequest()->get('srch_left');

//        // if we get data form request, we'll fill the search form with thoses data
//        if ($leftSearchFormRqst !== NULL) {
//            $vals = array();
//            // empty the session values
//            $this->getRequest()->getSession()->remove('srch_left');
//            // loop form fields
//            foreach (self::$checkboxFields as $fdName) {
//                // if field with name returns data
//                if (isset($leftSearchFormRqst[$fdName])) {
//                    $vals[$fdName] = $leftSearchFormRqst[$fdName];
//                    $this->getRequest()->getSession()->set('srch_left', $vals);
//                }
//            }
//        }
//
//        if ($this->getRequest()->getSession()->get('srch_left')) {
//            $sessionVals = $this->getRequest()->getSession()->get('srch_left');
//            foreach (self::$checkboxFields as $fdName) {
//                if (isset($sessionVals[$fdName])) {
//                    $fieldRqst = $sessionVals[$fdName];
//                    foreach ($fieldRqst as $valueRqst) {
//                        foreach ($leftSearchForm[$fdName] as $valType) {
//                            if ($valType->getName() === $valueRqst) {
//                                $leftSearchForm[$fdName]->get($valType->getName())->setData(TRUE);
//                            }
//                        }
//                    }
//                }
//            }
//        }

        return $leftSearchForm;
    }

    /**
     * Function prepare surface range, get values from session if exist
     * @return array
     */
    private function _prepareLeftSearchSqmRange() {
        $RqstSearchLeft_sqmMin = $this->getRequest()->get('sqmMin');
        $RqstSearchLeft_sqmMax = $this->getRequest()->get('sqmMax');
        $range = array(null, null);

        if ($RqstSearchLeft_sqmMin) {
            $this->getRequest()->getSession()->set('sqmMin', $RqstSearchLeft_sqmMin);
        }
        if ($RqstSearchLeft_sqmMax) {
            $this->getRequest()->getSession()->set('sqmMax', $RqstSearchLeft_sqmMax);
        }

        // retrieve values from session
        if ($this->getRequest()->getSession()->get('sqmMin')) {
            $range[0] = $this->getRequest()->getSession()->get('sqmMin');
        }
        if ($this->getRequest()->getSession()->get('sqmMax')) {
            $range[1] = $this->getRequest()->getSession()->get('sqmMax');
        }

        return $range;
    }

    private function _findLocationsIfUseSearchForm() {
        // initialiser array to be returned: [ifused, entitites]
        $results = array();
        // Detected if search form is used
        if ($this->getRequest()->getSession()->get('srch_left')) {
            $em = $this->getDoctrine()->getManager();
            $srchFormLeft = $this->getRequest()->getSession()->get('srch_left');
            $results['if'] = TRUE;
            // surface to be passed to entity repository
            $surface = array($this->getRequest()->getSession()->get('sqmMin'), $this->getRequest()->getSession()->get('sqmMax'));
            // surface to be passed to entity repository
            // array to be passed to entity repository: [[types], [Equipements], [bienEtres]]
            $conditions = array();
            // surface is always defined if form is validated
            // price is always defined if form is validated
            // insert conditions if fields is defined
            foreach (self::$checkboxFields as $field) {
                if (isset($srchFormLeft[$field])) {
                    $conditions[$field] = $srchFormLeft[$field];
                }
            }
            $entities = $em->getRepository('LefDataBundle:Location')->findAllBySrchFormLeft($surface, $conditions);
            $results['entities'] = $entities;
            $results['surface'] = $surface;
            $results['conditions'] = $conditions;
        } else {
            $results['if'] = FALSE;
        }
        return $results;
    }

}
