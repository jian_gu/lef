<?php

namespace Lef\FoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Form\LocationType;
use Symfony\Component\Locale\Locale;
use Lef\DataBundle\Controller\InterhomeWebServiceController;
use Lef\DataBundle\Controller\InterchaletWebServiceController;
use Lef\DataBundle\Controller\LeisureWebServiceController;

/**
 * Location controller.
 *
 * @Route("/location")
 */
class LocationController extends Controller {

    protected static $ifUseSearchFormTop = false;
    protected static $ifUseSearchFormLeft = false;
    protected static $requestDataTop;
    protected static $requestDataLeft;
    protected static $filtrePromo = false;
    protected static $filtreVacance;

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function indexAction($page) {
        $page = str_replace("page:", "", $page);
        $em = $this->getDoctrine()->getManager();


        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, null, null, null);
        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
        /* si utilise formulaire de recherche */
        if ($this->_ifUseLeftSearch() && !$this->_ifUseTopSearch()) {
            $paramsReady = $this->_getRequestParamLeft();
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormLeft($paramsReady['rangeSqm'], $paramsReady['rangePrix'], $paramsReady['checkboxes'], $page, $articles_per_page, $max_articles_on_listepage);
        } else if ($this->_ifUseTopSearch()) {
            // geoLoc, dateArrivee, duree, nbrPers
            $paramsReadyTop = $this->_getRequestParamTop();

            // Use more options in top search
            if ($this->_ifUseLeftSearch()) {
                $paramsReadyLeft = $this->_getRequestParamLeft();
//                $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormLeft($paramsReadyLeft['rangeSqm'], $paramsReadyLeft['rangePrix'], $paramsReadyLeft['checkboxes'], $page, $articles_per_page, $max_articles_on_listepage);
                $excludeList = $this->_getLocPartnerNonAvailable($paramsReadyTop['duree'], $paramsReadyTop['dateArrivee']);
                $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormTop($paramsReadyTop, $paramsReadyLeft, $page, $articles_per_page, $max_articles_on_listepage, NULL, NULL, NULL, NULL, $excludeList);
            } else {
                $excludeList = $this->_getLocPartnerNonAvailable($paramsReadyTop['duree'], $paramsReadyTop['dateArrivee']);
                $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormTop($paramsReadyTop, NULL, $page, $articles_per_page, $max_articles_on_listepage, NULL, NULL, NULL, NULL, $excludeList);
            }
        } else {
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage);
        }

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $nbAnnonce = count($entities1);
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        return array(
            'entities' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $nbAnnonce,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'niveauLocalisation' => null,
            'pays' => null,
            'region' => null,
            'ville' => null,
        );
    }

    /**
     *
     * @Template()
     */
    public function __paginationAction($page, $niveauLocalisation = null, $pays = null, $region = null, $ville = null) {
        $page = str_replace("page:", "", $page);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');

        /* Si utilise formulaire de recherche */
        if (self::$ifUseSearchFormLeft && !self::$ifUseSearchFormTop && !self::$filtrePromo && !is_array(self::$filtreVacance)) {              // Modified le 150814 par Jian
            $paramsReady = $this->_getRequestParamLeft();
            $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormLeft($paramsReady['rangeSqm'], $paramsReady['rangePrix'], $paramsReady['checkboxes'], $page, null, null, $pays, $region, $ville);
        } else if (self::$ifUseSearchFormTop) {
            $paramsReadyTop = $this->_getRequestParamTop();


            // Use more options in top search
            if (self::$ifUseSearchFormLeft) {
                $paramsReadyLeft = $this->_getRequestParamLeft();
//                $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormLeft($paramsReadyLeft['rangeSqm'], $paramsReadyLeft['rangePrix'], $paramsReadyLeft['checkboxes'], $page, $articles_per_page, $max_articles_on_listepage);
                $excludeList = $this->_getLocPartnerNonAvailable($paramsReadyTop['duree'], $paramsReadyTop['dateArrivee']);
                $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormTop($paramsReadyTop, $paramsReadyLeft, $page, null, null, $pays, $region, $ville, NULL, $excludeList);
            } else {
                $excludeList = $this->_getLocPartnerNonAvailable($paramsReadyTop['duree'], $paramsReadyTop['dateArrivee']);
                $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormTop($paramsReadyTop, NULL, $page, null, null, $pays, $region, $ville, NULL, $excludeList);
            }
        } else if (self::$filtrePromo) {
            $dureeFirstMinute = $this->container->getParameter('duree_heures_first_minutes');               // Added le 140814 par Jian
            if (self::$ifUseSearchFormLeft) {
                $paramsReady = $this->_getRequestParamLeft();
                $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationPromo($page, null, null, null, null, null, self::$filtrePromo, $dureeFirstMinute, $paramsReady['rangeSqm'], $paramsReady['rangePrix'], $paramsReady['checkboxes']);        // Added le 150814 par Jian
            } else {
                $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationPromo($page, null, null, null, null, null, self::$filtrePromo, $dureeFirstMinute);        // Modified le 140814 par Jian;
            }
        } else if (is_array(self::$filtreVacance)) {
            $dureeFirstMinute = $this->container->getParameter('duree_heures_first_minutes');               // Added le 140814 par Jian
            if (self::$ifUseSearchFormLeft) {
                $paramsReady = $this->_getRequestParamLeft();
                $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationVacs($page, null, null, null, null, null, self::$filtreVacance, $dureeFirstMinute, $paramsReady['rangeSqm'], $paramsReady['rangePrix'], $paramsReady['checkboxes']);        // Added le 150814 par Jian
            } else {
                $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationVacs($page, null, null, null, null, null, self::$filtreVacance, $dureeFirstMinute);        // Modified le 140814 par Jian;
            }
        } else {
            $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, $region, $ville);
        }

        /* total of résultat */
        $total_articles = count($total);
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        if (trim(self::$requestDataLeft) !== '' && count(self::$requestDataTop) == 0) {
            $rqstVar = trim(self::$requestDataLeft) != '' ? '?filters=' . self::$requestDataLeft : '';
        } else if (count(self::$requestDataTop)) {
            $rqstVar = '?';
            $rqstVar.= trim(self::$requestDataTop['geoLoc']) != '' ? '&geoLoc=' . self::$requestDataTop['geoLoc'] : '';
            $rqstVar.= trim(self::$requestDataTop['dateArrivee']) != '' ? '&dateArrivee=' . self::$requestDataTop['dateArrivee'] : '';
            $rqstVar.= trim(self::$requestDataTop['duree']) != '' ? '&duree=' . self::$requestDataTop['duree'] : '';
            $rqstVar.= trim(self::$requestDataTop['nbrPers']) != '' ? '&nbrPers=' . self::$requestDataTop['nbrPers'] : '';

            if (self::$requestDataTop) {
                $rqstVar .= trim(self::$requestDataLeft) != '' ? '&filters=' . self::$requestDataLeft : '';
            }
        } else {
            $rqstVar = '';
        }

        $rqstVar = str_replace(':', '%3A', $rqstVar);
        $rqstVar = str_replace('@', '%40', $rqstVar);
        $rqstVar = str_replace(',', '%2C', $rqstVar);
        $rqstVar = str_replace('/', '%2F', $rqstVar);

        return array(
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'niveauLocalisation' => $niveauLocalisation,
            'pays' => $pays,
            'region' => $region,
            'ville' => $ville,
            'rqstVar' => $rqstVar,
        );
    }

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function locByLoacalisationAction($page, $pays, $region, $ville) {
        $page = str_replace("page:", "", $page);
        $pays = str_replace("pays:", "", $pays);
        $region = str_replace("region:", "", $region);
        $ville = str_replace("ville:", "", $ville);
        $em = $this->getDoctrine()->getManager();

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, $region, $ville);

        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');

        /* si utilise formulaire de recherche */
        if ($this->_ifUseLeftSearch()) {
            $paramsReady = $this->_getRequestParamLeft();
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormLeft($paramsReady['rangeSqm'], $paramsReady['rangePrix'], $paramsReady['checkboxes'], $page, $articles_per_page, $max_articles_on_listepage, $pays, $region, $ville);
        } else {
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage, $pays, $region, $ville);
        }

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        return array(
            'entities1' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $nbAnnonce,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'pays' => $pays,
            'region' => $region,
            'ville' => $ville,
        );
    }

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function locByVilleAction($page, $pays, $region, $ville) {
        // die();
        $page = str_replace("page:", "", $page);
        $pays = str_replace("pays:", "", $pays);
        $region = str_replace("region:", "", $region);
        $ville = str_replace("ville:", "", $ville);
        $em = $this->getDoctrine()->getManager();

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, $region, $ville);

        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');

        /* si utilise formulaire de recherche */
        if ($this->_ifUseLeftSearch()) {
            $paramsReady = $this->_getRequestParamLeft();
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormLeft($paramsReady['rangeSqm'], $paramsReady['rangePrix'], $paramsReady['checkboxes'], $page, $articles_per_page, $max_articles_on_listepage, $pays, $region, $ville);
        } else {
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage, $pays, $region, $ville);
        }

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $niveauLocalisation = 'ville';

        return $this->render('LefFoBundle:Location:locByLoacalisation.html.twig', array(
                    'entities1' => $entities1,
                    'nbrItemCompa' => $nombreItemPanierCompa,
                    'nbAnnonce' => $nbAnnonce,
                    'last_page' => $last_page,
                    'previous_page' => $previous_page,
                    'current_page' => $page,
                    'next_page' => $next_page,
                    'total_articles' => $total_articles,
                    'pays' => $pays,
                    'region' => $region,
                    'ville' => $ville,
                    'niveauLocalisation' => $niveauLocalisation,
        ));
    }

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function locByRegionAction($page, $pays, $region) {
        // die();
        $page = str_replace("page:", "", $page);
        $pays = str_replace("pays:", "", $pays);
        $region = str_replace("region:", "", $region);
        $em = $this->getDoctrine()->getManager();

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, $region, null);

        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');

        /* si utilise formulaire de recherche */
        if ($this->_ifUseLeftSearch()) {
            $paramsReady = $this->_getRequestParamLeft();
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormLeft($paramsReady['rangeSqm'], $paramsReady['rangePrix'], $paramsReady['checkboxes'], $page, $articles_per_page, $max_articles_on_listepage, $pays, $region);
        } else {
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage, $pays, $region);
        }

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $niveauLocalisation = 'region';

        return $this->render('LefFoBundle:Location:locByLoacalisation.html.twig', array(
                    'entities1' => $entities1,
                    'nbrItemCompa' => $nombreItemPanierCompa,
                    'nbAnnonce' => $nbAnnonce,
                    'last_page' => $last_page,
                    'previous_page' => $previous_page,
                    'current_page' => $page,
                    'next_page' => $next_page,
                    'total_articles' => $total_articles,
                    'pays' => $pays,
                    'region' => $region,
                    'ville' => null,
                    'niveauLocalisation' => $niveauLocalisation,
        ));
    }

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function locByPaysAction($page, $pays) {
        $page = str_replace("page:", "", $page);
        $pays = str_replace("pays:", "", $pays);
        $em = $this->getDoctrine()->getManager();

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, null, null);

        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');

        /* si utilise formulaire de recherche */
        if ($this->_ifUseLeftSearch()) {
            $paramsReady = $this->_getRequestParamLeft();
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormLeft($paramsReady['rangeSqm'], $paramsReady['rangePrix'], $paramsReady['checkboxes'], $page, $articles_per_page, $max_articles_on_listepage, $pays);
        } else {
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage, $pays);
        }

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $niveauLocalisation = 'pays';

        return array(
            'entities1' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $nbAnnonce,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'pays' => $pays,
            'region' => null,
            'ville' => null,
            'niveauLocalisation' => $niveauLocalisation,
        );
    }

    //////////////////////////////////////////////////>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /**
     * Lists Location promo entities.
     *
     * @Template()
     */
    public function locsPromoAction($type_location, $page) {
        $page = str_replace("page:", "", $page);
        $em = $this->getDoctrine()->getManager();

        switch ($type_location) {
            case 'premiere-minute':
                self::$filtrePromo = 'premiere-minute';
                break;
            case 'derniere-minute':
                self::$filtrePromo = 'derniere-minute';
                break;
            case 'promotions':
                self::$filtrePromo = 'promotions';
                break;
        }

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');
        $dureeFirstMinute = $this->container->getParameter('duree_heures_first_minutes');               // Added le 140814 par Jian

        /* si utilise formulaire de recherche */
        if ($this->_ifUseLeftSearch()) {
            $paramsReady = $this->_getRequestParamLeft();
            $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationPromo($page, null, null, null, null, null, $type_location, $dureeFirstMinute, $paramsReady['rangeSqm'], $paramsReady['rangePrix'], $paramsReady['checkboxes']);        // Added le 150814 par Jian
        } else {
            $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationPromo($page, null, null, null, null, null, $type_location, $dureeFirstMinute);        // Modified le 140814 par Jian
        }
        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
        /* si utilise formulaire de recherche */
        if ($this->_ifUseLeftSearch()) {
            $paramsReady = $this->_getRequestParamLeft();
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationPromo($page, $articles_per_page, $max_articles_on_listepage, null, null, null, $type_location, $dureeFirstMinute, $paramsReady['rangeSqm'], $paramsReady['rangePrix'], $paramsReady['checkboxes']);        // Added le 150814 par Jian
        } else {
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationPromo($page, $articles_per_page, $max_articles_on_listepage, null, null, null, $type_location, $dureeFirstMinute);        // Modified le 140814 par Jian
        }

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        return array(
            'entities' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $total,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'type_location' => $type_location,
            'niveauLocalisation' => null,
            'pays' => null,
            'region' => null,
            'ville' => null,
        );
    }

    //////////////////////////////////////////////////>>>>>>>>>>>>>>>>>>>>>>>>>>>
    ///////////// M le 040914 par Jian ///////////////>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /**
     * Lists Location vacs entities.
     *
     * @Template()
     */
    public function locsVacsAction($type_location, $page) {                     // M le 040914 par Jian
        $page = str_replace("page:", "", $page);
        $em = $this->getDoctrine()->getManager();

        // ========= Manage type_vacs_location =========
        // if table location itself is used, set it first!!
        switch ($type_location) {
            case 'ete':
                self::$filtreVacance = array('saisons' => 'été', 'themes' => 'été');
                break;
            case 'hiver':
                self::$filtreVacance = array('saisons' => 'hiver', 'themes' => 'hiver');
                break;
            case 'ski':
                self::$filtreVacance = array('_location' => 'distanceRemontees < 10000', 'localisations' => 'ski');
                break;
            case 'soleil-lointain':
                self::$filtreVacance = array('_location' => 'latitude < 40 AND l.latitude > -45', 'themes' => 'soleil');
                break;
            case 'mer':
                self::$filtreVacance = array('_location' => 'distanceMer < 10000', 'localisations' => 'mer');
                break;
            case 'famille':
                self::$filtreVacance = array('themes' => 'famille');
                break;
            case 'vert':
                self::$filtreVacance = array('localisations' => 'montagne');
                break;
        }

        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');
        $dureeFirstMinute = $this->container->getParameter('duree_heures_first_minutes');               // Added le 140814 par Jian

        /* si utilise formulaire de recherche */
        if ($this->_ifUseLeftSearch()) {
            $paramsReady = $this->_getRequestParamLeft();
            $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationVacs($page, null, null, null, null, null, self::$filtreVacance, $dureeFirstMinute, $paramsReady['rangeSqm'], $paramsReady['rangePrix'], $paramsReady['checkboxes']);        // Added le 150814 par Jian
        } else {
            $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationVacs($page, null, null, null, null, null, self::$filtreVacance, $dureeFirstMinute);        // Modified le 140814 par Jian
        }
        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
        /* si utilise formulaire de recherche */
        if ($this->_ifUseLeftSearch()) {
            $paramsReady = $this->_getRequestParamLeft();
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationVacs($page, $articles_per_page, $max_articles_on_listepage, null, null, null, self::$filtreVacance, $dureeFirstMinute, $paramsReady['rangeSqm'], $paramsReady['rangePrix'], $paramsReady['checkboxes']);        // Added le 150814 par Jian
        } else {
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocationVacs($page, $articles_per_page, $max_articles_on_listepage, null, null, null, self::$filtreVacance, $dureeFirstMinute);        // Modified le 140814 par Jian
        }

//        var_dump(count($entities1));
        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        return array(
            'entities' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $total,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'type_location' => $type_location,
            'niveauLocalisation' => null,
            'pays' => null,
            'region' => null,
            'ville' => null,
        );
    }

    /**
     * Finds and displays a Location entity.
     *
     * @Template()
     */
    public function showAction($reference, Request $request) {
        //        // ==============================
        //        // If user is not connected
        //        // ==============================
        //        if ($this->_getConnectedUser() == 'anon.') {
        //            // set flash message
        //            $this->_createFlashMessage('info', 'Connectez-vous pour consulter des détails d\'une annonce.');
        //            // get current path
        //            $current_path = $this->container->get('request')->getPathInfo();
        //            // set current path into session
        //            $this->container->get('session')->set('refererPath', $current_path);
        //            // return to login page
        //            return $this->redirect($this->generateUrl('sonata_user_security_login'));
        //        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Location')->findOneByReference($reference);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Location entity.');
        }

        $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());

        $arrivee = $request->query->get('arrivee');
        if (isset($arrivee)) {
            $dateArrivee = $arrivee;
        } else {
            $dateArrivee = date('Y-m-d');
        }

        $depart = $request->query->get('depart');
        if (isset($depart)) {
            $dateDepart = $depart;
        } else {
            $dateDepart = date('Y-m-d', strtotime("+7 days"));
        }

        //////////////////////////// A 110814 Jian ///////////////////////////////
        $canCheckAvailability = FALSE;                                          //
        $availability = "";                                                     //
        $isPartner = FALSE;                                                     //
        //////////////////////////////////////////////////////////////////////////
        switch ($entity->getBrandPartner()) {
            case 'interhome':
                $webService = new InterhomeWebServiceController();
                $checkAvailability = $webService->availability($entity->getReference(), $dateArrivee, $dateDepart);
                $canCheckAvailability = true;
                if ($checkAvailability->Ok) {
                    $availability = substr($checkAvailability->State, -1);
                    $availability = str_replace(array('N', 'Y', 'Q'), array('Occupée', 'Disponible', 'Sur demande'), $availability);
                }
                ////////////// A 110814 Jian /////////////
                $isPartner = TRUE;                      //
                //////////////////////////////////////////
                break;

            case 'interchalet':
                $webService = new InterchaletWebServiceController();
                $canCheckAvailability = false;
                $availability = "";
                ////////////// A 110814 Jian /////////////
                $isPartner = TRUE;                      //
                //////////////////////////////////////////
                break;

            case 'leisure':
                $webService = new LeisureWebServiceController();
                $checkAvailability = $webService->checkAvailability($code, $dateArrivee, $dateDepart, 168);
                $canCheckAvailability = true;
                $availability = "";
                ////////////// A 110814 Jian /////////////
                $isPartner = TRUE;                      //
                //////////////////////////////////////////
                break;

            default:
                ////////////// A 110814 Jian /////////////
                $isPartner = FALSE;                     //
                //////////////////////////////////////////
                break;
        }

        $entity->medias_p = $mediasP;
        $currentUrl = $this->getRequest()->getUri();

        if (!$isPartner) {
            
        }

        return array(
            'entity' => $entity,
            'annonceur' => $entity->getAnnonceur(),
            'dateArrivee' => $dateArrivee,
            'dateDepart' => $dateDepart,
            'canCheckAvailability' => $canCheckAvailability,
            'availability' => $availability,
            ////////////// A 110814 Jian /////////////
            'isPartner' => $isPartner, //
            //////////////////////////////////////////
            'partnerBrand' => $entity->getBrandPartner(),
        );
    }

    //////////////////////////// A 110814 Jian ////////////////////>>>>>>>>>>>>>>>
    /**
     * display annonceur details
     * @param type $reference
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return type
     * @Template()
     */
    public function showDetailsAction($reference, Request $request) {
        // ==============================
        // If user is not connected
        // ==============================
        if ($this->_getConnectedUser() == 'anon.') {
            // set flash message
            $this->_createFlashMessage
                    ('info', 'Connectez-vous pour consulter des détails d\'une annonce.');
            // get current path
            $current_path = $this->container->get('request')->getPathInfo();
            // set current path into session
            $this->container->get('session')->set('refererPath', $current_path);
            // return to login page
            //            return $this->redirect(
            //                            $this->generateUrl('sonata_user_security_login')
            //            );

            return array(
                'requireConnection' => TRUE,
            );
        }
        // ==============================
        // If user is connected
        // ==============================
        $em = $this->getDoctrine()->getManager();

        $location = $em->getRepository('LefDataBundle:Location')->findOneByReference($reference);
        $annonceur = $location->getAnnonceur();
        $statVu = new \Lef\DataBundle\Entity\StatVusLocation();

        // ==============================
        // Record visit: ignore meme visiteur - meme location pendant 30 Minutes
        // ==============================
        $visited = $em->getRepository('LefDataBundle:StatVusLocation')->isConsultedIn30Min($this->_getConnectedUser(), $location);
        if (!$visited) {
            $statVu->setVisiteur($this->_getConnectedUser());
            $statVu->setLocation($location);
            $statVu->setDate(new \DateTime());

            $em->persist($statVu);
            $em->flush();
        }

        return array(
            'requireConnection' => FALSE,
            'annonceur' => $annonceur, // Attention, faut mieux que ne pas metter tous les champs
        );
    }

    ////////////////////////////////////////////////////////////<<<<<<<<<<<<<<<<<<
    //
    //////////////////////////// A 130814 par Jian ////////////////>>>>>>>>>>>>>>>
    /**
     * Lists favoris entities.
     *
     * @Template()
     */
    public function favorisAction($page) {
        $page = str_replace("page:", "", $page);
        $references = array();
        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request');
        $cookies = $request->cookies;

        if ($cookies->has('wish_list')) {
            $listStr = $cookies->get('wish_list');
            $references = strpos($listStr, ',') > -1 ? explode(',', $listStr) : array($listStr);
        }

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getFavoris($page, $references, null, null);
        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');

        $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getFavoris($page, $references, $articles_per_page, $max_articles_on_listepage);


        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $nbAnnonce = count($entities1);
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        return array(
            'entities' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $nbAnnonce,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
        );
    }

    /**
     * Pagination pour favoris
     * @Template()
     */
    public function __paginationFavorisAction($page) {
        $page = str_replace("page:", "", $page);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
        $references = array();

        $request = $this->get('request');
        $cookies = $request->cookies;

        if ($cookies->has('wish_list')) {
            $listStr = $cookies->get('wish_list');
            $references = strpos($listStr, ',') > -1 ? explode(',', $listStr) : array($listStr);
        }

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getFavoris($page, $references, null, null);
        /* total of résultat */
        $total_articles = count($total);
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;


        return array(
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
        );
    }

    ////////////////////////////////////////////////////////////<<<<<<<<<<<<<<<<<<
    //
    //
    // ===========================
    // ==== Private functions ====
    // ===========================
    /**
     * function get current connected user
     * @return object
     */
    private function _getConnectedUser() {
        if (!$this->get('security.context')->getToken()->getUser()) {
            throw $this->createNotFoundException('Utilisateur non connecté.');
        }
        return $this->get('security.context')->getToken()->getUser();
    }

    /**
     * Create flash message
     * @param string $type (notice, success...)
     * @param text $contents
     */
    private function _createFlashMessage($type, $contents) {
        $this->get('session')->getFlashBag()->add(
                $type, $contents
        );
    }

    private function _getRangeSqm() {
        $em = $this->getDoctrine()->getManager();
        $range = array();

        $entity = $em->getRepository('LefDataBundle:Location')->getRangeSqm();
        $prefill = $this->_prepareLeftSearchSqmRange();

        $range['min'] = $entity[0][1];
        $range['max'] = $entity[0][2];
        foreach ($prefill as $key => $node) {
            switch ($key) {
                case 0:
                    $nk = 'left';
                    break;
                case 1:
                    $nk = 'right';
                    break;
                default:
                    break;
            }
            if ($node !== null) {
                $range[$nk] = $node;
            } else {
                $range[$nk] = $entity[0][$key + 1];
            }
        }

        return $range;
    }

    private function _ifUseLeftSearch() {
        $filterExists = $this->getRequest()->query->get('filters');
        if ($filterExists !== NULL && trim($filterExists) != '') {
            self::$ifUseSearchFormLeft = true;
            self::$requestDataLeft = $filterExists;
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function _ifUseTopSearch() {
        // here, it may be only the geoLoc, dateArrivee, duree, nbrPers
        // Or also with filters
        $geoLocExists = $this->getRequest()->query->get('geoLoc');
        $dateArriveeExists = $this->getRequest()->query->get('dateArrivee');
        $dureeExists = $this->getRequest()->query->get('duree');
        $nbrPersExists = $this->getRequest()->query->get('nbrPers');
        if ($geoLocExists !== NULL && trim($geoLocExists) != '' || $dateArriveeExists !== NULL && trim($dateArriveeExists) != '' || $dureeExists !== NULL && trim($dureeExists) != '' || $nbrPersExists !== NULL && trim($nbrPersExists) != '') {
            self::$ifUseSearchFormTop = true;
            self::$requestDataTop = array('geoLoc' => $geoLocExists, 'dateArrivee' => $dateArriveeExists,
                'duree' => $dureeExists, 'nbrPers' => $nbrPersExists);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function _getRequestParamLeft() {
        $totalArray = array();
        $surface = array();
        $prix = array();
        $rawParam = self::$requestDataLeft;
        // if the data contains a ',', means multiple params
        if (strchr($rawParam, ',')) {
            $metaArry1 = explode(',', $rawParam);
            foreach ($metaArry1 as $value) {
                $metaArry2 = explode(':', $value);
                $k = $metaArry2[0];
                $v = $metaArry2[1];
                $totalArray[$k][] = $v;
            }
        } else {
            $metaArry = explode(':', $rawParam);
            $k = $metaArry[0];
            $v = $metaArry[1];
            $totalArray[$k][] = $v;
        }


        foreach ($totalArray as $key => $value) {
            switch (trim($key)) {
                case 'sqmMin':
                    $surface['min'] = $value;
                    unset($totalArray[$key]);
                    break;
                case 'sqmMax':
                    $surface['max'] = $value;
                    unset($totalArray[$key]);
                    break;
                case 'prixMin':
                    $prix['min'] = $value;
                    unset($totalArray[$key]);
                    break;
                case 'prixMax':
                    $prix['max'] = $value;
                    unset($totalArray[$key]);
                    break;

                default:
                    break;
            }
        }

        return array('rangeSqm' => $surface, 'rangePrix' => $prix, 'checkboxes' => $totalArray);
    }

    private function _getRequestParamTop() {
        $rawParam = self::$requestDataTop;
        $geoLoc = $this->_treatVisitorQuerySearchTop($rawParam['geoLoc']);
        $dateArrivee = $rawParam['dateArrivee'];
        $duree = $rawParam['duree'];
        $nbrPers = $rawParam['nbrPers'];
        return array('geoLoc' => $geoLoc, 'dateArrivee' => $dateArrivee,
            'duree' => $duree, 'nbrPers' => $nbrPers);
    }

    private function _treatVisitorQuerySearchTop($qry) {
        if (strchr($qry, ' ')) {
            $qryArray = explode(' ', $qry);
            $countries = $this->_countryNameToCode();
            foreach ($qryArray as $keyq => $valq) {
                $vq = trim(strtolower($valq));
                foreach ($countries as $nomc => $codec) {
                    $nc = strtolower($nomc);
                    if ($vq == $nc) {
                        // change 040814: use no more country code
                        // $qryArray[$keyq] = $codec;
                    }
                }
            }
        } else {
            $qryArray[0] = $qry;
        }

        return $qryArray;
    }

    private function _countryNameToCode() {
        $countries = Locale::getDisplayCountries('fr');
        $translator = $this->get('translator');
        $codeCountry = array();

        foreach ($countries as $key => $country) {
            $countryName = $translator->trans($country, array(), null, 'fr');
            $codeCountry[$countryName] = $key;
        }

        return $codeCountry;
    }

    // ============================================================
    // ====== use webservice to get  availability for filter ======
    // ============================================================
    /**
     * Function get excluded(locations) liste
     * A le 170914 par Jian
     * @param integer $duree days
     * @param date $dateArrivee arrival date
     */
    private function _getLocPartnerNonAvailable($duree, $dateArrivee = NULL) {
        $excludeList = array();
        $dateArriveeStandard = $dateArrivee == NULL ? date('Y-m-d') : \DateTime::createFromFormat('d/m/Y', $dateArrivee)->format('Y-m-d');
        $dateDepartStandard = date('Y-m-d', strtotime($dateArriveeStandard . '+ 7 days'));
        $allLocPartner = $this->_getAllLocationsPartner();

        foreach ($allLocPartner as $loc) {
            $availability = $this->_getPartnerAvailabilityByWebservice($loc['reference'], $loc['brandPartner'], $dateArriveeStandard, $dateDepartStandard, $loc['rentalprice']);
            // if not available
            if ($availability != NULL && $availability == 'N') {
                $excludeList[] = $loc['id'];
            }
        }
        return $excludeList;
    }

    /**
     * Function get all locations who have attribute brandpartner not null
     * A le 170914 par Jian
     * @return array
     */
    private function _getAllLocationsPartner() {
        $em = $this->getDoctrine()->getManager();
        $locations = $em->getRepository('LefDataBundle:Location')->getAllLocPartner();
        return $locations;
    }

    /**
     * Function get availability by using webservice
     * A le 170914 par Jian
     * @param string $reference
     * @param string $partnerBrand
     * @param date $dateArrivee
     * @param date $dateDepart
     * @return string
     */
    private function _getPartnerAvailabilityByWebservice($reference, $partnerBrand, $dateArrivee, $dateDepart, $price) {
        $availability = NULL;
        switch ($partnerBrand) {
            case 'interhome':
                $webService = new InterhomeWebServiceController();
                $checkAvailability = $webService->availability($reference, $dateArrivee, $dateDepart);
                $canCheckAvailability = true;
                if ($checkAvailability->Ok) {
                    $availability = substr($checkAvailability->State, -1);
//                    $availability = str_replace(array('N', 'Y', 'Q'), array('Occupée', 'Disponible', 'Sur demande'), $availability);
                }
                break;

            case 'interchalet':
                $webService = new InterchaletWebServiceController();
                $canCheckAvailability = false;
                $availability = NULL;
                break;

            case 'leisure':
                $webService = new LeisureWebServiceController();
                $checkAvailability = $webService->checkAvailability($reference, $dateArrivee, $dateDepart, $price);
                $canCheckAvailability = true;
                $availability = property_exists($checkAvailability, 'Available') ? $checkAvailability->Available : NULL;
                $availability = $availability == NULL ? NULL : str_replace(array('No', 'Yes'), array('N', 'Y'), $availability);
                break;

            default:
                break;
        }
        return $availability;
    }

}
