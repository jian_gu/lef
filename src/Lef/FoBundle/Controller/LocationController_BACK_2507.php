<?php

namespace Lef\FoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Lef\DataBundle\Entity\Location;
use Lef\DataBundle\Form\LocationType;

/**
 * Location controller.
 *
 * @Route("/location")
 */
class LocationController extends Controller {

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function indexAction($page) {
        $page = str_replace("page:", "", $page);
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        foreach ($entities as $ke => $entity) {
            $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
            $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

            // $entities = $em->getRepository('LefDataBundle:Location')->findAll();
            $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, null, null, null);
            /* total of résultat */
            $total_articles = count($total);
            $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
            $last_page = ceil($total_articles / $articles_per_page);
            $previous_page = $page > 1 ? $page - 1 : 1;
            $next_page = $page < $last_page ? $page + 1 : $last_page;

            $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getTotalLocas();

            /* total of résultat */
            $total_articles = count($total);
            $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
            $last_page = ceil($total_articles / $articles_per_page);
            $previous_page = $page > 1 ? $page - 1 : 1;
            $next_page = $page < $last_page ? $page + 1 : $last_page;

            /* résultat  à afficher */
            $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
            $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage);

            foreach ($entities1 as $ke => $entity) {
                $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
                if (!empty($mediasP)) {
                    $entity->medias_p = $mediasP;
                } else {
                    $entity->medias_p = null;
                }
            }

            return array(
                'entities' => $entities1,
                'nbrItemCompa' => $nombreItemPanierCompa,
                'nbAnnonce' => $nbAnnonce,
                'last_page' => $last_page,
                'previous_page' => $previous_page,
                'current_page' => $page,
                'next_page' => $next_page,
                'total_articles' => $total_articles,
                'niveauLocalisation' => null,
                'pays' => null,
                'region' => null,
                'ville' => null,
            );
        }
    }

    /**
     *
     * @Template()
     */
    public function __paginationAction($page, $niveauLocalisation = null, $pays = null, $region = null, $ville = null) {
        $page = str_replace("page:", "", $page);
        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, $region, $ville);
        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        return array(
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'niveauLocalisation' => $niveauLocalisation,
            'pays' => $pays,
            'region' => $region,
            'ville' => $ville,
        );
    }

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function locByLoacalisationAction($page, $pays, $region, $ville) {
        $page = str_replace("page:", "", $page);
        $pays = str_replace("pays:", "", $pays);
        $region = str_replace("region:", "", $region);
        $ville = str_replace("ville:", "", $ville);
        $em = $this->getDoctrine()->getManager();

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, $region, $ville);

        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');

        $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage, $pays, $region, $ville);

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        return array(
            'entities1' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $nbAnnonce,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'pays' => $pays,
            'region' => $region,
            'ville' => $ville,
        );
    }

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function locByVilleAction($page, $pays, $region, $ville) {
        // die();
        $page = str_replace("page:", "", $page);
        $pays = str_replace("pays:", "", $pays);
        $region = str_replace("region:", "", $region);
        $ville = str_replace("ville:", "", $ville);
        $em = $this->getDoctrine()->getManager();

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, $region, $ville);

        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
        $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage, $pays, $region, $ville);

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $niveauLocalisation = 'ville';

        return $this->render('LefFoBundle:Location:locByLoacalisation.html.twig', array(
                    'entities1' => $entities1,
                    'nbrItemCompa' => $nombreItemPanierCompa,
                    'nbAnnonce' => $nbAnnonce,
                    'last_page' => $last_page,
                    'previous_page' => $previous_page,
                    'current_page' => $page,
                    'next_page' => $next_page,
                    'total_articles' => $total_articles,
                    'pays' => $pays,
                    'region' => $region,
                    'ville' => $ville,
                    'niveauLocalisation' => $niveauLocalisation,
        ));
    }

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function locByRegionAction($page, $pays, $region) {
        // die();
        $page = str_replace("page:", "", $page);
        $pays = str_replace("pays:", "", $pays);
        $region = str_replace("region:", "", $region);
        $em = $this->getDoctrine()->getManager();

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, $region, null);

        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
        $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage, $pays, $region);

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $niveauLocalisation = 'region';

        return $this->render('LefFoBundle:Location:locByLoacalisation.html.twig', array(
                    'entities1' => $entities1,
                    'nbrItemCompa' => $nombreItemPanierCompa,
                    'nbAnnonce' => $nbAnnonce,
                    'last_page' => $last_page,
                    'previous_page' => $previous_page,
                    'current_page' => $page,
                    'next_page' => $next_page,
                    'total_articles' => $total_articles,
                    'pays' => $pays,
                    'region' => $region,
                    'ville' => null,
                    'niveauLocalisation' => $niveauLocalisation,
        ));
    }

    /**
     * Lists all Location entities.
     *
     * @Template()
     */
    public function locByPaysAction($page, $pays) {
        $page = str_replace("page:", "", $page);
        $pays = str_replace("pays:", "", $pays);
        $em = $this->getDoctrine()->getManager();

        $nbAnnonce = $em->getRepository('LefDataBundle:Location')->countAll();
        $nombreItemPanierCompa = $this->container->getParameter('nbr_item_comparaison');

        // $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        $total = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, null, null, $pays, null, null);

        /* total of résultat */
        $total_articles = count($total);
        $articles_per_page = $this->container->getParameter('max_articles_on_listepage');
        $last_page = ceil($total_articles / $articles_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;

        /* résultat  à afficher */
        $max_articles_on_listepage = $this->container->getParameter('max_articles_on_listepage');
        $entities1 = $this->getDoctrine()->getRepository('LefDataBundle:Location')->getAllLocation($page, $articles_per_page, $max_articles_on_listepage, $pays);

        foreach ($entities1 as $ke => $entity) {
            $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
            if (!empty($mediasP)) {
                $entity->medias_p = $mediasP;
            } else {
                $entity->medias_p = null;
            }
        }

        $niveauLocalisation = 'pays';

        return array(
            'entities1' => $entities1,
            'nbrItemCompa' => $nombreItemPanierCompa,
            'nbAnnonce' => $nbAnnonce,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_articles' => $total_articles,
            'pays' => $pays,
            'region' => null,
            'ville' => null,
            'niveauLocalisation' => $niveauLocalisation,
        );
    }

    /**
     * Lists Location vacs entities.
     *
     * @Template()
     */
    public function locsPromoAction($type_location) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        return array(
            'entities' => $entities,
            'type_location' => $type_location,
        );
    }

    /**
     * Lists Location vacs entities.
     *
     * @Template()
     */
    public function locsVacsAction($type_location) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LefDataBundle:Location')->findAll();

        return array(
            'entities' => $entities,
            'type_location' => $type_location,
        );
    }

    /**
     * Finds and displays a Location entity.
     *
     * @Template()
     */
    public function showAction($reference) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Location')->findOneByReference($reference);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Location entity.');
        }

        $mediasP = $em->getRepository('LefDataBundle:MediaPartner')->findByLocation($entity->getReference());
        $entity->medias_p = $mediasP;

        $currentUrl = $this->getRequest()->getUri();

        return array(
            'entity' => $entity
        );
    }

}
