<?php

namespace Lef\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Locale\Locale;
use Lef\DataBundle\Controller\InterhomeWebServiceController;
use Lef\DataBundle\Controller\InterchaletWebServiceController;
use Lef\DataBundle\Controller\LeisureWebServiceController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SEOController
 *
 * @author Jian
 */
class SEOController extends Controller {

    //put your code here
    // Serach inside LEF
    // Search use google place api
    public function autocompleteAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request');
        $geo = $request->query->get('geo');
        $maxResult = $this->container->getParameter('max_results_location_seo_lef');

        $criteres = $this->_treatVisitorQuery($geo);

        $objectAnnounces = $em->getRepository('LefDataBundle:Location')->getAllLocationBySrchFormTop($criteres, NULL, NULL, NULL, NULL, NULL, NULL, NULL, $maxResult, NULL);
        $pays = $em->getRepository('LefDataBundle:Location')->retrieveLocalisationByGeo($criteres, $maxResult, 'pays');
        $regions = $em->getRepository('LefDataBundle:Location')->retrieveLocalisationByGeo($criteres, $maxResult, 'pays', 'region');
        $villes = $em->getRepository('LefDataBundle:Location')->retrieveLocalisationByGeo($criteres, $maxResult, 'pays', 'region', 'ville');
//        var_dump($objectAnnounces);
        $annonces = array();
        foreach ($objectAnnounces as $key => $object) {
            $annonces[$key]['id'] = $object->getId();
            $annonces[$key]['reference'] = $object->getReference();
            $annonces[$key]['titre'] = $object->getTitre();
            $annonces[$key]['ville'] = $object->getVille();
            $annonces[$key]['region'] = $object->getRegion();
            $annonces[$key]['pays'] = $object->getPays();
        }

        $geoLocalisations = array();
//        var_dump(count($pays));
        if (count($pays) > 0) {
            foreach ($pays as $key => $value) {
                $geoLocalisations['pays'] = array();
                $geoLocalisations['pays']['code'] = $value['pays'];
//                $geoLocalisations['pays']['nom'] = $value['pays'];
                // change 040814, use no more country code
                $geoLocalisations['pays']['nom'] = $this->_translateCountryCodeToName($value['pays']);
            }
        }
        if (count($regions) > 0) {
            $geoLocalisations['region'] = '';
            foreach ($regions as $key => $value) {
                $countryCode = $value['pays'];
                $value['pays'] = array();
                $value['pays']['code'] = $countryCode;
//                $value['pays']['nom'] = $countryCode;
                // change 040814, use no more country code
                $value['pays']['nom'] = $this->_translateCountryCodeToName($countryCode);
                $geoLocalisations['region'] = $value;
            }
        }
        if (count($villes) > 0) {
            $geoLocalisations['ville'] = '';
            foreach ($villes as $key => $value) {
                $countryCode = $value['pays'];
                $value['pays'] = array();
                $value['pays']['code'] = $countryCode;
//                $value['pays']['nom'] = $countryCode;
                // change 040814, use no more country code
                $value['pays']['nom'] = $this->_translateCountryCodeToName($countryCode);
                $geoLocalisations['ville'] = $value;
            }
        }


        //prepare the response, e.g.
        $response = array("code" => 100, "success" => true, "geolocalisations" => $geoLocalisations, "locations" => $annonces);
        //you can return result as JSON
        $json_response = json_encode($response);
        return new Response($json_response);
    }

    /**
     * Get range for slides surface and price
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRangeSPAction() {
        $em = $this->getDoctrine()->getManager();
        // format array, keys: min_surface, max_surface, min_prix, max_prix
        $ranges = $em->getRepository('LefDataBundle:Location')->getRangeSurfacePrix();

        $json_response = json_encode($ranges);
        return new Response($json_response);
    }

    ////////////// A le 050914 par Jian ///////////////////>>>>>>>>>>>>>>>>>>>>
    public function getCountSgAction() {
        $request = $this->container->get('request');
        $rq_fields = $request->query->get('fields');
        $rq_conditions = $request->query->get('conditions');
        // params defined in url path, like for Geoloc
        // format: {{[geoloc:{pays:france, ville:paris]}]}}
        $rq_conditionsInUrlPath = $request->query->get('conditionsInUrlPath');
        $dureeFirstMinute = $this->container->getParameter('duree_heures_first_minutes');

        $arry_fields = count($rq_fields) > 0 ? $rq_fields : NULL;
        $arry_conditions = count($rq_conditions) > 0 ? $rq_conditions : NULL;
        $arry_conditionsInUrlPath = count($rq_conditionsInUrlPath) > 0 ? $rq_conditionsInUrlPath[0] : NULL;

        $em = $this->getDoctrine()->getManager();
        $result = array();

        // get exclude list
        $excludeList = $this->_prepareExcludeList($arry_conditions);

        // Loop fields
        foreach ($arry_fields as $field) {
            $kvPair = explode(':', $field);
            $key = $kvPair[0];
            $value = $kvPair[1];
            $fieldData[$key] = $value;

            $qryResult = $em->getRepository('LefDataBundle:Location')->getCountSingle($fieldData, $arry_conditions, $arry_conditionsInUrlPath, $dureeFirstMinute, $excludeList);
            $result[$key . ':' . $value] = $qryResult;
        }

        $json_response = json_encode($result);
        return new Response($json_response);
    }

    ///////////////////////////////////////////////////////<<<<<<<<<<<<<<<<<<<<
    //
    //
    //
    //
    ///////////////////////////
    //// private functions ////
    ///////////////////////////
    private function _treatVisitorQuery($qry) {
        if (strchr($qry, ',')) {
            $qryArray = explode(',', $qry);
            $countries = $this->_countryNamesToCodes();
            foreach ($qryArray as $keyq => $valq) {
                $vq = trim(strtolower($valq));
                $qryArray[$keyq] = $vq;
                // translate from county name to country code
                foreach ($countries as $nomc => $codec) {
                    $nc = strtolower($nomc);
                    if ($vq == $nc) {
                        // ===change 040814, use no more country code
//                        $qryArray[$keyq] = $codec;
                    }
                }
                // escape single quote '
                if (strchr($valq, "'")) {
                    $qryArray[$keyq] = str_replace("'", "''", $valq);
                }
            }
        } else if (strchr($qry, ' ') && !strchr($qry, ',')) {
            $qryArray = explode(' ', $qry);
            $countries = $this->_countryNamesToCodes();
            foreach ($qryArray as $keyq => $valq) {
                $vq = trim(strtolower($valq));
                $qryArray[$keyq] = $vq;
                // translate from county name to country code
                foreach ($countries as $nomc => $codec) {
                    $nc = strtolower($nomc);
                    if ($vq == $nc) {
                        // ===change 040814, use no more country code
//                        $qryArray[$keyq] = $codec;
                    }
                }
                // escape single quote '
                if (strchr($valq, "'")) {
                    $qryArray[$keyq] = str_replace("'", "''", $valq);
                }
            }
        } else {
            $qryArray[0] = $qry;
            // translate country name to country code
            $countries = $this->_countryNamesToCodes();
            $vq = trim(strtolower($qry));
            $qryArray[0] = $vq;
            foreach ($countries as $nomc => $codec) {
                $nc = strtolower($nomc);
                if ($vq == $nc) {
                    // ===change 040814, use no more country code
//                    $qryArray[0] = $codec;
                }
            }
            // escape single quote '
            if (strchr($vq, "'")) {
                $qryArray[0] = str_replace("'", "''", $vq);
            }
        }
//        return $qryArray;
        return array('geoLoc' => $qryArray, 'dateArrivee' => NULL,
            'duree' => NULL, 'nbrPers' => NULL);
    }

    private function _translateCountryCodeToName($code) {
        $result = '';
        $countryCodeName = $this->_countryCodesToNames();
        foreach ($countryCodeName as $codec => $nomc) {
            if ($code == $codec) {
                $result = $nomc;
                break;
            } else {
                // ===change 040814, use no more country code
                $result = $code;
            }
        }
        return $result;
    }

    private function _countryNamesToCodes() {
        $paramLocale = $this->container->getParameter('locale');                // A le 010914 par Jian
        $countries = Locale::getDisplayCountries($paramLocale);                 // M le 010914 par Jian
        $translator = $this->get('translator');
        $codeCountry = array();

        foreach ($countries as $key => $country) {
            $countryName = $translator->trans($country, array(), null, $paramLocale);
            $codeCountry[$countryName] = $key;
        }

        return $codeCountry;
    }

    private function _countryCodesToNames() {
        $paramLocale = $this->container->getParameter('locale');                // A le 010914 par Jian
        $countries = Locale::getDisplayCountries($paramLocale);                 // M le 010914 par Jian
        return $countries;
    }

    // ============================================================
    // ====== use webservice to get  availability for filter ======
    // ============================================================
    /**
     * Function get excluded(locations) liste
     * A le 170914 par Jian
     * @param integer $duree days
     * @param date $dateArrivee arrival date
     */
    private function _getLocPartnerNonAvailable($duree, $dateArrivee = NULL) {
        $excludeList = array();
        $dateArriveeStandard = $dateArrivee == NULL ? date('Y-m-d') : \DateTime::createFromFormat('d/m/Y', $dateArrivee)->format('Y-m-d');
        $dateDepartStandard = defined($duree) && $duree != NULL ? date('Y-m-d', strtotime($dateArriveeStandard . '+ ' . $duree . ' days')) : date('Y-m-d', strtotime($dateArriveeStandard . '+ 7 days'));
        $allLocPartner = $this->_getAllLocationsPartner();

        foreach ($allLocPartner as $loc) {
            $availability = $this->_getPartnerAvailabilityByWebservice($loc['reference'], $loc['brandPartner'], $dateArriveeStandard, $dateDepartStandard, $loc['rentalprice']);
            // if not available
            if ($availability != NULL && $availability == 'N') {
                $excludeList[] = $loc['id'];
            }
        }
        return $excludeList;
    }

    /**
     * Function get all locations who have attribute brandpartner not null
     * A le 170914 par Jian
     * @return array
     */
    private function _getAllLocationsPartner() {
        $em = $this->getDoctrine()->getManager();
        $locations = $em->getRepository('LefDataBundle:Location')->getAllLocPartner();
        return $locations;
    }

    /**
     * Function get availability by using webservice
     * A le 170914 par Jian
     * @param string $reference
     * @param string $partnerBrand
     * @param date $dateArrivee
     * @param date $dateDepart
     * @return string
     */
    private function _getPartnerAvailabilityByWebservice($reference, $partnerBrand, $dateArrivee, $dateDepart, $price) {
        $availability = NULL;
        switch ($partnerBrand) {
            case 'interhome':
//                $webService = new InterhomeWebServiceController();
                $webService = $this->container->get('lef_data.web_service.interhome');
                $checkAvailability = $webService->availability($reference, $dateArrivee, $dateDepart);
                $canCheckAvailability = true;
                if ($checkAvailability->Ok) {
                    $availability = substr($checkAvailability->State, -1);
//                    $availability = str_replace(array('N', 'Y', 'Q'), array('Occupée', 'Disponible', 'Sur demande'), $availability);
                }
                break;

            case 'interchalet':
                $webService = new InterchaletWebServiceController();
                $canCheckAvailability = false;
                $availability = NULL;
                break;

            case 'leisure':
//                $webService = new LeisureWebServiceController();
                $webService = $this->container->get('lef_data.web_service.leisure');
                $checkAvailability = $webService->checkAvailability($reference, $dateArrivee, $dateDepart, $price);
                $canCheckAvailability = true;
                $availability = property_exists($checkAvailability, 'Available') ? $checkAvailability->Available : NULL;
                $availability = $availability == NULL ? NULL : str_replace(array('No', 'Yes'), array('N', 'Y'), $availability);
                break;

            default:
                break;
        }
        return $availability;
    }

    /**
     * 
     * @param type $condiArry
     * @return array
     */
    private function _prepareExcludeList($condiArry = NULL) {
        $excludeList = NULL;
        $duree = NULL;
        $dateArrivee = NULL;

        if ($condiArry == NULL) {
            return NULL;
        }

        foreach ($condiArry as $condi) {
            $subPair = explode(':', $condi);
            $k = $subPair[0];
            $v = $subPair[1];
            if ($k == '__duree') {
                $duree = $v;
            }
            if ($k == '__dateArrivee') {
                $dateArrivee = urldecode($v);
            }
        }

        $excludeList = $this->_getLocPartnerNonAvailable($duree, $dateArrivee);
        return $excludeList;
    }

}
