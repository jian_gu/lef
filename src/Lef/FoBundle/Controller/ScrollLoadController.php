<?php

namespace Lef\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class ScrollLoadController extends Controller {

    public function indexAction() {
        $request = $this->container->get('request');
        $position = $request->query->get('position');
        $itemsPerGroup = $this->container->getParameter('max_articles_on_listepage');
        ;
        $result = new \stdClass();
        $result->content = $this->_getLocationsByGroup($position, $itemsPerGroup);
        $resultArrayFy = (array) $result;

        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new
            JsonEncoder()));
        $resultJsonFy = $serializer->serialize($resultArrayFy, 'json');
        $resultUtf8Fy = utf8_encode($resultJsonFy);

//        echo('<pre>');
//        var_dump($resultUtf8Fy);
//        echo('</pre>');
        return new Response($resultUtf8Fy);
    }

    private function _getLocationsByGroup($position, $itemsPerGroup) {
        $em = $this->getDoctrine()->getManager();
        $itemGroup = $em->getRepository('LefDataBundle:Location')->getMoreByGroup('array', $position, $itemsPerGroup);
        if (is_array($itemGroup)) {
//            echo('<pre>');
//            var_dump($itemGroup);
//            echo('</pre>');
            foreach ($itemGroup as $key => $location) {
                // Get media partner
                $mediasArray = $this->_getPartnerImagesByLocationRef($location['reference']);
                // Get media lef if exist
                if (in_array($location['brandPartner'], array('lef', 'LEF', 'louerenfrance', 'louer-en-france'))) {
                    $mediasArray = $this->_getMediaLefByLocId($location['id']);
                    $itemGroup[$key]['medias'] = (array) $mediasArray;
                }
                if (count($mediasArray) >= 1) {
                    $itemGroup[$key]['media_p'] = (array) $mediasArray;
//                    $itemGroup[$key]['media_p'] = json_encode((array)$mediasArray);
                }
            }
        }
        return $itemGroup;
    }

    private function _getPartnerImagesByLocationRef($ref) {
        $em = $this->getDoctrine()->getManager();
        $itemGroup = $em->getRepository('LefDataBundle:MediaPartner')->findByLocationRef($ref);
        return $itemGroup;
    }

    private function _getMediaLefByLocId($id) {
        $em = $this->getDoctrine()->getManager();
        $medias = $em->getRepository('LefDataBundle:Location')->getMediaLefByLocId($id);

//        echo('<pre>');
//        var_dump($medias);
//        echo('</pre>');
        return (array) $medias;
    }

}
