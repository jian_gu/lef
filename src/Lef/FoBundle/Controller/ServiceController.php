<?php

/**
 * Controller offers web services
 */

namespace Lef\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class ServiceController extends Controller {

    protected static $RESULTS_ARRAY_OBJECT;

    public function mapJsonFluxAction() {
//        $boundsResults = '{"e":3.161005,"n":45.3235811,"w":2.6453759,"s":40.182053}';
//        $boundsMap = '{"e":3.761005,"n":50.3235811,"w":-1.1453759,"s":10.182053}';

        $request = $this->container->get('request');
        $boundsResultsArray = $request->request->get('boundsResults');
        $boundsMapArray = $request->request->get('boundsMap');
        $paysConstrain = $request->request->get('pays');
        $regionConstrain = $request->request->get('region');
        $villeConstrain = $request->request->get('ville');
        $queryData = $request->query->all();

        $em = $this->getDoctrine()->getManager();

        $dqmHead = "SELECT DISTINCT "
                . "lc.reference, lc.pays, lc.longitude, lc.latitude "
                . "FROM LefDataBundle:Location lc WHERE lc.id > 0 ";

        ///////////////////////////////
        ///////////////////////////////
        ///////// BOUNDS MAP //////////

        $dqlMapSize = $dqmHead . "AND lc.longitude > " . $boundsMapArray['w']
                . " AND lc.longitude < " . $boundsMapArray['e']
                . " AND lc.latitude > " . $boundsMapArray['s']
                . " AND lc.latitude < " . $boundsMapArray['n'] . " ";
//        if ($paysConstrain != NULL) {
//            $dqlMapSize = $dqlMapSize . " AND lc.pays = '" . $paysConstrain . "' ";
//        }
//        var_dump($request->request);
        $dqmEnd = $dqlMapSize. "GROUP BY lc.longitude, lc.latitude ";
        $queryBoundsMap = $em->createQuery($dqmEnd);
        $resultInBoundsMap = $queryBoundsMap->getResult();
        ///////////////////////////////
        //
        // If results bounds is defined, means results should be in certain zone
        if (is_array($boundsResultsArray) && count($boundsResultsArray) > 0) {
            ///////////////////////////////
            ///// Result in map bounds /////
            if (count($resultInBoundsMap) <= 0) {
                self::$RESULTS_ARRAY_OBJECT = 'No result found.';
            } else {
                if ($paysConstrain != NULL) {
                    self::$RESULTS_ARRAY_OBJECT = $this->_addIdentifierToResultNeeded($resultInBoundsMap, $boundsResultsArray, $paysConstrain);
                } else {
                    self::$RESULTS_ARRAY_OBJECT = $this->_addIdentifierToResultNeeded($resultInBoundsMap, $boundsResultsArray);
                }
            }
        } else {
            if (count($resultInBoundsMap) <= 0) {
                self::$RESULTS_ARRAY_OBJECT = 'No result found.';
            } else {
                self::$RESULTS_ARRAY_OBJECT = $this->_addIdentifierToResultNeeded($resultInBoundsMap, $boundsMapArray);
            }
            ///////////////////////////////
        }

        ///////////////////////////////
        ///////////////////////////////
        ///////////////////////////////


        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new
            JsonEncoder()));
//        var_dump(self::$RESULTS_ARRAY_OBJECT);
        $resultBoundsJsonFy = $serializer->serialize(self::$RESULTS_ARRAY_OBJECT, 'json');
        $resultBoundsUtf8Fy = utf8_encode($resultBoundsJsonFy);
        return new Response($resultBoundsUtf8Fy);
//        $resultMapSizeJsonFy = $serializer->serialize($resultMapSize, 'json');
//        $resultMapSizeUtf8Fy = utf8_encode($resultMapSizeJsonFy);
//        return new Response($resultMapSizeUtf8Fy);
    }

    /**
     * 
     * @param array $rawResultsArray
     * @param object $boundsResultsArray
     * @return boolean
     */
    private function _addIdentifierToResultNeeded($rawResultsArray, $boundsResultsArray, $pays = NULL) {
        foreach ($rawResultsArray as $key => $value) {
            if ($value['latitude'] < $boundsResultsArray['n'] && $value['latitude'] > $boundsResultsArray['s'] && $value['longitude'] < $boundsResultsArray['e'] && $value['longitude'] > $boundsResultsArray['w']) {
                if ($pays != NULL && $value['pays'] == $pays) {
                    $rawResultsArray[$key]['inresult'] = TRUE;
                } else {
                    $rawResultsArray[$key]['inresult'] = FALSE;
                }
            } else {
                $rawResultsArray[$key]['inresult'] = FALSE;
            }
        }
        return $rawResultsArray;
    }

    public function locationJsonAction() {
        $request = $this->container->get('request');
        $reference = $request->request->get('ref');

        $em = $this->getDoctrine()->getManager();

        $dql = "SELECT DISTINCT lc.reference, lc.rentalPrice, lc.titre, "
                . "lc.pays, lc.region, lc.ville, lc.nombreDePersonne, "
                . "lc.nombreEtoiles, lc.nombrePiece, lc.nombreChambre, "
                . "lc.brandPartner "
                . "FROM LefDataBundle:Location lc WHERE lc.reference = '" . $reference . "' ";

        $queryBoundsMap = $em->createQuery($dql);
        $infosLocation = $queryBoundsMap->getResult();

        if (is_array($infosLocation) && count($infosLocation) > 0) {
            $results = $infosLocation[0];
        } else {
            $results = 'NO INFOS FOUND.';
        }

        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new
            JsonEncoder()));

        $resultBoundsJsonFy = $serializer->serialize($results, 'json');
        $resultBoundsUtf8Fy = utf8_encode($resultBoundsJsonFy);
        return new Response($resultBoundsUtf8Fy);
    }

}
