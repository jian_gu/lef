<?php

namespace Lef\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class StaticController extends Controller
{
    public function indexAction($page)
    {
        if ($page != null) {
        	if ( $this->get('templating')->exists('LefFoBundle:Static:'.$page.'.html.twig') ) {
	            return $this->render('LefFoBundle:Static:'.$page.'.html.twig', array('titrePage'=>ucfirst($page), 'page'=>$page));
			}else{
	            return $this->render('LefFoBundle::erreur404.html.twig', array('page'=>$page));
			}
        }
        else{
            return $this->redirect($this->generateUrl('home'));
        }
    }
}
