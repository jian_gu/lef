<?php

namespace Lef\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lef\DataBundle\Entity\Annonces;
use Lef\FoBundle\Form\SearchFormType;
use Symfony\Component\HttpFoundation\Response;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AnnoncesController
 *
 * @author HugoG
 */
class AnnoncesController extends Controller {

    /**
     * Lists all Annonces entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

//        $dql = "SELECT a FROM LefDataBundle:Annonces a";
//        $query = $em->createQuery($dql);
//        $entities = $em->getRepository('LefDataBundle:Annonces')->findAll();
//        $entities = $em->getRepository('LefDataBundle:Annonces')->getLimitedAnnonces(5);

        $query = $em->getRepository('LefDataBundle:Annonces')->getAllQry();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $this->get('request')->query->get('page', 1)/* page number */, 10/* limit per page */
        );

//        $entity = new Annonces();
//        $form   = $this->createSearchForm($entity);

        return $this->render('LefFoBundle:Annonces:index.html.twig', array(
                    'pagination' => $pagination,
                        //'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays an Annonce entity.
     *
     */
    public function showAction($immoid) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LefDataBundle:Annonces')->find($immoid);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Annonces entity.');
        }

        return $this->render('LefFoBundle:Annonces:show.html.twig', array(
                    'entity' => $entity,
        ));
    }

    
    public function filterAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request');
        $criteres = $request->get('criteres');
        
        $query = $em->getRepository('LefDataBundle:Annonces')->filtreAnnonceQry($criteres);
        
        //
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, $this->get('request')->query->get('page', 1)/* page number */, 10/* limit per page */
        );
        //prepare the response, e.g.
        //$response = array("code" => 100, "success" => true);
        //you can return result as JSON
        //return new Response(json_encode($response));
        return $this->render('LefFoBundle:Annonces:__listeAnnonces.html.twig', array(
                    'pagination' => $pagination,
        ));
    }

}
