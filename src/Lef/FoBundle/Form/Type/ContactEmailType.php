<?php
namespace Lef\FoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactEmailType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array())
                ->add('prenom', 'text', array())
                ->add('societe', 'text', array('required' => FALSE))
                ->add('phone', 'text', array())
                ->add('email', 'email', array())
                ->add('subject', 'text', array())
                ->add('contents', 'textarea', array())
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'lef_fobundle_contact_email';
    }

}

