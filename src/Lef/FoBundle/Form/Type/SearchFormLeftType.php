<?php

namespace Lef\FoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SearchFormLeftType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('types', 'entity', array('class' => 'LefDataBundle:Type', 
                    'expanded' => TRUE, 'multiple' => TRUE, 
                    'required' => FALSE,
                    'attr'=> array('name' => '03')))
                ->add('equipements', 'entity', array('class' => 'LefDataBundle:Equipement', 
                    'expanded' => TRUE, 'multiple' => TRUE, 
                    'required' => FALSE,
                    'attr'=> array('name' => '04')))
                ->add('bienEtres', 'entity', array('class' => 'LefDataBundle:BienEtre', 
                    'expanded' => TRUE, 'multiple' => TRUE, 
                    'required' => FALSE,
                    'attr'=> array('name' => '05')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
                // 'data_class' => 'Lef\DataBundle\Entity\Map'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return '';
    }

}
