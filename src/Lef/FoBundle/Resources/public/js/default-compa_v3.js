/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    var $_maxItemCompaAuthorise = $('#p-nbr-item-compa-authorise').attr('value'),
            $_panierContents = $('#div-thumbnail-compa'),
            $_modalBody = $('#div-modal-body'),
            $_btnAddCompa = $('[class*="btn-add-compa"]'),
            $_btnRemoveCompa = $('[class*="btn-remove-compa"]'),
            $_btnStartCompa = $('#btn-comparer-item-compa'),
            $_btnEmptyCompa = $('#btn-clear-item-compa');

    prepareWithCookie();
    activDesactivBtnStartCompa();
    feedCompa('panier');
    feedCompa('modal');

// ====================== CLICK ACTIONS

    // Comparer button clicked
    $_btnAddCompa.on('click', function() {
        var itemId = $(this).attr('value');
        $btnAddCompaClicked(itemId);
    });
    function $btnAddCompaClicked($id) {
        _setItemToCookie($id);
        feedCompa('panier');
        feedCompa('modal');
        _refeshCompa();
        activDesactivBtnStartCompa();
        hideShowBtnsAddCompa();

        generateRemoveButton($id);
        console.log('btn compa clicked');
    }

    // Remove an item form panier by clicking button retirer
    $_btnRemoveCompa.on('click', function() {
        $id = $(this).attr('value');
        btnRemoveCompaClicked($id);
    });

    function btnRemoveCompaClicked($itemId) {
        _unsetItemFromCookie($itemId);
        feedCompa('panier');
        feedCompa('modal');
        _refeshCompa();
        removeBtnRmCompaRecoverButtonAddCompa($itemId);
        activDesactivBtnStartCompa();
        hideShowBtnsAddCompa();
        console.log('btn remove BIG compa clicked');
    }

    // Empty the comparison basket
    $_btnEmptyCompa.on('click', function() {
        _emptyCookieItems();
        emptyItemsCompa();
    });

    function emptyItemsCompa() {
        activeAllButtonAddCompa();
        $.each($('[class*="btn-remove-compa"]'), function() {
            $id = $(this).attr('value');
            removeBtnRmCompaRecoverButtonAddCompa($id);
        });
        activDesactivBtnStartCompa();
        hideShowBtnsAddCompa();
        feedCompa('panier');
        feedCompa('modal');
        _refeshCompa();
        console.log('btn empty compa clicked');
    }

// ====================== BUTTONS

    function hideButtonsAddCompa() {
        $('.btn-add-compa:not(:disabled)').addClass('hidden');
    }

    function activeAllButtonAddCompa() {
        $_btnAddCompa.removeAttr('disabled');
    }

    function showButtonsAddCompa() {
        $('.btn-add-compa.hidden').removeClass('hidden');
    }
    function activDesactivBtnStartCompa() {
        var $itemsInCookie = _getCountItemsCompaInCookie();
        if ($itemsInCookie < 1) {
            $_btnStartCompa.attr('disabled', 'disabled');
            $_btnEmptyCompa.attr('disabled', 'disabled');
        } else {
            if ($itemsInCookie < 2) {
                $_btnStartCompa.attr('disabled', 'disabled');
            } else {
                $_btnStartCompa.removeAttr('disabled');
            }

            $_btnEmptyCompa.removeAttr('disabled');
        }

    }

    function hideShowBtnsAddCompa() {
        if (!_getItemsInCookie()) {
            return;
        }
//        var $itemsInPanier = _getCountItemsCompaInCookie();
        var $itemsInCookie = _getItemsInCookie(),
                $cookieItemCount = $itemsInCookie.length;
        if ($cookieItemCount < $_maxItemCompaAuthorise) {
            showButtonsAddCompa();
        } else {
            hideButtonsAddCompa();
        }

    }

//    function activeButtonAddCompa($id) {
//        $('.btn-add-compa[id="btn-add-compa-' + $id + '"]').removeAttr('disabled');
//    }

    function generateRemoveButton($id) {
        var $btnAddCompa = $('[id="btn-add-compa-' + $id + '"]'),
                $btnRm1 = $('<button class="btn btn-primary btn-xs btn-remove-compa" id="btn-remove-compa-' + $id + '" value="' + $id + '">Retirer du comparateur</button>');
        $btnAddCompa.replaceWith($btnRm1);

        $btnRm1.on('click', function() {
            btnRemoveCompaClicked($id);
            console.log('yo-');
        });

    }

    function removeBtnRmCompaRecoverButtonAddCompa($id) {
        var $btnRm2 = $('[id="btn-remove-compa-' + $id + '"]'),
                $btnAdd2 = $('<button class="btn btn-primary btn-add-compa btn-xs" id="btn-add-compa-' + $id + '" value="' + $id + '">Comparer</button>');
        $btnRm2.replaceWith($btnAdd2);

        $btnAdd2.on('click', function() {
            $btnAddCompaClicked($id);
            console.log('yo+2');
        });
        console.log('recover add button');
    }


// ============================================

// ====================== COOKIES

    function _setItemToCookie($id) {
        // if cookie exists
        if ($.cookie('items_compa')) {
            var $idArray = [$.cookie('items_compa')];
            $idArray.push($id);

            $.cookie('items_compa', $idArray, {path: '/'});
            console.log('.A executed');
        } else {
            var $idN = [$id];

            $.cookie('items_compa', $idN, {path: '/'});
            console.log('.N executed');
        }

        console.log($.cookie('items_compa'));
    }

    function _unsetItemFromCookie($idStr) {
        if (!_getItemsInCookie()) {
            return;
        }

        var $id = parseInt($idStr),
                $itemsInCookie = _getItemsInCookie();

        // there are more than One item in the backet
        if ($itemsInCookie.length > 1 && $.inArray($id, $itemsInCookie) > -1) {
            console.log('lng>1');
            var $arrayInCookie = $itemsInCookie;

            $arrayInCookie = $.grep($arrayInCookie, function(value) {
                return value !== $id;
            });
            $.cookie('items_compa', $arrayInCookie, {path: '/'});

        }
        // there are only One item in the backet
        if ($itemsInCookie.length === 1 && $.inArray($id, $itemsInCookie) === 0 && $itemsInCookie[0] === $id) {
            console.log('lng=1');
            $.removeCookie('items_compa', {path: '/'});
        }
    }

    function _emptyCookieItems() {
        if ($.cookie('items_compa')) {
            $.removeCookie('items_compa', {path: '/'});
        }
        return;
    }

    /**
     * 
     * @returns {array}
     */
    function _getItemsInCookie() {
        if ($.cookie('items_compa')) {
            var $idStr = $.cookie('items_compa'),
                    $idStrPrepa = '[' + $idStr + ']';
            return $idArry = JSON.parse($idStrPrepa);
        }
    }

    /**
     * 
     * @returns {Number}
     */
    function _getCountItemsCompaInCookie() {
        var $items = $.cookie('items_compa');
        if (!$items) {
            return 0;
        }
        return $count = $items.split(',').length;

    }

    /**
     * 
     * @returns {undefined}
     */
    function prepareWithCookie() {
        if (!_getItemsInCookie()) {
            return;
        }
        // if cookie has items compa

        var $itemsInCookie = _getItemsInCookie();

        $.each($itemsInCookie, function(key, val) {
            generateRemoveButton(val);
            hideShowBtnsAddCompa();
        });
    }


    function feedCompa($container) {
        $.ajax({
            url: "/compa/feed",
            data: {
                container: $container
            },
            success: function(data) {
                switch ($container) {
                    case 'panier':
                        $_panierContents.children().remove();
                        $_panierContents.append(data);

                        // remove item from panier by clicking the cross x
                        $('[class*="cross-remove-compa"]').on('click', function() {
                            var $id = $(this).attr('id');
                            btnRemoveCompaClicked($id);
                            console.log('cross "X" remove compa clicked');
                        });
                        break;
                    case 'modal':
                        $_modalBody.children().remove();
                        $_modalBody.append(data);
                        break;
                    default:
                        break;
                }
            },
            error: function(xhr, status, error) {
                console.log(status + '; ' + error);
            }
        });
    }

    function _refeshCompa() {
        // panier
        $('#div-location-comparator')
                .fadeOut(200)
                .fadeIn(function() {
//                            $('#div-panier-external').append(data);
                }).delay();

        // modal
        $('#div-modal-body')
                .fadeOut()
                .fadeIn().delay();
    }

});
