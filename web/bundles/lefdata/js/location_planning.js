/* 
 * @Author abdel le 15/12/2014
 */

$(document).ready(function () {
    var tdatesBrut = new Array();
    var tdatesFormat = new Array();

//    period tarifaire ajouter par abdel
    $('#dateBeginPeriod').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '0d',
        weekStart: 1,
        autoclose: true,
        todayHighlight: true,
        language: 'fr',
        clearBtn: true,
    }).on('changeDate', function (e) {
        $('#dateBegin').val(e.format('dd/mm/yyyy'));
    });
    $('#dateFinishPeriod').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '0d',
        weekStart: 1,
        autoclose: true,
        todayHighlight: true,
        language: 'fr',
        clearBtn: true,
    }).on('changeDate', function (e) {
        $('#dateFinish').val(e.format('dd/mm/yyyy'));
    });

    $("#saveCalendar").click(function () {

        var idLocation = $("#idLocation").val();
        var titrePeriod = $("#titreTranche").val();
        var priceOneDay = $("#priceOneDay").val();
        var priceWeekEnd = $("#priceWeekEnd").val();
        var priceOneWeek = $("#priceOneWeek").val();
        var priceTwoWeek = $("#priceTwoWeek").val();
        var priceThreeWeek = $("#priceThreeWeek").val();
        var priceMonth = $("#priceMonth").val();
        var dateBeginPeriod = $("#dateBegin").val();
        var dateFinishPeriod = $("#dateFinish").val();
        //        var route ="http://"+_getBaseURL()+"/profile/location/editCalendar";
        var route = "http://localhost/lef/web/app_dev.php/profile/location/addTranchePrix";
        $.ajax({
            type: "POST",
            url: route,
            data: {
                idLocation: idLocation,
                titrePeriod: titrePeriod,
                priceOneDay: priceOneDay,
                priceWeekEnd: priceWeekEnd,
                priceOneWeek: priceOneWeek,
                priceTwoWeek: priceTwoWeek,
                priceThreeWeek: priceThreeWeek,
                priceMonth: priceMonth,
                dateBeginPeriod: dateBeginPeriod,
                dateFinishPeriod: dateFinishPeriod

            },
            success: function (data)
            {
                hideModal();
//                alert (data);
                var objet = $.parseJSON(data);
                $("#tpinit").empty();
                for (var i = 0; i < objet.length; i++) {
                    var blockBaliseTranchePrix = " <div class='form-group row'>"
                            + "<div class='col-md-6 col-sm-6'>"
                            + "<div class='col-md-6 col-sm-6'>"
                            + "<div class='form-group'>"
                            + "<div class='input-group date' id='datetimepickerDebut'>"
                            + "<input class='form-control' type='text' name='lef_databundle_location[tranchePrix][" + i + "][dateDebut]' value='" + objet[i][1] + "'/>"
                            + "<span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>"
                            + "</div>"
                            + "</div>"
                            + "</div>"
                            + "<div class='col-md-6 col-sm-6'>"
                            + "<div class='form-group'>"
                            + "<div class='input-group date' id='datetimepickerFin'>"
                            + "<input class='form-control' type='text' name='lef_databundle_location[tranchePrix][" + i + "][dateFin]' value='" + objet[i][2] + "'/>"
                            + "<span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>"
                            + "</div>"
                            + "</div>"
                            + "</div>"
                            + "</div>"
                            + "</div>"
                            + "<div class='form-group row'>"
                            + "<div class='col-md-8 col-sm-8'>"
                            + "<div>"
                            + "<input class='form-control' type='text' name='lef_databundle_location[tranchePrix][" + i + "][titre]' value='" + objet[i][0].titre + "'/>"
                            + "</div>"
                            + "</div>"
                            + "<div class='form-group row'>"
                            + "<div class='col-md-8 col-sm-8'>"
                            + "<div class='col-md-6 col-sm-6'>"
                            + "   <div class='form-group'>"
                            + "<div class='input-group'>"
                            + "<input class='form-control' type='text' name='lef_databundle_location[tranchePrix][" + i + "][day]' value='" + objet[i][0].day + "'/>"
                            + "<span class='input-group-addon'><span class='glyphicon glyphicon-euro'></span></span>"
                            + "</div>"
                            + "</div>"
                            + "</div>"
                            + "<div class='col-md-6 col-sm-6'>"
                            + "   <div class='form-group'>"
                            + "<div class='input-group'>"
                            + "<input class='form-control' type='text' name='lef_databundle_location[tranchePrix][" + i + "][weekend]' value='" + objet[i][0].weekend + "'/>"
                            + "<span class='input-group-addon'><span class='glyphicon glyphicon-euro'></span></span>"
                            + "</div>"
                            + "</div>"
                            + "</div>"
                            + "<div class='col-md-6 col-sm-6'>"
                            + "   <div class='form-group'>"
                            + "<div class='input-group'>"
                            + "<input class='form-control' type='text' name='lef_databundle_location[tranchePrix][" + i + "][week]' value='" + objet[i][0].week + "'/>"
                            + "<span class='input-group-addon'><span class='glyphicon glyphicon-euro'></span></span>"
                            + "</div>"
                            + "</div>"
                            + "</div>"
                            + "<div class='col-md-6 col-sm-6'>"
                            + "   <div class='form-group'>"
                            + "<div class='input-group'>"
                            + "<input class='form-control' type='text' name='lef_databundle_location[tranchePrix][" + i + "][weeks2]' value='" + objet[i][0].weeks2 + "'/>"
                            + "<span class='input-group-addon'><span class='glyphicon glyphicon-euro'></span></span>"
                            + "</div>"
                            + "</div>"
                            + "</div>"
                            + "<div class='col-md-6 col-sm-6'>"
                            + "   <div class='form-group'>"
                            + "<div class='input-group'>"
                            + "<input class='form-control' type='text' name='lef_databundle_location[tranchePrix][" + i + "][weeks3]' value='" + objet[i][0].weeks3 + "'/>"
                            + "<span class='input-group-addon'><span class='glyphicon glyphicon-euro'></span></span>"
                            + "</div>"
                            + "</div>"
                            + "</div>"
                            + "<div class='col-md-6 col-sm-6'>"
                            + "   <div class='form-group'>"
                            + "<div class='input-group'>"
                            + "<input class='form-control' type='text' name='lef_databundle_location[tranchePrix][" + i + "][month]' value='" + objet[i][0].month + "'/>"
                            + "<span class='input-group-addon'><span class='glyphicon glyphicon-euro'></span></span>"
                            + "</div>"
                            + "</div>"
                            + "</div>"

                            + "</div>"
                            + "</div>"
                            + "</div>";

                    $("#tpinit").append(blockBaliseTranchePrix);


                }

            }
            ,
            error: function (xhr, status, error) {
                console.log(status + '; ' + error);
            }});
    });
    //   fin  period tarifaire 

//    planning ajouter par abdel
    $('#datePlanning').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '0d',
        weekStart: 1,
        autoclose: true,
        multidate: true,
        todayHighlight: true,
        language: 'fr',
        clearBtn: true,
    }).on('changeDate', function (e) {
        tdatesBrut = e.dates;

    });


    $("#savePlanning").click(function () {

        for (var i = 0; i < tdatesBrut.length; i++) {
            var jour = tdatesBrut[i].getDate();
            var mois = tdatesBrut[i].getMonth() + 1;
            var annee = tdatesBrut[i].getFullYear();
            tdatesFormat[i] = jour + "/" + mois + "/" + annee;
        }

        var idLocation = $("#idLocation").val();
        var statut = $("#statutPlanning").val();
        //        var route ="http://"+_getBaseURL()+"/profile/location/editCalendar";
        var route = "http://localhost/lef/web/app_dev.php/profile/location/majPlanning";
        $.ajax({
            type: "POST",
            url: route,
            data: {
                idLocation: idLocation,
                datePlanning: tdatesFormat,
                statut: statut
            },
            success: function (data)
            {
                alert(data);
//                alert($.parseJSON(data));
////                $('#datePlanning').datepicker({
////                    format: 'dd/mm/yyyy',
////                    startDate: '0d',
////                    weekStart: 1,
////                    autoclose: true,
////                    multidate: true,
////                    todayHighlight: true,
////                    language: 'fr',
////                    clearBtn: true,
////                    beforeShowDay: function (date) {
////                            switch (date.getDate()) {
////                                case 4:
////                                    return {
////                                        tooltip: 'Example tooltip',
////                                        classes: 'active'
////                                    };
////                                case 8:
////                                    return false;
////                                case 12:
////                                    return "green";
////                            }
////                    }
//                }).on('changeDate', function (e) {
//                    tdatesBrut = e.dates;
//
//                });
            }
            ,
            error: function (xhr, status, error) {
                console(status + '; ' + error);
            }});
    });
});

function _getBaseURL() {
    var url = location.href;
    var baseURL = url.substring(0, url.indexOf('/', 14));
    if (baseURL.indexOf('http://localhost') != -1) {
        var pathname = location.pathname;
        var index1 = url.indexOf(pathname);
        var index2 = url.indexOf("/", index1 + 1);
        var baseLocalUrl = url.substr(0, index2);
        return baseLocalUrl;
    }
    else {
        return baseURL;
    }
}
function hideModal() {
    $("#modalTarif").removeClass("in");
    $(".modal-backdrop").remove();
    $("#modalTarif").hide();
    $('body').removeClass("modal-open");
}