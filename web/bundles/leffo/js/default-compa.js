/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//$(document).ready(function() {
//    var $itemsCompaInCookie = $('#p-item-compa-record').attr('value');
//
//    $('#spn-nbr-items-compa').text($itemsCompaInCookie);
//
//    // when button comparaison is clicked
//    $('.btn-add-compa').click(function() {
//        // get current location ID value
//        $loc_id = $(this).attr('value');
//        // get url path
//        $path = $(this).attr('path');
//        compaInsertItemToCookie($loc_id, $path);
//    });
//
//    // Empty the comparison basket
//    $('#btn-clear-item-compa').click(function() {
//        $path = $(this).attr('path');
//        clearItemCompaInCookie($path);
//    });
//
//    function compaInsertItemToCookie(loc_id, path) {
//        $.ajax({
//            url: path,
//            data: {
//                loc_id: loc_id
//            },
//            success: function(data)
//            {
//                $.cookie('items_compa', data);
//                console.log($.cookie('items_compa'));
//                $('#p-item-compa-record').attr('value', data);
//
//                $('#spn-nbr-items-compa').text('');
//                $('#spn-nbr-items-compa').text($itemsCompaInCookie);
//                console.log('ajax1 executed');
//            }
//        });
//    }
//
//    function clearItemCompaInCookie(path) {
//        $.ajax({
//            url: path,
//            data: {
//            },
//            success: function(data)
//            {
//                $('#spn-nbr-items-compa').text('');
//                console.log('ajax2 executed');
//            }
//        });
//
////        $.removeCookie('items_compa');
//    }
//
//});

$(document).ready(function() {
    var $_maxItemCompaAuthorise = $('#p-nbr-item-compa-authorise').attr('value'),
            $_modalBody = $('#div-modal-body'),
            $_panierCompaFace = $('div#div-panier-comparaison'),
            $_btnAddCompa = $('[class*="btn-add-compa"]'),
            $_btnRemoveCompa = $('[class*="btn-remove-compa"]'),
            $_btnStartCompa = $('#btn-comparer-item-compa'),
            $_btnEmptyCompa = $('#btn-clear-item-compa');

    activDesactivBtnStartCompa();

// ======================

    // Comparer button clicked
    $_btnAddCompa.on('click', function() {
        itemId = $(this).attr('value');
        btnAddCompaClicked(itemId);
    });
    function btnAddCompaClicked($id) {
//        disableButtonAddCompa($id);
        addThumbNailToPanierCompaFace($id);
        appendHiddenDivsToModalBody($id);
        activDesactivBtnStartCompa();
        hideShowBtnsAddCompa();

        generateRemoveButton($id);
        console.log('btn compa clicked');
    }
//    $_btnAddCompa.click(function() {
//        disableButtonAddCompa($(this));
//        addThumbNailToPanierCompaFace($(this));
//        appendHiddenDivsToModalBody($(this));
//        activDesactivBtnStartCompa();
//        hideShowBtnsAddCompa();
//
//        generateRemoveButton($(this));
//        console.log('btn compa clicked');
//    });

    // Remove an item form panier by clicking button retirer
    $_btnRemoveCompa.on('click', function() {
        $id = $(this).attr('value');
        btnRemoveCompaClicked($id);
    });

    function btnRemoveCompaClicked($itemId) {

        removeItemFromModalBody($itemId);
        removeThumbNailFromPanierFace($itemId);
//        activeButtonAddCompa($itemId);
        removeBtnRmCompaRecoverButtonAddCompa($itemId);
        activDesactivBtnStartCompa();
        hideShowBtnsAddCompa();
        console.log('btn remove BIG compa clicked');
    }

    // Empty the comparison basket
    $_btnEmptyCompa.on('click', function() {
        emptyItemsCompa();
    });

    function emptyItemsCompa() {
        cleanThumbNailCompaFace();
        cleanModalBodyCompa();
        activeAllButtonAddCompa();
        $.each($('[class*="btn-remove-compa"]'), function() {
            $id = $(this).attr('value');
            removeBtnRmCompaRecoverButtonAddCompa($id);
        });
        activDesactivBtnStartCompa();
        hideShowBtnsAddCompa();
        console.log('btn empty compa clicked');
    }

// ====================== BUTTONS

    function disableButtonAddCompa($id) {
        btnAddCompa = $('[id="btn-add-compa-' + $id + '"]');
        btnAddCompa.attr('disabled', true);
    }

    function hideButtonsAddCompa() {
        $('.btn-add-compa:not(:disabled)').addClass('hidden');
    }

    function activeAllButtonAddCompa() {
        $_btnAddCompa.removeAttr('disabled');
    }

    function showButtonsAddCompa() {
        $('.btn-add-compa.hidden').removeClass('hidden');
    }
    function activDesactivBtnStartCompa() {
        $itemsInPanier = getItemsInPanierCompa();
        if ($_modalBody.is(':empty') || !$_modalBody.html().trim()) {
            $_btnStartCompa.attr('disabled', 'disabled');
            $_btnEmptyCompa.attr('disabled', 'disabled');
        } else {
            console.log($itemsInPanier + 'ssss');
            if ($itemsInPanier < 2) {
                $_btnStartCompa.attr('disabled', 'disabled');
            } else {
                $_btnStartCompa.removeAttr('disabled');
            }

            $_btnEmptyCompa.removeAttr('disabled');
        }

    }

    function hideShowBtnsAddCompa() {
        $itemsInPanier = getItemsInPanierCompa();
        if ($itemsInPanier < $_maxItemCompaAuthorise) {
            showButtonsAddCompa();
        } else {
            hideButtonsAddCompa();
        }

    }

//    function activeButtonAddCompa($id) {
//        $('.btn-add-compa[id="btn-add-compa-' + $id + '"]').removeAttr('disabled');
//    }

    function generateRemoveButton($id) {
        btnAddCompa = $('[id="btn-add-compa-' + $id + '"]');
        $btnRm1 = $('<button class="btn btn-default btn-remove-compa" id="btn-remove-compa-' + $id + '" value="' + $id + '">Retirer du comparateur</button>');
        btnAddCompa.replaceWith($btnRm1)

        $btnRm1.on('click', function() {
            btnRemoveCompaClicked($id);
            console.log('yo-');
        });

    }

    function removeBtnRmCompaRecoverButtonAddCompa($id) {
        $btnRm2 = $('[id="btn-remove-compa-' + $id + '"]');
        $btnAdd2 = $('<button class="btn btn-default btn-add-compa" id="btn-add-compa-' + $id + '" value="' + $id + '">Comparer</button>');
        $btnRm2.replaceWith($btnAdd2);

        $btnAdd2.on('click', function() {
            btnAddCompaClicked($id);
            console.log('yo+2');
        });
        console.log($btnAdd2);
    }
// ====================== ELEMENTS -- MODAL BODY

    function appendHiddenDivsToModalBody($id) {
        btnAddCompa = $('[id="btn-add-compa-' + $id + '"]');
//        console.log(btnAddCompa);
        $hiddenDivs = btnAddCompa.parents('div.annonce-content').find('div.div-compa-hidden');
        $newCopy = $hiddenDivs.clone();
        $CompalUnit = $('<div class="compaUnit" id="' + $id + '"></div>');
        $newCopy.appendTo($CompalUnit).removeClass('hidden');

        // append to modal body and make it hidenless
        $CompalUnit.appendTo($_modalBody).removeClass('hidden');
    }

    function removeItemFromModalBody(id) {
        $_modalBody.find('div.compaUnit[id="' + id + '"]').remove();
    }

    function cleanModalBodyCompa() {
        $_modalBody.children().remove();
    }

// ====================== ELEMENTS -- PANIER FACE

    function addThumbNailToPanierCompaFace($id) {
        btnAddCompa = $('button[id="btn-add-compa-' + $id + '"]');
        console.log($id);
        $titre = btnAddCompa.parents('div.annonce-content').find('h4');
        $thumbNail = btnAddCompa.parents('div.annonce-content').find('div.media');
        $newCopyTitre = $titre.clone();
        $newCopy = $thumbNail.clone();

        $btnRemove = $('<button type="button" class="close btn-remove-compa" id="' + $id + '">\n\
                            <span aria-hidden="false">×</span>\n\
                            <span class="sr-only">Close</span>\n\
                        </button>');

        // Remove One item form the basket
        $btnRemove.click(function() {
            btnRemoveCompaClicked($id);
        });
        $thumbNailUnit = $('<div class="thumbNailUnit" id="' + $id + '"></div>');

        $newCopy.appendTo($thumbNailUnit);
        $newCopyTitre.appendTo($thumbNailUnit);
        $btnRemove.appendTo($thumbNailUnit);

        // append to modal body and make it hidenless
        $thumbNailUnit.appendTo($_panierCompaFace.find('div#div-thumbnail-compa')).addClass('thumbnail thumbnail-compa');
    }

    function removeThumbNailFromPanierFace(id) {
        $_panierCompaFace.find('div.thumbNailUnit[id="' + id + '"]').remove();
    }

    function cleanThumbNailCompaFace() {
        $_panierCompaFace.find('div#div-thumbnail-compa').children().remove();
    }

    function getItemsInPanierCompa() {
        return $itemsInPanierCompa = $('#div-thumbnail-compa').find('div.thumbNailUnit').length;
    }
// ======================
});