$(document).ready(function() {
    // get window height
    var $windowHeight = $(window).height(); /* get the window height */

    scrollLoadImages();

    /**
     * charge image when we scroll
     * @returns {undefined}
     */
    function scrollLoadImages() {
        $(window).scroll(function() { /* window on scroll run the function using jquery and ajax */
            /* Loop each image container */
            $.each($('div.media[need-feed="need-feed"]').find('img'), function(key, image) {
                var ref = $(image).attr('id');
                var imageTop = parseInt($(image).offset().top);
                var windowBottom = parseInt($(window).scrollTop() + $windowHeight);

                /* if window bottom reaches image container, which is not filled yet */
                /* good practice to anticipate 50px before */
                if (parseInt(windowBottom) + 50 >= parseInt(imageTop)) {
                    /* Remove default src */
                    $(image).removeAttr('src');
                    /* call ajax function load image, with ref */
                    var src = load_image_by_ref_ajax(ref);
                    $(image).attr('src', src).fadeIn();
                    $(this).parent('.media').attr('need-feed', 'deed-done');
                }
            });
        });
    }

    /**
     * Retrieve image with ref
     * @param {type} ref
     * @returns {String|media.src}
     */
    function load_image_by_ref_ajax(ref) {
        var host = window.location.hostname,
                src = '';
        $.ajax({/* post the values using AJAX */
            type: "POST",
            url: "/load_image_by_ref",
            data: {
                ref: ref
            },
            cache: true,
            async: false,
            success: function(media) {
                media = $.parseJSON(media);
                if (media) {
                    src = $.trim(media.src) !== '' ? media.src : 'img/no-photo.min.png';
                } else {
                    src = 'img/no-photo.min.png';
                }
            },
            fail: function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            }
        });
        return src;
    }

});