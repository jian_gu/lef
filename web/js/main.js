$(document).ready(function() {
    $('.media').height($('.media').width() / 1.6);
    // $('.block-top-right').css({'min-height': $('.block-top-left').height()+40});
    // Datepicker
    
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '0d',
        weekStart: 1,
        autoclose: true,
        todayHighlight: true,
        language: 'fr',
        clearBtn: true,
        changeDate: function() {
        },
        clearDate: function() {
        }
    });
});