$(document).ready(function() {
    /////////////////////////
    /////// variables ///////
    var boundsNeedToFitMarkers = true;
    var map;
    var zoomLevel;
    var infoWindow = new google.maps.InfoWindow({
        content: ''
    });
    var urlParams = null;
    // Center of returned results 
    var centerResults = $('#results-center').attr('data') === 'null' ?
            null : $.parseJSON($('#results-center').attr('data'));
    // Bounds of returned results 
    var boundsResults = $('#results-bounds').attr('data') === 'null' ?
            null : $.parseJSON($('#results-bounds').attr('data'));
    // Pays of returned results 
    var paysResults = $('#results-pays').attr('data') === 'null' ?
            null : $('#results-pays').attr('data');
    // Region of returned results 
    var regionResults = $('#results-region').attr('data') === 'null' ?
            null : $('#results-region').attr('data');
    // Ville of returned results 
    var villeResults = $('#results-ville').attr('data') === 'null' ?
            null : $('#results-ville').attr('data');
    // Bounds of map when map is resized 
    var boundsMapBoundsChanged = {n: null, e: null, s: null, w: null};
    // Cluster styles
    var styles = [[{
                url: '/img/markercluster/gm1.png',
                height: 52,
                width: 52,
                anchor: [0, 0], // coordinate [y, x]
                textColor: '#000000',
                textSize: 10
            }, {
                url: '/img/markercluster/gm2.png',
                height: 56,
                width: 55,
                anchor: [0, 0],
                textColor: '#000000',
                textSize: 10
            }, {
                url: '/img/markercluster/gm3.png',
                height: 66,
                width: 65,
                anchor: [0, 0],
                textColor: '#000000',
                textSize: 11
            }, {
                url: '/img/markercluster/gm4.png',
                height: 78,
                width: 77,
                anchor: [0, 0],
                textColor: '#000000',
                textSize: 12
            }, {
                url: '/img/markercluster/gm5.png',
                height: 90,
                width: 89,
                anchor: [0, 0],
                textColor: '#000000',
                textSize: 13
            }]];

    // Get pure url params with prefix ? 
    var urlParamsRaw = window.location.search.substring(1);
    urlParams = $.trim(urlParamsRaw);
    console.log(urlParams);

    ///////////////////////////
    ///////////////////////////
    var data = {"count": 4,
        "photos": [{"photo_id": 27932, "photo_title": "Atardecer en Embalse", "photo_url": "http://www.panoramio.com/photo/27932", "photo_file_url": "http://mw2.google.com/mw-panoramio/photos/medium/27932.jpg", "longitude": -64.404945, "latitude": -32.202924, "width": 500, "height": 375, "upload_date": "25 June 2006", "owner_id": 4483, "owner_name": "Miguel Coranti", "owner_url": "http://www.panoramio.com/user/4483"}
            ,
            {"photo_id": 522084, "photo_title": "In Memoriam Antoine de Saint Exupéry", "photo_url": "http://www.panoramio.com/photo/522084", "photo_file_url": "http://mw2.google.com/mw-panoramio/photos/medium/522084.jpg", "longitude": 17.470493, "latitude": 47.867077, "width": 500, "height": 350, "upload_date": "21 January 2007", "owner_id": 109117, "owner_name": "Busa Péter", "owner_url": "http://www.panoramio.com/user/109117"}
            ,
            {"photo_id": 1578881, "photo_title": "Rosina Lamberti,Sunset,Templestowe , Victoria, Australia", "photo_url": "http://www.panoramio.com/photo/1578881", "photo_file_url": "http://mw2.google.com/mw-panoramio/photos/medium/1578881.jpg", "longitude": 145.141754, "latitude": -37.766372, "width": 500, "height": 474, "upload_date": "01 April 2007", "owner_id": 140796, "owner_name": "rosina lamberti", "owner_url": "http://www.panoramio.com/user/140796"}
            ,
            {"photo_id": 97671, "photo_title": "kin-dza-dza", "photo_url": "http://www.panoramio.com/photo/97671", "photo_file_url": "http://mw2.google.com/mw-panoramio/photos/medium/97671.jpg", "longitude": 30.785408, "latitude": 46.639301, "width": 500, "height": 375, "upload_date": "09 December 2006", "owner_id": 13058, "owner_name": "Kyryl", "owner_url": "http://www.panoramio.com/user/13058"}
        ]};
    function initialize() {
        var center = centerResults !== null ?
                new google.maps.LatLng(centerResults.lat, centerResults.lng) :
                new google.maps.LatLng(49, 25);
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 3,
            minZoom: 3,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: true,
            streetViewControl: true,
            overviewMapControl: true,
            rotateControl: true
        });
        /////////////////
        /////////////////
        //
        // ||||||||||||||||||||||||
        // vvvvvvvvvvvvvvvvvvvvvvvv
        // Map idle event. After any change: zoom, drag...
        google.maps.event.addListener(map, 'idle', function() {
            zoomLevel = map.getZoom();
            // get current map bounds
            var boundsMapCurrent = getMapBounds(map);
            // Retrieve json flux who contains marks infos
            var jsonFluxCurrent = [];
            jsonFluxCurrent = get_json_flux(urlParams, boundsResults, boundsMapCurrent, paysResults, regionResults, villeResults);
            // If no result found
            if (!$.isArray(jsonFluxCurrent) || !jsonFluxCurrent.length > 0) {
                return;
            } else {
                // Push marks
                // Marksers In Result
                var markersPushTreatInc = pushMarkers(jsonFluxCurrent, true);
                var markersInResult = markersPushTreatInc.markers;
                // Marksers Outside Result
                var markersOutResult = pushMarkers(jsonFluxCurrent, false).markers;
                // Display markers clusters: Inside
                var markerClusterIn = new MarkerClusterer(map, markersInResult, {
                    gridSize: 60,
                });
                console.log(markerClusterIn.getGridSize());
                console.log(zoomLevel);
                // Display markers clusters: Outside
                var markerClusterOt = new MarkerClusterer(map, markersOutResult, {
                    gridSize: 120,
                    styles: styles[0]
                });
                // Make map contain all markers 
                if (boundsNeedToFitMarkers) {
                    var boundsToBeAdjusted = markersPushTreatInc.boundsAdjusted;
                    map.fitBounds(boundsToBeAdjusted);
                    boundsNeedToFitMarkers = false;
                }
                google.maps.event.trigger(map, 'resize');
            }
        });
        // ||||||||||||||||||||||||
        // vvvvvvvvvvvvvvvvvvvvvvvv
        // Click on blank part of map event
        google.maps.event.addListener(map, 'click', function() {
            infoWindow.close();
        });
        //
    }


//    // Initialize map
//    google.maps.event.addDomListener(window, 'load', initialize);

    // Tab switch event -- bootstrap tab
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var target = $(e.target).attr("href");
        if ((target === '#carte')) {
            // Map tab show
            $('#map-container').removeClass('hidden');
            // Initialize
            initialize();
            google.maps.event.addListenerOnce(map, 'idle', function() {
                google.maps.event.trigger(map, 'resize');
            });
        } else {
            // List shows, map hide
            $('#map-container').addClass('hidden');
        }
    });

    ///////////////////

    /**
     * Get map bounds when map is resized, dragged
     * @param {map object} map
     * @returns {_L2.boundsMapBoundsChanged}
     */
    function getMapBounds(map) {
        // Actual bounds when map size changed 
        var currentBounds = map.getBounds();
        boundsMapBoundsChanged.n = currentBounds.getNorthEast().lat();
        boundsMapBoundsChanged.e = currentBounds.getNorthEast().lng();
        boundsMapBoundsChanged.s = currentBounds.getSouthWest().lat();
        boundsMapBoundsChanged.w = currentBounds.getSouthWest().lng();

        return boundsMapBoundsChanged;
    }

    /**
     * Insert single markers into markercluster
     * @param {type} dataArray
     * @param {type} resultWeNeed
     * @returns {_L1.pushMarkers.results}
     */
    function pushMarkers(dataArray, resultWeNeed) {
        // Define map bounds to be adjusted and displayed 
        var boundsToBeAdjusted = new google.maps.LatLngBounds();
        var results = {'markers': null, 'boundsAdjusted': null};
        var markers = [];
        // Clear markers array
//        var markers = [];
        var grayPushpin = '/img/markercluster/gpp.png';
        $.each(dataArray, function(index, value) {
            var latLng = new google.maps.LatLng(value.latitude,
                    value.longitude);
            var ref = value.reference;

            // Markers as results we get for search: included
            if (resultWeNeed === true) {
                if (value.inresult === true) {
                    var markerIn = new google.maps.Marker({
                        position: latLng,
                        title: ref
                    });
                    // Marker click event
                    google.maps.event.addListener(markerIn, 'click', function() {
                        // Close infoWindow first incase it's opened
                        infoWindow.close();
                        // Get infoWindow content string
                        var contentString = constructInfoWindowContentString(ref);
                        // Set infoWinwod content
                        infoWindow.setContent(contentString);
                        // Open the infoWindow
                        infoWindow.open(map, markerIn);
                    });

                    // Push single marker to marker cluster
                    markers.push(markerIn);
                    // Extend bounds if the result is what we searched
                    boundsToBeAdjusted.extend(latLng);
                }
            } else {
                // Markers out of search results: exluded
                if (value.inresult === false) {
                    // Exlucded markers, gray color
                    var markerOt = new google.maps.Marker({
                        position: latLng,
                        title: ref,
                        icon: grayPushpin
                    });
                    // Marker click event
                    google.maps.event.addListener(markerOt, 'click', function() {
                        // Close infoWindow first incase it's opened
                        infoWindow.close();
                        // Get infoWindow content string
                        var contentString = constructInfoWindowContentString(ref);
                        // Set infoWinwod content
                        infoWindow.setContent(contentString);
                        // Open the infoWindow
                        infoWindow.open(map, markerOt);
                    });
                    // Push single marker to marker cluster
                    markers.push(markerOt);
                }
            }
        });
        results.boundsAdjusted = boundsToBeAdjusted;
        results.markers = markers;
        return results;
    }

    /**
     * Return string for infoWindow content
     * @param {string} reference
     * @returns {String}
     */
    function constructInfoWindowContentString(reference) {
        var contentString = '';
        var location = retrieveInfosLocationByAjax(reference);
        console.log(location);
        var imageUrl = load_image_by_ref_ajax(reference);
        // Titre
        var titre = typeof location.titre === 'undefined' ? '--' : location.titre;
        // Etoiles
        var etoiles = typeof location.nombreEtoiles === 'undefined' ? 0 : parseInt(location.nombreEtoiles);
        var stars = '';
        for (var i = 0; i < etoiles; i++) {
            stars = stars + '<i class="fa fa-star"></i> ';
        }
        // Prix
        var prix = typeof location.rentalprix === 'undefined' ? '--' : location.rentalprix;
        // nombre de personnes
        var nbrPerso = typeof location.nombreDePersonne === 'undefined' ? '--' : location.nombreDePersonne;
        // nombre de pièces
        var nbrPiece = typeof location.nombrePiece === 'undefined' ? '--' : location.nombrePiece;
        // nombre de chambres
        var nbrChambre = typeof location.nombreChambre === 'undefined' ? '--' : location.nombreChambre;
        // pays
        var pays = typeof location.pays === 'undefined' ? '--' : $.trim(location.pays).replace('/', '_');
        // region
        var region = typeof location.region === 'undefined' ? '--' : $.trim(location.region).replace('/', '_');
        // ville
        var ville = typeof location.ville === 'undefined' ? '--' : $.trim(location.ville).replace('/', '_');
        // view url
        var urlVoir = '/locations/' + pays + '/' + region + '/' + ville + '/locations-de-vacances-' + $.trim(location.brandPartner) + '-' + location.reference;

        contentString += '<div id="content" style="max-width:330px;>' +
                '<div id="siteNotice">' +
                '<div id="image"">' +
                '<img src="' + imageUrl + '" style="min-height:250px !important; width:330px !important;"/></div>' +
                '<div id="mid">' +
                '<span id="titre">' + titre + '</span>&nbsp' +
                '<span id="nbr-etoile">' + stars + '</span>' +
                '<br/>' +
                '<span id="nbr-prix"> EUR ' + prix + '</span>' +
                '<div id="desc">' +
                '<span id="geoloca">' + pays + ', ' + region + ', ' + ville + '</span>' +
                '<br/>' +
                '<span id="carac">' + nbrPerso + ' Personnes / ' + nbrPiece + ' Pièces / ' + nbrChambre + ' Chambres</span>' +
                '<br/>' +
                '<span id="reference">Reference : ' + location.reference + '</span>' +
                '</div>' +
                '<div id="link-div" class="col-sm-6" style="float:right;">' +
                '<a href="' + urlVoir + '" class="btn btn-block btn-warning">Voir</a>';
        '</div>' +
                '</div>' +
                '</div>';
        return contentString;
    }

    /**
     * Get location infos by reference, using ajax
     * @param {string} ref  reference of location
     * @returns {jsonObject}
     */
    function retrieveInfosLocationByAjax(ref) {
        var infoLoc;
        $.ajax({/* post the values using AJAX */
            type: "POST",
            url: "/service/location/info-by-ref",
            data: {
                ref: ref
            },
            cache: true,
            async: false,
            success: function(location) {
                infoLoc = $.parseJSON(location);
            },
            fail: function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            }
        });
        return infoLoc;
    }

    /**
     * Retrieve image with ref
     * @param {string} ref
     * @returns {String|media.src}
     */
    function load_image_by_ref_ajax(ref) {
        src = '';
        $.ajax({/* post the values using AJAX */
            type: "POST",
            url: "/load_image_by_ref",
            data: {
                ref: ref
            },
            cache: true,
            async: false,
            success: function(media) {
                media = $.parseJSON(media);
                if (media) {
                    src = $.trim(media.src) !== '' ? media.src : 'img/no-photo.min.png';
                } else {
                    src = 'img/no-photo.min.png';
                }
            },
            fail: function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            }
        });
        return src;
    }

    ///////////////////
    ///////////////////
    /**
     * 
     * @param {type} boundsResults
     * @param {type} conditions
     * @param {type} boundsMap
     * @returns {unresolved}
     */
    function get_json_flux(conditions, boundsResults, boundsMap, paysResults, regionResults, villeResults) {
        var flux;
        $.ajax({/* post the values using AJAX */
            type: "POST",
            url: "/service/map/json-flux?" + conditions,
            data: {
                boundsResults: boundsResults,
                boundsMap: boundsMap,
                pays: paysResults,
                region: regionResults,
                ville: villeResults
            },
            cache: true,
            async: false,
            success: function(geoDataFlux) {
                flux = geoDataFlux;
            },
            fail: function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            }
        });
        return $.parseJSON(flux);
    }

});