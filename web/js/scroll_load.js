$(document).ready(function() {

    var $loadingBarSrc = 'img/loadingBar.gif';
    var $content = $('div.main-content').find('div.row div.col-sm-8');
//    var $documentHeight = $(document).height(); /* Window height */
    var $contentBottom = $content.position().top + $content.height();
    var $windowHeight = $(window).height(); /* get the window height */
    var $track_load = 0; //total loaded record group(s)
    var $loading = false; //to prevents multipal ajax loads
    var $total_groups = _getTotalGroupNbr(); //total record group(s) VALUE GOT FROM CONTROLLER

    initiateGoToTopButton();
    initiateLoadingImage();
    showHideGoToTop();
    goToTopWhenClick();
    scroll_load();
    $('select#select-tri').change(triChange_load);

    /**
     * Initiate loding animation image
     * @returns none
     */
    function initiateLoadingImage() {
        var $codeHtml = '<div class="animation_loading_image" style="display:none" align="center">\n\
                            <img src="' + $loadingBarSrc + '">\n\
                        </div>';
        $($content).append($codeHtml);
    }

    function goToTopWhenClick() {
        $('div#link-top').on('click', function() {
            $('body').animate({
                scrollTop: 0
            }, 450);
        });
    }

    function initiateGoToTopButton() {
        var $codeHtml = '<div style="width: 44px; height: 44px; margin: 8px 0 0; \n\
                            color: #bbb; background-color: #fcfcfc; border: 1px solid #ddd; \n\
                            position: relative; display: block; cursor: pointer;\n\
                            text-align:center; padding:3px;\n\
                            bottom:32px; right:32px; display:none; position:fixed;"\n\
                            id="link-top">\n\
                            <i class="fa fa-chevron-up fa-2x"></i></div>';
        $('body').append($codeHtml);
    }

    function showHideGoToTop() {
        $(window).scroll(function() { /* window on scroll run the function using jquery and ajax */
            if ($(window).scrollTop() + 1 >= $windowHeight) {
                $('div#link-top').fadeIn(300);
            } else {
                $('div#link-top').fadeOut(300);
            }
        });
    }

    /**
     * Ajax load more
     * @returns {undefined}
     */
    function scroll_load() {
        $(window).scroll(function() { /* window on scroll run the function using jquery and ajax */
            $contentBottom = $content.position().top + $content.height(); /* RE-Detecte content bottom position */
            if ($(window).scrollTop() - 3 > $contentBottom - $windowHeight && _atTabListeLocation()) { /* check is that user scrolls down to the bottom of the page */

                if ($track_load <= $total_groups && $loading === false) {   /* There is more data to load */

                    $loading = true; //prevent further ajax loading
                    $('.animation_loading_image').show(); //show loading image

                    $("#loader").html("<img src='loading_icon.gif' alt='loading'/>"); /* displa the loading content */
                    var $lastDiv = $("div.main-content div.annonce").find(".annonce-content:last"); /* get the last div of the dynamic content using ":last" */
                    var $lastId = $("div.main-content div.annonce").find(".annonce-content:last").attr("id"); /* get the id of the last div */
                    var $position = _getNumberLocationShown();
                    $.ajax({/* post the values using AJAX */
                        type: "GET",
                        url: "scroll_load",
                        data: {
                            position: $position
                        },
                        cache: true,
                        async: true,
                        success: function(html) {
                            html = $.parseJSON(html);
                            if (html) {
//                                $lastDiv.after(html.content); /* get the out put of the getdata.php file and append it after the last div using after(), for each scroll this function will execute and display the results */
                                if ($.isArray(html.content)) {
                                    $.each(html.content.reverse(), function(key, location) {
                                        var $codeStr = _constructCell(location);
//                                        console.log($($codeStr));
                                        $lastDiv.after($($codeStr));
                                    });
                                }
                            }
                            //hide loading image
                            $('.animation_loading_image').hide(); //hide loading image once data is received

                            $track_load++; //loaded group increment
                            $loading = false;
                        },
                        fail: function(jqXHR, textStatus) {
                            alert("Request failed: " + textStatus);
                            $('.animation_loading_image').hide(); //hide loading image
                            $loading = false;
                        }
                    });
                } /* loading condition test */
            } /* window position test */
        });
    }

    /**
     * Construct cell block code, in format string
     * @param object $location
     * @returns string
     */
    function _constructCell($location) {
        var $content = "<div class='annonce-content annonce-intern'>\n\
                    <div class='row'>\n\
                        <div class='col-sm-5'>";

        if ($.inArray($location.brandPartner, ['lef', 'LEF', 'louerenfrance', 'louer-en-france']) > -1) {
            if ($.isArray($location.medias)) {                                   /* If media is an array, get the first one */
                var $count = $location.medias.length;
                var $ramdom = Math.floor(Math.random() * $count);
                $content += "<div class='media' style='height: 176.875px;'>\n\
                                    <img src='" + $location.medias[$ramdom].src + "'>\n\
                                </div>";
            }
        } else if (typeof $location.media_p !== 'undefined' && $.isArray($location.media_p)) {                                /* If media is an array, get the first one */
            var $count = $location.media_p.length;
            var $ramdom = Math.floor(Math.random() * $count);
            $content += "<div class='media' style='height: 176.875px;'>\n\
                                    <img src='" + $location.media_p[$ramdom].src + "'>\n\
                                </div>";

        } else {
            $content += "<div class='media no-media'style='height: 176.875px;'>\n\
                                    <img src='img/no-photo.min.png'>\n\
                                </div>";
        }
        $content += "</div>";
        $content += "<div class='col-sm-5'>\n\
                            <div class='body'>\n\
                                <h4 class='heading titre-location'> ";          /* Show titre */
        $content += $location.pays !== null ? $location.pays + ' / ' : '';
        $content += $location.region !== null ? $location.region + ' / ' : '';
        $content += $location.ville !== null ? $location.ville + ' / ' : '';
        $content += $location.titre !== null ? $location.titre : '';
        $content += $location.typePartner !== null ? $location.typePartner : '';
        $content += "</h4>";
        $content += "<span class='ref-location'> Ref : " + $location.reference + " </span>";
        $content += "<p class='teaser-location'>";                              /* Info générale */
        $content += "<span class='item-loc sqmpax-location'>\n\
                         " + $location.surfaceHabitable + " m² / " + $location.nombreDePersonne + " personnes \n\
                    </span>";
        $content += "<span class='item-loc note-location'>";                    /* Note stars */
        if ($location.nombreEtoiles) {
            var $starEmpty = 5 - $location.nombreEtoiles;
            for ($i = 1; $i <= $location.nombreEtoiles; $i++) {
                $content += "<i class='fa fa-star'></i> ";
            }
            if ($starEmpty > 0) {
                for ($i = 1; $i <= $starEmpty; $i++) {
                    $content += "<i class='fa fa-star-o'></i> ";
                }
            }
        } else {
            for ($i = 1; $i <= 5; $i++) {
                $content += "<i class='fa fa-star-o'></i> ";
            }
        }
        $content += "</span>";
        $content += "<span class='item-loc desc-location'>";                    /* Description bref */
        if ($location.caracteristiques) {
            $content += $location.caracteristiques.length > 40 ? $location.caracteristiques.substr(0, 40) + "..." : $location.caracteristiques;
        }
        if ($location.descriptionBreve) {
            $content += $location.descriptionBreve.length > 40 ? $location.descriptionBreve.substr(0, 40) + "..." : $location.descriptionBreve;
        }
        if ($location.descriptionDetaillee) {
            $content += $location.descriptionDetaillee.length > 40 ? $location.descriptionDetaillee.substr(0, 40) + "..." : $location.descriptionDetaillee;
        }
        $content += "</span>";
        $content += "</p>";
        $content += "</div>";
        $content += "</div>";

        $content += "<div class='col-sm-2'>";
        $content += "<span class='prix-label'>à partir de</span>";
//        $content += "<span class='prix'> " + $location.rentalprice ? $location.rentalprice : '----' + "<sup class='prix-devise'>&euro;</sup></span>";
        var $rentalPrice = $location.rentalprice ? $location.rentalprice : '----';
        $content += "<span class='prix'> " + $rentalPrice + "<sup class='prix-devise'>&euro;</sup></span>";
        var $pays = $location.pays !== null ? $location.pays : "pays",
                $region = $location.region !== null ? $location.region : "region",
                $ville = $location.ville !== null ? $location.ville : "ville";
        $content += "<a href='/locations/locations-partenaire/" + $pays + '-' + $region + '-' + $ville + '/' + $location.reference + "/voir'\n\
                        class='btn btn-block btn-warning'>Voir</a>";
        $content += "<button class='btn btn-block btn-warning btn-go-wish-list' valref='" + $location.refernce + "'>------</button>";
        $content += "<button class='btn btn-block btn-primary btn-add-compa btn-xs' id='btn-add-compa-" + $location.id + "' value='" + $location.id + "'>Comparer</button>";
        $content += "<div class='btn-group-vertical'></div>";
        $content += "</div>";
        $content += "</div>";
        $content += "<div class='border-bottom'></div>";
        $content += "<div class='hidden div-compa-hidden'><td>" + $location.pays + "</td></div>";
        $content += "</div>";

        /* Return block code */
        return $content;
    }

    function triChange_load() {
        // Get current path
        var $path = _getUrlPath('path');
        // Initiate new params to blank
        var $newParams = '';
        // Initiate new path to current
        var $newUrl = $path;
        // Get selected value
        var $triValue = $(this).val();
        $triValue = parseInt($triValue);
        // Get url params
        var $params = _getUrlParameter();
        var $paramTri = _getUrlParameter('tri');
        /* Tri not defined */
        if (typeof $paramTri === 'undefined') {
            $newParams = $params + '&tri=' + $triValue;
        } else if ($paramTri.length > 0) {
            /* Tri is already defined */
            $newParams = _replaceUlrParam('tri', $triValue, $params);
        }
        /* set new path */
        $newUrl += '?' + $newParams;
        // Reload current page with new params
        _reloadCurrentPageWithParams($newUrl);
    }

    /**
     * Get number of locations which are already shown on page
     * @returns {Integer}
     */
    function _getNumberLocationShown() {
        var $numberLocationShown = $('div.annonce > div.annonce-content').size();
        return $numberLocationShown;
    }

    /**
     * Get total group number, which is defined in template file
     * @returns {_L1._getTotalGroupNbr.$nbr|Number}
     */
    function _getTotalGroupNbr() {
        var $val = $('div#pre-annonce > span#spn-nbr-group').html(),
                $nbr = parseInt($val),
                $result = 200;                                                  /* set default value to 200 */
        $result = $nbr > 0 ? $nbr : $result;
        return $result;
    }

    /**
     * Get Complete url path without params
     * @param {string} $part
     * @returns {window.location.hostname|String.hostname|DOMString|String|window.location.pathname|String.pathname}
     */
    function _getUrlPath($part) {
        var $path = '';
        switch ($part) {
            case 'all':
                $path = window.location.hostname + window.location.pathname;
                break;
            case 'path':
                $path = window.location.pathname;
                break;
            case 'href':
                $path = window.location.href;
                break;
            default:
                break;
        }
        return $path;
    }

    /**
     * Get url parameters
     * @param {type} $sParam param name
     * @returns {_L1._getUrlParameter.sParameterName}
     */
    function _getUrlParameter($sParam)
    {
        var sPageURL = window.location.search.substring(1);                     /* Eliminate the first letter '?' */
        /* if parameter is not given, return all params */
        if (!$sParam) {
            return sPageURL;
        }
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === $sParam)
            {
                return sParameterName[1];
            }
        }
    }

    function _replaceUlrParam($paramName, $paramValue, $params) {
        var $newParamStrProto = '',
                $paramsArray = $params.split('&');
        for (var i = 0; i < $paramsArray.length; i++)
        {
            var $paramNameValue = $paramsArray[i].split('=');
            if ($paramNameValue[0] === $paramName)
            {
//                $paramNameValue[1] = $paramValue;
                $newParamStrProto += '&' + $paramName + '=' + $paramValue;
            } else {
                /* Keep other params */
                $newParamStrProto += '&' + $paramsArray[i];
            }
        }
        $newParamStr = $newParamStrProto.slice(1);
        return $newParamStr;
    }

    function _reloadCurrentPageWithParams($completeUrl) {
        window.location.href = $completeUrl;
    }
    
    function _atTabListeLocation(){
        var $atTabListe = $('div.tab-pane#liste[role="tabpanel"]').hasClass('active');
        return $atTabListe;
    }
});