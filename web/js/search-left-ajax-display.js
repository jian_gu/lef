/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Prototype, ajax fill reults count for the search form
 * purpose: show result count after each critics, once condition is changed (not
 * submit yet), the number should changed.
 */

$(document).ready(function() {
    // Once page loaded, execute function to display result counts
    processSingle();

    /**
     * Detected user intervention, trigger
     */
    // checkbox changed or text input changed or select changed
    // Also, if search form top conditions changed
    $("input.filter[type = 'checkbox'], input.filter[type='text'], select.filter, #inputNbPers").change(function() {
        processSingle();
    });



    /**
     * function get all fields of the filter form. ! NO TEXT INPUT -- RANGES
     * @returns {Array}
     */
    function getAllFilterFormFields() {
        var $fields = [];
        // get checkboxes and select options
        $('form.srch_left, #srch-top-seo').find("input.filter[type = 'checkbox'], option[value*=':']").each(function(index, value) {
            $fields.push($(value).val());
        });
        return $fields;
    }

    /**
     * function get all fields of the filter form(LEFT) which are checked, input or selected
     * Also, some fileds in TOP search form
     * @returns {Array}
     */
    function getSelectedFilterFormFields() {
        var $conditions = [];
        // get checked checkboxes, text input no empty and selected options in select LEFT
        $('form.srch_left, #srch-top-seo').find("input.filter[type = 'checkbox']:checked, input.filter[type = 'text'], option:selected[value*=':']").each(function(index, value) {
            // For text input
            if ($(value).attr('type') === 'text' && $(value).val() !== '') {
                var $pair = $(value).attr('name') + ':' + $(value).val();
                $conditions.push($pair);
            } else if ($(value).attr('type') === 'text' && $(value).val() === '') {
//                console.log('Default ranges.');
                return;
            } else {
                $conditions.push($(value).val());
            }
        });

        // get conditions form search form TOP
        $.each(getConditionSearchTop(), function(index, condition) {
            $conditions.push(condition);
        });

        // get conditions params url
        $.each(getConditionUrlParam(), function(index, condition) {
            $conditions.push(condition);
        });
        
        // Return results
        return $conditions;
    }

    /**
     * Function get params defined in url path, like geoloc...
     * For geoloc(pays, region, ville), vac(été, ski...) et promos
     * @returns {Array}
     */
    function getConditionsInUrlPath() {
        var $conditionsUrlPath = [],
                $currentPaht = window.location.pathname,
                $pathSegments = $currentPaht.split('/'),
                $conditionsPathGeo = '';
        $.each($pathSegments, function(index, value) {
            // Geoloc: pays, region, ville
            if (value.indexOf('pays') > -1) {
                $conditionsPathGeo = value.split('-');
                var $geolocArry = {'geoloc': $conditionsPathGeo};
                $conditionsUrlPath.push($geolocArry);
            }
            // Promos
            if (value.indexOf('promotions') > -1 && index === 1) {
                // in promos root
                // detailed condition:
                var $detail = $pathSegments[2];
                switch ($detail) {
                    case 'promotions':
                        var $arryPromo = {'promos': 'promotions'};
                        $conditionsUrlPath.push($arryPromo);
                        break;
                    case 'premiere-minute':
                        var $arryPromo = {'promos': 'premiere-minute'};
                        $conditionsUrlPath.push($arryPromo);
                        break;
                    case 'derniere-minute':
                        var $arryPromo = {'promos': 'derniere-minute'};
                        $conditionsUrlPath.push($arryPromo);
                        break;
                    default:
                        break;
                }
            }
            // Vacs
            if (value.indexOf('espace-vacances') > -1 && index === 1) {
                // in promos root
                // detailed condition:
                var $detail = $pathSegments[2];
                switch ($detail) {
                    case 'ete':
                        var $arryPromo = {'vacs': 'ete'};
                        $conditionsUrlPath.push($arryPromo);
                        break;
                    case 'ski':
                        var $arryPromo = {'vacs': 'ski'};
                        $conditionsUrlPath.push($arryPromo);
                        break;
                    case 'mer':
                        var $arryPromo = {'vacs': 'mer'};
                        $conditionsUrlPath.push($arryPromo);
                        break;
                    case 'soleil-lointain':
                        var $arryPromo = {'vacs': 'soleil-lointain'};
                        $conditionsUrlPath.push($arryPromo);
                        break;
                    default:
                        break;
                }
            }
        });

        return $conditionsUrlPath;
    }

    /*
     * Function get all url params
     * @param {type} sParam
     * @returns {_L11.getUrlParameter.sParameterName}
     */
    function _getUrlParameter(sParam)
    {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam)
            {
                return sParameterName[1];
            }
        }
    }

    /**
     * Function get certain url param
     * @returns {Array}
     */
    function getConditionUrlParam() {
        var $conditionsUrlParam = [];
        if (_getUrlParameter('nbrPers') !== undefined && _getUrlParameter('nbrPers') !== null) {
            var $num = _getUrlParameter('nbrPers');
            $conditionsUrlParam.push('_location:nombreDePersonne@' + $num);
        }
        if (_getUrlParameter('duree') !== undefined && _getUrlParameter('duree') !== null) {
            var $num = _getUrlParameter('duree');
            $conditionsUrlParam.push('__duree:' + $num);
        }
        if (_getUrlParameter('dateArrivee') !== undefined && _getUrlParameter('dateArrivee') !== null) {
            var $num = _getUrlParameter('dateArrivee');
            $conditionsUrlParam.push('__dateArrivee:' + $num);
        }
        return $conditionsUrlParam;
    }

    /**
     * Function get condition fields of search form TOP
     * @returns {Array}
     */
    function getConditionSearchTop() {
        var $conditionSearchTop = [],
                $condiDuree = '',
                $condiNbPers = '';

//        $('#srch-top-seo').find('select#inputDuree>option').each(function(index, value) {
//
//        });

        if ($('#srch-top-seo').find('select#inputNbPers').length > 0 && $('#srch-top-seo').find('select#inputNbPers').val() !== null) {
            $condiNbPers = $('#srch-top-seo').find('select#inputNbPers').val();
            $conditionSearchTop.push('_location:nombreDePersonne@' + $condiNbPers);
        }
        return $conditionSearchTop;
    }

    /**
     * Ajax function retrieve results count
     * @param {array} $fields
     * @param {array} $conditions
     * @param {array} $conditionsInUrlPath
     * @returns {undefined}
     */
    function ajaxGetResultCountSingle($fields, $conditions, $conditionsInUrlPath) {
        $.ajax({
            dataType: 'json',
            url: window.location.protocol + "//" + window.location.host + "/" + 'service/search/get_count_sg',
            data: {
                fields: $fields,
                conditions: $conditions,
                conditionsInUrlPath: $conditionsInUrlPath
            },
            success: function(data)
            {
                // append numbers after critères
                displayNumbers(data);
            },
            error: function(xhr, status, error) {
                console.log(status + '; ' + error);
            },
            async: true
        });
    }

    // send condition to back-work-unit
    function processSingle() {
        var $fields = getAllFilterFormFields(),
                $conditions = getSelectedFilterFormFields(),
                $conditionsInUrlPath = getConditionsInUrlPath();
        // if no field found
        if ($conditions.length === 0 && $fields.length === 0) {
            console.log('No field in filter.');
            return;
        }
        // call ajax function get results count
        ajaxGetResultCountSingle($fields, $conditions, $conditionsInUrlPath);

//        console.log($fields);
//        console.log($conditions);
    }
    // Display in form with retrieced count numbers
    function displayNumbers($arryObjectReceived) {
        //// Initiat max number
        var $maxCount = 0;
        //// remove numbers if they exist
        if ($('span.spn-num-ajax').length) {
            $('span.spn-num-ajax').remove();
        }
        $.each($arryObjectReceived, function(index, value) {
            var $element = $('form.srch_left, form#srch-top-seo').find('input[value=\'' + index + '\'], li[value=\'' + index + '\']'),
                    $num = value[0]['num'],
                    $numElement = $('<span class="spn-num-ajax" style="padding-left:3em;">(' + $num + ')</span>');

            // for checkboxes
            if ($($element).attr('type') === 'checkbox') {
                $($element).parent().append($numElement);
            }
            // for dropdown list
            if ($($element).is('li')) {
                // for options
                $($element).append($numElement);
                // for selected item with EASYDROPDOWN
                if ($($element).hasClass('active')) {
                    $($element).parents('div.dropdown').find('span.selected').append($numElement.clone());
                }
            }
//            console.log($num);
            $maxCount = $maxCount >= parseInt($num) ? $maxCount : parseInt($num);
        });

        //// show maxCount to the form bottom
//        var $maxNumElement = $('<span class="spn-num-ajax">' + $maxCount + ' Résultats</span>');
//        $('div#div-num-result').prepend($maxNumElement);
    }
});