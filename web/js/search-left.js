/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    ////////////
    // variables
    ////////////
    var $surfaceMin,
            $surfaceMax,
            $surfaceLeft,
            $surfaceRight,
            $prixMin,
            $prixMax,
            $prixLeft,
            $prixRight;

    // get ranges for sliders
    getSliderRangeSurfacePrix();
    // define sliders range params
    defineParamsSliders();
    // create slider surface
    sliderSurface();
    // create slider prix
    sliderPrix();
    // pre-fill form
    preFillForm();


//===========================
//=== get ranges sliders ====
//===========================
    function getSliderRangeSurfacePrix() {

        $.ajax({
            dataType: 'json',
            url: window.location.protocol + "//" + window.location.host + "/" + 'service/search/get_range_sp',
            data: {
//                data: data
            },
            success: function(data)
            {
                var $data_object = data[0];
//                $surfaceMin = parseInt($data_object.min_surface);
//                $surfaceMax = parseInt($data_object.max_surface);
//                $prixMin = parseInt($data_object.min_prix);
//                $prixMax = parseInt($data_object.max_prix);
                $surfaceMin = 0;
                $surfaceMax = Math.ceil(parseInt($data_object.max_surface) / 10) * 10;
                $prixMin = 0;
                $prixMax = Math.ceil(parseInt($data_object.max_prix) / 100) * 100;
            },
            error: function(xhr, status, error) {
                console.log(status + '; ' + error);
            },
            async: false
        });
    }

//====================================
//=== define sliders range params ====
//====================================
    function defineParamsSliders() {
        // pre-define selected range to min and max
        $surfaceLeft = $surfaceMin,
                $surfaceRight = $surfaceMax,
                $prixLeft = $prixMin,
                $prixRight = $prixMax;

        var $url_params = getFilterParamFromUrl().length > 0 ? getFilterParamFromUrl()[1] : {};
        // if ranges params can be found in url
        // assign indicators with url param value
        $.each($url_params, function($key, $val) {

            switch ($val[0]) {
                case 'sqmMin':
                    $surfaceLeft = $val[1];
                    break;
                case 'sqmMax':
                    $surfaceRight = $val[1];
                    break;
                case 'prixMin':
                    $prixLeft = $val[1];
                    break;
                case 'prixMax':
                    $prixRight = $val[1];
                    break;
                default :
                    return;
            }
        });
    }
//=======================
//=== slider surface ====
//=======================
    function sliderSurface() {
        $("#slider-range-surface").slider({
            animate: true,
//            handle: 'square',
            step: 10,
            range: true,
            min: $surfaceMin,
            max: $surfaceMax,
            values: [$surfaceLeft, $surfaceRight],
            create: function(event, ui) {
                // text, label displayed
                var $label;
                if ($surfaceLeft === $surfaceMin && $surfaceRight === $surfaceMax) {
                    $label = 'N\'importe';
                } else {
                    $label = 'M&sup2 ' + $surfaceLeft + ' - ' + $surfaceRight;
                }
                $('#label-range-surface').html($label);
            },
            slide: function(event, ui) {
                var $sMin = ui.values[0] === $surfaceMin ? '' : ui.values[0],
                        $sMax = Math.ceil(ui.values[1]) === $surfaceMax ? '' : ui.values[1];
                $('#sqmMin').val($sMin).change();
                $('#sqmMax').val($sMax).change();
                // text, label displayed
                var $tsMin = $sMin === '' && $sMax !== '' ? 0 : $sMin,
                        $tsMax = $sMax === '' && $sMin !== '' ? $surfaceMax : $sMax,
                        $label = '';
                if ($sMin === '' && $sMax === '') {
                    $label = 'N\'importe';
                } else {
                    $label = 'M&sup2 ' + $tsMin + ' - ' + $tsMax;
                }
                $('#label-range-surface').html($label);
            }
        });

    }
    ;
//=====================
//=== slider prix =====
//=====================
    function sliderPrix() {

        $("#slider-range-prix").slider({
            animate: true,
//            handle: 'square',
            step: 100,
            range: true,
            min: $prixMin,
            max: $prixMax,
            values: [$prixLeft, $prixRight],
            create: function(event, ui) {
                // text, label displayed
                var $label;
                if ($prixLeft === $prixMin && $prixRight === $prixMax) {
                    $label = 'N\'importe';
                } else {
                    $label = 'EUR ' + $prixLeft + ' - ' + $prixRight;
                }
                $('#label-range-prix').html($label);
            },
            slide: function(event, ui) {
                var $pMin = ui.values[0] === $prixMin ? '' : ui.values[0],
                        $pMax = Math.ceil(ui.values[1]) === $prixMax ? '' : ui.values[1];
                $('#prixMin').val($pMin).change();
                $('#prixMax').val($pMax).change();
                // text, label displayed
                var $tpMin = $pMin === '' && $pMax !== '' ? 0 : $pMin,
                        $tpMax = $pMax === '' && $pMin !== '' ? $prixMax : $pMax,
                        $label = '';
                if ($pMin === '' && $pMax === '') {
                    $label = 'N\'importe';
                } else {
                    $label = 'EUR ' + $tpMin + ' - ' + $tpMax;
                }
                $('#label-range-prix').html($label);
            }
        });
    }
    ;

//==================================================
//=== form data format treatement before submit ====
//==================================================
    $(".srch_left").submit(function(eventObj) {
        // Declaire URL parameters string
        var $dataStr = '';
        _removeCheckboxNames();
        _removeSelectNames();
        _removeInputNames();

        $dataStr += _rangeFieldsTraitement();
        // if ranges values are given and there are checkboxes checked, add a ',' at the end of string
        $dataStr += $.trim($dataStr) !== '' && $.trim(_checkboxesTraitement()) !== '' ? ',' + _checkboxesTraitement() : _checkboxesTraitement();
        // Same for the drop down lists
        $dataStr += $.trim($dataStr) !== '' && $.trim(_dropDownListTraitement()) !== '' ? ',' + _dropDownListTraitement() : _dropDownListTraitement();

        if ($.trim($dataStr) !== '') {
            $('<input />').attr('type', 'hidden')
                    .attr('name', "filters")
                    .attr('value', $dataStr)
                    .appendTo('.srch_left');
            if (getTopSrchParam().length > 0) {
                $.each(getTopSrchParam(), function() {
                    $(this).appendTo('.srch_left');
                });
            }

            // prepare action path
            var $currentPath = window.location.pathname,
                    $currentPathPage0 = $currentPath.replace(/\/\d/, '');       // remove pagination
            $(".srch_left").attr('action', $currentPathPage0);
            
            return true;
        } else {
            return true;                                                        // M le 090914 par Jian
        }   

    });

    /**
     * Remove checkboxes names, so that their values will not be sent by default,
     * Cos we need to treat/modify the values before form validation
     * @returns {undefined}
     */
    function _removeCheckboxNames() {
        $('input[type="checkbox"].filter[name]').each(function() {
            $(this).removeAttr('name');
        });
    }

    /**
     * Do the same for selects
     * @returns {undefined}
     */
    function _removeSelectNames() {
        $('select.filter[name]').each(function() {
            $(this).removeAttr('name');
        });
    }

    /**
     * Do the same for input text
     * @returns {undefined}
     */
    function _removeInputNames() {
        $('input[type="text"]').each(function() {
            $(this).removeAttr('name');
        });
    }


    /**
     * Traitement of ranges for prix and surface
     * @returns {String}
     */
    function _rangeFieldsTraitement() {
        var $dataStr = '';
        $('input.filter-range').each(function() {
            if ($.trim($(this).val()) !== '') {
                $dataStr += $.trim($dataStr) === ''
                        ? $(this).attr('id') + ':' + $(this).val()
                        : ',' + $(this).attr('id') + ':' + $(this).val();
            }
        });
        return $.trim($dataStr) === '' ? '' : $dataStr;
    }

    /**
     * Get checkboxes values in format: "associatedTableName:value"
     * @returns {String}
     */
    function _checkboxesTraitement() {
        // if at least a checkbox is checked
        if ($('input[type="checkbox"].filter:checked').length) {
            var $dataStr = '';
            $('input[type="checkbox"].filter:checked').each(function() {
                $dataStr += $.trim($dataStr) === '' ? $(this).attr('value') : ',' + $(this).attr('value');
            });
        }
        return $.trim($dataStr) === '' ? '' : $dataStr;
    }

    function _dropDownListTraitement() {
        if ($('select.filter option:selected[value]').length) {
            var $dataStr = '';
            $('select.filter option:selected[value]').each(function() {
                $dataStr += $.trim($dataStr) === '' ? $(this).attr('value') : ',' + $(this).attr('value');
            });
        }
        return $.trim($dataStr) === '' ? '' : $dataStr;
    }

    // ====================
    // ==== fill form =====
    // ====================
    /**
     * Function get all url params
     * @returns {string}
     */
    function getURLParameter()
    {
        var $_GET = {};
        document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function() {
            function decode(s) {
                return decodeURIComponent(s.split("+").join(" "));
            }

            $_GET[decode(arguments[1])] = decode(arguments[2]);
        });

        return $_GET;
    }

    /**
     * If Top search form is used, get its data
     * @returns {Array}
     */
    function getTopSrchParam() {
        var $geoLoc = getURLParameter()['geoLoc'],
                $dateArrivee = getURLParameter()['dateArrivee'],
                $duree = getURLParameter()['duree'],
                $nbrPers = getURLParameter()['nbrPers'],
                $param1 = $.trim($geoLoc) !== '' ? $('<input />').attr('type', 'hidden')
                .attr('name', "geoLoc")
                .attr('value', $geoLoc) : '',
                $param2 = $.trim($dateArrivee) !== '' ? $('<input />').attr('type', 'hidden')
                .attr('name', "dateArrivee")
                .attr('value', $dateArrivee) : '',
                $param3 = $.trim($duree) !== '' ? $('<input />').attr('type', 'hidden')
                .attr('name', "duree")
                .attr('value', $duree) : '',
                $param4 = $.trim($nbrPers) !== '' ? $('<input />').attr('type', 'hidden')
                .attr('name', "nbrPers")
                .attr('value', $nbrPers) : '';
        return [$param1, $param2, $param3, $param4];
    }

    /**
     * Get Parameters for filter from url
     * @returns {Boolean|Array}
     */
    function getFilterParamFromUrl() {
        // get all filter param
        var $urlParamStr = getURLParameter()['filters'];
        if ($.trim($urlParamStr) !== '') {
            var $meta1 = $urlParamStr.split(','),
                    $meta2 = [];
            $.each($meta1, function($key, $val) {
                $meta2[$key] = $val.split(':');
            });

            // $meta1: keys
            // $meta2: key->val
            return [$meta1, $meta2];
        } else {
            return false;
        }
    }

    /**
     * Fill filter form with data got from url param string
     * @returns {undefined}
     */
    function preFillForm() {
        $('form .filter').each(function($index, $value) {
            var $filterArry = getFilterParamFromUrl();
            if (!$filterArry) {
                return false;
            }
            var $elementValue = $($value).attr('value'),
                    $elementName = $($value).attr('name'),
                    $elementTag = $($value).prop('tagName'), // A le 040914 par Jian
                    $elementType = $($value).attr('type');                      // A le 040914 par Jian

            // for checkboxes and drop down list
            // array format: [key:val]
            // the key-val pair should be a value of an element
            $.each($filterArry[0], function($key, $val) {                       // M le 040914 par Jian
                if ($elementType === 'checkbox') {
                    if ($elementValue === $val) {
                        $($value).attr('checked', 'checked');
                    }
                } else if ($elementTag === 'SELECT') {
                    $($value).find('option').each(function($k, $v) {
                        if ($($v).val() === $val) {
                            // add attribut selected to this option
                            $($v).attr('selected', 'selected');
                        }
                    });

                }

            });

            // for textbox
            // array format: [key, val]
            // use key to find field, assign with value
            $.each($filterArry[1], function($key, $val) {
                var $nk = $val[0],
                        $nv = $val[1];
                // element name = key
                if ($elementName === $nk) {
                    $($value).val($nv);
                }

            });

        });
    }

});


