/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
// inputPaysVilleRegion
// inputDateArrivee

// Category
    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _create: function() {
            this._super();
            this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
        },
        _renderMenu: function(ul, items) {
            var that = this,
                    currentCategory = "";
            $.each(items, function(index, item) {
                var li;
                if (item.category != currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>== " + item.category + " ==</li>");
                    currentCategory = item.category;
                }
                li = that._renderItemData(ul, item);
                if (item.category) {
                    li.attr("aria-label", item.category + " : " + item.label);
                }
            });
        }
    });

// Autocomplete
    $('.autocomplete#inputPaysVilleRegion').catcomplete({
        minLength: 1,
        source: function(request, response) {
            $.ajax({
                url: "/service/search/autocomplete",
//                jsonp: "callback",
//                jsonpCallback: "myCallBack",
                dataType: "json",
//                crossDomain: false,
                data: {
                    geo: request.term
                },
                success: function(data) {
                    var $itmDisplay = [];
                    // Locations
                    $(data.locations).each(function(k, v) {
                        var $txtDisplay = v.reference + ' ' + v.titre,
                                $link = '/locations/locations-partenaire/' + v.pays + '-' + v.region + '-' + v.ville + '/' + v.reference + '/voir';
                        $itmDisplay[k] = [];
                        $itmDisplay[k]['label'] = ($txtDisplay);
                        $itmDisplay[k]['link'] = ($link);
                        $itmDisplay[k]['category'] = 'Annonces';
                    });
                    // get annonces count, so that the keys for list can be in order
                    var $annoncesCount = $(data.locations).length

                    // Geolocalisations
                    $(data.geolocalisations).each(function(k, v) {
                        var $nk = $annoncesCount + k;
                        if (typeof v.pays !== "undefined") {
                            var $txtDisplay = v.pays.nom,
                                    $link = '/locations/locations-partenaire/pays:' + v.pays.code;
                            var $kp = $nk;
                            $itmDisplay[$kp] = [];
                            $itmDisplay[$kp]['label'] = ($txtDisplay);
                            $itmDisplay[$kp]['link'] = ($link);
                            $itmDisplay[$kp]['category'] = 'Listes';
                            $nk += 1;
                        }
                        if (typeof v.region !== "undefined") {
                            var $txtDisplay = v.region.region + ', ' + v.region.pays.nom,
                                    $link = '/locations/locations-partenaire/pays:' + v.region.pays.code + '-region:' + v.region.region;
                            var $kr = $nk;
                            $itmDisplay[$kr] = [];
                            $itmDisplay[$kr]['label'] = ($txtDisplay);
                            $itmDisplay[$kr]['link'] = ($link);
                            $itmDisplay[$kr]['category'] = 'Listes';
                            $nk += 1;
                        }
                        if (typeof v.ville !== "undefined") {
                            var $txtDisplay = v.ville.ville + ', ' + v.ville.region + ', ' + v.ville.pays.nom,
                                    $link = '/locations/locations-partenaire/pays:' + v.ville.pays.code + '-region:' + v.ville.region + '-ville:' + v.ville.ville;
                            var $kv = $nk;
                            $itmDisplay[$kv] = [];
                            $itmDisplay[$kv]['label'] = ($txtDisplay);
                            $itmDisplay[$kv]['link'] = ($link);
                            $itmDisplay[$kv]['category'] = 'Listes';
                            $nk += 1;
                        }

                    });

                    response($itmDisplay);
//                    console.log(data);
                },
                error: function(xhr, status, error) {
                    console.log(status + '; ' + error);
                }
            });
        },
        select: function(event, ui) {
            var $link = ui.item.link;
            window.location.href = $link;
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });
// Datepicker
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '0d',
        weekStart: 1,
        autoclose: true,
        todayHighlight: true,
        language: 'fr',
        clearBtn: true,
        changeDate: function() {
        },
        clearDate: function() {
        }
    });

// form submit
    $('#srch-top-seo').submit(function() {
        var $inputLoc = $('input#inputPaysVilleRegion'),
                $inputArr = $('input#inputDateArrivee'),
                $inputDur = $('select#inputDuree');

        if ($.trim($($inputLoc).val()) !== '') {
            $($inputLoc).attr('name', 'geoLoc');
        }
        if ($.trim($($inputArr).val()) !== '') {
            $($inputArr).attr('name', 'dateArrivee');
            // if duree is not defined, do not validate
            if ($.trim($($inputDur).val()) === '') {
                $($inputDur).attr('title', 'Choisir la durée').attr('data-toggle', 'tooltip');
                $($inputDur).tooltip().focus();
                return false;
            }
        }

        $('select#inputDuree').attr('name', 'duree');
        $('select#inputNbPers').attr('name', 'nbrPers');

        return true;
    });
});

