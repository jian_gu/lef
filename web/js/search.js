/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    var currentPath = window.location.pathname.substr(-1) === '/' ? window.location.pathname.slice(0, -1) : window.location.pathname;
    var hostPath = window.location.host.substr(-1) === '/' ? window.location.host.slice(0, -1) : window.location.host;
    var autocomplete;
    var typesArray = {'street_address': 'adresse', 'route': 'rue', 'postal_code': 'cp',
        'locality': 'ville', 'administrative_area3': 'ville',
        'administrative_area_level_2': 'departement',
        'administrative_area_level_1': 'region', 'country': 'pays'};
    //////////////////
    //////////////////
    //////////////////
    function initialize() {
        // Create the autocomplete object, restricting the search
        // to geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('search-input-geo')),
                {
                    types: []
//                    componentRestrictions: {'country': 'fr'}
                }
        );
        // When the user selects an address from the dropdown,
        // populate the address fields in the form.
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            retrieveAddress();
        });
    }

    function retrieveAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
//        console.log(place);
        var adresseInput = $.trim($('#search-input-geo').text()) !== '' ? $.trim($('#search-input-geo').text()) : null;
        var geocodeGet = {'center': {'lng': null, 'lat': null}, 'type': null,
            bounds: {'northeast': {'lng': null, 'lat': null}, 'southwest': {'lng': null, 'lat': null}},
            'adresse': adresseInput, 'fulladdr': null, 'cp': null,
            'ville': null, 'departement': null, 'region': null, 'pays': null};

        /* Get complete search word/address */
        geocodeGet['fulladdr'] = place.formatted_address;
        /* set ville region pays according to address_components composition */
        $.each(place.address_components, function(key, component) {
            var type = component.types[0];
            var name = component.long_name;
            switch (type) {
                case 'street_address':    /* street address */
                case 'route':    /* rue */
                case 'postal_code':    /* CP */
                    geocodeGet['adresse'] = name;
                    break;
                case 'administrative_area_level_2':    /* departement */
                    geocodeGet['departement'] = name;
                    break;
                case 'postal_code':    /* CP */
                    geocodeGet['cp'] = name;
                    break;
                case 'locality':
                case 'administrative_area3':    /* Ville */
                    geocodeGet['ville'] = name;
                    break;
                case 'administrative_area_level_1':    /* Région */
                    geocodeGet['region'] = name;
                    break;
                case 'country':    /* Pays */
                    geocodeGet['pays'] = name;
                    break;
                default :
                    break;
            }
        });
        /* Define center */
        geocodeGet['center']['lng'] = parseFloat(place.geometry.location['B']);
        geocodeGet['center']['lat'] = parseFloat(place.geometry.location['k']);
        /* Define type */
        geocodeGet['type'] = typeof typesArray[place.types[0]] === 'undefined' ? null : typesArray[place.types[0]];
        /* Define bounds */
        /* If has bounds */
        if (typeof place.geometry.bounds !== 'undefined') {
            /* Original bounds */
            geocodeGet['bounds']['northeast']['lng'] = place.geometry.bounds.getNorthEast()['B'];
            geocodeGet['bounds']['northeast']['lat'] = place.geometry.bounds.getNorthEast()['k'];
            geocodeGet['bounds']['southwest']['lng'] = place.geometry.bounds.getSouthWest()['B'];
            geocodeGet['bounds']['southwest']['lat'] = place.geometry.bounds.getSouthWest()['k'];
        } else if (typeof place.geometry.viewport !== 'undefined' && $.inArray(geocodeGet['type'], ['pays', 'departement', 'cp', 'region', 'ville']) >= 0) {
            /* Original bounds */
            geocodeGet['bounds']['northeast']['lng'] = place.geometry.viewport.getNorthEast()['B'];
            geocodeGet['bounds']['northeast']['lat'] = place.geometry.viewport.getNorthEast()['k'];
            geocodeGet['bounds']['southwest']['lng'] = place.geometry.viewport.getSouthWest()['B'];
            geocodeGet['bounds']['southwest']['lat'] = place.geometry.viewport.getSouthWest()['k'];
        } else {
            geocodeGet['bounds']['northeast'] = geocodeGet['center'];
            geocodeGet['bounds']['southwest'] = geocodeGet['center'];
        }

//        console.log(place);
//        console.log(geocodeGet);
        //////////////////////////
        //////////////////////////
        //////////////////////////
        return  geocodeGet;
    }

    initialize();
    formInputChangePost();
    sliders();
    //////////////////////////
    //////////////////////////
    //////////////////////////

    function dispatch_generate_url(type, pays, region, ville, fulladdr, urlParams) {
        switch (type) {
            case 'pays':
//                return hostPath + '/locations/' + pays + '?' + urlParams;
                return '/locations/' + pays + '?' + urlParams;
                break;
            case 'region':
                return '/locations/' + pays + '/' + region + '?' + urlParams;
                break;
            case 'ville':
                return '/locations/' + pays + '/' + region + '/' + ville + '?' + urlParams;
                break;
            default:
                return '/locations?adresse=' + fulladdr + '&' + urlParams;
                break;
        }
    }

    function constructUrlParams(formid) {
        var paramsArrayTreated = [];
        var params = '';
        // Serialize form inputs, get params in string
        var paramsSerilized = $('#' + formid).serialize();
        // Add adresse/place value to the params string
        if ($.trim($('#search-input-geo').attr('value')) !== '' && paramsSerilized.indexOf('adresse') < 0) {
            paramsSerilized = 'adresse=' + $.trim($('#search-input-geo').attr('value')) + '&' + paramsSerilized;
        }
        // explode params by separator: '&', and get an array
        var parmasExploded = paramsSerilized.split('&');
        // explode each array element by separator: '=', inorder to test if value is blank
        $.each(parmasExploded, function(index, value) {
            var paramPair = value.split('=');
            if (paramPair[1] !== '') {
                // Formaliser date format
                if (paramPair[0] === 'datearrivee' || paramPair[0] === 'datedepart') {
                    var dateArray = paramPair[1].split('%2F');
                    value = paramPair[0] + '=' + dateArray[2] + '-' + dateArray[1] + '-' + dateArray[0];
                }
                paramsArrayTreated.push(value);
            }
        });
        // Stringfy params array
        var paramsStringfied = paramsArrayTreated.join('&');
        // If params contain geodata, remove it
        var paramsWithoutGeodata = paramsStringfied.indexOf('geodata') > -1 ? paramsStringfied.split('geodata')[0] : paramsStringfied;
        // If params contain '&' as last char, remove it
        params = paramsWithoutGeodata.substr(-1) === '&' ? paramsWithoutGeodata.slice(0, -1) : paramsWithoutGeodata;
//        console.log(params);
        return params;
    }

    // form submit
    $('#search-form').submit(function() {
        var geocode = null;
        /* get radius value */
        var radius = parseFloat($('input:radio[name="radius"]:checked').val());

        /* If geo search word is defined */
        /* redefine bounds of search zone according to radius */
        if ($.trim($('#search-input-geo').val()) !== '' && typeof autocomplete.getPlace() !== 'undefined') {
            /* Get original retrieved address */
            geocode = retrieveAddress();
            /* 10km <-> 0.01=1/100, im <-> 1/100000*/
            var codeRadiusDiff = parseFloat(radius / 100000);

            /* Stringfy / jsonfy the object */
            var geocodeJSonfy = JSON.stringify((geocode));
            /* Regularize json string (escape) before post it */
            geocodeJSonfy = geocodeJSonfy.replace("\'", "-");

            var geodataInput = "<input type='hidden' name='geodata' value='" +
                    geocodeJSonfy + "' />";
            $('#search-form').append($(geodataInput));

            // Dispatcher
            var type = geocode.type,
                    pays = geocode.pays,
                    region = geocode.region,
                    ville = geocode.ville,
                    fulladdr = geocode.fulladdr;
            var urlParamsG = constructUrlParams('search-form');
            var postUrlG = dispatch_generate_url(type, pays, region, ville, fulladdr, urlParamsG);
            $('#search-form').removeAttr('action');
            $('#search-form').attr('action', postUrlG);
//            console.log(postUrlG);
        } else {
            // Here we do not have geo info input, so remove radius
            if ($.trim($('#search-input-geo').val()) === ''
                    && $('input[name="radius"]').length > 0) {
                $('input[name="radius"]').removeAttr('name');
            }
            var urlParams = constructUrlParams('search-form');
            var postUrl = currentPath + '?' + urlParams;
            $('#search-form').removeAttr('action');
            $('#search-form').attr('action', postUrl);
//            console.log(urlParams);
        }

//        console.log(urlParams);
        return true;
    });

    // Dectect widget change
    function formInputChangePost() {
        // Combobox
        $('#search-form select.srch-load-on-change').on('change', function() {
            // Once a value is change,, we do the post
            if (true) {
                coverSrchFrom();
                $('#btn-srch-submit').click();
            }
        });
        // Checkbox
        $('#search-form input[type="checkbox"].srch-load-on-change').on('click', function() {
            // Once a value is change,, we do the post
            if (true) {
                coverSrchFrom();
                $('#btn-srch-submit').click();
            }
        });
    }

    // Lock components
    function coverSrchFrom() {
        var loadingBar = '/img/loadingBar.gif';
        var modalBg = '/img/modal-dot-wt.png';
        var windowMid = $(window).scrollTop() - $(window).height() / 2;
        var formTopBorder = $('#search-form').position().top;
        var formLeftBorder = $('#search-form').position().left;
        var formWidth = $('#search-form').width();
        var formHeight = $('#search-form').height();
        var modalDiv = '<div style="width:' + formWidth + 'px;height:' + formHeight + 'px;\n\
                            top:' + formTopBorder + 'px;\n\
                            position:absolute;\n\
                            background-image:url(' + modalBg + ');background-size:100% 100%;\n\
                            opacity:0.5;">sdffff</div>';

        var loadingBarImg = '<img src="' + loadingBar + '" \n\
                                style="position:absolute;left:' + (parseInt(formLeftBorder) + parseInt(formWidth) / 3) + 'px;\n\
                                top:' + windowMid + 'px;width:25%;"/>';
        $('#search-form').append($(modalDiv));
        $('#search-form').append($(loadingBarImg));
    }

    //////////////////////////
    //////////////////////////
    //////////////////////////
    // ====== Sliders =======
    function sliders() {
        var rangeSurfaceMin = parseInt($('#rangeSurfaceMin').attr('value'));
        var rangeSurfaceMax = parseInt($('#rangeSurfaceMax').attr('value'));
        var surfaceMin = parseInt($('#surfaceMin').attr('value'));
        var surfaceMax = parseInt($('#surfaceMax').attr('value'));
        var rangePrixMin = parseInt($('#rangePrixMin').attr('value'));
        var rangePrixMax = parseInt($('#rangePrixMax').attr('value'));
        var prixMin = parseInt($('#prixMin').attr('value'));
        var prixMax = parseInt($('#prixMax').attr('value'));

        // Make beautiful numbers
        rangeSurfaceMin = mankeNumberInt(rangeSurfaceMin, 'floor');
        rangeSurfaceMax = mankeNumberInt(rangeSurfaceMax, 'ceil');
        rangePrixMin = mankeNumberInt(rangePrixMin, 'floor');
        rangePrixMax = mankeNumberInt(rangePrixMax, 'ceil');

        // Slider for surface
        $('#slider-range-surface').slider({
            range: true,
            min: rangeSurfaceMin,
            max: rangeSurfaceMax,
            values: [surfaceMin, surfaceMax],
            step: 10,
            slide: function(event, ui) {
                $("#surfaceMin").attr('value', ui.values[ 0 ]);
                $("#surfaceMax").attr('value', ui.values[ 1 ]);
                $("#label-range-surface-min").text(ui.values[ 0 ]);
                $("#label-range-surface-max").text(ui.values[ 1 ]);
            },
            stop: function(event, ui) {
                if (parseInt(ui.values[ 0 ]) !== surfaceMin || parseInt(ui.values[ 1 ]) !== surfaceMax) {
                    $('#surfaceMin').attr('name', 'surfacemin');
                    $('#surfaceMax').attr('name', 'surfacemax');
                    coverSrchFrom();
                    $('#btn-srch-submit').click();
                }
            }
        });
        // Slider for price
        $('#slider-range-prix').slider({
            range: true,
            min: rangePrixMin,
            max: rangePrixMax,
            values: [prixMin, prixMax],
            step: 10,
            slide: function(event, ui) {
                $("#prixMin").attr('value', ui.values[ 0 ]);
                $("#prixMax").attr('value', ui.values[ 1 ]);
                $("#label-range-prix-min").text(ui.values[ 0 ]);
                $("#label-range-prix-max").text(ui.values[ 1 ]);
            },
            stop: function(event, ui) {
                if (parseInt(ui.values[ 0 ]) !== prixMin || parseInt(ui.values[ 1 ]) !== prixMax) {
                    $('#prixMin').attr('name', 'prixmin');
                    $('#prixMax').attr('name', 'prixmax');
                    coverSrchFrom();
                    $('#btn-srch-submit').click();
                }
            }
        });
        // Affichage initial
        $("#label-range-surface-min").text($('#slider-range-surface').slider("values", 0));
        $("#label-range-surface-max").text($('#slider-range-surface').slider("values", 1));
        $("#label-range-prix-min").text($('#slider-range-prix').slider("values", 0));
        $("#label-range-prix-max").text($('#slider-range-prix').slider("values", 1));
    }

    /**
     * Follor or round a number
     * @param {type} number
     * @param {type} floorRound
     * @returns {Number}
     */
    function mankeNumberInt(number, floorCeil) {
        var result = parseInt(0);
        number = parseInt(number);
        // Round up
        if (floorCeil === 'ceil') {
            var meta = number / 10;
            result = Math.ceil(meta) * 10;
        }
        // Round down
        if (floorCeil === 'floor') {
            var meta = number / 10;
            result = Math.floor(meta) * 10;
        }
        return result;
    }
});