/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    var $btnAddToList = $('button.btn-go-wish-list'),
            $maxListLength = 2,
            $listArray = null;

//    cleanWishList();
    console.log("wish list exists: " + isCookieExists());

    // prepare wish buttons
    prepareWishBtn();
    // wish list button click event
    $btnAddToList.on('click', function() {
        var $ref = '';
        $ref = $(this).attr('valref');
        addToWishList($ref);
    });

    // Add to wishlist 
    function addToWishList($ref) {
        // if list is empty
        if (!isCookieExists()) {
            // create cookie and insert the first wish
            var $wish = [$ref];
            $.cookie('wish_list', $wish);
            // update list array
            $listArray = getListArrayFromCookie();
            // update wish btn
            prepareWishBtn();
            console.log('first wish inserted: ' + $.cookie('wish_list'));
        } else {
            // if list exists
            // do nothing is the reference exists in the cookie list
            if (isRefInList($ref, $listArray)) {
                console.log('wish already exists');
                return;
            }
            // list is full
            if ($listArray.length >= $maxListLength) {
                console.log('list full');
                return;
            }

            // insert new wish
            var $wishes = [$.cookie('wish_list')];
            $wishes.push($ref);
            $.cookie('wish_list', $wishes);
            // update list array
            $listArray = getListArrayFromCookie();
            // update wish btns
            prepareWishBtn();
            console.log('new wish inserted: ' + $.cookie('wish_list'));

        }
    }

    /////////////////////////////
    ///// support functions /////
    /////////////////////////////
    //
    // check if cookie exists
    function isCookieExists() {
        return $.cookie('wish_list') ? true : false;
    }

    function cleanWishList() {
        if ($.cookie('wish_list')) {
            $.removeCookie('wish_list');
        } else {
            return;
        }
    }

    function isRefInList($ref, $listArray) {
        return $.inArray($ref, $listArray) > -1 ? true : false;
    }

    function getListArrayFromCookie() {
        if ($.cookie('wish_list')) {
            return $.inArray(',', $.cookie('wish_list')) > -1 ? $.cookie('wish_list').split(',') : [$.cookie('wish_list')];
        } else {
            return null;
        }

    }

    function prepareWishBtn() {
        var $listArray = getListArrayFromCookie();
        $('button.btn-go-wish-list').each(function() {
            var $ref = $(this).attr('valref');
            if ($.inArray($ref, $listArray) > -1) {
                $(this).attr('disabled', 'disabled');
            }
        });
    }
});


